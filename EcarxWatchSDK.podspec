#
# Be sure to run `pod lib lint EcarxWatchSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'EcarxWatchSDK'
  s.version          = '0.4.0'
  s.summary          = 'EcarxWatchSDK 领克二代表'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: 领克APP 手表模块包含UI.
                       DESC

  s.homepage         = 'https://www.ecarx.com.cn'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'itrealm' => '3273948281@qq.com' }
  s.source           = { :git => 'https://gitlab.com/itrealm/EcarxWatchSDK.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.pod_target_xcconfig  = {
   'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'
   }

  s.frameworks = 'CoreBluetooth', 'AddressBook', 'Contacts'

  s.vendored_frameworks = ['EcarxWatchSDK/Classes/EcarxWatchSDK.framework', 'EcarxWatchSDK/Classes/LPASDK.framework',] #自己的framework在工程中的路径

  s.resource_bundles = {
      'EcarxWatchSDK' => ['EcarxWatchSDK/**/*.{png,gif,xcassets}']
#      'Resources' => 'EcarxWatchSDK/Assets/EcarxWatchSDK.bundle'
  }


  s.source_files = 'EcarxWatchSDK/Classes/**/*.h'


  s.public_header_files = 'Pod/Classes/**/*.h'


  s.dependency 'SweetCoreKit'
  s.dependency 'GLServiceSDK'

  s.dependency 'Toast'
  s.dependency 'Masonry'
  s.dependency 'MJRefresh'
  s.dependency 'AFNetworking', '>= 4.0'
  s.dependency 'SVProgressHUD'
  s.dependency 'SDWebImage'
  s.dependency 'Protobuf', '~> 3.6.1' #https://www.jianshu.com/p/3c49fcd3afd8
  
  s.dependency 'MJExtension', '~> 3.0.13'


end
