//
//  CMPService.h
//  LPASDK
//
//  Created by Jone Yin on 2020/6/12.
//  Copyright © 2020 Jone Yin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CMPService : NSObject

typedef void(^RequestBlock)(NSDictionary *_Nullable dictionary,NSError *_Nullable error);
typedef void(^OderResultBlock)(NSDictionary *_Nullable dic,NSError *_Nullable error);
typedef void(^unbindBlock)(bool status,NSString *message);

@property (copy, nonatomic) RequestBlock resultBlock;
@property (copy, nonatomic) OderResultBlock oderResult;

+(CMPService *)shared;


//msisdn是否存在
-(void)checkExistsMsisdn:(NSString * _Nullable)eid msisdnList:(NSString * _Nullable)list reusltBlock:(RequestBlock _Nullable )resultDictionary;

//订购码号
-(void)oderNumber:(NSString *)eid msisdn:(NSString *)mssisdn packageID:(NSString *)pkID resultBlock:(OderResultBlock _Nullable)result;

//解绑
-(void)unbind:(NSString * _Nullable)ihuMsisdn watchMsisdn:(NSString * _Nullable)watchMsisdn eid:(NSString * _Nullable)eid resultBlock:(unbindBlock _Nullable )result;

-(NSString *)getVesion;

-(void) setDebugMode:(Boolean)isdebug;
-(NSString*) getMode;

@end

NS_ASSUME_NONNULL_END
