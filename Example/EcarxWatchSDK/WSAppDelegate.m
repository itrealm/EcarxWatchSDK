//
//  WSAppDelegate.m
//  EcarxWatchSDK
//
//  Created by itrealm on 07/29/2020.
//  Copyright (c) 2020 itrealm. All rights reserved.
//

#import "WSAppDelegate.h"

#import <GLDebugTool/GLDebugTool.h>
#import <GeelyLynkcoCore/GLNetworkEnvConfig.h>
#import <SweetCoreKit/SWRouterService.h>
#import <Bugly/Bugly.h>



@implementation WSAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
        BOOL isOK = [super application:application didFinishLaunchingWithOptions:launchOptions];
        [self showGLDebug];
        return isOK;
}
- (void)showGLDebug
{
    //app辅助开发工具，应用信息，日志记录，网络，沙盒
    [[GLDebug sharedInstance] addHelper:[GLAppHelper sharedHelper]];
    [[GLDebug sharedInstance] addHelper:[GLLogHelper sharedHelper]];
    [[GLDebug sharedInstance] addHelper:[GLNetworkHelper sharedHelper]];
    [[GLDebug sharedInstance] addHelper:[GLSandboxHelper sharedHelper]];

    [[GLDebug sharedInstance] showAssisitiveTouch:[GLAppHelper sharedHelper]];

    [[GLAssisitiveTouch sharedTouch] registerSubMenu:@"切换环境" action:^{
        [[GLNetworkEnvConfig sharedInstance]showSwitchMenu];
    }];

}

@end
