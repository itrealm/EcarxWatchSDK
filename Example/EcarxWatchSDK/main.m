//
//  main.m
//  EcarxWatchSDK
//
//  Created by itrealm on 07/29/2020.
//  Copyright (c) 2020 itrealm. All rights reserved.
//

@import UIKit;
#import "WSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WSAppDelegate class]));
    }
}
