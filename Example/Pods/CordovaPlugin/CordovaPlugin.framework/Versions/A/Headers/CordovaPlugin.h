//
//  CordovaPlugin.h
//  Cordova
//
//  Created by 殷海超 on 2018/5/17.
//

#import "CDVPlugin.h"

@interface CordovaPlugin : CDVPlugin

- (void)test:(CDVInvokedUrlCommand *)command;

@end

@interface SWLogPlugin : CDVPlugin

- (void)upload:(CDVInvokedUrlCommand *)command;

- (void)getLog:(CDVInvokedUrlCommand *)command;

- (void)getDiskUsage:(CDVInvokedUrlCommand *)command;

- (void)clean:(CDVInvokedUrlCommand *)command;

- (void)logError:(CDVInvokedUrlCommand *)command;

- (void)logWarn:(CDVInvokedUrlCommand *)command;

- (void)logInfo:(CDVInvokedUrlCommand *)command;

- (void)logDebug:(CDVInvokedUrlCommand *)command;

@end


