//
//  CordovaScan.h
//  AFNetworking
//
//  Created by Goot on 2019/1/14.
//

#import "CDVPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@interface CordovaScan : CDVPlugin

- (void)scan:(CDVInvokedUrlCommand *)command;

@end

NS_ASSUME_NONNULL_END
