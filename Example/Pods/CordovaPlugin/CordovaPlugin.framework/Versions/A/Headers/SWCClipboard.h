#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>


@interface SWCClipboard : CDVPlugin {}

- (void)copy:(CDVInvokedUrlCommand*)command;
- (void)paste:(CDVInvokedUrlCommand*)command;

@end
