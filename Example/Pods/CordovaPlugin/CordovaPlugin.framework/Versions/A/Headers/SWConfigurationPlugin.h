//
//  SWConfigurationPlugin.h
//  Pods
//
//  Created by Goot on 2018/7/5.
//

#import "CDVPlugin.h"

@interface SWConfigurationPlugin : CDVPlugin

- (void)setConfig:(CDVInvokedUrlCommand *)command;

- (void)getAllConfig:(CDVInvokedUrlCommand *)command;

- (void)getConfig:(CDVInvokedUrlCommand *)command;

- (void)getEnvironments:(CDVInvokedUrlCommand *)command;

- (void)getCurrentEnvironment:(CDVInvokedUrlCommand *)command;

- (void)addEnvironment:(CDVInvokedUrlCommand *)command;

- (void)setCurrentEnvironment:(CDVInvokedUrlCommand *)command;

@end
