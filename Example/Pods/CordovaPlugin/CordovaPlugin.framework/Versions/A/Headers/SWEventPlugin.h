//
//  SWEventPlugin.h
//  Pods
//
//  Created by Goot on 2018/7/5.
//

#import "CDVPlugin.h"

@interface SWEventPlugin : CDVPlugin

- (void)subscribe:(CDVInvokedUrlCommand *)command;

- (void)unsubscribe:(CDVInvokedUrlCommand *)command;

- (void)publish:(CDVInvokedUrlCommand *)command;

@end
