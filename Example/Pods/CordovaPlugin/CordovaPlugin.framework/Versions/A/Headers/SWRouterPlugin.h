//
//  SWRouterPlugin.h
//  Pods
//
//  Created by Goot on 2018/7/4.
//

#import "CDVPlugin.h"

@interface SWRouterPlugin : CDVPlugin

- (void)navigateRouter:(CDVInvokedUrlCommand *)command;

@end
