//
//  SWScanViewController.h
//  AFNetworking
//
//  Created by Goot on 2019/1/18.
//

#import <UIKit/UIKit.h>

typedef void(^ScanBlock)(NSString *value);

NS_ASSUME_NONNULL_BEGIN

@interface SWScanViewController : UIViewController

@property (nonatomic, copy) ScanBlock block;

@end

NS_ASSUME_NONNULL_END
