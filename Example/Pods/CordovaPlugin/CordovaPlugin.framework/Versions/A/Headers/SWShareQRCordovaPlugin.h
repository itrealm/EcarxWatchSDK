//
//  SWShareQRCordovaPlugin.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/11/10.
//

#import <Cordova/CDVPlugin.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWShareQRCordovaPlugin : CDVPlugin

@end

NS_ASSUME_NONNULL_END
