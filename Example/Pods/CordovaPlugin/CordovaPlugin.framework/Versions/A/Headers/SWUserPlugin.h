//
//  SWUserPlugin.h
//  Pods
//
//  Created by Goot on 2018/7/5.
//

#import "CDVPlugin.h"

@interface SWUserPlugin : CDVPlugin

- (void)login:(CDVInvokedUrlCommand *)command;

- (void)update:(CDVInvokedUrlCommand *)command;

- (void)isLogin:(CDVInvokedUrlCommand *)command;

- (void)getUser:(CDVInvokedUrlCommand *)command;

- (void)authenticate:(CDVInvokedUrlCommand *)command;

- (void)sessioninfo:(CDVInvokedUrlCommand *)command;

- (void)nvalidate:(CDVInvokedUrlCommand *)command;

@end
