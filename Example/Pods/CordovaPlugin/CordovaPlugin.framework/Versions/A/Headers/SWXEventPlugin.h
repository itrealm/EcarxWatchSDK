//
//  SWXEventPlugin.h
//  CordovaPlugin
//
//  Created by shuaishuai on 2019/9/4.
//

#import "CDVPlugin.h"

NS_ASSUME_NONNULL_BEGIN


typedef void(^SWCEVENTBLOCK)(id obj);
@interface SWCSubscriber : NSObject
@property(nonatomic,copy)SWCEVENTBLOCK block;
@end

@interface SWCEventManger : NSObject
@property(nonatomic,strong)NSMutableDictionary *subscriberMap;
@end

@interface SWXEventPlugin : CDVPlugin

/*
 config.xml 配置:
     <feature name="SWXEventPlugin">
     <param name="ios-package" value="SWXEventPlugin" />
     </feature>
 */
@end

NS_ASSUME_NONNULL_END
