//
//  SWXMediaImagePlugin.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/11/26.
//

#import "CDVPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWXMediaImagePlugin : CDVPlugin

- (void)pick:(CDVInvokedUrlCommand *)command;

- (void)preview:(CDVInvokedUrlCommand *)command;

@end

NS_ASSUME_NONNULL_END
