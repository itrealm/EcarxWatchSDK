#ifndef __EDK_CRYPT_WRAP_H__
#define __EDK_CRYPT_WRAP_H__

extern int _sha256(unsigned char *input, int inlen, unsigned char * output);

extern int _hmac_sha256(unsigned char *buffer, int inlen, unsigned char *secret,
                    int secret_len,unsigned char * digest);


extern int Dklib_aes_gcm_encrypt_and_tag(
            unsigned char       *key,
            unsigned int        keybits,
            const unsigned char *input,
            size_t              length,
            const unsigned char *iv,
            size_t              iv_len,
            const unsigned char *add,
            size_t              add_len,
            unsigned char       *output,
            size_t              tag_len,
            unsigned char       *tag );

extern int Dklib_aes_gcm_auth_decrypt(
            unsigned char       *key,
            unsigned int        keybits,
            const unsigned char *input,
            size_t              length,
            const unsigned char *iv,
            size_t              iv_len,
            const unsigned char *add,
            size_t              add_len,
            unsigned char       *output,
            size_t              tag_len,
            unsigned char       *tag );

//deprecated, only used in none padding mode.
extern int Dklib_aes_crypt_cbc(     
                          int is_encrypt,
                          unsigned char *input, unsigned int len,
                          unsigned char *key, unsigned int keybits,
                          unsigned char iv[16],
                          unsigned char *output
                          );
extern int DklibAesCryptCBC(     
                        int is_encrypt,
                        unsigned char *input, unsigned int len,
                        unsigned char *key, unsigned int keybits,
                        unsigned char iv[16],
                        unsigned char *output, size_t *olen
                        );

extern int DklibAesEncryptCBC(     unsigned char *input, unsigned int len,
                        unsigned char *key, unsigned int keybits,
                        unsigned char *output, size_t *olen);
extern int DklibAesDecryptCBC(     unsigned char *input, unsigned int len,
                        unsigned char *key, unsigned int keybits,
                        unsigned char *output, size_t *olen);


//==============================================================================
/*
 * function: DklibAesEncryptGCM
 * len: length of plain text;
 * key: a buf hold the key.
 * keybits: number of bits, not bytes. eg: 128
 * cipher text: length= \p len
 * tag_len: usually 16 bytes
 * tag: buf to hold the gmac, output parameter.
 */
extern int DklibAesEncryptGCM(unsigned char  * plain_text, unsigned int len,
                        unsigned char * key, unsigned int keybits, 
                        unsigned char * iv, size_t iv_len, unsigned char *cipher_text,
                        size_t tag_len, unsigned char *tag);
/*
 * function: DklibAesDecryptGCM
 * len: length of cipher text;
 * key: a buf hold the key.
 * keybits: number of bits, not bytes. eg: 128
 * plain text: length= \p len
 * tag_len: usually 16 bytes
 * tag: buf to hold the gmac, input parameter.
 */

extern int DklibAesDecryptGCM(     unsigned char  * cipher_text, unsigned int len,
                        unsigned char * key, unsigned int keybits, 
                        unsigned char * iv, size_t iv_len, unsigned char *plain_text,
                        size_t tag_len, unsigned char *tag);



/*
 * 获取当前sdk版本
 */
extern char * DklibGetVersion(void);
                        
/*
 * 初始化（如果获取的包名和默认包名签名不一致，程序不能运行）
 */
extern int DklibInitialize(char * s);
                        
extern int DklibCalculateCounter(unsigned char mk1[32], unsigned char vk2[32]);
/**
* 会话密钥
* 前16字节为会话加密密钥，后16字节为会话MAC密钥.
 */
extern int DklibCreateSessionKey(unsigned char vk2[32], 
            unsigned char mk1[32], 
            unsigned char vin[17], 
            unsigned char moblieId[16], 
            unsigned char * authKey, size_t authKey_len,
            unsigned char output[32]);

/*
 * 认证密钥
 * 认证密钥由16字节长度
 *
 * Explain
 * 1. MRnd,VRnd由双方产生并交换。
 * 2. randomT0为数字钥匙DKInfo中的字段。
 * 3. 身份密钥参考身份密钥的生成规则。
 * 4. Truncate中的目前为取前16个字节。
 * 认证密钥= Truncate(MHAC{ MoblieId+MRnd+VelId+VRnd+randomT0+DKID},身份密钥)
 */
extern int DklibCreateAuthKey(unsigned char mobileId[16],
                        unsigned char mRand[8], 
                        unsigned char vin[17], 
                        unsigned char vRand[8],
                        unsigned char randomT0[2], 
                        unsigned char * dkId, size_t dkId_len,
                        unsigned char * idKey, size_t idKey_len,
                        unsigned char output[32]);
                        
/**
* 特征值交换
 */
extern int DklibCharacteristicValue(unsigned char mobileId[16],
                                unsigned char mRand[8], 
                                unsigned char *dkId, size_t dkId_len, 
                                unsigned char *txKey, size_t txKey_len, 
                                unsigned char *IV, size_t IV_len,
                                unsigned char *output, size_t *olen);

                        
//PASSKEY 算法
//长度8字节。Passkey用来蓝牙链接后，车辆与手机身份的初次身份验证。
extern int DklibPassKey(unsigned char *communicationkey, size_t communicationkey_len,
    unsigned char bqrand[8],
    unsigned char output[8]);


#endif

