//
//  ExBluetoothBaseRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/16.
//

#import <Foundation/Foundation.h>
#import "ExBluetoothMessageHandle.h"

NS_ASSUME_NONNULL_BEGIN

//-------- 蓝牙指令code(nCmdId) --------
typedef NS_ENUM(NSUInteger, ExCmdBleReqCode) {
    //连接后2秒内发送的确认信息
    ExCmdBleConfirmReqCode = 1,
    //特征交换
    ExCmdInfoExchangeReqCode = 2,
    //身份认证
    ExCmdAuthReqCode = 3,
    //DK传输验证
    ExCmdVkeyReqCode = 4,
    //会话密钥协商
    ExCmdSessionReqCode = 5,
    //控车指令
    ExCmdControlReqCode = 256,
    //ack
    ExCmdACKReqCode = 8192,
    //(RPA) command request
    Cmd_RpaReq = 275,
    //(RPA) Q&A authtication process
    Cmd_RpaAnswer = 278,
    //Command is used to transfer calibration parameters and location parameters from APP to TBOX.
    Cmd_Cust = 2561,
    //PE查询/设置开关
    Cmd_PeSwitch_Req = 1281
};

@interface ExBluetoothBaseRequest : NSObject

//魔鬼数，0Xfe
@property (nonatomic, assign) unsigned char bMagicNumber;
//包格式版本号
@property (nonatomic, assign) unsigned char bVer;
//命令号
@property (nonatomic, assign) ExCmdBleReqCode nCmdId;
//递增，一个req对应一个Resp,并且它们的nSeq相同
@property (nonatomic, assign) int nSeq;
//指令数据域
@property (nonatomic, copy) NSData *controlData;
//循环冗余校验,CRC8
@property (nonatomic, copy) NSData *crc;

/**
 生成蓝牙请求data
 
 @return a new NSData, or nil if an error occurs.
 */
- (NSData *)toData;

/**
 生成基础payload数据（seq+时间戳）
 
 @return a new NSData, or nil if an error occurs.
 */
- (NSData *)createBasePayloadData;

/**
 描述，日志用
 
 @return a new NSString, or nil if an error occurs.
 */
- (NSString *)description;

@end

NS_ASSUME_NONNULL_END
