//
//  ExBluetoothBaseResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//-------- 蓝牙指令code(nCmdId) --------
typedef NS_ENUM(NSUInteger, ExCmdBleRspCode) {
    //未知
    ExCmdBleUnknownRspCode = 0,
    //连接后2秒内发送的确认信息
    ExCmdBleConfirmRspCode = 1,
    //特征交换
    ExCmdInfoExchangeRspCode = 2,
    //身份认证
    ExCmdAuthRspCode = 3,
    //DK传输验证
    ExCmdVkeyRspCode = 4,
    //会话密钥协商
    ExCmdSessionRspCode = 5,
    //控车指令
    ExCmdControlRspCode = 256,
    //控车指令执行结果
    ExCmdControlResultRspCode = 257,
    //控车指令车辆状态
    ExCmdControlVehicleStateRspCode = 258,
    //(RPA) Response from BSRM to APP
    Cmd_RpaStatus = 276,
    //(RPA) Q&A authtication process
    Cmd_RpaChallenge = 277,
    //RPA泊车执行过程中，泊车数据的状态同步，BSRM发送个APP
    Cmd_RpaSync = 279,
    //PE查询/设置开关
    Cmd_PeSwitch_Rsp = 1281
};

@interface ExBluetoothBaseResponse : NSObject

//魔鬼数，0Xfe
@property (nonatomic, assign) int bMagicNumber;
//包格式版本号
@property (nonatomic, assign) unsigned char bVer;
//包头+包体长度（不包括CRC）
@property (nonatomic, assign) int nLength;
//命令号
@property (nonatomic, assign) ExCmdBleRspCode nCmdId;
//递增，一个req对应一个Resp,并且它们的nSeq相同
@property (nonatomic, assign) int nSeq;
//指令内容数据域
@property (nonatomic, copy) NSData *controlData;
//循环冗余校验,CRC8
@property (nonatomic, assign) unsigned short crc;
//自定义是否返回成功
@property (nonatomic, assign) BOOL isSuccess;
//自定义消息
@property (nonatomic, copy) NSString *message;
//自定义code
@property (nonatomic, copy) NSString *code;

/**
 初始化
 
 @param data 蓝牙返回data
 */
- (instancetype)initWithData:(NSData *)data;

/**
 描述，日志用
 
 @return a new NSString, or nil if an error occurs.
 */
- (NSString *)description;

@end

NS_ASSUME_NONNULL_END
