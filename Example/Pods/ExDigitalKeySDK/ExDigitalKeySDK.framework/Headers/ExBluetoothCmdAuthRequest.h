//
//  ExBluetoothCmdAuthRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdAuthRequest : ExBluetoothBaseRequest

/**
 第三和四阶段：双向身份验证；验证手机身份
 
 @param vellId VIN号
 @param vRnd 车辆随机数
 @param identityKey 身份秘钥
 */
- (instancetype)initWithVellId:(NSData *)vellId vRnd:(NSData *)vRnd identityKey:(NSData *)identityKey;

@end

NS_ASSUME_NONNULL_END
