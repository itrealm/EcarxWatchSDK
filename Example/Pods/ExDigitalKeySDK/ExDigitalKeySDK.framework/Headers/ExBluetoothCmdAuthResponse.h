//
//  ExBluetoothCmdAuthResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdAuthResponse : ExBluetoothBaseResponse

//手机唯一ID，验证通过才有此项
@property (nonatomic, copy) NSData *mobileld;
//手机随机数，验证通过才有此项
@property (nonatomic, copy) NSData *mRnd;

@end

NS_ASSUME_NONNULL_END
