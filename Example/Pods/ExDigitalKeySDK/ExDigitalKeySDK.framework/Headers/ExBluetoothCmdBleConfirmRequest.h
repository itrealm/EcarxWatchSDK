//
//  ExBluetoothCmdBleConfirmRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdBleConfirmRequest : ExBluetoothBaseRequest

/**
 第一阶段：蓝牙配对连接
 
 @param broadcastRandom 广播码8位随机数
 @param communicateKey 通信秘钥
 */
- (instancetype)initWithBroadcastRandom:(NSData *)broadcastRandom communicateKey:(NSData *)communicateKey;

@end

NS_ASSUME_NONNULL_END
