//
//  ExBluetoothCmdBleConfirmResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdBleConfirmResponse : ExBluetoothBaseResponse

@end

NS_ASSUME_NONNULL_END
