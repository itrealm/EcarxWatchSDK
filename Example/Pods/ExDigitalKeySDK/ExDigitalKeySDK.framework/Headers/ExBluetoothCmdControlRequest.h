//
//  ExBluetoothCmdControlRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

//-------- 车控指令code --------
typedef NS_ENUM(NSUInteger, ExCmdControlBleReqCode) {
    //闭锁，需要门控权限
    ExCmdCloseDoorReqCode = 1,
    //解锁，需要门控权限
    ExCmdOpenDoorReqCode = 2,
    //关窗，需要相应权限
    ExCmdControlPowerCloseWindowDownReqCode = 3,
    //开窗，需要相应权限
    ExCmdControlPowerOpenWindowReqCode = 4,
    //关后备箱，需要相应权限（暂无该功能）
    ExCmdControlCloseTrunkReqCode = 5,
    //开后备箱，需要相应权限
    ExCmdControlOpenTrunkReqCode = 6,
    //寻车，需要相应权限
    ExCmdControlPanicAndSearchReqCode = 7,
    //预授权引擎，需要相应权限
    ExCmdControlPreAuthenticationEngineReqCode = 8
};
////无操作，无权限要求
//static int const ExCmdControlNoActionReqCode = 0;
////闭锁，需要门控权限
//static int const ExCmdControlLockReqCode = 1;
////解锁，需要门控权限
//static int const ExCmdControlNoUnlockReqCode = 2;
////中心锁定，需要相应权限
//static int const ExCmdControlCenterLockReqCode = 3;
////中心解锁，需要相应权限
//static int const ExCmdControlCenterUnlockReqCode = 4;
////后备箱上锁，需要相应权限
//static int const ExCmdControlLockTrunkReqCode = 5;
////后备箱解锁，需要相应权限
//static int const ExCmdControlUnlockTrunkReqCode = 6;
////关窗，需要相应权限
//static int const ExCmdControlPowerWindowUpReqCode = 7;
////开窗，需要相应权限
//static int const ExCmdControlPowerWindowDownReqCode = 8;
////寻车，需要相应权限
//static int const ExCmdControlPanicAndSearchReqCode = 9;
////通风，需要相应权限
//static int const ExCmdControlVentilationReqCode = 10;
////关闭车窗和天窗，需要相应权限
//static int const ExCmdControlLockWindowUpSunroofReqCode = 11;
////开引擎盖，需要相应权限
//static int const ExCmdControlUnlockHoodReqCode = 12;
////预授权引擎，需要相应权限
//static int const ExCmdControlPreAuthenticationEngineReqCode = 13;
////启动引擎，需要相应权限
//static int const ExCmdControlPStartEngineReqCode = 14;
////关闭引擎，需要相应权限
//static int const ExCmdControlStopEngineReqCode = 15;
////远程泊车，需要相应权限
//static int const ExCmdControlRemoteParkReqCode = 16;
////获取配对列表，需要相应权限
//static int const ExCmdControlGetPairedListReqCode = 17;
////预认证条目，需要相应权限
//static int const ExCmdControlPreAuthenticationEntryReqCode = 18;
////欢迎，需要相应权限
//static int const ExCmdControlWelcomeReqCode = 19;
////限速，需要相应权限
//static int const ExCmdControlLimitSpeedReqCode = 20;
////电子围栏，需要相应权限
//static int const ExCmdControlGeoGenceReqCode = 21;

@interface ExBluetoothCmdControlRequest : ExBluetoothBaseRequest

@property (nonatomic, copy) NSData *sessionKey;
//车控cmdId
@property (nonatomic, assign) ExCmdControlBleReqCode cmdControlId;
/**
 第八阶段：车控指令发送
 
 @param cmdControlId 车控指令id
 @param ssc 计数器SSC
 @param sessionKey 会话密钥
 */
- (instancetype)initWithCmdControlId:(ExCmdControlBleReqCode)cmdControlId ssc:(long)ssc sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
