//
//  ExBluetoothCmdControlResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

//-------- 车控指令code --------
typedef NS_ENUM(NSUInteger, ExCmdControlBleRspCode) {
    //预授权引擎，需要相应权限
    ExCmdControlPreAuthenticationEngineRspCode = 1,
    //寻车，需要相应权限
    ExCmdControlPanicAndSearchRspCode = 2,
    //解锁，需要门控权限
    ExCmdControlOpenDoorRspCode = 3,
    //闭锁，需要门控权限
    ExCmdControlCloseDoorRspCode = 4,
    //开窗，需要相应权限
    ExCmdControlPowerOpenWindowRspCode = 5,
    //关窗，需要相应权限
    ExCmdControlPowerCloseWindowDownRspCode = 6,
    //后备箱解锁，需要相应权限
    ExCmdControlOpenTrunkRspCode = 7,
    //关后备箱，需要相应权限（暂无该功能）
    ExCmdControlCloseTrunkEnableRspCode = 8
};

@interface ExBluetoothCmdControlResponse : ExBluetoothBaseResponse

//车控命令Id：0x00E0:预打火，0x001A:寻车，0x0001:上锁和开锁， 0x0024:开窗， 0x0025:关窗， 0x0015:开后备箱
@property (nonatomic, assign) ExCmdControlBleRspCode cmdId;
//执行结果：0：成功；1：失败；2：指令正在执行
@property (nonatomic, assign) int result;
//错误码
@property (nonatomic, assign) int errorCode;
//保留
@property (nonatomic, assign) unsigned short reserved;
//自定义消息
@property (nonatomic, copy) NSString *controlMsg;

- (instancetype)initWithData:(NSData *)data sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
