//
//  ExBluetoothCmdCustRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2020/4/13.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

//-------- RPA泊出模式 --------
typedef NS_ENUM(NSUInteger, ExCmdBleCustType) {
    //无
    ExCmdCustTypeNone = 0,
    //rssi信号同步
    ExCmdCustTypeSignalStrength = 1
};

@interface ExBluetoothCmdCustRequest : ExBluetoothBaseRequest

/**
 Command is used to transfer calibration parameters and location parameters from APP to TBOX.
 
 @param type Refer to Type defintion in below
 @param data TBD, depend on message type, the formats can be defined dynamically.
 */
- (instancetype)initWithType:(ExCmdBleCustType)type
                        data:(NSData *)data
                  sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
