//
//  ExBluetoothCmdInfoExchangeRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdInfoExchangeRequest : ExBluetoothBaseRequest

/**
 第二阶段：双方特征交换
 
 @param mobileId 手机唯一ID
 @param mRnd 手机随机数
 @param digitalKeyId 蓝牙钥匙唯一ID
 @param communicateKey 通信秘钥
 */
- (instancetype)initWithMobileId:(NSData *)mobileId mRnd:(NSData *)mRnd digitalKeyId:(NSData *)digitalKeyId communicateKey:(NSData *)communicateKey;

@end

NS_ASSUME_NONNULL_END
