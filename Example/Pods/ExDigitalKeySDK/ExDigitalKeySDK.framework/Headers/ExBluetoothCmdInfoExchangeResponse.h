//
//  ExBluetoothCmdInfoExchangeResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdInfoExchangeResponse : ExBluetoothBaseResponse

//VIN码
@property (nonatomic, copy) NSData *velId;
//车辆随机数
@property (nonatomic, copy) NSData *vRnd;
//认证密钥是否有效（0x00：无效，0x01:有效）
@property (nonatomic, assign) BOOL authValid;

@end

NS_ASSUME_NONNULL_END
