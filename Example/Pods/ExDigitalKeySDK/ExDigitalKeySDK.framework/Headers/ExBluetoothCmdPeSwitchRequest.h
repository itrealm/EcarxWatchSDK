//
//  ExBluetoothCmdPeSwitchRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2020/5/9.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

//-------- PE功能操作 --------
typedef NS_ENUM(NSUInteger, ExCmdPeSwitchType) {
    //关闭
    ExCmdPeSwitchTypeOff = 0,
    //打开
    ExCmdPeSwitchTypeOn = 1,
    //查询
    ExCmdPeSwitchTypeQuery = 2,
    //无结果
    ExCmdPeSwitchTypeNone = 3
};

@interface ExBluetoothCmdPeSwitchRequest : ExBluetoothBaseRequest

/**
 PE功能操作
 
 @param peSwitch PE操作
 @param sessionKey 会话密钥
 */
- (instancetype)initWithPeSwitch:(ExCmdPeSwitchType)peSwitch sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
