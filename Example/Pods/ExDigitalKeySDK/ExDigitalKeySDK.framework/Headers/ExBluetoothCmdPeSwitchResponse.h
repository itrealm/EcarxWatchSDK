//
//  ExBluetoothCmdPeSwitchResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2020/5/9.
//

#import "ExBluetoothBaseResponse.h"
#import "ExBluetoothCmdPeSwitchRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdPeSwitchResponse : ExBluetoothBaseResponse

//PE请求操作
@property (nonatomic, assign) ExCmdPeSwitchType peSwitch;
//PE当前状态
@property (nonatomic, assign) ExCmdPeSwitchType peStatus;

@end

NS_ASSUME_NONNULL_END
