//
//  ExBluetoothCmdRpaAnswerRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/8/23.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

//-------- RPA手势 --------
typedef NS_ENUM(NSUInteger, ExCmdBleRpaGuesture) {
    //手势有效
    Rpa_GuestureG_Valid = 0,
    //手势无效
    Rpa_GuestureG_Invalid = 1
};

@interface ExBluetoothCmdRpaAnswerRequest : ExBluetoothBaseRequest

/**
 移动终端APP通过BSRM发送给RPA模块的题目答案
 
 @param phoneStatus 手机状态
 @param batteryLevel 电量
 @param randX 题目序号randX
 @param randY 题目序号randY
 @param guesture RPA手势
 */
- (instancetype)initWithPhoneStatus:(NSData *)phoneStatus batteryLevel:(NSData *)batteryLevel randX:(int)randX randY:(int)randY guesture:(ExCmdBleRpaGuesture)guesture sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
