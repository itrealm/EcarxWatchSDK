//
//  ExBluetoothCmdRpaChallengeResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/8/23.
//

#import "ExBluetoothBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

//-------- RPA认证状态 --------
typedef NS_ENUM(NSUInteger, ExCmdBleRpaAuthStatus) {
    RPA_AUTH_SUCCESS = 0,
    RPA_AUTH_FAILED = 1,
    RPA_AUTH_NONE = 2
};

@interface ExBluetoothCmdRpaChallengeResponse : ExBluetoothBaseResponse

//题目序号RandX
@property (nonatomic, assign) NSInteger randX;
//题目序号RandY
@property (nonatomic, assign) NSInteger randY;
//认证状态
@property (nonatomic, assign) ExCmdBleRpaAuthStatus authStatus;

- (instancetype)initWithData:(NSData *)data sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
