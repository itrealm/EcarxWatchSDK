//
//  ExBluetoothCmdRpaRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/8/23.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

//-------- RPA控制指令 --------
typedef NS_ENUM(NSUInteger, ExCmdBleRpaControl) {
    //同步手机状态
    Rpa_SyncPhoneStatus = 0,
    //RPA请求
    Rpa_Request = 1,
    //RSPA泊车请求
    Rspa_Request = 2,
    //RPA开始
    Rpa_Start = 3,
    //RPA泊车暂停
    Rpa_Pause = 4,
    //RPA泊车继续
    Rpa_Continue = 5,
    //RPA停止
    Rpa_Stop = 6,
    //RPA泊车完成
    Rpa_Finish = 7,
    //RPA撤销控制指令
    Rpa_Undo = 8,
    //RPA泊出模式选择
    Rpa_Out_Mode_Set = 9,
    //直线泊车前进
    Rpa_Forward = 10,
    //直线泊车后退
    Rpa_Backward = 11,
    //记忆路径泊出
    Rpa_Memory_Parking_Out = 13,
    //路口选择
    Rpa_Intersection_Select = 14,
    //确认泊入车位
    Rpa_Confirm = 15,
    //取消所有
    Rpa_Cancel_All = 16
};

//-------- RPA泊出模式 --------
typedef NS_ENUM(NSUInteger, ExCmdBleRpaOutMode) {
    //无
    Rpa_Out_Mode_None = 0,
    //水平向右泊出
    Parallel_Right_Out = 1,
    //水平向左泊出
    Parallel_Left_Out = 2,
    //车头垂直向右泊出
    Head_Perpendicular_Right_Out = 3,
    //车头垂直向左泊车
    Head_Perpendicular_Left_Out = 4,
    //车尾垂直向右泊出
    Tail_Perpendicular_Right_Out = 5,
    //车尾垂直向左泊出
    Tail_Perpendicular_Left_Out = 6
};

//-------- RPA转角方向 --------
typedef NS_ENUM(NSUInteger, ExCmdBleIntersectionMode) {
    //无
    No_Command = 0,
    //左拐
    Turn_Left = 1,
    //右拐
    Turn_Right = 2,
    //直行
    Straight = 3
};


@interface ExBluetoothCmdRpaRequest : ExBluetoothBaseRequest

/**
 发送RPA指令
 
 @param rpaControl RPA控制指令
 @param rpaOutMode RPA泊出模式 rpaControl = 3:rpaOutMode filled with <RPA out mode>;else:rpaOutMode set to 0
 @param phoneStatus 手机状态
 @param batteryLevel 电量
 @param intersection 转角方向
 @param sessionKey 秘钥
 */
- (instancetype)initWithRpaControl:(ExCmdBleRpaControl)rpaControl
                        rpaOutMode:(ExCmdBleRpaOutMode)rpaOutMode
                       phoneStatus:(NSData *)phoneStatus
                       batteryLevel:(NSData *)batteryLevel
                      intersection:(ExCmdBleIntersectionMode)intersection
                        sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
