//
//  ExBluetoothCmdRpaResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/8/23.
//

#import "ExBluetoothBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ExCmdBleRpaStatus) {
    //Temp no use
    NoException = 0,
    //RPA 泊入模式 ，Tbox 返回泊车类型状态
    RPAInMode = 1,
    //RPA+ 泊入模式，Tbox 返回泊车类型状态
    RPAAddInMode = 2,
    //RPA 泊出模式 ，Tbox 返回泊车类型状态
    RPAOutMode = 3,
    //RPA+ 泊出模式，Tbox 返回泊车类型状态
    RPAAddOutMode = 4,
    //RSPA 模式，Tbox 返回泊车类型状态
    RSPAMode = 5,
    //泊出模式选择请求 RPA out mode select
    RPAOutModeSelect = 6,
    //显示泊车就绪状态/按钮 TBox/TCAM 收到 park in mode 或者 control Active 发送给 APP，告诉 APP 控制按钮可以使用了
    RPAPrepared = 7,
    //泊车功能退出 RPAcancel
    RPACancel = 8,
    //泊车功能退出 RPAQuit
    RPAQuit = 9,
    //泊车功能吊起 RPASuspend
    RPASuspend = 10,
    //泊车功能吊起恢复 RPA Suspend Recover
    RPASuspendRecover = 11,
    //泊车完成 RPAcompleted
    RPACompleted = 12,
    //泊车撤销 RPAundo
    RPAUndo = 13,
    //泊车过程中的车辆数据同步，如坐标、档位、速度等
    RPASync = 14,
    //泊车失败，终止异常触发
    RPAFailure = 15,
    //记忆泊出
    RPAMemoryParkOut = 32,
    //发现路口
    RPAIntersection = 33,
    //搜索到车位
    RPASlotFound = 34,
    //未搜索到车位
    RPASlotNotFound = 35,
    //已经泊出车位
    RPADriverOutSucc = 36,
    //未泊出车位
    RPADriverOutFail = 37
};

@interface ExRpaStatusIntersectionModel : BaseModel

//左转 yes:可行 no:不可行
@property (nonatomic, assign) BOOL intersectionLeft;
//右转 yes:可行 no:不可行
@property (nonatomic, assign) BOOL intersectionRight;
//直行 yes:可行 no:不可行
@property (nonatomic, assign) BOOL intersectionStraight;

- (instancetype)initWithData:(NSData *)data;

- (NSString *)description;

@end

@interface ExBluetoothCmdRpaResponse : ExBluetoothBaseResponse

//rpa状态
@property (nonatomic, assign) ExCmdBleRpaStatus rpaStatus;
//rpa错误原因
@property (nonatomic, assign) NSInteger reason;
//泊车过程中辅助提示 ID
@property (nonatomic, assign) NSInteger auxiliaryInfo;
//0x00: (开始按钮或者 RSPA 前进后退按钮)置灰 0x01: 高亮
@property (nonatomic, assign) NSInteger startButton;
//转角
@property (nonatomic, copy) ExRpaStatusIntersectionModel *intersection;

- (instancetype)initWithData:(NSData *)data sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
