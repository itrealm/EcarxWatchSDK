//
//  ExBluetoothCmdRpaSyncResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2020/3/5.
//

#import "ExBluetoothBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

//每个字节表示一个方向雷达显示的颜色情况(12个字节表示车辆拥有12个雷达):0x00:不显示 0x01:红色 0x02:黄色 0x03:黄色 0x04:绿色 0x05:绿色 0x06:绿色
@interface ExPasPdcDistanceModel : BaseModel

//前左
@property (nonatomic, assign) NSInteger pasPdcDistanceFL;
//前中左
@property (nonatomic, assign) NSInteger pasPdcDistanceFML;
//前中右
@property (nonatomic, assign) NSInteger pasPdcDistanceFMR;
//前右
@property (nonatomic, assign) NSInteger pasPdcDistanceFR;
//后左
@property (nonatomic, assign) NSInteger pasPdcDistanceRL;
//后中左
@property (nonatomic, assign) NSInteger pasPdcDistanceRML;
//后中右
@property (nonatomic, assign) NSInteger pasPdcDistanceRMR;
//后右
@property (nonatomic, assign) NSInteger pasPdcDistanceRR;
//左侧前
@property (nonatomic, assign) NSInteger pasPdcDistanceLSF;
//右侧前
@property (nonatomic, assign) NSInteger pasPdcDistanceRSF;
//左侧后
@property (nonatomic, assign) NSInteger pasPdcDistanceLSR;
//右侧后
@property (nonatomic, assign) NSInteger pasPdcDistanceRSR;

- (instancetype)initWithData:(NSData *)data;

@end

@interface ExBluetoothCmdRpaSyncResponse : ExBluetoothBaseResponse

//档位  1: P 2: R 3: N 4: D
@property (nonatomic, assign) NSInteger gear;
//速度
@property (nonatomic, assign) NSInteger speed;
//车位类型 0: 没有车位 1: 垂直车位 2: 水平车位
@property (nonatomic, assign) NSInteger slotType;
//与UP点直连的点的象限 0: 无象限 1: 第一象限 2: 第二象限 3: 第三象限 4: 第四象限
@property (nonatomic, assign) NSInteger quartile;
//车位和车的相对坐标
@property (nonatomic, assign) NSInteger upCorX;
//车位和车的相对坐标
@property (nonatomic, assign) NSInteger upCorY;
//车位和车的相对坐标
@property (nonatomic, assign) NSInteger downCorX;
//车位和车的相对坐标
@property (nonatomic, assign) NSInteger downCorY;
//每个字节表示一个方向雷达显示的颜色情况(12个字节表示车辆拥有12个雷达)
@property (nonatomic, copy) ExPasPdcDistanceModel *obstacleColor;
//方向盘转向角度
@property (nonatomic, assign) NSInteger steerWheelAngle;

- (instancetype)initWithData:(NSData *)data sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
