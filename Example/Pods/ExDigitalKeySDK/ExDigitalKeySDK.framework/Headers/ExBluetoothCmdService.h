//
//  ExBluetoothCmdService.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/19.
//

#import <Foundation/Foundation.h>
#import <GLServiceSDK/GLServiceSDK.h>
@class ExBluetoothBaseRequest, ExBluetoothCmdControlRequest, ExBluetoothCmdControlResponse, ExBluetoothBaseResponse;

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdService : NSObject

+ (instancetype)sharedManager;

/**
 蓝牙认证指令请求
 
 @param request 蓝牙请求
 @param responseClass 蓝牙响应，ExBluetoothBaseResponse
 @return 蓝牙请求信号
 */
- (RACSignal *)startAuthRequest:(ExBluetoothBaseRequest *)request response:(Class)responseClass;

/**
 蓝牙车控指令请求
 
 @param request 蓝牙请求
 @param responseClass 蓝牙响应，ExBluetoothBaseResponse
 @return 蓝牙请求信号
 */
- (RACSignal *)startControlRequest:(ExBluetoothCmdControlRequest *)request response:(Class)responseClass;

//请求日志上传
- (void)requestLogWithRequest:(ExBluetoothBaseRequest *)request;

//返回错误日志上传
- (void)responseErrorLogWithResponse:(ExBluetoothBaseResponse *)response error:(NSError *)error;

//返回日志上传
- (void)responseLogWithResponse:(ExBluetoothBaseResponse *)response;

@end

NS_ASSUME_NONNULL_END
