//
//  ExBluetoothCmdSessionRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdSessionRequest : ExBluetoothBaseRequest

/**
 第七阶段：会话秘钥协商
 
 @param mk1 手机生成随机数
 @param authKey 认证密钥
 */
- (instancetype)initWithMk1:(NSData *)mk1 authKey:(NSData *)authKey;

@end

NS_ASSUME_NONNULL_END
