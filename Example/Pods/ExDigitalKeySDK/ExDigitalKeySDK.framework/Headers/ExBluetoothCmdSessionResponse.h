//
//  ExBluetoothCmdSessionResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdSessionResponse : ExBluetoothBaseResponse

//车辆生成的随机数, 验证通过才有此项
@property (nonatomic, copy) NSData *vk2;

@end

NS_ASSUME_NONNULL_END
