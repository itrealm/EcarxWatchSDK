//
//  ExBluetoothCmdViewModel.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/29.
//

#import <Foundation/Foundation.h>
#import "ExBluetoothCmdAPI.h"
#import "ExBluetoothConnectManager.h"
@class ExDigitalkeyInfoModel;

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdViewModel : NSObject

//车控指令
@property (nonatomic, assign) int controlId;
//手机唯一设备码
@property (nonatomic, copy) NSData *mobileIdData;
//手机随机数
@property (nonatomic, copy) NSData *mRndData;
//认证密钥
@property (nonatomic, copy) NSData *authKey;
//计数器
@property (nonatomic, assign) long calculateCounter;
//会话密钥
@property (nonatomic, copy, readonly) NSData *sessionKey;
//pe功能
@property (nonatomic, assign) ExCmdPeSwitchType *peSwitchType;
//钥匙信息
@property (nonatomic, strong) ExDigitalkeyInfoModel *digitalkeyInfo;
//手机状态
@property (nonatomic, assign) ExBluetoothPhoneStatus phoneStatus;
//RPA手势是否在有效区域
@property (nonatomic, assign) ExCmdBleRpaGuesture guesture;

//车辆与手机身份的初次身份验证
@property (nonatomic, strong) RACSignal *bleConfirmSignal;
//双方特征交换
@property (nonatomic, strong) RACSignal *infoExchangeSignal;
//双向身份验证
@property (nonatomic, strong) RACSignal *authSignal;
//数字钥匙传输与验证
@property (nonatomic, strong) RACSignal *vkeySignal;
//数字钥匙传输与验证
@property (nonatomic, strong) RACSignal *sessionSignal;
//车控指令
@property (nonatomic, strong) RACSignal *controlSignal;
//pe指令
@property (nonatomic, strong) RACSignal *peSwitchSignal;

//激活特征通道
- (void)activeCharacteristic;

//RPA指令下发（BX11）
- (void)rpaRequstStartWithRpaControl:(ExCmdBleRpaControl)rpaControl rpaOutMode:(ExCmdBleRpaOutMode)rpaOutMode;

//RPA指令下发（GE13）
- (void)rpaRequstStartWithRpaControl:(ExCmdBleRpaControl)rpaControl rpaOutMode:(ExCmdBleRpaOutMode)rpaOutMode intersection:(ExCmdBleIntersectionMode)intersection;

//RSSI同步指令
- (void)custRssiRequest:(int)rssi;

//判断外设是否是当前需要查询的设备(判断crptedId)
- (BOOL)isTboxBluetoothBroadcastWithBroadcastCode:(NSData *)broadcastCode;

//获取手机状态（前台/后台/打电话）
- (NSData *)getPhoneStatusData;

//获取手机电量
- (NSData *)getBatteryLevel;

@end

NS_ASSUME_NONNULL_END
