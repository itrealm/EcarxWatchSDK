//
//  ExBluetoothCmdVkeyRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/17.
//

#import "ExBluetoothBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothCmdVkeyRequest : ExBluetoothBaseRequest

/**
 第五阶段：数字钥匙传输与验证
 
 @param digitalKey 数字钥匙
 @param identityKey 身份秘钥
 */
- (instancetype)initWithDigitalKey:(NSData *)digitalKey identityKey:(NSData *)identityKey;

@end

NS_ASSUME_NONNULL_END
