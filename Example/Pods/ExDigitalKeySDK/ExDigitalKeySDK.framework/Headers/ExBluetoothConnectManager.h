//
//  ExBluetoothConnectManager.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/11.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <ExDigitalKeyTspServiceSDK/ExDigitalKeyTspServiceSDK.h>
#import "ExBluetoothCmdAPI.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ExBluetoothConnectState) {
    ExBluetoothConnectStateNone,
    ExBluetoothConnectStateScanning,
    ExBluetoothConnectStateConnected,
    ExBluetoothConnectStateConnecting,
    ExBluetoothConnectStateDisConnected
};

typedef NS_ENUM(NSUInteger, ExBluetoothPhoneStatus) {
    //前台
    ExBluetoothPhoneStatusNormal = 0,
    //后台
    ExBluetoothPhoneStatusBackgrund = 1,
    //电话中
    ExBluetoothPhoneStatusCalling = 2
};

//蓝牙连接返回
typedef void(^BluetoothConnectBlock)(NSError * _Nullable error, ExBluetoothConnectState state);

//车控执行结果
typedef void(^CarControlResultBlock)(BOOL isSuccess, NSError * _Nullable error);
//Pe执行结果
typedef void(^PeSwitchResultBlock)(ExCmdPeSwitchType peStatus, NSError * _Nullable error);

//rpa执行结果
typedef void(^RpaControlResultBlock)(ExBluetoothCmdRpaResponse *response, NSError * _Nullable error);
//rpa泊车状态同步结果
typedef void(^RpaControlSyncBlock)(ExBluetoothCmdRpaSyncResponse *response, NSError * _Nullable error);

//车辆状态结果
typedef void(^CarControlVehicleStateBlock)(ExBluetoothVehicleStateModel *vehicleStateModel, NSError * _Nullable error);

@interface ExBluetoothConnectManager : NSObject

//蓝牙连接状态
@property (assign, nonatomic) ExBluetoothConnectState connectState;
//钥匙信息
@property (nonatomic, copy) ExDigitalkeyInfoModel *digitalkeyInfo;
//mobile id
@property (nonatomic, copy, readonly) NSString *mobileId;
//连接到的外设
@property (nonatomic, strong, readonly) CBPeripheral *currentPeripheral;
//手机发送到tbox
@property (nonatomic, strong, readonly) CBCharacteristic *sendDataChara;
//tbox发送到手机
@property (nonatomic, strong, readonly) CBCharacteristic *receiveDataChara;
//同步信号-手机发送到tbox
@property (nonatomic, strong, readonly) CBCharacteristic *sendSyncDataChara;
//蓝牙信号强度
@property (nonatomic, copy, readonly) NSNumber *RSSI;

//监听车辆状态变化
@property (nonatomic, copy) CarControlVehicleStateBlock carControlVehicleStateBlock;
//rpa请求结果返回
@property (nonatomic, copy) RpaControlResultBlock rpaControlResultBlock;
//rpa状态同步
@property (nonatomic, copy) RpaControlSyncBlock rpaControlSyncBlock;

/**
 初始化（需先初始化，再调用连接方法）
 */
+ (instancetype)sharedManager;

/**
 搜索并连接tbox（默认超时时间20s）
 
 @param connectComplete 中的connectStatus 当前报错前一次的连接状态
 */
- (void)connectWithComplete:(BluetoothConnectBlock)connectComplete;

/**
 搜索并连接tbox
 
 @param timeout 超时时间
 @param connectComplete 中的connectStatus 当前报错前一次的连接状态
 */
- (void)connectTimeout:(CGFloat)timeout WithComplete:(BluetoothConnectBlock)connectComplete;

/**
 车控指令
 
 @param controlId 车控id
 @param carControlResultBlock 车控执行结果
 */
- (void)carControlWithControlId:(ExCmdControlBleReqCode)controlId WithCarControlResultBlock:(CarControlResultBlock)carControlResultBlock;

/**
 PE功能（查询/设置开关）
 
 @param peSwitchType pe指令
 @param peSwitchResultBlock pe执行结果
 */
- (void)peSwitchWithPeSwitchType:(ExCmdPeSwitchType)peSwitchType WithCarControlResultBlock:(PeSwitchResultBlock)peSwitchResultBlock;

/**
 RPA指令
 
 @param rpaControl RPA控制指令
 @param rpaOutMode RPA泊出模式：若rpaControl=Rpa_Out_Mode_Set，选择对应泊出模式；否则，rpaOutMode选择Rpa_Out_Mode_None
 @param phoneStatus 手机状态 0：normal 1：backgrund 2：calling
 @param intersection 转角方向
 @param guesture RPA手势是否在有效区域
 */
- (void)rpaControlWithRpaControl:(ExCmdBleRpaControl)rpaControl rpaOutMode:(ExCmdBleRpaOutMode)rpaOutMode phoneStatus:(ExBluetoothPhoneStatus)phoneStatus intersection:(ExCmdBleIntersectionMode)intersection guesture:(ExCmdBleRpaGuesture)guesture;
;

/**
 断开蓝牙连接
 */
- (void)disConnect;

/**
 断开蓝牙连接、清除缓存
 */
- (void)reset;

/**
 清空认证秘钥，在调用重新认证数字钥匙接口后执行
 */
- (void)cleanAuthKey;

@end

NS_ASSUME_NONNULL_END
