//
//  ExBluetoothMessageHandle.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/16.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "ExBluetoothBaseResponse.h"

typedef void (^ExBLEMessageCompletionBlock)(NSData * _Nullable response, NSError * _Nullable error);
typedef void (^ExBLEVehicleStatusBlock)(NSData * _Nullable response, NSError * _Nullable error);
typedef void (^ExBLERpaStatusBlock)(NSData * _Nullable response);
typedef void (^ExBLERpaSyncBlock)(NSData * _Nullable response);
typedef void (^ExBLERpaAnswerStatusBlock)(NSData * _Nullable response);

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothMessageHandle : NSObject

//车辆状态改变
@property (nonatomic ,copy) ExBLEVehicleStatusBlock vehicleStatusBlock;
//rpa状态返回
@property (nonatomic ,copy) ExBLERpaStatusBlock rpaStatusBlock;
//rpa车辆状态同步返回
@property (nonatomic ,copy) ExBLERpaSyncBlock rpaSyncBlock;
//rpa收到答题
@property (nonatomic ,copy) ExBLERpaAnswerStatusBlock rpaAnswerStatusBlock;

+ (instancetype)sharedManager;

/**
 发送消息
 
 @param data 发送请求数据，类型ExBluetoothBaseRequest或NSData
 @param writeCharacteristic 特征写入
 @param completion 返回block
 */
- (void)sendMessageData:(id)data writeCharacteristic:(CBCharacteristic *)writeCharacteristic completion:(ExBLEMessageCompletionBlock)completion;

/**
 接受消息
 
 @param responseData 接受数据
 */
- (void)receiveMessageData:(NSData *)responseData;

/**
 清除当前指令
 */
- (void)cleanCurrentCmd;

@end

NS_ASSUME_NONNULL_END
