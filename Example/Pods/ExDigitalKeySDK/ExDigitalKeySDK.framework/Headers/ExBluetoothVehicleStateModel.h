//
//  ExBluetoothVehicleStateModel.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/5/22.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothVehicleStateModel : BaseModel

//左前门状态 0：关，1：开，2：无效值 3：无效值
@property (nonatomic, assign) int doorFrontLeftStatus;
//右前门 0：关，1：开，2：无效值 3：无效值
@property (nonatomic, assign) int doorFrontRightStatus;
//左后门 0：关，1：开，2：无效值 3：无效值
@property (nonatomic, assign) int doorBackLeftStatus;
//右后门 0：关，1：开，2：无效值 3：无效值
@property (nonatomic, assign) int doorBackRightStatus;

//左前门锁状态 0：未锁，1：已锁，2：无效值 3：无效值
@property (nonatomic, assign) int doorLockFrontLeftStatus;
//右前门锁 0：未锁，1：已锁，2：无效值 3：无效值
@property (nonatomic, assign) int doorLockFrontRightStatus;
//左后门锁 0：未锁，1：已锁，2：无效值 3：无效值
@property (nonatomic, assign) int doorLockBackLeftStatus;
//右后门锁 0：未锁，1：已锁，2：无效值 3：无效值
@property (nonatomic, assign) int doorLockBackRightStatus;

//左前门窗状态 0：关，1：开，2：半开 3：无效值
@property (nonatomic, assign) int doorWindowsFrontLeftStatus;
//右前门窗 0：关，1：开，2：半开 3：无效值
@property (nonatomic, assign) int doorWindowsFrontRightStatus;
//左后门窗 0：关，1：开，2：半开 3：无效值
@property (nonatomic, assign) int doorWindowsBackLeftStatus;
//右后门窗 0：关，1：开，2：半开 3：无效值
@property (nonatomic, assign) int doorWindowsBackRightStatus;

//后备箱解锁使能：0：未使能，1：已使能，2：无效值 3：无效值
@property (nonatomic, assign) int trunkUnlockEnabled;
//引擎盖状态：0：关闭，1：打开，2：无效值 3：无效值
@property (nonatomic, assign) int hoodStatus;
//后备箱状态：0：关闭，1：打开，2：无效值 3：无效值
@property (nonatomic, assign) int trunkStatus;
//启动认证状态：0：无效，1：有效
@property (nonatomic, assign) int startAuthenticationStatus;

//天窗状态：0：关闭， 1：打开，2~7：无效值
@property (nonatomic, assign) int sunroofStatus;
//电驱状态：0：PT ready not active, 1：PT Ready active，2：无效值, 3：无效值
@property (nonatomic, assign) int eptStatus;
//发动机状态: 0:发动机关闭, 1: 发动机启动，2：无效值, 3：无效值
@property (nonatomic, assign) int engineStatus;

//一键透气状态：0：关闭， 1：打开，2：无效值， 3：无效值
@property (nonatomic, assign) int oneClickVentilationStatus;
//电子手刹状态：0：放开， 1：拉起，7：无效值
@property (nonatomic, assign) int epbStatus;
//ACC状态：0：OFF， 1：ON，2：无效值， 3：无效值
@property (nonatomic, assign) int accStatus;

//刹车灯：0: 关闭 1: 打开 2: 无效值 3: 无效值
@property (nonatomic, assign) int breakLight;
//大灯：0: 关闭 1: 打开 2: 无效值 3: 无效值
@property (nonatomic, assign) int lowBeamLight;
//氛围灯：0: 关闭 1: 打开 2: 无效值 3: 无效值
@property (nonatomic, assign) int innerLight;
//后视镜：0: 关闭 1: 打开 2: 无效值 3: 无效值
@property (nonatomic, assign) int mirror;

//整车锁车状态：0: 关闭 1: 打开 2: 无效值 3: 无效值
@property (nonatomic, assign) int vehicleLockStatus;
//设防状态：0: 关闭 1: 打开 2: 无效值 3: 无效值
@property (nonatomic, assign) int fortifyStatus;
/*遮阳帘：
 0: Position unknwon/invalid
 1: Completely closed
 2: Completely opened
 3: Between Mid Position
 4: Mid Position
 5: Between Mid Position and Completely closed
 6~7: Unknown
*/
@property (nonatomic, assign) int rolloPos;

- (instancetype)initWithVehicleStatusData:(NSData *)vehicleStatusData;

/**
 描述，日志用
 
 @return a new NSString, or nil if an error occurs.
 */
- (NSString *)description;

@end

NS_ASSUME_NONNULL_END
