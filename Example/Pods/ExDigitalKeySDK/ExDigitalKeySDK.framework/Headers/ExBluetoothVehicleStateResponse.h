//
//  ExBluetoothVehicleStateResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/5/22.
//

#import "ExBluetoothBaseResponse.h"
#import "ExBluetoothVehicleStateModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExBluetoothVehicleStateResponse : ExBluetoothBaseResponse

//车辆状态
@property (nonatomic, strong) ExBluetoothVehicleStateModel *vehicleStateModel;

- (instancetype)initWithData:(NSData *)data sessionKey:(NSData *)sessionKey;

@end

NS_ASSUME_NONNULL_END
