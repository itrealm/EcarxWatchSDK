//
//  ExActiveRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExActiveRequest : ExRequest

/**
 远程激活车辆蓝牙数字钥匙功能
 
 @param vin 车辆识别码
 @param temId tem唯一识别码
 */
- (instancetype)initWithVin:(NSString *)vin temId:(NSString *)temId;

@end

NS_ASSUME_NONNULL_END
