//
//  ExAuthSharedDigitalkeyRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ExAuthSharedDigitalkeyStatus) {
    ExAuthSharedDigitalkeyStatusUnknown, //0未知
    ExAuthSharedDigitalkeyStatusAccept,  //1表示接收分享
    ExAuthSharedDigitalkeyStatusReject   //2表示拒绝分享
};

@interface ExAuthSharedDigitalkeyRequest : ExRequest

/**
 确认分享的数字钥匙
 
 @param phoneId 手机唯一识别码，由特定sdk产生
 @param digitalKeyId 数字钥匙id
 @param status 确认钥匙结果：1表示接收分享，2表示拒绝分享
 */
- (instancetype)initWithPhoneId:(NSString *)phoneId digitalKeyId:(NSString *)digitalKeyId status:(ExAuthSharedDigitalkeyStatus)status;

@end

NS_ASSUME_NONNULL_END
