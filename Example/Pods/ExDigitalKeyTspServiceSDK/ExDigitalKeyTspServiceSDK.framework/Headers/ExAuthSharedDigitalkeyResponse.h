//
//  ExAuthSharedDigitalkeyResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExAuthSharedDigitalkeyResponse : ExResponse

@end

NS_ASSUME_NONNULL_END
