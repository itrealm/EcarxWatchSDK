//
//  ExCreateOwnerDigitalkeyRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExCreateOwnerDigitalkeyRequest : ExRequest

/**
 创建车主的数字钥匙
 
 @param vin 车辆识别码
 @param temId tem唯一识别码
 @param phoneId 手机唯一识别码，由特定sdk产生
 */
- (instancetype)initWithVin:(NSString *)vin temId:(NSString *)temId phoneId:(NSString *)phoneId;

@end

NS_ASSUME_NONNULL_END
