//
//  ExCreateOwnerDigitalkeyResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import "ExCreateSharedDigitalkeyResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExCreateOwnerDigitalkeyResponse : ExCreateSharedDigitalkeyResponse

@end

NS_ASSUME_NONNULL_END
