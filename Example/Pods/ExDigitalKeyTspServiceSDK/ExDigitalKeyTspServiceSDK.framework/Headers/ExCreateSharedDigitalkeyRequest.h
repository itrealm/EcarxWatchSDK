//
//  ExCreateSharedDigitalkeyRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ExkeyTypeStatus) {
    ExkeyTypeStatusUnknown, //0未知
    ExkeyTypeStatusOwner,  //1表示接收分享
    ExkeyTypeStatusFriend   //2表示拒绝分享
};

@interface ExCreateSharedDigitalkeyRequest : ExRequest

/**
 创建分享的数字钥匙
 
 @param vin 车辆识别码
 @param temId tem唯一识别码
 @param sharedId 被分享者的id
 @param startTime 数字钥匙可用的开始时间，时间戳，单位为秒
 @param endTime 数字钥匙可用的结束时间，时间戳，单位为秒
 @param keyType 数字钥匙的类型，1是车主，2是亲友
 @param times 数字钥匙可用的次数
 */
- (instancetype)initWithVin:(NSString *)vin temId:(NSString *)temId sharedId:(NSString *)sharedId startTime:(NSInteger)startTime endTime:(NSInteger)endTime keyType:(ExkeyTypeStatus)keyType times:(NSInteger)times;

@end

NS_ASSUME_NONNULL_END
