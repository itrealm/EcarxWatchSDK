//
//  ExCreateSharedDigitalkeyResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ExCreateSharedDigitalkeyData @end
@interface ExCreateSharedDigitalkeyData : BaseModel

//数字钥匙id
@property (nonatomic, copy) NSString *digitalKeyId;

@end

@interface ExCreateSharedDigitalkeyResponse : ExResponse

@property (nonatomic, copy) ExCreateSharedDigitalkeyData *data;

@end

NS_ASSUME_NONNULL_END
