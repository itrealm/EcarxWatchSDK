//
//  ExDigitalkeyInfoModel.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/15.
//  钥匙详情

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ExDigitalkeyInfoModel @end
@interface ExDigitalkeyInfoModel : BaseModel

//数字钥匙id
@property (nonatomic, copy) NSString *id;
//版本号
@property (nonatomic, copy) NSNumber *version;
//创建时间
@property (nonatomic, copy) NSNumber *createTime;
@property (nonatomic, copy) NSNumber *randomT0;
//确认时间
@property (nonatomic, copy) NSNumber *authCodeTime;
@property (nonatomic, copy) NSNumber *exclusive;
@property (nonatomic, copy) NSNumber *lockIndex;
@property (nonatomic, copy) NSString *permission;
//确认钥匙结果：1表示接收分享，2表示拒绝分享
@property (nonatomic, copy) NSNumber *status;
//手机唯一识别码，由特定sdk产生
@property (nonatomic, copy) NSString *phoneId;
//数字钥匙
@property (nonatomic, copy) NSString *digitalKey;
//身份秘钥
@property (nonatomic, copy) NSString *identityKey;
//车辆唯一识别码
@property (nonatomic, copy) NSString *vin;
//创建者id
@property (nonatomic, copy) NSString *creatorId;
//分享者id
@property (nonatomic, copy) NSString *sharedId;
//客户端设备id
@property (nonatomic, copy) NSString *deviceId;
//开始时间
@property (nonatomic, copy) NSNumber *startTime;
//结束时间
@property (nonatomic, copy) NSNumber *endTime;
//数字钥匙的类型，0是车主，1是分享
@property (nonatomic, copy) NSNumber *keyType;
//数字钥匙可用的次数
@property (nonatomic, copy) NSNumber *times;
//通信秘钥
@property (nonatomic, copy) NSString *communicateKey;

@end

NS_ASSUME_NONNULL_END
