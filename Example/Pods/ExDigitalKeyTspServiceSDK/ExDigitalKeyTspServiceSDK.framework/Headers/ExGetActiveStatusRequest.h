//
//  ExGetActiveStatusRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExGetActiveStatusRequest : ExRequest

/**
 获取车辆激活状态和车主钥匙Id
 
 @param vin 车辆识别码
 @param temId tem唯一识别码
 */
- (instancetype)initWithVin:(NSString *)vin temId:(NSString *)temId;

@end

NS_ASSUME_NONNULL_END
