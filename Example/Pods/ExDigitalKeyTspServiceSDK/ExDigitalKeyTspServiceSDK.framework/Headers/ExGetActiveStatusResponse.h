//
//  ExGetActiveStatusResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import "ExUpdateActiveStatusResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExGetActiveStatusResponse : ExUpdateActiveStatusResponse

@end

NS_ASSUME_NONNULL_END
