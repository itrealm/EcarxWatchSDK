//
//  ExGetBeSharedDigitalkeysRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExGetBeSharedDigitalkeysRequest : ExRequest

/**
 获取所有被分享的数字钥匙
 */
- (instancetype)init;

@end

NS_ASSUME_NONNULL_END
