//
//  ExGetBeSharedDigitalkeysResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ExBeSharedDigitalkey @end
@interface ExBeSharedDigitalkey : BaseModel

//数字钥匙id
@property (nonatomic, copy) NSString *id;
//版本号
@property (nonatomic, copy) NSNumber *version;
//创建时间
@property (nonatomic, copy) NSNumber *createTime;
@property (nonatomic, copy) NSNumber *randomT0;
//确认时间
@property (nonatomic, copy) NSNumber *authCodeTime;
@property (nonatomic, copy) NSNumber *exclusive;
@property (nonatomic, copy) NSNumber *lockIndex;
@property (nonatomic, copy) NSString *permission;
//确认钥匙结果：1表示接收分享，2表示拒绝分享
@property (nonatomic, copy) NSNumber *status;

@end

@protocol ExGetBeSharedDigitalkeysData @end
@interface ExGetBeSharedDigitalkeysData : BaseModel

//所有被分享的数字钥匙列表
@property (nonatomic, copy) NSArray<ExBeSharedDigitalkey> *digitalKeys;

@end

@interface ExGetBeSharedDigitalkeysResponse : ExResponse

@property (nonatomic, copy) ExGetBeSharedDigitalkeysData *data;

@end

NS_ASSUME_NONNULL_END
