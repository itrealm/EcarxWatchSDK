//
//  ExGetDigitalkeyDetailRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExGetDigitalkeyDetailRequest : ExRequest

/**
 获取数字钥匙详细信息
 
 @param digitalKeyId 数字钥匙的唯一识别码，16字节
 */
- (instancetype)initWithDigitalKeyId:(NSString *)digitalKeyId;

@end

NS_ASSUME_NONNULL_END
