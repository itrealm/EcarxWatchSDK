//
//  ExGetDigitalkeyDetailResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>
#import "ExDigitalkeyInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExGetDigitalkeyDetailResponse : ExResponse

@property (nonatomic, copy) ExDigitalkeyInfoModel *data;

@end

NS_ASSUME_NONNULL_END
