//
//  ExGetShareDigitalkeysRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExGetShareDigitalkeysRequest : ExRequest

/**
 获取的所有分享的数字钥匙
 
 @param vin 车辆识别码
 */
- (instancetype)initWithVin:(NSString *)vin;

@end

NS_ASSUME_NONNULL_END
