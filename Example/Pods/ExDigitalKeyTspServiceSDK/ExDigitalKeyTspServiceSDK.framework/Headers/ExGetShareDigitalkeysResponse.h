//
//  ExGetShareDigitalkeysResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import "ExGetBeSharedDigitalkeysResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExGetShareDigitalkeysResponse : ExGetBeSharedDigitalkeysResponse

@end

NS_ASSUME_NONNULL_END
