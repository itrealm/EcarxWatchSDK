//
//  ExRecertificationDigitalkeyRequest.h
//  ExDigitalKeyTspServiceSDK
//
//  Created by 杨沁 on 2019/7/24.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExRecertificationDigitalkeyRequest : ExRequest

/**
 重新认证数字钥匙
 
 @param oldPhoneId 手机唯一识别码，由特定sdk产生
 @param newPhoneId 手机唯一识别码，由特定sdk产生
 @param digitalKeyId 数字钥匙id，16字节的hex字符串，由服务器返回
 */
- (instancetype)initWithOldPhoneId:(NSString *)oldPhoneId newPhoneId:(NSString *)newPhoneId digitalKeyId:(NSString *)digitalKeyId;

@end

NS_ASSUME_NONNULL_END
