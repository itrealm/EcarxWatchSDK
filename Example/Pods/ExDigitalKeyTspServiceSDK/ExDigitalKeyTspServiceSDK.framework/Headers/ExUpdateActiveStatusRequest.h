//
//  ExUpdateActiveStatusRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//  

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ExActiveStatus) {
    ExActiveStatusUnActive, //未激活
    ExActiveStatusActive    //激活
};

@interface ExUpdateActiveStatusRequest : ExRequest

/**
 更新车辆激活状态
 
 @param vin 车辆识别码
 @param temId tem唯一识别码
 @param activeStatus 车辆数字钥匙激活状态，0表示未激活，1表示激活
 */
- (instancetype)initWithVin:(NSString *)vin temId:(NSString *)temId activeStatus:(ExActiveStatus)activeStatus;

@end

NS_ASSUME_NONNULL_END
