//
//  ExUpdateActiveStatusResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/4/9.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ExUpdateActiveStatusData @end
@interface ExUpdateActiveStatusData : BaseModel

//创建时间
@property (nonatomic, copy) NSString *createTime;
//vin码
@property (nonatomic, copy) NSString *vin;
//车辆数字钥匙激活状态，0表示未激活，1表示激活
@property (nonatomic, copy) NSString *activeStatus;
//tem id
@property (nonatomic, copy) NSString *deviceId;
//手机设备id
@property (nonatomic, copy) NSString *phoneId;
//数字钥匙id
@property (nonatomic, copy) NSString *digitalKeyId;

@end

@interface ExUpdateActiveStatusResponse : ExResponse

@property (nonatomic, copy) ExUpdateActiveStatusData *data;

@end

NS_ASSUME_NONNULL_END
