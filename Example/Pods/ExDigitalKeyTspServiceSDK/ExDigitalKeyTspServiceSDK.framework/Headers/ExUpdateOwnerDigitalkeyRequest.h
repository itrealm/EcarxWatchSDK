//
//  ExUpdateOwnerDigitalkeyRequest.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/8/13.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExUpdateOwnerDigitalkeyRequest : ExRequest

/**
 更新车主的数字钥匙
 
 @param digitalKeyId 数字钥匙的唯一识别码，16字节
 */
- (instancetype)initWithDigitalKeyId:(NSString *)digitalKeyId;

@end

NS_ASSUME_NONNULL_END
