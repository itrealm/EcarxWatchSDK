//
//  ExUpdateOwnerDigitalkeyResponse.h
//  ExDigitalKeySDK
//
//  Created by 杨沁 on 2019/8/13.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExUpdateOwnerDigitalkeyResponse : ExResponse

@end

NS_ASSUME_NONNULL_END
