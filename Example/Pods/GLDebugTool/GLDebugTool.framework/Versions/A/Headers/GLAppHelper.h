//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GLDebug.h"

/**
 Notifications when CPU / Memory / FPS updates.
 Cpu is between 0.0% to 100.0%.
 Memory's unit is byte.
 Fps is float between 0.0 to 60.0
 */
UIKIT_EXTERN NSNotificationName const GLAppHelperUpdateAppInfosNotificationName;
UIKIT_EXTERN NSString * const GLAppHelperCPUKey;
UIKIT_EXTERN NSString * const GLAppHelperMemoryUsedKey;
UIKIT_EXTERN NSString * const GLAppHelperMemoryFreeKey;
UIKIT_EXTERN NSString * const GLAppHelperMemoryTotalKey;
UIKIT_EXTERN NSString * const GLAppHelperFPSKey;

/**
 Monitoring app's properties.
 */
@interface GLAppHelper : NSObject<GLDebugHelper,GLDebugAssisitiveHelper>

/**
 Singleton to monitoring appinfos.
 
 @return Singleton
 */
+ (instancetype)sharedHelper;

-(void)enable;
- (NSString*)moduleName;
- (UIViewController*)rootViewController;
-(Class)debugAssisitiveView;

/**
 Get current app infos.Include "CPU Usage","Memory Usage","FPS","App Name","Bundle Identifier","App Version","App Start Time","Device Model","Phone Name","System Version","Screen Resolution","Language Code","Battery Level","CPU Type","Disk","Network States" and "SSID".
 */
- (NSMutableArray <NSArray <NSDictionary *>*>*)appInfos;


@end
