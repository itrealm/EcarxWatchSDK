//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLDebug.h"


typedef NS_ENUM(NSInteger , GLPieViewShowStyle) {
    GLPieViewNone,
    GLPieViewRoate_X,
    GLPieViewRoate_Y,
    GLPieViewRoate_Z,
    GLPieViewScale,
    GLPieViewOpacity,
    GLPieViewSpring
};

typedef NS_ENUM(NSInteger , GLTouchPosition) {
    GLTouchPositionNone             = 0,
    GLTouchPositionTop              = 1 << 0,
    GLTouchPositionBottom           = 1 << 1,
    GLTouchPositionLeft             = 1 << 2,
    GLTouchPositionRight            = 1 << 3,
};

typedef void(^AssisitiveTouchClicked)(void);


@interface GLAssisitiveTouch : UIView

@property (nonatomic, assign, getter=isCancelMove) BOOL cancelMove;
@property (nonatomic, assign, getter=isHaveAnimation) BOOL haveAnimation;

@property (nonatomic, copy) AssisitiveTouchClicked clickedBlock;

+ (instancetype)sharedTouch;
- (void)registerHelper:(id<GLDebugAssisitiveHelper>)helper;
- (void)registerSubMenu:(NSString*)title action:(AssisitiveTouchClicked)block;

- (void)show;
- (void)hidden;
 

/**
 数据数组，文字或者图片
 */
@property (nonatomic, readonly) NSMutableArray *dataArray;
@property (nonatomic, readonly) NSMutableDictionary *subMenuActions;
/**
 填充色，默认为Alpha0.5的紫色
 */
@property (nonatomic, strong) UIColor *fillColor;
/**
 文字大小，默认为13.0
 */
@property (nonatomic, assign) CGFloat fontSize;
/**
 出现动画，如果设置了该属性，style则不会生效
 */
@property (nonatomic, strong) CAAnimation *animation;
/**
 动画风格
 */
@property (nonatomic, assign) GLPieViewShowStyle style;


@end



@interface GLShapeLayer : CAShapeLayer
/**
 起始半径
 */
@property (nonatomic, assign) CGFloat minRadius, maxRadius;
/**
 起始角度
 */
@property (nonatomic, assign) CGFloat startAngle, endAngle;
/**
 中心
 */
@property (nonatomic, assign) CGPoint centerPoint;
/**
 填充的颜色
 */
@property (nonatomic, strong) UIColor *fullColor;
/**
 文字
 */
@property (nonatomic, copy) NSString *text;
/**
 图像
 */
@property (nonatomic, copy) UIImage *image;
/**
 tag
 */
@property (nonatomic, assign) NSInteger tag;
/**
 文字大小
 */
@property (nonatomic, assign) CGFloat fontSize;

-(void)creatShape;

-(void)selectedState;

-(void)unSelectedState;

@end
