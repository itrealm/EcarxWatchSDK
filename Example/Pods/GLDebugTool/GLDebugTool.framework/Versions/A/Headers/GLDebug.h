//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define GL_IS_SHOW_GLDEBUG @"GL_IS_SHOW_GLDEBUG"



@protocol GLDebugHelper;
@protocol GLDebugAssisitiveHelper;

@interface GLDebug : NSObject
@property(nonatomic,assign)BOOL networkingEnable;
@property(nonatomic,assign)BOOL isShow;

+ (instancetype)sharedInstance;

-(void)addHelper:(id<GLDebugHelper>)helper;
-(void)showAssisitiveTouch:(id<GLDebugAssisitiveHelper>)helper;

@end


@protocol GLDebugHelper <NSObject>
@required

+ (instancetype)sharedHelper;
-(void)enable;
- (NSString*)moduleName;
- (UIViewController*)rootViewController;

@end


@protocol GLDebugAssisitiveHelper <NSObject>
@required

-(Class)debugAssisitiveView;

@end
