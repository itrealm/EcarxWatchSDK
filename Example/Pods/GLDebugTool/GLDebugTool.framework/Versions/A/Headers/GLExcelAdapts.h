//
//  GLExcelAdapts.h
//  GLExcelTestDemo
//
//  Created by zhiyong.kuang on 2019/3/14.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLExcelView.h"


@interface GLExcelXLSXDataAdapt : GLExcelDataAdapt


@end

@interface GLExcelCSVDataAdapt : GLExcelDataAdapt
@end


@interface GLExcelDataBaseAdapt : GLExcelDataAdapt
@end
