//
//  GLExcelBaseParser.h
//  GLExcelTestDemo
//
//  Created by zhiyong.kuang on 2019/3/14.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GLExcelBaseParser : NSObject  

@property (nonatomic, copy) NSString *parseFilePath;    // 要解析的文件路径
@property (nonatomic, strong) NSXMLParser *xmlParser;   // xml解析器
@property (nonatomic, copy) NSString *currentElement;   // 当前节点的名称
@property (nonatomic, strong) NSString *fileRootPath;   // 解压文件目录

- (id)startParse;

@end
