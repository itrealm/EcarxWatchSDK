//
//  GLExcelView.h
//  KTools
//
//  Created by 匡志勇 on 2017/9/25.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GLSortButtonState) {
    GLSortButtonStateNone = 333,
    GLSortButtonStateAsc,
    GLSortButtonStateDesc
};

typedef NS_ENUM(NSInteger, GLLinePosition) {
    GLLinePositionNone          = 0,
    GLLinePositionLeft          = 1 << 0,
    GLLinePositionRight         = 1 << 1,
    GLLinePositionTop           = 1 << 2,
    GLLinePositionBottom        = 1 << 3
};


@interface UIView (GLExcelLine)
-(void)showLineWithColor:(UIColor*)color postion:(GLLinePosition)lineP;
-(void)hideLinePostion:(GLLinePosition)lineP;
@end
@class GLGridAttribute;

@interface GLExcelCell : UICollectionViewCell
@property(nonatomic,strong)NSString* title;
@property(nonatomic,strong)UIColor* lineColor;
@property(nonatomic,assign)GLLinePosition lineStyle;
@property(nonatomic,assign)CGFloat margin;

@property(nonatomic,weak)GLGridAttribute* attr;
@end


@interface GLExcelHeader : GLExcelCell
@property(nonatomic,assign)GLSortButtonState curSortState;
@end


//@interface GLSortButton : UIButton
//@property(nonatomic,assign)GLSortButtonState curSortState;
//@property(nonatomic,strong)UIImage* sortImageNone;
//@property(nonatomic,strong)UIImage* sortImageAsc;
//@property(nonatomic,strong)UIImage* sortImageDesc;
//@property(nonatomic,assign)BOOL showBottomLab;
//@property(nonatomic,strong)UILabel* bottomLab;
//@end



