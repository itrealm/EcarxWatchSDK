//
//  GLExcelView.h
//  KTools
//
//  Created by 匡志勇 on 2017/9/25.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易

#import <UIKit/UIKit.h>
#import "GLExcelView.h"


@interface GLExcelLayout : UICollectionViewLayout
@property(nonatomic,strong)CGSize (^configItemMergeSizeBlock)(NSInteger row,NSInteger column);
@property(nonatomic,strong)CGFloat (^configItemRowHeightBlock)(NSInteger row);
@property(nonatomic,strong)NSMutableArray *rowHeights;//每列的实际高度数组

@property(nonatomic,strong)GLExcelConfiguration* configuration;
@property(nonatomic,assign)BOOL enable;


/**
 设置每一列的实际宽度，确定表的总宽度
 
 @param columnWidths 每一列的宽度（放在数组里）
 @param allColumns 总列数
 @param maxWidth 报表最大的宽度
 */
-(NSArray*)setColumnWidths:(NSArray *)columnWidths withColumns:(NSInteger)allColumns withMaxWidth:(CGFloat)maxWidth;

//重置每一个item的大小和布局
-(void)reset;

@end
