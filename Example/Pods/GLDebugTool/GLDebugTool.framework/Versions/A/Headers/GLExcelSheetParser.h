//
//  GLExcelSheetParser.h
//  GLExcelTestDemo
//
//  Created by zhiyong.kuang on 2019/3/14.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import "GLExcelBaseParser.h"

@interface GLExcelSheetParser : GLExcelBaseParser <NSXMLParserDelegate>

@property (nonatomic, strong) NSArray *sheetArr;    // 从workbook解析出的sheet的名称等信息


@end
