//
//  GLExcelView.h
//  KTools
//
//  Created by 匡志勇 on 2017/9/25.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易

#import <UIKit/UIKit.h>
#import "GLExcelCell.h"
@interface GLExcelItem :NSObject
@property(nonatomic,strong)NSString* value;
@property(nonatomic,assign)CGSize mergeSize;
@property(nonatomic,assign)BOOL isMerged;
@property(nonatomic,assign)BOOL isChecked;

@end

@class GLExcelView;
@interface GLExcelDataAdapt : NSObject
@property(nonatomic,strong)NSString* filePath;

//需要子类实现
-(void)asynParseExcel:(GLExcelView*)excel competion:(void(^)(id data))competion;

-(NSInteger)numberOfRowsInExcel:(GLExcelView*)excelV;
-(CGSize)mergeSizeInExcel:(GLExcelView*)excelV row:(NSInteger)row column:(NSInteger)col;
-(NSArray*)valuesInExcel:(GLExcelView*)excelV row:(NSInteger)row;
@end




typedef NS_ENUM(NSInteger, GLExcelLineStyle) {
    GLExcelLineStyleGrid = 0,
    GLExcelLineStyleList,
    GLExcelLineStyleNone
};

typedef NSArray* (^GLExcelRowText)(GLExcelView* excel,NSInteger row);

typedef void (^GLExcelRowCellAttr)(GLExcelView* excel,NSInteger row,UICollectionViewCell* cell);

typedef NSInteger (^GLExcelRowCount)(GLExcelView* excel);
typedef CGSize (^GLExcelItemMergeSize)(GLExcelView* excel,NSInteger row,NSInteger column);
typedef CGFloat (^GLExcelRowHeight)(NSInteger row);

typedef void (^GLExcelDidSelectRow)(GLExcelView* excel,NSInteger row,NSInteger column);
typedef void (^GLExcelCellSort)(GLExcelView* excel,NSInteger column,GLSortButtonState state,void(^newStateHandler)(GLSortButtonState newsState));
typedef GLSortButtonState (^GLExcelCellSortState)(GLExcelView* excel,NSInteger column);

@class GLGridAttribute;
@class GLExcelAttribute;
@interface GLExcelCollectionView : UICollectionView
@property(nonatomic,assign)BOOL shouldRecognizeSimultaneouslyWithGestureRecognizer;
@end




@interface GLExcelConfiguration : NSObject

/*
 @description cell内边距
 @default 8
 */
@property(nonatomic,assign)CGFloat cellMargin;
/*header*/
/*
 @description 头部颜色
 @default [UIColor whiteColor]
 */
@property(nonatomic,strong)UIColor* headerColor;
/*
 @description 头部线条颜色
 @default [UIColor lightGrayColor]
 */
@property(nonatomic,strong)UIColor* headerLineColor;
/*
 @description 头部高度
 @default 33
 */
@property(nonatomic,assign)CGFloat headerHeight;
/*
 @description 每一列宽度权重，如cell度100。权重数组为@[@1,@2,@1],则第1列宽25，第2列宽50，第三列宽25
 @itemType NSNumber
 @default nil 为nil时，等比均分宽度
 */
@property(nonatomic,strong)NSArray* weights;
@property(nonatomic,assign)CGFloat contentWidth;

@property(nonatomic,assign)NSInteger lockColumn;
@property(nonatomic,assign)NSInteger lockRow;


/*
 @description 定制每列label显示属性，分头部属性和数据属性
 @itemType GLExcelAttribute
 @default nil
 */
@property(nonatomic,strong)GLExcelAttribute* attribute;
@property(nonatomic,strong)GLExcelAttribute* headerAttribute;

@property(nonatomic,strong)GLGridAttribute *defaultAttr;

/*
 @description 数据单元格高度，当configRowHeightBlock实现时，defaultRowHeight不再生效
 @default 44
 */
@property(nonatomic,assign)CGFloat defaultRowHeight;
/**
 自动估算行高 default YES
 设置后columnHeight为最小行高
 configRowHeightBlock将不生效
 */
@property(nonatomic,assign)BOOL autoRowHeight;


/*
 @description 头部与表格间距
 @default 0
 */
@property(nonatomic,assign)CGFloat headMargin;
/*
 @description 是否分单双行显示，双数行会有背景色
 @default NO
 */
@property(nonatomic,assign)BOOL showOddEven;
@property(nonatomic,strong)UIColor* cellBackColor;
@property(nonatomic,strong)UIColor* cellBackColor2;

/*
 @description 是否显示网格线
 @default NO
 */
@property(nonatomic,assign)GLExcelLineStyle lineStyle;
@property(nonatomic,strong)UIColor *gridLineColor;


/*
 @description 头部是否可以被点击，默认NO
 */
@property(nonatomic,assign)BOOL headerCanClick;
@property(nonatomic,assign)UIEdgeInsets headerContentInset;

/*
 @description 头部标题
 @itemType NSString
 数组个数必须与weights、attribute保持一致
 */
@property(nonatomic,strong)NSArray* titles;


+ (GLExcelConfiguration*)defaultConfiguration;
@end







/*
 @description 数据表格
 */
@interface GLExcelView : UIView
/*@required *********************************以下属性必须填写******************************/

/*
 @description 配置数据，返回每一行数据数组。
 @itemType NSString，如果该列有多行，需要嵌套数组
 返回数据示例 @[@[@"第一列主text",@"第一列附text"],@"第二列text",@"第三列text"]
 */
@property(nonatomic,strong)GLExcelRowText configRowsBlock;
/*
 @description 配置数据个数，返回行数
 */
@property(nonatomic,strong)GLExcelRowCount configRowCountBlock;
@property(nonatomic,strong)GLExcelItemMergeSize configItemRowLayoutBlock;

@property(nonatomic,assign)NSInteger numOfColumns;

@property(nonatomic,weak)id<UIScrollViewDelegate>scrollViewDelegate;

@property(nonatomic,strong)GLExcelConfiguration* configuration;

- (instancetype)initWithConfiguration:(GLExcelConfiguration*)configuration;

/*
 @description 开始初始化数据
 */
-(void)start;
-(void)startWithAdapt:(GLExcelDataAdapt*)adapt;
-(void)startWithAdapt:(GLExcelDataAdapt*)adapt parserCompetion:(void(^)(id value))parserCompetion;
/*
 @description 重新加载数据
 */
-(void)reloadData;


/*@option *********************************以下属性可选******************************/
/*
 @description tableView
 */
@property(nonatomic,readonly)GLExcelCollectionView* tableView;
/*
 @description 回调block，用户单击cell时回调
 */
@property(nonatomic,strong)GLExcelDidSelectRow didRowSelectedBlock;


@property(nonatomic,strong)GLExcelRowHeight configRowHeightBlock;



/*
 @description 配置数据样式，具体做到每一个cell。
 */
@property(nonatomic,strong)GLExcelRowCellAttr configCellAttrRowsBlock;


/*
 @description 回调block，排序
 */
@property(nonatomic,strong)GLExcelCellSort cellSort;
@property(nonatomic,strong)GLExcelCellSortState configCellSortState;




@end





@class GLGridAttribute;

@interface GLExcelAttribute : NSObject
-(void)setGridAttribute:(GLGridAttribute*)attr forRegularKey:(NSString*)key;
-(GLGridAttribute*)attribureByIndexPath:(NSIndexPath*)indexPath;
@end




@interface GLGridAttribute : NSObject
@property(nonatomic,assign)NSInteger level;
/*
 @description 字体大小
 */
@property(nonatomic,strong) UIFont      *font;
/*
 @description 字体颜色
 */
@property(nonatomic,strong) UIColor     *textColor;
/*
 @description 文字对齐方式
 */
@property(nonatomic,assign) NSTextAlignment    textAlignment;
/*
 @description 行数
 */
@property(nonatomic,assign) NSInteger numberOfLines;

@property(nonatomic,strong) UIColor* gridBackgroundColor;

@property(nonatomic,strong) UIColor* gridLineColor;


@end



@interface GLGridHeaderAttribute : GLGridAttribute

@property(nonatomic,strong)UIImage* sortImageNone;
@property(nonatomic,strong)UIImage* sortImageAsc;
@property(nonatomic,strong)UIImage* sortImageDesc;
@property(nonatomic,assign)CGRect imageBounds;

@end



