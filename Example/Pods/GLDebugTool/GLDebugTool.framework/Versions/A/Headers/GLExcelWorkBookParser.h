//
//  GLExcelWorkBookParser.h
//  GLExcelTestDemo
//
//  Created by zhiyong.kuang on 2019/3/14.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "GLExcelBaseParser.h"

@interface GLExcelWorkBookParser : GLExcelBaseParser <NSXMLParserDelegate>
@end

@interface GLExcelWorkbookPathParser : GLExcelBaseParser <NSXMLParserDelegate>
@property (nonatomic, strong) NSMutableArray *sheetArr;    // 从workbook解析出的sheet的名称等信息
@end
