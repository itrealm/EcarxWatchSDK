//
//  GLExcelXLSXParser.h
//  GLExcelTestDemo
//
//  Created by zhiyong.kuang on 2019/3/14.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GLExcelXLSXParser : NSObject

- (void)parseFile:(NSString*)filePath competion:(void(^)(NSArray* results,BOOL success))comipetion;

@end
