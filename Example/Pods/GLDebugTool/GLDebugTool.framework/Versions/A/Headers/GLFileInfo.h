//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, GLFileType) {
    GLFileTypeUnknown,
    GLFileTypeDirectory,
    // Image
    GLFileTypeJPG, GLFileTypePNG, GLFileTypeGIF, GLFileTypeSVG, GLFileTypeBMP, GLFileTypeTIF,
    // Audio
    GLFileTypeMP3, GLFileTypeAAC, GLFileTypeWAV, GLFileTypeOGG,
    // Video
    GLFileTypeMP4, GLFileTypeAVI, GLFileTypeFLV, GLFileTypeMIDI, GLFileTypeMOV, GLFileTypeMPG, GLFileTypeWMV,GLFileTypeRMVB,
    // Apple
    GLFileTypeDMG, GLFileTypeIPA, GLFileTypeNumbers, GLFileTypePages, GLFileTypeKeynote,
    // Google
    GLFileTypeAPK,
    // Microsoft
    GLFileTypeWord, GLFileTypeExcel, GLFileTypePPT, GLFileTypeEXE, GLFileTypeDLL,GLFileTypeWPS,
    // Document
    GLFileTypeTXT, GLFileTypeRTF, GLFileTypePDF, GLFileTypeZIP,GLFileTypeRAR, GLFileType7z, GLFileTypeCVS, GLFileTypeMD,
    // Programming
    GLFileTypeSwift, GLFileTypeJava, GLFileTypeC, GLFileTypeCPP, GLFileTypePHP,
    GLFileTypeJSON, GLFileTypePList, GLFileTypeXML, GLFileTypeDatabase,GLFileTypeDBSQLITE,
    GLFileTypeJS, GLFileTypeHTML, GLFileTypeCSS,GLFileTypeINI,GLFileTypeJSP,GLFileTypeBAT,
    GLFileTypeBIN, GLFileTypeDat, GLFileTypeSQL, GLFileTypeJAR,
    // Adobe
    GLFileTypeFlash, GLFileTypePSD, GLFileTypeEPS,
    // Other
    GLFileTypeTTF, GLFileTypeTorrent,
};

@interface GLFileInfo : NSObject

@property (nonatomic, strong) NSURL *URL;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *extension;
@property (nonatomic, strong) NSString *modificationDateText;
@property (nonatomic, strong) NSDictionary<NSString *, id> *attributes;

@property (nonatomic, assign) GLFileType type;
@property (nonatomic, assign, readonly) BOOL isDirectory;
@property (nonatomic, assign) NSUInteger filesCount; // File always 0

@property (nonatomic, strong, readonly) NSString *typeImageName;
@property (nonatomic, assign, readonly) BOOL isCanPreviewInQuickLook;
@property (nonatomic, assign, readonly) BOOL isCanPreviewInWebView;
@property (nonatomic, assign, readonly) BOOL isCanPreviewInDB;

- (instancetype)initWithFileURL:(NSURL *)URL;

+ (NSDictionary<NSString *, id> *)attributesWithFileURL:(NSURL *)URL;
+ (NSMutableArray<GLFileInfo *> *)contentsOfDirectoryAtURL:(NSURL *)URL;

@end
