//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <UIKit/UIKit.h>

@class GLFileInfo;

@interface GLFilePreviewController : UIViewController

@property (nonatomic, strong) GLFileInfo *fileInfo;

@end
