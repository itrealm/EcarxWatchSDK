//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaLumberjack/CocoaLumberjack.h>
#import "DDLog.h"
#import "GLDebug.h"

extern const int ddLogLevel;

#define GLLogError(frmt, ...)   DDLogError(frmt, ##__VA_ARGS__)
#define GLLogWarn(frmt, ...)    DDLogWarn(frmt, ##__VA_ARGS__)
#define GLLogInfo(frmt, ...)    DDLogInfo(frmt, ##__VA_ARGS__)
#define GLLogDebug(frmt, ...)   DDLogDebug(frmt, ##__VA_ARGS__)
#define GLLogVerbose(frmt, ...) DDLogVerbose(frmt, ##__VA_ARGS__)

extern const int ddLogLevel;

@protocol GLLogHelperDelegate <NSObject>

- (void)logMessage:(NSString *)logMessage;
@end

@interface GLLogHelper : DDAbstractLogger <DDLogger,GLDebugHelper>
@property(nonatomic,weak)id<GLLogHelperDelegate> delegate;
@property(nonatomic,readonly,weak)id <DDLogFileManager> fileManager;

+ (instancetype)sharedHelper;

-(void)enable;
- (NSString*)moduleName;
- (UIViewController*)rootViewController;

-(void)sendLogToMailByVC:(UIViewController*)vc;
-(void)sendLogToOtherAppByVC:(UIViewController*)vc;
@end
