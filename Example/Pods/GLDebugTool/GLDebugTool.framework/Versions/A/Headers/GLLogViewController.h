//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLDebugViewController.h"

@interface GLLogViewController : GLDebugViewController
- (void)logMessage:(NSString *)logMessage;
@end

