//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLNetworkModel.h"

@interface GLNetworkDetailViewController : UIViewController

@property (nonatomic , strong) GLNetworkModel *model;

@end
