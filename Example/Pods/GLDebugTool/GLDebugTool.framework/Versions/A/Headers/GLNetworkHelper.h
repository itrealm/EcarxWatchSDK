//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLDebug.h"
extern NSString *const kNetworkTableName;


@interface GLNetworkHelper : NSObject<GLDebugHelper>
@property (nonatomic, assign, getter=isNetworkDeletable) BOOL networkDeletable; // Default is YES
@property(nonatomic,strong)NSDateFormatter* dateFormatter;
@property(nonatomic,strong)NSString* launchDateStr;
@property(nonatomic,assign)BOOL isEnable;

+ (instancetype)sharedHelper;
- (void)enable;

- (NSString*)moduleName;
- (UIViewController*)rootViewController;


-(NSString*)currDateStr;
-(NSString*)dateStrByDate:(NSDate*)date;

@end
