//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLDebugViewController.h"

@interface GLNetworkListViewController : GLDebugTableController

@property (nonatomic , copy) NSString *launchDate;

@end
