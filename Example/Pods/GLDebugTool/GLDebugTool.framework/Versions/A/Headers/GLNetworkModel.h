//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLStorageManager.h"
/**
 Network model. Save and show network request infos.
 */
@interface GLNetworkModel : GLStorageModel
@property (nonatomic , copy , nonnull) NSString *startDate;

/**
 Network request URL.
 */
@property (nonatomic , copy , nullable) NSURL *url;

/**
 Network request method.
 */
@property (nonatomic , copy , nullable) NSString *method;

/**
 Network request mine type.
 */
@property (nonatomic , copy , nullable) NSString *mineType;

/**
 Network request request body.
 */
@property (nonatomic , copy , nullable) NSString *requestBody;

/**
 Network request status code.
 */
@property (nonatomic , copy , nonnull) NSString *statusCode;

/**
 Network request response data.
 */
@property (nonatomic , copy , nullable) NSData *responseData;

/**
 Is image or not.
 */
@property (nonatomic , assign) BOOL isImage;

/**
 Network request used duration.
 */
@property (nonatomic , copy , nonnull) NSString *totalDuration;

/**
 Network request error.
 */
@property (nonatomic , strong , nullable) NSError *error;

/**
 Network request header.
 */
@property (nonatomic , copy , nullable) NSDictionary <NSString *,NSString *>*headerFields;

@end
