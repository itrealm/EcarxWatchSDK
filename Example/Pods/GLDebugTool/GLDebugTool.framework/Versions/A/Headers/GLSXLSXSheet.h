//
//  GLExcelXLSXItem.h
//  GLExcelTestDemo
//
//  Created by zhiyong.kuang on 2019/3/14.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GLExcelXLSXItem : NSObject

@property (nonatomic, strong) NSString *r;    // sheet中的c节点中属性r
@property (nonatomic, strong) NSString *s;    // sheet中的c节点中属性s
@property (nonatomic, strong) NSString *t;    // sheet中的c节点中属性t
@property (nonatomic, strong) NSString *v;    // sheet中的c节点中属性v
@property (nonatomic, strong) NSString *row;  // sheet中的c列所处的行号
@property (nonatomic, strong) NSString *value;        // 对应文件下对应标识下的值
@property (nonatomic, assign) CGSize mergeSize;        // 对应文件下对应标识下的值
@end


@interface GLSXLSXSheet : NSObject

@property (nonatomic, strong) NSString *name;     // sheet的name属性值
@property (nonatomic, strong) NSString *sheetId;  // sheet的sheetId属性值
@property (nonatomic, strong) NSString *rid;      // sheet的r:id属性值
@property (nonatomic, strong) NSString *filePath;      // sheet的路径

@property (nonatomic, assign) NSInteger rowNum;
@property (nonatomic, assign) NSInteger colNum;

@property (nonatomic, strong) NSMutableDictionary *mergeCells;
@property (nonatomic, strong) NSArray *colArray;

@end
