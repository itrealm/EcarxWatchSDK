//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GLDebug.h"

@interface GLSandboxHelper : NSObject<GLDebugHelper>


@property (nonatomic, assign, getter=isSystemFilesHidden) BOOL systemFilesHidden; // Default is YES
@property (nonatomic, copy) NSURL *homeFileURL; // Default is Home Directory
@property (nonatomic, copy) NSString *homeTitle; // Default is `Sandbox`

@property (nonatomic, assign, getter=isExtensionHidden) BOOL extensionHidden; // Default is NO

@property (nonatomic, assign, getter=isShareable) BOOL shareable; // Default is YES

@property (nonatomic, assign, getter=isFileDeletable) BOOL fileDeletable; // Default is YES
@property (nonatomic, assign, getter=isDirectoryDeletable) BOOL directoryDeletable; // Default is YES
@property(nonatomic,assign)BOOL isEnable;

+ (instancetype)sharedHelper;

-(void)enable;

- (NSString*)moduleName;
- (UIViewController*)rootViewController;



+ (NSString *)fileModificationDateTextWithDate:(NSDate *)date;



//Get Folder Size
+ (NSString *)sizeOfFolder:(NSString *)folderPath;
//Get File Size
+ (NSString *)sizeOfFile:(NSString *)filePath;
@end
