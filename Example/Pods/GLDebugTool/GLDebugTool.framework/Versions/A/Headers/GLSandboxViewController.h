//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLFileInfo.h"
#import "GLDebugViewController.h"

@interface GLSandboxViewController : GLDebugTableController

@property (nonatomic, assign, getter=isHomeDirectory) BOOL homeDirectory;
@property (nonatomic, strong) GLFileInfo *fileInfo;

@end
