//
//  GLDebugTool
//
//  Created by kzy on 2018/5/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GLStorageModel : NSObject<NSCoding , NSCopying>
@property (nonatomic , copy) NSString *identity;
@property (nonatomic , copy ) NSString *launchDate;

@end

@interface GLStorageManager : NSObject

+ (instancetype)sharedManager;

- (BOOL)saveModel:(GLStorageModel *)model inTable:(NSString*)tableName;
- (NSArray <GLStorageModel*>*)getAllModelsWithLaunchDate:(NSString *)launchDate fromeTable:(NSString*)tableName;
- (BOOL)removeModels:(NSArray <GLStorageModel*>*)models fromTable:(NSString*)tableName;
- (BOOL)removeModel:(GLStorageModel*)model fromeTable:(NSString*)tableName;

- (BOOL)removeAllDataNotEqual:(NSString *)launchDate fromeTable:(NSString*)tableName;

@end
