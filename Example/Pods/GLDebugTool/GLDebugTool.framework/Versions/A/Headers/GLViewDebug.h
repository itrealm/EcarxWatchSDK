//
//  GLViewDebug.h
//  TestTe
//
//  Created by ibireme on 13-3-8.
//  2013 ibireme.
//

#import <UIKit/UIKit.h>

@interface GLViewDebug : UIWindow
+ (void)show;
+ (void)hide;
@end
