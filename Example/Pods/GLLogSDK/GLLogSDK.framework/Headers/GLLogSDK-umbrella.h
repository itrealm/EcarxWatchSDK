#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "GLLogSDK.h"
#import "AliyunLogObjc.h"
#import "Const.h"
#import "Log.h"
#import "LogClient.h"
#import "LogGroup.h"
#import "NSData+GZIP.h"
#import "NSData+MD5Digest.h"
#import "NSString+Crypto.h"
#import "UIDevice+Identifier.h"

FOUNDATION_EXPORT double GLLogSDKVersionNumber;
FOUNDATION_EXPORT const unsigned char GLLogSDKVersionString[];

