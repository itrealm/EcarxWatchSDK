//
//  GLLogSDK.h
//  GLLogSDK
//
//  Created by fish on 2018/6/7.
//

#import <Foundation/Foundation.h>

//日志类型
typedef NS_OPTIONS(NSUInteger, GLLogType) {
	GLLogTypeConsole = 1 << 0,//控制台
	GLLogTypeAliLog  = 1 << 1,//阿里日志
};

@interface GLLogSDK : NSObject

/**
 日志开关,默认为关
 */
@property(class,nonatomic,assign)BOOL logEnabled;

/**
 日志类型
 */
@property(class,nonatomic,assign)GLLogType logType;

/**
 绑定帐号

 @param account 帐号
 */
+ (void)bindAccount:(NSString *)account;
+ (void)setValue:(NSString *)value forHeaderField:(NSString *)header;

/**
 打印日志

 @param topic   主题
 @param content 内容
 */
+ (void)logContent:(NSDictionary *)content topic:(NSString *)topic;

/**
 打印网络

 @param content 内容
 */
+ (void)logHttpEvent:(NSDictionary *)content;

/**
 打印推送

 @param content 内容
 */
+ (void)logPushEvent:(NSDictionary *)content;

/**
 打印调试日志

 @param content 内容
 */
+ (void)logDebugEvent:(NSDictionary *)content;

@end
