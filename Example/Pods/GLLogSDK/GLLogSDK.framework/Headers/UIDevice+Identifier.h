//
//  UIDevice+Identifier.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2017/11/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IS_IPAD   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define IS_IPHONE_5_LESS   	  ([UIScreen mainScreen].bounds.size.height <  568.0)
#define IS_IPHONE_5           ([UIScreen mainScreen].bounds.size.height == 568.0)
#define IS_IPHONE_6           ([UIScreen mainScreen].bounds.size.height == 667.0)
#define IS_IPHONE_6P          ([UIScreen mainScreen].bounds.size.height == 736.0)
#define IS_IPHONE_X           (CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size)

@interface UIDevice (Identifier)

- (NSString*)deviceVersion;

@end
