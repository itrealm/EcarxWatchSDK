//
//  BaseScrollViewController.h
//  Pods
//
//  Created by fish on 2017/4/23.
//
//

#import "BaseViewController.h"

@interface BaseScrollViewController : BaseViewController

@property (nonatomic,strong,readonly)IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong,readonly)IBOutlet UIView       *contentView;

@end
