//
//  BaseTabBarViewController.h
//  Mintour
//
//  Created by Fish on 15/11/13.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewProtocol.h"

@interface BaseTabBarViewController : UITabBarController<BaseViewProtocol>

@end
