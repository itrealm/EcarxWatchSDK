//
//  BaseTableViewController.h
//  Mintour
//
//  Created by Fish on 15/11/13.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewProtocol.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import <ReactiveObjC/ReactiveObjC.h>

@protocol BaseViewRefreshProtocol <NSObject>  @end

@protocol BaseEmptyDataSetProtocol <NSObject> @end

@interface BaseTableViewController : UITableViewController<BaseViewProtocol,
                                                           BaseEmptyDataSetProtocol,
                                                           DZNEmptyDataSetSource,
                                                           DZNEmptyDataSetDelegate>

@property (nonatomic, strong,readwrite) RACCommand *didSelectedCommand;

- (id<BaseViewProtocol>)initWithViewModel:(id<BaseViewModelProtocol>)viewModel style:(UITableViewStyle)style;

- (void)setRefreshEnable:(BOOL)enable;
- (void)setPullUpRefreshEnable:(BOOL)enable;
- (void)setPullDownRefreshEnable:(BOOL)enable;
- (void)setEmptyDataSetEnable:(BOOL)enable;

//正在加载
+ (NSString *)defaultLoadingText;
//没有数据
+ (NSString *)defaultEmptyText;

//刷新数据
- (void)refreshDataSource;

@end


