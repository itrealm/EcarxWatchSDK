//
//  BaseTableViewModel.h
//  Mintour
//
//  Created by Fish on 15/11/16.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import "BaseViewModel.h"

@interface BaseTableViewModel : BaseViewModel

@property (nonatomic,  copy) NSArray *dataSource;//一维数组或者二维数组

@property (nonatomic,strong) RACCommand *didSelectedCommand;
@property (nonatomic,strong) RACCommand *requestRemoteDataCommand;


/**
 记录分页数据页码，默认从1开始
 */
@property (nonatomic,assign) NSUInteger defaultPage;
@property (nonatomic,assign) NSUInteger page;


/**
 设置是否没有更多数据，YES:没有更多数据 NO：更多数据
 */
@property (nonatomic,assign) BOOL hasNoMoreData;

- (RACSignal *)requestRemoteDataSignalWithPage:(NSUInteger)page;

@end
