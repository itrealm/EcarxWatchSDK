//
//  BaseViewModel.h
//  Mintour
//
//  Created by Fish on 15/11/13.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewModelProtocol.h"

@interface BaseViewModel : NSObject<BaseViewModelProtocol>

@end
