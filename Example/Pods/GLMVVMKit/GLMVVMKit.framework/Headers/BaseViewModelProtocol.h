//
//  BaseViewModelProtocol.h
//  Mintour
//
//  Created by Fish on 15/11/13.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>

@protocol BaseViewModelProtocol <NSObject>

@required
- (void)initialize;

@end
