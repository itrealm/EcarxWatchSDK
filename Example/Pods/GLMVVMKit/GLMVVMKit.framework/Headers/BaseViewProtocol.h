//
//  BaseViewProtocol.h
//  Mintour
//
//  Created by Fish on 15/11/13.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewModelProtocol.h"

typedef void(^ViewWillAppearBlock)(UIViewController *vc,BOOL animated);
typedef void(^ViewDidLoadBlock)(UIViewController *vc);

@protocol BaseViewProtocol <NSObject>

@required
- (id<BaseViewProtocol>)initWithViewModel:(id<BaseViewModelProtocol>)viewModel;

@property (nonatomic,strong,readonly) id<BaseViewModelProtocol> viewModel;


@optional
- (void)bindViewModel;

+ (instancetype)lifeCycleInstance;
@property (nonatomic,copy) ViewWillAppearBlock viewWillAppear;
@property (nonatomic,copy) ViewDidLoadBlock viewDidLoadBlock;

@end
