//
//  GLColorRingView.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2018/1/14.
//  Copyright © 2018年 ecarx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLColorRingView : UIView

@property (nonatomic) UIColor *ringBackgroundColor;
@property (nonatomic) UIColor *ringForegroundColor;
@property (nonatomic) float    ringLineWidth;
@property (nonatomic) float    ringProgress;
@property (nonatomic) BOOL     clockwise;

@end
