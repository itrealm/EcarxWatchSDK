//
//  GLDateFormat.h
//  GLMVVMKit
//
//  Created by fish on 2019/3/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLDateFormat : NSObject

/**
 相对时间格式化
 刚刚         < 1分钟
 x分钟前      < 1小时
 今天 08:08   今天
 周一 08:08   一周内
 1月1日 08:08 同一年
 1月1日,2019  非同一年
 @return 时间字符串
 **/
+ (NSString *)defaultTimeFromDate:(NSDate *)date;

/**
 中文：M月d日 HH:mm
 英文：MMM d HH:mm
 马来：MMM d HH:mm

 @param date 时间
 @return 时间字符串
 */
+ (NSString *)dateTimeFromDate:(NSDate *)date;

/**
 中文：yyyy年MM月dd日
 英文：MMM d,yyyy
 马来：dd-MM-yy

 @param date 时间
 @return 日期字符串
 */
+ (NSString *)dateFromDate:(NSDate *)date;

@end

NS_ASSUME_NONNULL_END
