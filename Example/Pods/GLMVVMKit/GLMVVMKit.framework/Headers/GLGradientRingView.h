//
//  GLGradientRingView.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2018/1/14.
//  Copyright © 2018年 ecarx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLGradientRingView : UIView

@property (nonatomic) UIColor *ringBackgroundColor;
/**
 环颜色: [UIColor]或者[CGColorRef]
 */
@property (nonatomic) NSArray *ringForegroundColors;

/**
 环颜色渐变坡度: [NSNumber],配合ringForegroundColors使用
 */
@property (nonatomic) NSArray *ringForegroundLocations;

/**
 环宽度
 */
@property (nonatomic) float ringLineWidth;

/**
 环方向:YES:顺时针 NO:逆时针，默认NO
 */
@property (nonatomic) BOOL clockwise;

/**
 环进度: 取值范围 [0-1]
 */
@property (nonatomic) float ringProgress;

/**
 动画更新进度

 @param progress 进度: 取值范围 [0-1]
 @param animated 动画
 */
- (void)setRingProgress:(float)progress animated:(BOOL)animated;

@end
