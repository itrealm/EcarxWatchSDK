//
//  GLLanguageManager.h
//  GLMVVMKit
//
//  Created by fish on 2019/3/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

// 英文字符串
typedef NSString *GLLanguageName NS_EXTENSIBLE_STRING_ENUM;

extern GLLanguageName const GLLanguageHans;
extern GLLanguageName const GLLanguageEnglish;
extern GLLanguageName const GLLanguageMalay;

#define kEnglish @"en"
#define kHans    @"zh-Hans"
#define kMalay   @"ms"

//应用内语言切换通知
FOUNDATION_EXPORT NSNotificationName const GLLanguageDidChangeNotification;

#define GLLanguageIsEnglish [GLLanguageManager isEnglish]
#define GLLanguageIsChinese [GLLanguageManager isChinese]

@interface GLLanguageManager : NSObject

//支持的应用内语言
@property(nonatomic,copy,readonly) NSArray *supportLanguages;
//默认语言
@property(nonatomic,copy,readonly) NSString *defaultLanguage;

//系统设置语言
@property(nonatomic,copy,readonly) NSString *systemLanguage;

//应用内设置语言
@property(nonatomic,copy,readonly) NSString *userLanguage;

//当前应用语言
@property(nonatomic,copy,readonly) NSString *appLanguage;

+ (instancetype)sharedManager;

//初始化
+ (void)configSupportLanguages:(NSArray<GLLanguageName> *)languages defaultLanguage:(GLLanguageName)defaultLanguage;
+ (void)setupSupportLanguages:(NSArray<GLLanguageName> *)languages defaultLanguage:(GLLanguageName)defaultLanguage;
//切换应用语言
+ (void)switchLanguage:(GLLanguageName)language;

+ (NSString *)appLanguage;
+ (BOOL)isEnglish;
+ (BOOL)isChinese;

@end

NS_ASSUME_NONNULL_END
