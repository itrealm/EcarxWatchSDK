//
//  GLMVVMKit+Vender.h
//  GLMVVMKit
//
//  Created by fish on 2019/4/10.
//

#ifndef GLMVVMKit_Vender_h
#define GLMVVMKit_Vender_h

///UIKit
#import "UIView+Shadow.h"
#import "UITableViewCell+Attributes.h"
#import "UICollectionViewCell+Attributes.h"
#import "UIColor+MVVMKit.h"
#import "UIDevice+Util.h"
#import "UIImage+MVVMKit.h"
///Foundation
#import "NSMutableAttributedString+X.h"
#import "NSString+Ext.h"
#import "NSUserDefaults+Utils.h"
#import "NSDate+MVVMKit.h"
//Custom
#import "GLNavigationBar.h"
#import "GLGradientRingView.h"
#import "GLRollingLabel.h"
#import "GLTabScrollView.h"
#import "MBProgressHUD+Utils.h"
#import "GLColorRingView.h"

#import <Masonry/Masonry.h>
#import <MJRefresh/MJRefresh.h>

#define WS(ws) __weak __typeof(&*self)ws = self;

#define GLIsiPhoneX ([UIDevice isPhoneXSeries])

#define IS_NULL(string) (![string isKindOfClass:[NSString class]] || \
						   string == nil                          || \
						  [string isEqualToString:@""]            || \
						  [string isKindOfClass:[NSNull class]]   || \
						  [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0)

#define GL_ONE_PIXEL  (1/[UIScreen mainScreen].scale)

//屏幕宽度
#define GLSCreenWidth  [UIScreen mainScreen].bounds.size.width

//屏幕高度
#define GLSCreenHeight [UIScreen mainScreen].bounds.size.height

//状态栏高度
#define GLStatusBarHeight     ([UIApplication sharedApplication].statusBarFrame.size.height)
#define GLNavigationBarHeight 44.0
#define GLTopViewMargin       (GLStatusBarHeight+GLNavigationBarHeight)

//标签栏高度
#define GLTabBarHeight (GLIsiPhoneX ? 83.0 : 49.0)

#define GLScaleWidth   (GLSCreenWidth  / 375.)
#define GLScaleHeight  (GLSCreenHeight / 667.)

#define GLScreenScale  GLScaleWidth

#endif /* GLMVVMKit_Vender_h */
