//
//  UINavigationBar+GLAddition.h
//  UIKit
//

#import <UIKit/UIKit.h>
#import "UITabBar+MVVMKit.h"
#import "UINavigationBar+MVVMKit.h"
#import "UIViewController+MVVMKit.h"
#import "UINavigationController+MVVMKit.h"

@interface UIViewController (GLAddition)

// status bar
- (void)wr_setStatusBarStyle:(UIStatusBarStyle)style;

//navigation bar
- (void)wr_setNavBarHidden:(BOOL)hidden;
- (void)wr_setNavBarBackgroundImage:(UIImage *)image;
- (void)wr_setNavBarBarTintColor:(UIColor *)color;
- (void)wr_setNavBarBackgroundAlpha:(CGFloat)alpha;
- (void)wr_setNavBarTintColor:(UIColor *)color;
- (void)wr_setNavBarShadowImageHidden:(BOOL)hidden;
- (void)wr_setNavBarTitleColor:(UIColor *)color;

@end
