//
//  GLRollingLabel.h
//  GLRollingLabel
//
//  Created by admin on 16/5/12.
//  Copyright © 2016年 Yvan Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kNumberOfLabel 2

typedef NS_ENUM(NSInteger, GLRollingOrientation){
    GLRollingOrientationNone = 0,
    GLRollingOrientationLeft,
    GLRollingOrientationRight
};

@interface GLRollingLabel : UIView{
    NSTimer *_timer;
    
    UILabel *_labels[kNumberOfLabel];
    
    NSArray *_textArray;
    
    BOOL _CanRolling;
    
    NSMutableArray *_textRectArray;
    
    NSInteger _currentIndex;//index of the Current Display Label
    
    UIColor *_textColor;
    
    UIFont *_font;
}



@property (nonatomic, assign) GLRollingOrientation orientation;

@property (nonatomic, assign) CGFloat speed;

//In order to limit only two Labels showing in the view,
//the internalWidth can't be too big.
@property (nonatomic, assign) CGFloat internalWidth;

@property (nonatomic, assign) CGFloat offsetX;

@property (nonatomic, assign) NSTextAlignment textAlignment;

-(id)initWithFrame:(CGRect)frame textArray:(NSArray *)textArray;

-(id)initWithFrame:(CGRect)frame textArray:(NSArray *)textArray font:(UIFont *)font textColor:(UIColor *)textColor;

-(void)beginTimer;

-(void)pauseTimer;

- (void)resumeTimer;

-(void)stopTimer;

-(BOOL)isCanRolling;

@end
