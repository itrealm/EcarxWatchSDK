//
//  GLTabScrollView.h
//  UIKit
//
//  Created by Fish on 17/2/24.
//  Copyright © 2016年 yuqyuan@live.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GLTabScrollViewDelegate;

IB_DESIGNABLE
@interface GLTabScrollView : UIView

@property (nonatomic, weak)IBOutlet id<GLTabScrollViewDelegate> delegate;

@property (nonatomic, strong) IBInspectable NSArray *titles;
@property (nonatomic, assign) NSUInteger selectedIndex;

@property (nonatomic, strong) UIColor     *itemBackColor;
@property (nonatomic, strong) UIColor     *selectedTitleColor;
@property (nonatomic, strong) UIColor     *normalTitleColor;
@property (nonatomic, strong) UIColor     *lineColor;
@property (nonatomic, assign) CGFloat      lineWidth;
//只有设置left,right有效,top和bottom无效
@property (nonatomic, assign) UIEdgeInsets lineEdgeInsets;

@end

@protocol GLTabScrollViewDelegate <NSObject>

- (void)tabScrollView:(GLTabScrollView *)tabScrollView didSelectedAtIndex:(NSUInteger)index;

@end

//兼容旧类
#define TabScrollView 		  GLTabScrollView
#define TabScrollViewDelegate GLTabScrollViewDelegate
