//
//  MBProgressHUD+Utils.h
//
//  Created by xiaomage on 15-6-6.
//  Copyright (c) 2015年 xiaomage. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>

@interface MBProgressHUD (Utils)

//短暂显示文本
+ (instancetype)showMessage:(NSString *)message;

//延时显示文本
+ (void)showDelayMessage:(NSString *)message;

//显示等待Loading
+ (instancetype)showLoading:(NSString *)message;

//隐藏HUD
+ (void)hideHUD;

//短暂显示成功
+ (instancetype)showSuccess:(NSString *)success;

//短暂显示错误
+ (instancetype)showError:(NSString *)error;

//短暂显示文本和图片
+ (instancetype)show:(NSString *)text image:(UIImage *)image;

@end
