//
//  ModuleRouter.h
//  GLMVVMKit
//
//  Created by Fish on 15/11/16.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MGJRouter/MGJRouter.h>
#import "BaseViewProtocol.h"

@interface ModuleRouter : MGJRouter

+ (instancetype)shareInstance;

- (id<BaseViewProtocol>)viewControllerWithViewModel:(id<BaseViewModelProtocol>)viewModel OBJC_DEPRECATED("");

+ (void)resetRootViewController:(UIViewController *)vc OBJC_DEPRECATED("");

+ (void)registerServiceProvider:(MGJRouterObjectHandler)provider forProtocol:(Protocol *)protocol;
+ (id)serviceProviderForProtocol:(Protocol *)protocol;

@end
