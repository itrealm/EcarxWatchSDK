//
//  NSDate+MVVMKit.h
//  GLMVVMKit
//
//  Created by fish on 2019/3/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (MVVMKit)

/**
 *  是否今年
 */
- (BOOL)isThisYear;

/**
 *  是否今天
 */
- (BOOL)isToday;

/**
 *  是否昨天
 */
- (BOOL)isYesterday;

/**
 *  是否明天
 */
- (BOOL)isTomorrow;

/**
 一周内
 */
- (BOOL)isSameWeek;

/**
 按fomatter转换时间为字符串

 @param formatter 格式
 @return 时间字符串
 */
- (NSString *)stringWithFormatter:(NSString *)formatter;

+ (NSString *)stringFromTimeInterval:(NSTimeInterval)interval formatter:(NSString *)formatter;

+ (NSDate *)dateFromString:(NSString *)str formatter:(NSString *)formatter;

@end

NS_ASSUME_NONNULL_END

