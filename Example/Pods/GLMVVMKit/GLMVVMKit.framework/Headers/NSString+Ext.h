//
//  NSString+Ext.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/2.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Ext)

- (BOOL)isBlankString;

- (CGFloat)widthForFont:(UIFont *)font;

#pragma mark - json
/**
 字符串转json

 @return json object
 */
- (id)jsonValue;

/**
 初始化json

 @return string
 */
+ (NSString *)stringWithJson:(id)json;

#pragma mark - validate
//中国手机号码
+ (BOOL)isMobile:(NSString *)string;
//邮箱
+ (BOOL)isEmail:(NSString *)string;
//密码
+ (BOOL)isPassword:(NSString *)string;
//昵称
+ (BOOL)isNickName:(NSString *)string;
//地址
+ (BOOL)isAddress:(NSString *)string;
//验证正则
+ (BOOL)valite:(NSString *)string withRegex:(NSString *)regex;

@end
