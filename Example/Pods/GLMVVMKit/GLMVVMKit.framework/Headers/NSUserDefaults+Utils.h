//
//  NSUserDefaults+Utils.h
//  FlowWallet
//
//  Created by YangQin on 14-12-26.
//  Copyright (c) 2014年 YangQin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (Utils)

// 写入
+ (void)writeObjectByEncoder:(id) object forKey:(NSString *) key;

// 读取
+ (id)readObjectWithKeyByEncoder:(NSString *) key;

// 删除
+ (void)removeObjectForKey:(NSString *) key;

// 判断是否存在
+ (BOOL)boolForKey:(NSString *)key;

@end
