//
//  SegmentedViewController.h
//  Mintour
//
//  Created by Fish on 15/11/19.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import "BaseViewController.h"

@protocol SegmentedControllerDelegate;

@interface SegmentedViewController : BaseViewController

@property (nonatomic, strong, readonly) UISegmentedControl *segmentedControl;
@property (nonatomic, strong, readwrite) NSArray *viewControllers;

@property (nonatomic, weak) id<SegmentedControllerDelegate> delegate;

@end


@protocol SegmentedControllerDelegate <NSObject>

- (void)segmentedController:(SegmentedViewController *)segmentedController
	didSelectViewController:(UIViewController *)viewController;

@end
