//
//  UICollectionViewCell+Attributes.h
//  Pods
//
//  Created by fish on 2017/5/15.
//
//

#import <UIKit/UIKit.h>

@interface UICollectionViewCell (Attributes)

+ (NSString *)identifier;
+ (UINib *)nib;
+ (CGSize)size;

@end
