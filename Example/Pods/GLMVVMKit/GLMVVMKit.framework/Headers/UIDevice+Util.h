//
//  UIDevice+Util.h
//  Geelyconsumer
//
//  Created by 杨沁 on 2017/9/28.
//  Copyright © 2017年 Rdic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Util)

+ (BOOL)isPhoneXSeries;

@end
