//
//  UINavigationBar+MVVMKit.h
//  GLMVVMKit
//
//  Created by fish on 2019/10/9.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (MVVMKit)

@property (nonatomic,assign) BOOL wr_isFakeBar;
@property (nonatomic,assign) CGFloat exBackgroundAlpha;
@property (nonatomic,strong) UIColor *exBackgroundColor;
@property (nonatomic,strong) UIColor *exTitleColor;
@property (nonatomic,strong) UIFont *exTitleFont;

@end
