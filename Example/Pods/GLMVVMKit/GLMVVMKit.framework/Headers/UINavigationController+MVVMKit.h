//
//  UINavigationController+MVVMKit.h
//  GLMVVMKit
//
//  Created by fish on 2019/10/9.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (MVVMKit)

@property (nonatomic, assign) BOOL wr_backgroundViewHidden;
@property (nonatomic, weak) UIViewController *wr_transitionContextToViewController;

@end
