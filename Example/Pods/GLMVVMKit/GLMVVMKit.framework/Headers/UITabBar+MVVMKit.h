//
//  UITabBar+MVVMKit.h
//  AFNetworking
//
//  Created by fish on 2019/10/10.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITabBar (MVVMKit)

//@property(nonatomic,assign)BOOL exShadowImageHidden;
//@property(nonatomic,weak)UIImage *exShadowImage;
//@property(nonatomic,weak)UIColor *exShadowImageColor;

//设置线条及背景色
- (void)setShadowImageHidden:(BOOL)hidden barTintColor:(UIColor *)color;

@end

NS_ASSUME_NONNULL_END
