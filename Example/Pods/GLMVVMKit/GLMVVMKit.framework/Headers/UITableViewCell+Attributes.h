//
//  UITableViewCell+Attributes.h
//  Pods
//
//  Created by fish on 2017/4/20.
//
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Attributes)

+ (NSString *)identifier;
+ (UINib *)nib;
+ (CGFloat)cellHeight;

@end

@interface UITableView (Attributes)

- (void)registerCellWithNib:(Class)nibClass;
- (void)registerCellWithClass:(Class)nibClass;

@end
