//
//  UIView+Shadow.h
//  GLMVVMKit
//
//  Created by fish on 2018/2/7.
//

#import <UIKit/UIKit.h>

@interface UIView (Shadow)

- (void)addBlackShadow;
- (void)addBlueShadow;
- (void)addShadow:(UIColor *)color offset:(CGSize)offset radius:(CGFloat)radius opacity:(CGFloat)opacity;

@end
