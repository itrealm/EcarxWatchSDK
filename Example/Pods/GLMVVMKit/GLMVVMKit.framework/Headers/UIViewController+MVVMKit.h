//
//  UIViewController+MVVMKit.h
//  GLMVVMKit
//
//  Created by fish on 2019/10/9.
//

#import <UIKit/UIKit.h>

@interface UIViewController (MVVMKit)

@property(nonatomic,strong)UINavigationBar *wr_transitionNavigationBar;

- (void)wr_addTransitionNavigationBarIfNeeded;

- (BOOL)canUpdateNavigationBar;

//StatusBar样式
@property(nonatomic,assign)UIStatusBarStyle exStatusBarStyle;

@property(nonatomic,assign)BOOL exNavBarHidden;

//背景色
@property(nonatomic,assign)CGFloat  exNavBarBackgroundAlpha;
@property(nonatomic,strong)UIColor *exNavBarBarTintColor;

//标题
@property(nonatomic,strong)UIFont *exNavBarTitleFont;
@property(nonatomic,strong)UIColor *exNavBarTitleColor;

//阴影
@property(nonatomic,assign)BOOL exNavBarShadowHidden;

@end
