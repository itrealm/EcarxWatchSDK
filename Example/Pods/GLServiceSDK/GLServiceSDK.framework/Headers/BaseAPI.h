//
//  BaseAPI.h
//  Pods
//
//  Created by ecarx on 2017/3/27.
//
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "BaseModel.h"
#import "BaseRequest.h"
#import "BaseResponse.h"

//错误通知
extern NSString * const kHTTPFailureNotification;
extern NSString * const kHTTPSuccessNotification;

@class AFHTTPSessionManager;

typedef void(^SuccessBlock)(id x);
typedef void(^FailureBlock)(NSError *error);

@interface BaseAPI : NSObject

@property (nonatomic,strong) AFHTTPSessionManager *sessionManager;

/// baseURL 服务器地址
@property (nonatomic,copy) NSString *baseURL;

/// 错误域
@property (nonatomic,copy) NSString *errorDomain;

/// 错误重试次数,默认3次(包括原请求)
@property (nonatomic,assign) NSUInteger errorRetryCount;

/// 发送请求，响应数据模型匹配对应Response文件名,如: XXRequest & XXResponse
/// @param request HTTP请求
- (RACSignal *)startRequest:(BaseRequest *)request;

/// 网络请求RAC方式
/// @param request HTTP请求
/// @param responseClass HTTP响应，BaseResponse
- (RACSignal *)startRequest:(BaseRequest *)request response:(Class)responseClass;

/// 取消网络请求
/// @param request HTTP请求
- (void)stopRequest:(BaseRequest *)request;

/// 取消所有网络请求
- (void)stopAllRequests;

/// 打印日志
/// @param responseObject 响应body
/// @param error   错误
/// @param request 请求
- (void)logResponse:(id)responseObject error:(NSError *)error withRequest:(BaseRequest *)request;

@end
