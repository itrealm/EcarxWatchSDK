//
//  BaseRequest.h
//  MVVMKit
//
//  Created by ecarx on 15/11/13.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

typedef NS_ENUM(NSUInteger, HTTPMethod) {
	HTTPMethodGet = 0,
	HTTPMethodPost,
	HTTPMethodPut,
	HTTPMethodDelete,
	HTTPMethodHead,
	HTTPMethodPatch,
    HTTPMethodOptions
};

typedef NS_ENUM(NSUInteger,HTTPRequestFormat) {
    HTTPRequestFormatJSON = 0,//json
    HTTPRequestFormatHTTP = 1 //form
};

typedef void (^BaseConstructingBodyBlock)(id <AFMultipartFormData> formData);

@interface BaseRequest : NSObject

#pragma mark -
/** baseUrl,默认nil */
@property (nonatomic,copy) NSString *url;

/** 请求路径，默认:@"" */
@property (nonatomic,copy) NSString *path;

@property (nonatomic,assign) HTTPRequestFormat format;

/** 是否使用字符转义,默认URL字符转义 */
@property (nonatomic,assign) BOOL usePercentEncoding;

/** 请求方法，默认:GLHTTPMethodGet */
@property (nonatomic,assign,readwrite) HTTPMethod method;

/** 请求参数,默认:nil [数组,字典]*/
@property (nonatomic,copy) id params;

//for multipart form
@property (nonatomic,copy) BaseConstructingBodyBlock constructingBodyBlock;
@property (nonatomic,copy) NSData *constructingBodyData;

//http headers
@property (nonatomic,copy) NSDictionary *headers;

#pragma mark -
/** 错误重试次数 */
@property (nonatomic,assign) NSUInteger retryCount;

/** 禁用错误 **/
@property (nonatomic,assign) BOOL disableError;

/** 模块 */
@property (nonatomic,copy) NSString *module;

@end
