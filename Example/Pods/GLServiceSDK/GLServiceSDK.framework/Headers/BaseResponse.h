//
//  BaseResponse.h
//  Pods
//
//  Created by ecarx on 2017/3/30.
//
//

#import "BaseModel.h"

@interface BaseResponse : BaseModel

@property (nonatomic,assign,readonly) BOOL isSuccess;
@property (nonatomic,assign,readonly) NSInteger code;
@property (nonatomic,copy  ,readonly) NSString *msg;

@end
