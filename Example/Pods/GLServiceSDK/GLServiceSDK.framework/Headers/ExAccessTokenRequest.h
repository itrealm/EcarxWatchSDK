//
//  ExAccessTokenRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"

@interface ExAccessTokenRequest : ExRequest

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

/**
 刷新令牌
 */
- (instancetype)initWithAccessToken:(NSString *)token refreshToken:(NSString *)refreshToken;

@end
