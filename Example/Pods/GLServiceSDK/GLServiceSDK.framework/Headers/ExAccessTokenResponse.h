//
//  ExAccessTokenResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExResponse.h"

@protocol ExAccessToken;

@interface ExAccessToken : BaseModel

//访问令牌，每次请求需要携带
@property (nonatomic, copy) NSString *accessToken;
//访问令牌的超时时间
@property (nonatomic, copy) NSString *expiresIn;

@end

@interface ExAccessTokenResponse : ExResponse

@property (nonatomic, copy) ExAccessToken *data;

@end
