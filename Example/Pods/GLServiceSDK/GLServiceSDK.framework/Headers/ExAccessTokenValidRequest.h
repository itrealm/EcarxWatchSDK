//
//  ExAccessTokenValidRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/5/2.
//

#import "ExRequest.h"

@interface ExAccessTokenValidRequest : ExRequest

- (instancetype)initWithAccessToken:(NSString *)token mobile:(NSString *)mobile;

@end
