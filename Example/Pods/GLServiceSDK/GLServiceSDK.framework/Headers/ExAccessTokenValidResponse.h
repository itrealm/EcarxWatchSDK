//
//  ExAccessTokenValidResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/5/2.
//

#import "BaseAPI.h"
#import "ExResponse.h"

@interface ExTokenValidData : BaseModel

@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *mobile;

@end

@interface ExAccessTokenValidResponse : ExResponse

@property(nonatomic,copy)ExTokenValidData *data;

@end
