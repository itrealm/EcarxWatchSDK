//
//  ExAliyunInfoResponse.h
//  GLServiceSDK
//
//  Created by 杨沁 on 2018/4/3.
//

#import "ExResponse.h"

@protocol ExAliyunInfoData @end

@interface ExAliyunInfoData : BaseModel

@property (nonatomic, copy) NSString *bucket;
@property (nonatomic, copy) NSString *objectKey;
@property (nonatomic, copy) NSString *endpoint;

@end

@interface ExAliyunInfoResponse : ExResponse

@property (nonatomic, copy) ExAliyunInfoData *data;

@end
