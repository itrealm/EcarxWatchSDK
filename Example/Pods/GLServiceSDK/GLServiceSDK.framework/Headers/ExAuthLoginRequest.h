//
//  ExAuthLoginRequest.h
//  GLServiceSDK
//
//  Created by fish on 2020/6/12.
//  通用授权登录

#import <GLServiceSDK/ExRequest.h>
#import <GLServiceSDK/ExLoginResponse.h>

@interface ExAuthLoginRequest : ExRequest

/// auth2登录
/// @param identity 认证类型：需对接申请
/// @param code 第三方授权码
- (instancetype)initWithIdentity:(NSString *)identity code:(NSString *)code;

/// auth2登录
/// @param identity 认证类型：需对接申请
/// @param code 第三方授权码
/// @param url  回调url
- (instancetype)initWithIdentity:(NSString *)identity code:(NSString *)code url:(NSString *)url;

@end

@interface ExAuthLoginResponse : ExLoginResponse

@end
