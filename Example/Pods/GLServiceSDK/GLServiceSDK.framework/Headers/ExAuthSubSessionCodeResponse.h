//
//  ExAuthSubSessionCodeResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/9/27.
//

#import "ExResponse.h"

@interface ExAuthSubSessionCodeData : BaseModel

@property (nonatomic, copy) NSString *expiresIn;
@property (nonatomic, copy) NSString *code;
@end

@interface ExAuthSubSessionCodeResponse : ExResponse

@property (nonatomic, copy) ExAuthSubSessionCodeData *data;

@end
