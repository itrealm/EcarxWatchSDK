//
//  ExAvatarUploadRequest.h
//  GLServiceSDK
//
//  Created by ecarx on 2019/8/8.
//

#import "ExRequest.h"

@interface ExAvatarUploadRequest : ExRequest

/**
 上传用户头像
 
 @param imgString 图片数据
 */
- (id)initWithImageString:(NSString *)imgString;

/**
 上传用户头像

 @param imgData UIImage Data
 */
- (id)initWithImageData:(NSData *)imgData;

@end
