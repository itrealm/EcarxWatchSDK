//
//  ExCacheMessageRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "ExRequest.h"

@interface ExCacheMessageRequest : ExRequest

- (id)initWithVin:(NSString *)vin sessionIds:(NSArray *)sessionIds;

@end
