//
//  ExCacheMessageResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "ExResponse.h"
#import "GLPushNotification.h"

@interface ExCacheMessage : BaseModel

@property (nonatomic, copy) GLPushNotification *body;

@end

@protocol ExCacheMessage @end

@interface ExCacheMessageData : BaseModel

@property(nonatomic, copy)NSArray<ExCacheMessage> *list;

@end


@interface ExCacheMessageResponse : ExResponse

@property(nonatomic,copy)ExCacheMessageData *data;

@end

