//
//  ExCaptchaCheckRequest.h
//  GLServiceSDK
//
//  Created by fish on 2019/4/23.
//

#import "ExRequest.h"
#import "ExResponse.h"
#import "ExVerification.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExCaptchaCheckRequest : ExRequest

/**
 验证短信验证码

 @param mobile       手机号
 @param purpose      验证码申请用途
 @param captcha      验证码
 @param operatorCode 运营商(XIAOKA)
 */
- (id)initWithMobile:(NSString *)mobile
			 purpose:(NSString *)purpose
			 captcha:(NSString *)captcha
			operator:(NSString *)operatorCode;

@end

@interface ExCaptchaCheckResponse : ExResponse

@end

NS_ASSUME_NONNULL_END
