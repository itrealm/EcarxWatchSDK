//
//  ExCaptchaGetRequest.h
//  GLServiceSDK
//
//  Created by fish on 2019/4/23.
//

#import "ExRequest.h"
#import "ExResponse.h"
#import "ExVerificationGetResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExCaptchaGetRequest : ExRequest

/**
 通过手机号获取验证码

 @param mobile 手机号
 @param purpose 验证码申请用途
 @param operatorCode 运营商(XIAOKA)
 */
- (id)initWithMobile:(NSString *)mobile
			 purpose:(NSString *)purpose
			operator:(NSString *)operatorCode;

@end

@interface ExCaptchaGetResponse : ExVerificationGetResponse

@end

NS_ASSUME_NONNULL_END
