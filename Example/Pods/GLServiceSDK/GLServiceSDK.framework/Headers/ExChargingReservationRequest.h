//
//  ExChargingReservationRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/3/9.
//

#import "ExRequest.h"

@interface ExChargingReservationRequest : ExRequest

@property (nonatomic,  copy) NSString  *vin;
@property (nonatomic,assign) NSUInteger pageSize;
@property (nonatomic,assign) NSUInteger pageIndex;
@property (nonatomic,  copy) NSString  *sortField;
@property (nonatomic,  copy) NSString  *direction;

- (instancetype)initWithVin:(NSString *)vin;

@end
