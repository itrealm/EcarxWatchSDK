//
//  ExChargingReservationResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/3/9.
//

#import "ExResponse.h"
#import "GLChargingReservationAPI.h"

@interface ExChargingReservationResponse : ExResponse

@property(nonatomic,copy) GLChargingReservationResponse *data;

@property(nonatomic,readonly)NSArray <GLChargingReservation> *list;

@end
