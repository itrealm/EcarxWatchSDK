//
//  ExChargingSoc.h
//  GLServiceSDK
//
//  Created by fish on 2019/7/15.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExChargingSoc : BaseModel

@property(nonatomic,copy)NSString *soc;
@property(nonatomic,copy)NSString *socTime;

@end

NS_ASSUME_NONNULL_END
