//
//  ExChargingSocRequest.h
//  GLServiceSDK
//
//  Created by fish on 2019/7/15.
//  获取充放电SOC设置

#import "ExRequest.h"

NS_ASSUME_NONNULL_BEGIN

typedef NSString *ExSocSettingName NS_EXTENSIBLE_STRING_ENUM;

extern ExSocSettingName const ExSocSettingCharging;     //充电
extern ExSocSettingName const ExSocSettingDisCharging;  //放电

@interface ExChargingSocRequest : ExRequest

- (id)initWithVin:(NSString *)vin setting:(ExSocSettingName)setting;

@end

NS_ASSUME_NONNULL_END
