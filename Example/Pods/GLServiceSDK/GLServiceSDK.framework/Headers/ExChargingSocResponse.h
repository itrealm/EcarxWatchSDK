//
//  ExChargingSocResponse.h
//  GLServiceSDK
//
//  Created by fish on 2019/7/15.
//

#import "ExResponse.h"
#import "ExChargingSoc.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExChargingSocResponse : ExResponse

@property(nonatomic,copy)ExChargingSoc *data;

@end

NS_ASSUME_NONNULL_END
