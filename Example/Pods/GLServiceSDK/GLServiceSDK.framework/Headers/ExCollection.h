//
//  ExCollection.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/2/26.
//

#import "GLCspServiceSDK.h"

@protocol ExCollection;

@interface ExCollection : BaseModel

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSNumber *createTime;
@property (nonatomic, copy) NSNumber *updateTime;
@property (nonatomic, assign) double  lon;
@property (nonatomic, assign) double  lat;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *poiId;
@property (nonatomic, copy) NSString *label;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *userid;

- (GLCollection *)changeToGLCollection;

@end
