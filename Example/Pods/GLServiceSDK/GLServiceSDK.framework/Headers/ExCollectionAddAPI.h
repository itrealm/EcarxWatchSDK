//
//  ExCollectionAddAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLTspServiceSDK.h"
#import "ExCollection.h"

@interface ExCollectionAddRequest : ExRequest

- (instancetype)initWithCollection:(ExCollection *)collection;

@end

#pragma mark - Response

@interface ExCollectionAddResponse : ExResponse

@end
