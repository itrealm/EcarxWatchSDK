//
//  ExCollectionDelAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLTspServiceSDK.h"

@interface ExCollectionDelRequest : ExRequest

- (instancetype)initWithMapCollectionId:(NSString *)mapCollectionId;

@end

@interface ExCollectionDelResponse : ExResponse

@end
