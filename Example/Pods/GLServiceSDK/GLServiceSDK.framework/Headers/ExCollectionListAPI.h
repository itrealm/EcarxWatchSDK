//
//  ExCollectionListAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLTspServiceSDK.h"
#import "ExCollection.h"

@interface ExCollectionListRequest : ExRequest

@property (nonatomic,assign) NSUInteger pageSize;
@property (nonatomic,assign) NSUInteger pageIndex;

@end

#pragma mark - response

@interface ExCollectionPagination : BaseModel

@property (nonatomic,assign) NSUInteger pageSize;
@property (nonatomic,assign) NSUInteger pageIndex;
@property (nonatomic,assign) NSUInteger totalSize;

@end

@interface ExCollectionListData : BaseModel

@property (nonatomic, copy) NSArray<ExCollection> *list;
@property (nonatomic, copy) ExCollectionPagination *pagination;

@end

@interface ExCollectionListResponse : ExResponse

@property (nonatomic, copy) ExCollectionListData *data;

@end
