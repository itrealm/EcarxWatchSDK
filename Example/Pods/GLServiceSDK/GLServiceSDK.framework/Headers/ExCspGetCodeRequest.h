//
//  ExCspGetCodeRequest.h
//  GLServiceSDK
//
//  Created by 杨沁 on 2018/4/23.
//

#import "ExRequest.h"

@interface ExCspGetCodeRequest : ExRequest

/**
 通过手机号获取验证码
 
 @param username 用户名
 @param purpose  验证码申请用途
 */
- (instancetype)initWithUsername:(NSString *)username purpose:(NSString *)purpose;

@end
