//
//  ExCspQrLoginRequest.h
//  Pods
//
//  Created by fish on 2017/10/7.
//
//

#import "ExRequest.h"

@interface ExCspQrLoginRequest : ExRequest

/**
 扫码登录
 
 @param uid 用户id
 @param vin vin号
 */
- (instancetype)initWithUser:(NSString *)uid vin:(NSString *)vin;

@end
