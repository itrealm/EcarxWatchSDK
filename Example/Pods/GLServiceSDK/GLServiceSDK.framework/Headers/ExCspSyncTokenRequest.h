//
//  ExCspSyncTokenRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/20.
//

#import "ExRequest.h"

@interface ExCspSyncTokenRequest : ExRequest

- (instancetype)initWithAccessToken:(NSString *)accessToken cspId:(NSString *)cspId;

@end
