//
//  ExCspUserRegisterRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/12.
//	

#import "ExRequest.h"

@interface ExCspUserRegisterRequest : ExRequest

- (instancetype)initWithUsername:(NSString *)username password:(NSString *)password;

@end
