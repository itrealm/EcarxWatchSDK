//
//  ExCspVehicleBindRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/5/16.
//

#import "ExRequest.h"

@interface ExCspVehicleBindRequest : ExRequest

@property(nonatomic,copy,readonly) NSString *vin;

- (instancetype)initWithVin:(NSString *)vin platform:(NSString *)platform;

@end
