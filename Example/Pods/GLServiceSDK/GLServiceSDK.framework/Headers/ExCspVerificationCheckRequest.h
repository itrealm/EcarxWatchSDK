//
//  ExCspVerificationCheckRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/12.
//	

#import "ExRequest.h"

@interface ExCspVerificationCheckRequest : ExRequest

- (instancetype)initWithUsername:(NSString *)username
					   	 captach:(NSString *)captach
					     purpose:(NSString *)purpose;

@end
