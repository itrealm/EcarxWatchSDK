//
//  ExCspVerificationGetRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/12.
//	

#import "ExRequest.h"

@interface ExCspVerificationGetRequest : ExRequest

/**
 通过手机号获取验证码
 
 @param username 用户名
 @param purpose  验证码申请用途
 */
- (instancetype)initWithUsername:(NSString *)username purpose:(NSString *)purpose;

@end
