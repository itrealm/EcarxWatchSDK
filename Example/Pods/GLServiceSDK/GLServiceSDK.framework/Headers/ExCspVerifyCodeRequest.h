//
//  ExCspVerifyCodeRequest.h
//  GLServiceSDK
//
//  Created by 杨沁 on 2018/4/23.
//

#import "ExRequest.h"

@interface ExCspVerifyCodeRequest : ExRequest

- (instancetype)initWithUsername:(NSString *)username
                verificationcode:(NSString *)verificationcode
                         purpose:(NSString *)purpose
                        deviceId:(NSString *)deviceId;

@end
