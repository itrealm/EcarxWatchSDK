//
//  ExDataSyncUserRequest.h
//  Pods
//
//  Created by fish on 2017/10/7.
//
//

#import "ExRequest.h"

@interface ExDataSyncUser : BaseModel

@property (nonatomic, copy) NSString *hashedPassword;
@property (nonatomic, copy) NSString *avatarUri;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *origin;
@property (nonatomic, copy) NSString *userId;

@end

@interface ExDataSyncUserRequest : ExRequest

/**
 同步用户信息

 @param user      用户数据
 @param isUpdate  YES:更新数据 NO:插入新数据
 */
- (instancetype)initWithUser:(ExDataSyncUser *)user isUpdate:(BOOL)isUpdate;

@end
