//
//  ExDataSyncVehicleRequest.h
//  Pods
//
//  Created by fish on 2017/10/7.
//
//

#import "ExRequest.h"

extern NSString * const kExOpBind;  //绑车
extern NSString * const kExOpUnbind;//解绑

@interface ExDataSyncVehicleRequest : ExRequest

/**
 绑定车辆

 @param uid    用户ID
 @param vin    vin
 @param owner  是否车主
 @param op     操作[bind,unbind]
 */
- (instancetype)initWithUser:(NSString *)uid vin:(NSString *)vin owner:(BOOL)owner op:(NSString *)op;

@end
