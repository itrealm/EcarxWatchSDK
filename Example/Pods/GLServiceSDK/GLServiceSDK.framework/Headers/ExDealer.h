//
//  ExDealer.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/12.
//

#import "BaseModel.h"
#import "GLDealer.h"

@protocol ExDealer;

@interface ExDealer : BaseModel

//地址
@property (nonatomic, copy) NSString *address;
//状态
@property (nonatomic, copy) NSString *bizStatus;
//web服务
@property (nonatomic, copy) NSString *webSeriese;
//品牌
@property (nonatomic, copy) NSString *brand;
//创建时间
@property (nonatomic, copy) NSNumber *createTime;
//经销商编码
@property (nonatomic, copy) NSString *dealerCode;
//经销商Id
@property (nonatomic, copy) NSNumber *dealerId;
//经销商名称
@property (nonatomic, copy) NSString *dealerName;
//热线电话
@property (nonatomic, copy) NSString *hotLine;
//编号
@property (nonatomic, copy) NSNumber *id;
//更新时间
@property (nonatomic, copy) NSNumber *updateTime;

@end
