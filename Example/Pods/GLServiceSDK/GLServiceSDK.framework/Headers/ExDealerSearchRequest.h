//
//  ExDealerSearchRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/12.
//

#import "ExRequest.h"

@interface ExDealerSearchRequest : ExRequest

- (instancetype)initWithSearch:(NSString *)search;

@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) NSUInteger pageIndex;

@end
