//
//  ExDealerSearchResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/12.
//

#import "ExResponse.h"
#import "ExDealer.h"

@interface ExDealerSearchResponse : ExResponse

//页大小
@property (nonatomic, copy) NSNumber *pageSize;
//当前页码
@property (nonatomic, copy) NSNumber *pageIndex;
//总页数
@property (nonatomic, copy) NSNumber *totalPageCount;
//记录总数
@property (nonatomic, copy) NSNumber *record;
//列表数据
@property (nonatomic, copy) NSArray<ExDealer> *data;

@end
