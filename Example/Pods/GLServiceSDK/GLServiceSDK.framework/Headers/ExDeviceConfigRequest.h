//
//  ExDeviceConfigRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "ExRequest.h"

@interface ExDeviceConfigRequest : ExRequest

/** 开启OTA升级功能 */
@property(nonatomic,assign) BOOL hasOtaFunction;

/**
 初始化客户端

 @param deviceId ecarxId
 @param appId    appId
 */
- (id)initWithDeviceId:(NSString *)deviceId appId:(NSString *)appId;

/**
 初始化客户端

 @param deviceId  ecarxId
 @param appId     appId
 @param aliasName 别名或唯一标识符
 */
- (id)initWithDeviceId:(NSString *)deviceId appId:(NSString *)appId aliasName:(NSString *)aliasName;

@end
