//
//  ExDeviceConfigResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "ExResponse.h"

@interface ExDeviceConfigAppCode : BaseModel

@property(nonatomic,copy)NSString *appId;
@property(nonatomic,copy)NSString *tokenExpiration;

@end

@interface ExDeviceConfigData : BaseModel

@property(nonatomic,copy)NSString *env;
@property(nonatomic,copy)NSString *aliasName;
@property(nonatomic,copy)NSString *theme;
@property(nonatomic,copy)NSString *ecarxId;
@property(nonatomic,copy)ExDeviceConfigAppCode *appCode;

@end

@interface ExDeviceConfigResponse : ExResponse

@property(nonatomic,copy)ExDeviceConfigData *data;

@end
