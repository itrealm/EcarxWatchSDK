//
//  ExDeviceSessionUpdateRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "ExRequest.h"

@interface ExDeviceSessionUpdateRequest : ExRequest

- (instancetype)initWithVin:(NSString *)vin token:(NSString *)token language:(NSString *)language;

@end
