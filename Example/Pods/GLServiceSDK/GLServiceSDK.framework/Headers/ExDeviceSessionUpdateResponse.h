//
//  ExDeviceSessionUpdateResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "ExResponse.h"

@interface ExDeviceSessionUpdateData : BaseModel

@property(nonatomic,copy)NSString *alias;

@end

@interface ExDeviceSessionUpdateResponse : ExResponse

@property (nonatomic,copy)ExDeviceSessionUpdateData *data;

//扩展
@property(nonatomic,copy,readonly)NSString *alias;

@end
