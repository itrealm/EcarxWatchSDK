//
//  ExDisableMatCodeResponse.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2017/12/26.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "BaseResponse.h"

@interface ExDisableMatCodeResponse : BaseResponse

@property (nonatomic,copy)NSArray *disableMatCodes;

@end
