//
//  ExFlowData.h
//  GLServiceSDK
//
//  Created by fish on 2018/7/25.
//

#import "BaseModel.h"

@interface ExFlowPackage : BaseModel

@property (nonatomic,   copy) NSString *productId;
@property (nonatomic,   copy) NSString *orderId;
@property (nonatomic, assign) NSInteger dataVolumeInit;
@property (nonatomic, assign) NSInteger dataVolume;
@property (nonatomic, assign) NSInteger usedDataVolume;
@property (nonatomic,   copy) NSString *activeDate;
@property (nonatomic,   copy) NSString *expireDate;

@end

@protocol ExFlowPackage @end

/** 基础流量包 */
@interface ExFlowBasicPackage : BaseModel

@property (nonatomic, assign) NSInteger authTime;
@property (nonatomic, assign) NSInteger startTime;
@property (nonatomic, assign) NSInteger limitTime;
@property (nonatomic, assign) NSInteger availableDays;

@end

/** 增值套餐 */
@interface ExFlowIncreasePackage : BaseModel

@property (nonatomic,copy) NSArray<ExFlowPackage> *package;

@end

@interface ExFlowData : BaseModel

@property (nonatomic, assign) NSInteger sum;    //当前用户初始数据流量（不限量为-1）
@property (nonatomic, assign) NSInteger left;   //当前用户总数据余量
@property (nonatomic, assign) NSInteger used;   //当前用户总数据用量
@property (nonatomic,   copy) ExFlowIncreasePackage *increasePackage;
@property (nonatomic,   copy) ExFlowBasicPackage     *basicPackage;

@end
