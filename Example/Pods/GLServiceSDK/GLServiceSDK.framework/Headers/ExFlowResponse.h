//
//  ExFlowResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/15.
//

#import "ExResponse.h"
#import "ExFlowData.h"

@interface ExFlowResponse : ExResponse

@property (nonatomic,copy) ExFlowData *data;

@end


