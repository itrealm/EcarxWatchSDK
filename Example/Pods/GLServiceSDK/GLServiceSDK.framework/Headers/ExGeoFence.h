//
//  ExGeoFence.h
//  GLServiceSDK
//
//  Created by fish on 2018/5/28.
//

#import "BaseModel.h"

@protocol ExGeoFence @end

@interface ExGeoFence : BaseModel

@property (nonatomic,assign) NSInteger id;         //围栏ID(新增围栏时不用填写，服务下发)
@property (nonatomic,  copy) NSString *vin;        //车架号
@property (nonatomic,  copy) NSString *name;       //围栏名称
@property (nonatomic,  copy) NSString *centerName; //围栏中心
@property (nonatomic,assign) NSInteger latitude;   //纬度*3600000
@property (nonatomic,assign) NSInteger longitude;  //经度*3600000
@property (nonatomic,assign) NSInteger radius;     //单位:km
@property (nonatomic,assign) long long startTime;  //时间戳(毫秒)
@property (nonatomic,assign) long long endTime;    //时间戳(毫秒)
@property (nonatomic,assign) BOOL status;          //有效状态 YES:有效 NO:无效
@property (nonatomic,assign) BOOL expired;         //过期状态 YES:已过期 NO:有效期内
@property (nonatomic,assign) long long createTime; //创建时间
@property (nonatomic,assign) long long updateTime; //更新时间

@end
