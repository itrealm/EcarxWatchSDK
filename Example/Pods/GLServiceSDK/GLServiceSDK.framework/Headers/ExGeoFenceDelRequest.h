//
//  ExGeoFenceDelRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "ExRequest.h"

@interface ExGeoFenceDelRequest : ExRequest

- (instancetype)initWithId:(NSInteger)Id;

@end
