//
//  ExGeoFenceListRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "ExRequest.h"

@interface ExGeoFenceListRequest : ExRequest

- (instancetype)initWithVin:(NSString *)vin;

@end
