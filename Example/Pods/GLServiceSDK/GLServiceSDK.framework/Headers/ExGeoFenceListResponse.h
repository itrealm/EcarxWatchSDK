//
//  ExGeoFenceListResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "ExResponse.h"
#import "ExGeoFence.h"

@interface ExGeoFenceListResponse : ExResponse

@property (nonatomic,copy) NSArray<ExGeoFence> *data;

@property (nonatomic,copy,readonly) NSArray<ExGeoFence> *list;

@end
