//
//  ExGeoFenceSaveRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "ExRequest.h"
#import "ExGeoFence.h"

@interface ExGeoFenceSaveRequest : ExRequest

- (instancetype)initWithData:(ExGeoFence *)data;

@end
