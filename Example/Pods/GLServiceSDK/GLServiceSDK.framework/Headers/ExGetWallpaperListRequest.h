//
//  ExGetWallpaperListRequest.h
//  GLServiceSDK
//
//  Created by 杨沁 on 2018/4/3.
//

#import "ExRequest.h"

@interface ExGetWallpaperListRequest : ExRequest

- (instancetype)initWithVin:(NSString *)vin;
@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) NSUInteger pageIndex;

@end
