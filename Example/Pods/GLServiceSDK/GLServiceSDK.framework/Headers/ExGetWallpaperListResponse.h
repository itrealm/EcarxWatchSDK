//
//  ExGetWallpaperListResponse.h
//  GLServiceSDK
//
//  Created by 杨沁 on 2018/4/3.
//

#import "ExResponse.h"
#import "ExWallpaper.h"

@protocol ExGetWallpaperListPagination @end

@interface ExGetWallpaperListPagination : BaseModel

@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, assign) NSInteger totalSize;
@property (nonatomic, assign) NSInteger pageIndex;

@end

@protocol ExGetWallpaperListData @end

@interface ExGetWallpaperListData : BaseModel

@property (nonatomic, copy) NSArray<ExWallpaper> *list;
@property (nonatomic, copy) ExGetWallpaperListPagination *pagination;

@end

@interface ExGetWallpaperListResponse : ExResponse

@property (nonatomic, copy) ExGetWallpaperListData *data;

@end
