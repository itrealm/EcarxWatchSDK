//
//  ExGrowEventRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/6/14.
//

#import "ExRequest.h"
#import "ExResponse.h"

@interface ExGrowEventRequest : ExRequest

@property (nonatomic,copy) NSString *vin;
@property (nonatomic,copy) NSString *event;

- (instancetype)initWithVin:(NSString *)vin event:(NSString *)event;

@end

@interface ExGrowEventResponse : ExResponse

@end
