//
//  ExLoginRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"

@interface ExLoginRequest : ExRequest

/**
 用户登录(账号密码)
 
 @param username 用户名（手机号等）
 @param password 密码
 */
- (instancetype)initWithUsername:(NSString *)username password:(NSString *)password;


/**
 验证码登录

 @param username    用户名（手机号等）
 @param captcha 	验证码
 */
- (instancetype)initWithUsername:(NSString *)username captcha:(NSString *)captcha;

@end
