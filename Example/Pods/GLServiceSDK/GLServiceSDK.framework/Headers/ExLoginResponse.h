//
//  ExLoginResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExResponse.h"

@protocol ExLogin;

@interface ExLogin : BaseModel

//用户唯一标识
@property (nonatomic, copy) NSString *userId;
//访问令牌，每次请求需要携带
@property (nonatomic, copy) NSString *accessToken;
//访问令牌的超时时间
@property (nonatomic, copy) NSNumber *expiresIn;
//用于刷新访问令牌使用
@property (nonatomic, copy) NSString *refreshToken;

@property (nonatomic, copy) NSString *idToken;

@property (nonatomic, copy) NSString *tcToken;

@property (nonatomic, copy) NSString *alias;

@property (nonatomic, copy) NSString *clientId;

@end

@interface ExLoginResponse : ExResponse

@property (nonatomic, copy) ExLogin *data;

@end
