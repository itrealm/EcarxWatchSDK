//
//  ExLogoutRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"

@interface ExLogoutRequest : ExRequest

/**
 用户登出
 */
- (instancetype)init;

@end
