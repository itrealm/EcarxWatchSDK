//
//  ExLogoutResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExResponse.h"

@protocol ExLogout;

@interface ExLogout : BaseModel

//处理代码 允许值: 1000
@property (nonatomic, copy) NSString *code;
//消息
@property (nonatomic, copy) NSString *message;

@end

@interface ExLogoutResponse : ExResponse

@property (nonatomic, copy) ExLogout *data;

@end
