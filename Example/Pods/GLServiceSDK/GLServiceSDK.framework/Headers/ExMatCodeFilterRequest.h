//
//  ExMatCodeFilterRequest.h
//  GLServiceSDK
//
//  Created by fish on 2019/11/15.
//

#import "ExRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExMatCodeFilterRequest : ExRequest

- (instancetype)initWithVin:(NSString *)vin matCode:(NSString *)matCode;

@end

NS_ASSUME_NONNULL_END
