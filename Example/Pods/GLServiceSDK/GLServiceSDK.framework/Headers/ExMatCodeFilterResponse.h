//
//  ExMatCodeFilterResponse.h
//  GLServiceSDK
//
//  Created by fish on 2019/11/15.
//

#import "ExResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExMatCodeFilterData : BaseModel

@property (nonatomic,assign) BOOL isFilter;

@end

@interface ExMatCodeFilterResponse : ExResponse

@property(nonatomic,copy)ExMatCodeFilterData *data;

@end

NS_ASSUME_NONNULL_END
