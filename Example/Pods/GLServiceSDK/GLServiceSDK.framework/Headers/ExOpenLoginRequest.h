//
//  ExOpenLoginRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"
#import "ExLoginResponse.h"

@interface ExOpenLoginRequest : ExRequest

/**
 用户登录(三方登录)
 
 @param accessToken 第三方系统的访问令牌(可选)
 @param openId      第三方系统的开放ID
 @param authCode    第三方授权code，通过authCode和openid，可获取第三方的accessToken，从而兑换本系统的令牌(可选)
 */
- (instancetype)initWithAccessToken:(NSString *)accessToken openId:(NSString *)openId authCode:(NSString *)authCode;

@end

@interface ExOpenLoginResponse : ExLoginResponse

@end
