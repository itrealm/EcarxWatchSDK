//
//  ExPeccancyRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/1/15.
//

#import "ExRequest.h"

@interface ExPeccancyRequest : ExRequest

/**
 违章查询
 
 @param carNumber 车牌号：用URLEncode utf-8编码
 @param carCode   车架号
 @param carDrive  发动机号
 @param tel       电话号码
 */
- (instancetype)initWithCarNumber:(NSString *)carNumber
                          carCode:(NSString *)carCode
                         carDrive:(NSString *)carDrive
                              tel:(NSString *)tel;

@end
