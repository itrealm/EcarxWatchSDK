//
//  ExPeccancyResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/1/15.
//

#import "ExResponse.h"

@protocol ExPeccancyDetail, ExPeccancyList;

@interface ExPeccancyDetail : BaseModel

@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *reason;
@property (nonatomic, copy) NSString *fine;
@property (nonatomic,assign) BOOL     handled;
@property (nonatomic, copy) NSString *point;
@property (nonatomic, copy) NSString *violation_type;

@end

@interface ExPeccancyList : BaseModel

@property (nonatomic, copy) NSString *carNumber;
@property (nonatomic, copy) NSString *carCode;
@property (nonatomic, copy) NSString *carDrive;
@property (nonatomic, copy) NSArray<ExPeccancyDetail> *todo;
@property (nonatomic, copy) NSArray<ExPeccancyDetail> *done;

@end

@interface ExPeccancyResponse : ExResponse

@property (nonatomic, copy) ExPeccancyList *data;

@end
