//
//  ExPinResetRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/6/13.
//

#import "ExRequest.h"

@interface ExPinResetRequest : ExRequest

@property(nonatomic,copy)NSString *userId;
@property(nonatomic,copy)NSString *pin;

- (instancetype)initWithUser:(NSString *)userId pin:(NSString *)pin OBJC_DEPRECATED("1.0.0");

@end
