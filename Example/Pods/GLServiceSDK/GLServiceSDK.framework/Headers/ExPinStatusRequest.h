//
//  ExPinStatusRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/6/13.
//

#import "ExRequest.h"

@interface ExPinStatusRequest : ExRequest

@property(nonatomic,copy)NSString *userId;

- (instancetype)initWithUser:(NSString *)userId;

@end
