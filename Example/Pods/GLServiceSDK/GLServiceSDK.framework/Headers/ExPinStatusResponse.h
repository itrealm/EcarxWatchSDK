//
//  ExPinStatusResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/6/13.
//

#import "ExResponse.h"

@interface ExPinStatusData : BaseModel

//0：关闭，1：开启，2:不存在
@property (nonatomic,assign) NSInteger pinstatus;

@end

@interface ExPinStatusResponse : ExResponse

@property (nonatomic,copy)ExPinStatusData *data;

@property (nonatomic,assign,readonly) BOOL verifyResult;

@end
