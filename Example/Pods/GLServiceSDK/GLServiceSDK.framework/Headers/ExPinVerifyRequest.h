//
//  ExPinVerifyRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/6/13.
//

#import "ExRequest.h"

@interface ExPinVerifyRequest : ExRequest

@property(nonatomic,copy)NSString *userId;
@property(nonatomic,copy)NSString *pin;
@property(nonatomic,copy)NSString *password;

/**
 验证PIN

 @param userId userId
 @param pin    pin
 */
- (instancetype)initWithUser:(NSString *)userId pin:(NSString *)pin OBJC_DEPRECATED("1.0.0");

/**
 验证密码

 @param userId   userId
 @param password password
 */
- (instancetype)initWithUser:(NSString *)userId password:(NSString *)password OBJC_DEPRECATED("1.0.0");

/**
 验证PIN

 @param pin    pin
 */
- (instancetype)initWithPin:(NSString *)pin;
/**
 验证密码

 @param password password
 */
- (instancetype)initWithPassword:(NSString *)password;

@end
