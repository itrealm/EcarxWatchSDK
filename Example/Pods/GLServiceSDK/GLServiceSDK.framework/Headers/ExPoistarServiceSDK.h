//
//  ExPoistarServiceSDK.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2017/10/26.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#ifndef ExPoistarServiceSDK_h
#define ExPoistarServiceSDK_h

#import "ExCollection.h"
#import "ExCollectionAddAPI.h"
#import "ExCollectionDelAPI.h"
#import "ExCollectionListAPI.h"

#endif /* ExPoistarServiceSDK_h */
