//
//  ExQrLoginRequest.h
//  GLServiceSDK
//
//  Created by fish on 2017/11/7.
//

#import "ExRequest.h"

@interface ExQrLoginRequest : ExRequest

/**
 扫码登录

 @param client 目标设备的ID号
 @return HTTP请求
 */
- (instancetype)initWithTargetClient:(NSString *)client;

@end
