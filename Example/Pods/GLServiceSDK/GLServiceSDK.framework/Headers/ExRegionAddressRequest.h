//
//  ExRegionAddressRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "ExRequest.h"
#import "ExResponse.h"

@interface ExRegionAddressRequest : ExRequest

/**
 根据省市区编号获取省市区名称

 @param provinceId 省id
 @param cityId     市id
 @param districtId 区id
 */
- (instancetype)initWithProvince:(NSString *)provinceId city:(NSString *)cityId district:(NSString *)districtId;

@end


@interface ExRegionAddressResponse : ExResponse

@property (nonatomic,copy) NSString *provinceName;
@property (nonatomic,copy) NSString *cityName;
@property (nonatomic,copy) NSString *districtName;

@end
