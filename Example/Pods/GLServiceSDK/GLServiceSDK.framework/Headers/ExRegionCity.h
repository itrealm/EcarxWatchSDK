//
//  ExRegionCity.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "BaseModel.h"

@interface ExRegionCity : BaseModel

@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *zoneId;
@property (nonatomic, copy) NSString *countryId;
@property (nonatomic, copy) NSString *name;

@end
