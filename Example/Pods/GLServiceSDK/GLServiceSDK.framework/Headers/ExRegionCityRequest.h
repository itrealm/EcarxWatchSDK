//
//  ExRegionCityRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "ExRequest.h"
#import "ExResponse.h"
#import "ExRegionDistrict.h"

@interface ExRegionCityRequest : ExRequest

/**
 获取区县信息

 @param cityId cityId
 */
- (instancetype)initWithCity:(NSString *)cityId;

@end


@interface ExRegionCityResponse : ExResponse

@property (nonatomic,copy)NSArray<ExRegionDistrict> *data;

@end
