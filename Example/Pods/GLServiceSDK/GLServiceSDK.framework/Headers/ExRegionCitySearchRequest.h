//
//  ExRegionCitySearchRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "ExRequest.h"
#import "ExResponse.h"

@interface ExRegionCitySearchRequest : ExRequest

/**
 模糊查询城市名称

 @param cityName 关键字(城市名称)
 */
- (instancetype)initWithCityName:(NSString *)cityName;

@end

@interface ExRegionCitySearchResponse : ExResponse

@end
