//
//  ExRegionCountryRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "ExRequest.h"
#import "ExResponse.h"
#import "ExRegionZone.h"

@interface ExRegionCountryRequest : ExRequest

/**
 获取所有国家信息
 */
- (instancetype)init;

/**
 获取制定国家信息

 @param countryId 国家id
 */
- (instancetype)initWithId:(NSString *)countryId;

@end


@interface ExRegionCountryResponse : ExResponse

@property(nonatomic,copy) NSString *status;
@property(nonatomic,copy) NSString *countryId;
@property(nonatomic,copy) NSString *addressFormat;
@property(nonatomic,copy) NSString *isoCode2;
@property(nonatomic,copy) NSString *isoCode3;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *postcodeRequired;
@property(nonatomic,copy) NSArray<ExRegionZone> *zone;

@end
