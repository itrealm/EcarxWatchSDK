//
//  ExRegionZone.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "BaseModel.h"

@interface ExRegionZone : BaseModel

@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *countryId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *zoneId;

@end

@protocol ExRegionZone @end
