//
//  ExRegionZoneRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/8.
//

#import "ExRequest.h"
#import "ExResponse.h"
#import "ExRegionZone.h"

@interface ExRegionZoneRequest : ExRequest

/**
 获取省级信息

 @param zoneId zoneId
 */
- (instancetype)initWithZone:(NSString *)zoneId;

@end


@interface ExRegionZoneResponse : ExResponse

@property(nonatomic,copy) NSString *status;
@property(nonatomic,copy) NSString *zoneId;
@property(nonatomic,copy) NSString *name;
//@property(nonatomic,copy)

@end
