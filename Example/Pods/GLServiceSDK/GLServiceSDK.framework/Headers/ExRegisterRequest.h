//
//  ExRegisterRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"

@interface ExRegisterRequest : ExRequest

/**
 用户注册
 
 @param mobilePhone   手机号
 @param password 密码
 */
- (instancetype)initWithMobilePhone:(NSString *)mobilePhone password:(NSString *)password;

@end
