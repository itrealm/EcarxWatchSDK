//
//  ExRegisterResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExResponse.h"

@protocol ExRegister;

@interface ExRegister : BaseModel

//设备ID，对应头中的X-CLIENT-ID。用户使用当前设备注册时，除了会生成用户信息，还会将当前设备注册进入系统。如果是浏览器访问，该值为空
@property (nonatomic, copy) NSString *clientId;
@property (nonatomic, copy) NSString *uid;

@end

@interface ExRegisterResponse : ExResponse

@property (nonatomic, copy) ExRegister *data;

@end
