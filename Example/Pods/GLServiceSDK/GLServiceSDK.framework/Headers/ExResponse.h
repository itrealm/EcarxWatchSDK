//
//  ExResponse.h
//  Pods
//
//  Created by fish on 2017/10/7.
//
//

#import "BaseAPI.h"

@interface ExResponse : BaseResponse

@property (nonatomic,   copy) NSString *message;
@property (nonatomic, assign) NSInteger code;

@end
