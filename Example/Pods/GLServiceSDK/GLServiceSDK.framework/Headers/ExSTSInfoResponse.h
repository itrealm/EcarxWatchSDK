//
//  ExSTSInfoResponse.h
//  GLServiceSDK
//
//  Created by 杨沁 on 2018/4/3.
//

#import "ExResponse.h"

@protocol ExSTSInfoAssumedRoleUser @end

@interface ExSTSInfoAssumedRoleUser : BaseModel

@property (nonatomic, copy) NSString *AssumedRoleId;
@property (nonatomic, copy) NSString *Arn;

@end

@protocol ExSTSInfoCredentials @end

@interface ExSTSInfoCredentials : BaseModel

@property (nonatomic, copy) NSString *AccessKeySecret;
@property (nonatomic, copy) NSString *AccessKeyId;
@property (nonatomic, copy) NSString *Expiration;
@property (nonatomic, copy) NSString *SecurityToken;

@end

@protocol ExSTSInfoData @end

@interface ExSTSInfoData : BaseModel

@property (nonatomic, copy) NSString *RequestId;
@property (nonatomic, copy) ExSTSInfoAssumedRoleUser *AssumedRoleUser;
@property (nonatomic, copy) ExSTSInfoCredentials *Credentials;

@end

@interface ExSTSInfoResponse : ExResponse

@property (nonatomic, copy) ExSTSInfoData *data;

@end
