//
//  ExServiceHistoryResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "GLCspServiceSDK.h"

@interface ExServiceHistoryData : BaseModel

@property (nonatomic, assign) NSUInteger lastRow;
@property (nonatomic, assign) NSUInteger totalRows;
@property (nonatomic,   copy) NSArray<GLPurchaseHistory> *purchaseHistories;

@end

@interface ExServiceHistoryResponse : GLPurchaseHistoryResponse

@property (nonatomic,copy) ExServiceHistoryData *data;

@end
