//
//  ExServiceInUseResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "GLCspServiceSDK.h"
#import "ExResponse.h"

@interface ExServiceInUseData : BaseModel

@property (nonatomic, assign) NSUInteger lastRow;
@property (nonatomic, assign) NSUInteger totalRows;
@property (nonatomic,   copy) NSArray<GLStoreService> *inUseServices;

@end

@interface ExServiceInUseResponse : ExResponse

@property(nonatomic,copy)ExServiceInUseData *data;

@property (nonatomic,assign,readonly) NSUInteger lastRow;
@property (nonatomic,assign,readonly) NSUInteger totalRows;
@property (nonatomic,  copy,readonly) NSArray<GLStoreService> *inUseServices;

@end
