//
//  ExSetUserDealerRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/12.
//

#import "ExRequest.h"

@interface ExSetUserDealerRequest : ExRequest

- (instancetype)initWithDealerId:(NSString *)dealerId preferred:(BOOL)preferred remark:(NSString *)remark;

@end
