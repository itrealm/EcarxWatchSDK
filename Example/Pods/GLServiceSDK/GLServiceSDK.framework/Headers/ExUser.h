//
//  ExUser.h
//  GLServiceSDK
//
//  Created by ecarx on 2019/8/8.
//

#import "BaseAPI.h"

typedef NSString *ExUserBloodType NS_EXTENSIBLE_STRING_ENUM;

extern ExUserBloodType const ExUserBloodTypeA;
extern ExUserBloodType const ExUserBloodTypeAB;
extern ExUserBloodType const ExUserBloodTypeO;
extern ExUserBloodType const ExUserBloodTypeB;

@protocol ExUser;

@interface ExUser : BaseModel

//用户ID
@property (nonatomic, copy) NSString *userId;

//用户手机号码
@property (nonatomic, copy) NSString *mobilePhone;

//用户昵称
@property (nonatomic, copy) NSString *name;

//用户性别（男/女）
@property (nonatomic, copy) NSString *gender;

//头像链接
@property (nonatomic, copy) NSString *avatarUri;

//用户邮箱
@property (nonatomic, copy) NSString *email;

//用户出生日期,时间戳(毫秒)
@property (nonatomic, copy) NSString *birth;

//紧急联系人姓名
@property (nonatomic, copy) NSString *emergencyContactName;

//紧急联系人手机号码
@property (nonatomic, copy) NSString *emergencyContactPhone;

//血型,如：A,AB,O,B
@property (nonatomic, copy) ExUserBloodType bloodType;

//身高,单位cm,两位小数
@property (nonatomic, copy) NSString *height;
//体重,单位kg,两位小数
@property (nonatomic, copy) NSString *weight;

//用户地址
@property (nonatomic, copy) NSString *address;

//地址信息
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *province_id;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *city_id;
@property (nonatomic, copy) NSString *district;
@property (nonatomic, copy) NSString *district_id;

@end

