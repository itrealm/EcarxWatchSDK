//
//  ExUserDealerRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/12.
//

#import "ExRequest.h"

@interface ExUserDealerRequest : ExRequest

- (instancetype)initWithType:(NSString *)type;

@end
