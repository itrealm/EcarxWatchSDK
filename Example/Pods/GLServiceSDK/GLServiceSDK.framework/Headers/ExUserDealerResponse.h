//
//  ExUserDealerResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/12.
//

#import "ExResponse.h"
#import "ExDealer.h"

@interface ExUserDealerResponse : ExResponse

@property (nonatomic, copy) ExDealer *data;

@end
