//
//  ExUserIdentityRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"

@interface ExUserIdentityRequest : ExRequest

/**
 添加身份凭证
 
 @param identityType 身份认证类型，不包括【wechat】，包括但不局限于【mobile、email】
 */
- (id)initWithIdentityType:(NSString *)identityType;

/**
 添加身份凭证(身份认证类型，仅局限于【wechat】)
 
 @param accessToken  微信登录凭证Token
 @param openId       微信公众号的唯一标识
 @param authCode     微信授权code
 */
- (id)initWithAccessToken:(NSString *)accessToken openId:(NSString *)openId authCode:(NSString *)authCode;

@end
