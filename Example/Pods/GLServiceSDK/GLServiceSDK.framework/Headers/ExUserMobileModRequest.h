//
//  ExUserMobileModRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/3/1.
//

#import "ExRequest.h"

@interface ExUserMobileModRequest : ExRequest

- (id)initWithUser:(NSString *)userId mobile:(NSString *)mobile;

@end
