//
//  ExUserProfileModRequest.h
//  GLServiceSDK
//
//  Created by ecarx on 2019/8/8.
//

#import "ExRequest.h"
#import "ExUser.h"

@interface ExUserProfileModRequest : ExRequest

/**
 修改用户信息
 
 @param profile 用户信息
 */
- (instancetype)initWithUserProfile:(ExUser *)profile;

@end
