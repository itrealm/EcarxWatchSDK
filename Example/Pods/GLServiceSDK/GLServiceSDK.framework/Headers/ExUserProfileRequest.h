//
//  ExUserProfileRequest.h
//  GLServiceSDK
//
//  Created by ecarx on 2019/8/8.
//

#import "ExRequest.h"

@interface ExUserProfileRequest : ExRequest

/**
 查询用户信息
 
 @param userId 用户ID
 */
- (id)initWithUserId:(NSString *)userId;

@end
