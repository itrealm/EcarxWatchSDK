//
//  ExUserProfileResponse.h
//  GLServiceSDK
//
//  Created by ecarx on 2019/8/8.
//

#import "ExResponse.h"
#import "ExUser.h"

@interface ExUserProfileResponse : ExResponse

//用户详情信息
@property (nonatomic, copy) ExUser *data;

@end
