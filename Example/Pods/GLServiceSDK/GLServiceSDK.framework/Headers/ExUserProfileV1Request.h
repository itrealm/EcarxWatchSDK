//
//  ExUserProfileV1Request.h
//  GLServiceSDK
//
//  Created by fish on 2019/10/29.
//

#import "ExRequest.h"
#import "ExUserProfileResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExUserProfileV1Request : ExRequest

- (id)initWithUserId:(NSString *)userId;

@end

@interface ExUserProfileV1Response : ExUserProfileResponse

@end

NS_ASSUME_NONNULL_END
