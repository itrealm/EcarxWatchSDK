//
//  ExUserPwdChangeRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"

@interface ExUserPwdChangeRequest : ExRequest

/**
 修改用户密码
 
 @param password    旧密码
 @param newPassword 新密码
 */
- (id)initWithPassword:(NSString *)password newPassword:(NSString *)newPassword;

@end
