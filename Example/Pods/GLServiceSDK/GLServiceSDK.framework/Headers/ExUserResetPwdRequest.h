//
//  ExUserResetPwdRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"

@interface ExUserResetPwdRequest : ExRequest

/**
 重置用户密码
 
 @param newPassword 用户密码
 @param mobile      手机号
 */
- (id)initWithMobile:(NSString *)mobile newPassword:(NSString *)newPassword verificationcode:(NSString *)verificationcode;

@end
