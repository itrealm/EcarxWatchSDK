//
//  ExVehicle.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/20.
//

#import "BaseAPI.h"

@protocol ExVehicle;

@interface ExVehicle : BaseModel

//车辆的序列号
@property (nonatomic, copy) NSString *seriesName;
@property (nonatomic, copy) NSString *seriesCodeVs;
@property (nonatomic, copy) NSString *modelCode;   //车辆型号编码

@property (nonatomic, copy) NSString *vin;      //车辆的VIN码
@property (nonatomic, copy) NSString *matCode;  //车辆型号编码
@property (nonatomic, copy) NSString *colorCode;//车辆的颜色编码
@property (nonatomic, copy) NSString *colorName;

@property (nonatomic, copy) NSString *factoryCode;//车辆的生产工厂编码

@property (nonatomic, copy) NSString *engineNo;//车辆的引擎号码
@property (nonatomic, copy) NSString *iccid;   //车机SIM卡编号
@property (nonatomic, copy) NSString *imsi;
@property (nonatomic, copy) NSString *msisdn;//车机SIM卡的手机号
@property (nonatomic, copy) NSString *fccode;

@property (nonatomic, copy) NSString *recordTime;//车辆信息记录时间
@property (nonatomic, copy) NSString *updateTime;//车辆信息更新时间
@property (nonatomic, copy) NSString *createTime;//车辆信息创建时间

//车牌号码
@property (nonatomic, copy) NSString *plateNo;
@property (nonatomic, copy) NSString *vehiclePhotoBig;  //车辆照片（大）
@property (nonatomic, copy) NSString *vehiclePhotoSmall;//车辆照片（小）

//车辆型号
@property (nonatomic,assign) NSInteger vehicleType;
//油箱容量
@property (nonatomic, copy) NSString *fuelTankCapacity;

/** 车机SIM卡是否激活，
 0：未认证、
 1：已认证、
 2：审核中、
 3：重新认证 允许值: "0", "1", "2", "3"
 **/
@property (nonatomic, copy) NSString *simActivited;

@property (nonatomic, copy) NSString *isIHUConfirm;//是否IHU确认
@property (nonatomic, copy) NSString *ihuId;	   //车辆的IHU标识

@property (nonatomic, copy) NSString *temId;
@property (nonatomic, copy) NSString *temType;

//默认关联车联
@property (nonatomic,assign) BOOL defaultVehicle;
@property (nonatomic, copy) NSString *tboxPlatform;
@property (nonatomic, copy) NSString *ihuPlatform;

//认证
- (BOOL)isAuthenticated;

#pragma mark - 用于整合版本
//CSP平台账号状态 0：未注册、1：已注册，没密码、2：已注册，有密码
@property (nonatomic, copy) NSString *cspAccountState;
//车辆所属平台 csp、tsp
@property (nonatomic, copy) NSString *carFromSoure;

@end
