//
//  ExVehicleCapabilityRequest.h
//  GLServiceSDK
//
//  Created by fish on 2017/11/16.
//

#import "ExRequest.h"

@interface ExVehicleCapabilityRequest : ExRequest

@property (nonatomic,   copy) NSString *vin;
@property (nonatomic, assign) NSInteger vehicleType;

/**
 查询车辆能力集

 @param vin vin
 @param vehicleType 车辆类型
 */
- (instancetype)initWithVin:(NSString *)vin vehicleType:(NSInteger)vehicleType;

@end
