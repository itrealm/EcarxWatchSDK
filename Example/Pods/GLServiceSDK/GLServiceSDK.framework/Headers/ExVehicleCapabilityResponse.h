//
//  ExVehicleCapabilityResponse.h
//  GLServiceSDK
//
//  Created by fish on 2017/11/20.
//

#import "ExResponse.h"
#import "GLCspServiceSDK.h"

@interface ExVehicleCapabilityData : BaseModel

@property(nonatomic,copy) NSArray<GLVehicleCapability> *list;

@end

@interface ExVehicleCapabilityResponse : ExResponse

@property (nonatomic,copy)ExVehicleCapabilityData *data;

//扩展CSP
@property (nonatomic,weak)NSArray<GLVehicleCapability> *list;

@end
