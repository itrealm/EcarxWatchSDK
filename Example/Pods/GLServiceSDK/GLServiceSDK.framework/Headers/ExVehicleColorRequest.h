//
//  ExVehicleColorRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/5/26.
//

#import "ExRequest.h"

@interface ExVehicleColorRequest : ExRequest

- (instancetype)initWithColorCode:(NSString *)colorCode;

@end
