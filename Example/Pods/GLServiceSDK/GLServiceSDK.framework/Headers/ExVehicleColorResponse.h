//
//  ExVehicleColorResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/5/26.
//

#import "ExResponse.h"
#import "GLColor.h"

@interface ExVehicleColorData : BaseModel

@property (nonatomic, copy) NSString *source;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSNumber *gmtCreate;
@property (nonatomic, copy) NSNumber *id;
@property (nonatomic, copy) NSString *colorCode;
@property (nonatomic, copy) NSString *vehicleType;
@property (nonatomic, copy) NSNumber *gmtModified;
@property (nonatomic, copy) NSString *colorName;

@end

@interface ExVehicleColorResponse : ExResponse

@property (nonatomic, copy) ExVehicleColorData *data;

@end
