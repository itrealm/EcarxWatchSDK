//
//  ExVehicleControlWaitTimeResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "GLCspServiceSDK.h"
#import "ExResponse.h"

@interface ExVehicleControlWaitTimeData : BaseModel

@property (nonatomic, copy) NSArray<GLVehicleControlWaitTime> *list;
@property (nonatomic, copy) NSString *platform;

@end

@interface ExVehicleControlWaitTimeResponse : ExResponse

@property (nonatomic, copy) ExVehicleControlWaitTimeData *data;

@property (nonatomic, copy) NSArray<GLVehicleControlWaitTime> *list;

@end
