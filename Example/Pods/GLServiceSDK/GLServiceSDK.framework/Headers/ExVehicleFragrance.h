//
//  ExVehicleFragrance.h
//  GLServiceSDK
//
//  Created by fish on 2019/7/15.
//

#import "BaseModel.h"

@interface ExVehicleFragrance : BaseModel

//id  java.lang.String 香氛盒ID 0-255
@property(nonatomic,assign)NSInteger id;
//type  java.lang.String 香氛种类
@property(nonatomic,copy)NSString *type;
//activated  java.lang.String 子香氛盒状态 0:off, 1:On
@property(nonatomic,assign) BOOL activated;
//level    java.util.String 香氛浓度 1-2-3
@property(nonatomic,copy)NSString *level;
//usingWarning java.util.String 香氛用完提醒 0:Off,1:0n
@property(nonatomic,assign) BOOL usingWarning;
//code  java.util.String 香氛代码号
@property(nonatomic,copy) NSString *code;

@end

@protocol ExVehicleFragrance @end
