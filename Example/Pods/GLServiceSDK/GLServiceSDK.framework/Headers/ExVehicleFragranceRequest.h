//
//  ExVehicleFragranceRequest.h
//  GLServiceSDK
//
//  Created by fish on 2019/7/15.
//

#import "ExRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExVehicleFragranceRequest : ExRequest

- (id)initWithVin:(NSString *)vin;

@end

NS_ASSUME_NONNULL_END
