//
//  ExVehicleFragranceResponse.h
//  GLServiceSDK
//
//  Created by fish on 2019/7/15.
//

#import "ExResponse.h"
#import "ExVehicleFragrance.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExVehicleFragranceData : BaseModel
//香氛状态 0-1    0:off, 1:On
@property(nonatomic,assign)BOOL activated;
//香氛通道数量 0-30
@property(nonatomic,assign)NSInteger number;
//香氛盒
@property(nonatomic,copy) NSArray<ExVehicleFragrance> *items;

@end

@interface ExVehicleFragranceResponse : ExResponse

@property(nonatomic,copy)ExVehicleFragranceData *data;

@end

NS_ASSUME_NONNULL_END
