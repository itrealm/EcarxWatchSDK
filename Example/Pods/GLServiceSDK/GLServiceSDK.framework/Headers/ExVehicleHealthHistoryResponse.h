//
//  ExVehicleHealthHistoryResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "GLCspServiceSDK.h"
#import "ExResponse.h"

@interface ExVehicleHealthHistoryData : BaseModel

@property (nonatomic, copy) NSArray<GLVehicleHealthHistory> *list;

@end

@interface ExVehicleHealthHistoryResponse : ExResponse

@property (nonatomic,copy)ExVehicleHealthHistoryData *data;

@property (nonatomic,copy,readonly) NSArray<GLVehicleHealthHistory> *list;

@end
