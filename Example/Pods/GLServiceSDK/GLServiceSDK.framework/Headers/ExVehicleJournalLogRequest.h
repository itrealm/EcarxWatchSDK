//
//  ExVehicleJournalLogRequest.h
//  GLServiceSDK
//
//  Created by fish on 2017/11/16.
//

#import "ExRequest.h"

@interface ExVehicleJournalLogRequest : ExRequest

@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) NSUInteger pageIndex;

- (id)initWithUserId:(NSString *)userId
				 vin:(NSString *)vin
		   startTime:(NSDate *)startTime
			 endTime:(NSDate *)endTime;

@end
