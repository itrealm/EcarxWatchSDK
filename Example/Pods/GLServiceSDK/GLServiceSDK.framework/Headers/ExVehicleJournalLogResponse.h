//
//  ExVehicleJournalLogResponse.h
//  GLVehicleModule
//
//  Created by fish on 2017/11/29.
//

#import "ExResponse.h"
#import "GLCspServiceSDK.h"

@interface ExVehicleJournalLogData : GLVehicleJournalLogResponse

@end

@interface ExVehicleJournalLogResponse : ExResponse

@property (nonatomic,copy)ExVehicleJournalLogData *data;

@property (nonatomic,copy)NSArray<GLJournalLog> *list;

@end
