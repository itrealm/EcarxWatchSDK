//
//  ExVehicleJournalLogSwitchRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/5/25.
//

#import "ExVehicleTelematicsRequest.h"

@interface ExVehicleJournalLogSwitchRequest : ExVehicleTelematicsRequest

/**
 切换行车日志开关

 @param vin 车架号
 @param enabled YES:打开 NO:关闭
 */
- (id)initWithVin:(NSString *)vin enabled:(BOOL)enabled;

@end
