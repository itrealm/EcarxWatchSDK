//
//  ExVehicleListByMobileRequest.h
//  GLServiceSDK
//
//  Created by fish on 2017/11/16.
//

#import "ExRequest.h"

@interface ExVehicleListByMobileRequest : ExRequest

@property (nonatomic,copy)NSString *mobile;

- (instancetype)initWithMobile:(NSString *)mobile;

@end
