//
//  ExVehicleListRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/20.
//

#import "ExRequest.h"

@interface ExVehicleListRequest : ExRequest

/**
 查询我的车辆列表
 
 @param userId 用户ID
 @param type   [all]
 */
- (id)initWithUserId:(NSString *)userId type:(NSString *)type OBJC_DEPRECATED("");

/**
 查询我的车辆列表
 
 @param userId 用户ID
 */
- (id)initWithUser:(NSString *)userId;

@end
