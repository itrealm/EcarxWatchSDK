//
//  ExVehicleListResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/20.
//

#import "ExResponse.h"
#import "ExVehicle.h"

@interface ExVehicleList : BaseModel

//绑定车辆信息列表
@property (nonatomic, copy) NSArray<ExVehicle> *list;

@end

@interface ExVehicleListResponse : ExResponse

@property (nonatomic, copy) ExVehicleList *data;

- (NSArray *)list;

@end
