//
//  ExVehicleListV1Request.h
//  GLServiceSDK
//
//  Created by fish on 2018/9/20.
//

#import "ExRequest.h"

@interface ExVehicleListV1Request : ExRequest

/**
 查询我的车辆列表
 
 @param userId 用户ID
 @param type   ["all"]
 */
- (id)initWithUser:(NSString *)userId type:(NSString *)type;

/**
 查询指定vin车辆

 @param vin vin
 */
- (id)initWithVin:(NSString *)vin;

@end
