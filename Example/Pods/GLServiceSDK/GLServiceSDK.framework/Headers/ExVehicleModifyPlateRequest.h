//
//  ExVehicleModifyPlateRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/17.
//

#import "ExRequest.h"

@interface ExVehicleModifyPlateRequest : ExRequest

- (instancetype)initWithVin:(NSString *)vin
				vehicleType:(NSNumber *)type
					plateNo:(NSString *)plate;

@end

