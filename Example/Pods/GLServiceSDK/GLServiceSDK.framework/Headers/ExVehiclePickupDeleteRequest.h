//
//  ExVehiclePickupDeleteRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/2/26.
//

#import "ExRequest.h"

@interface ExVehiclePickupDeleteRequest : ExRequest

/**
 接送好友 - 删除请求
 
 @param invatationId 邀请id
 @return       请求对象
 */
- (instancetype)initWithInvatationId:(NSString *)invatationId;

@end
