//
//  ExVehiclePickupRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/2/26.
//

#import "ExRequest.h"

@interface ExVehiclePickupRequest : ExRequest

/**
 接送朋友
 
 @param vin       vin
 @param alias     JPUSH
 @param shareType platform
 */
- (instancetype)initWithVin:(NSString *)vin alias:(NSString *)alias shareType:(NSString *)shareType;

@end
