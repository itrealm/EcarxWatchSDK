//
//  ExVehiclePickupResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/2/26.
//

#import "ExResponse.h"

@interface ExVehiclePickupData : BaseModel

@property (nonatomic, copy) NSString *invatationId;
@property (nonatomic, copy) NSString *uid;
@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *alias;
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, copy) NSString *link;
@property (nonatomic, copy) NSString *shareType;

@end

@interface ExVehiclePickupResponse : ExResponse

@property (nonatomic, copy) ExVehiclePickupData *data;

@end
