//
//  ExVehiclePickupStatusRequest.h
//  GLServiceSDK
//
//  Created by 杨沁 on 2018/11/26.
//

#import "ExRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExVehiclePickupStatusRequest : ExRequest

/**
 接送好友 - 请求查询
 
 @param invatationId 邀请id
 @return       请求对象
 */
- (instancetype)initWithInvatationId:(NSString *)invatationId;

@end

NS_ASSUME_NONNULL_END
