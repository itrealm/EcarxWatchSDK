//
//  ExVehiclePickupStatusResponse.h
//  GLServiceSDK
//
//  Created by 杨沁 on 2018/11/26.
//

#import "ExResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExVehiclePickupStatusData : BaseModel

@property (nonatomic, copy) NSString *invatationId; //邀请id
@property (nonatomic, copy) NSNumber *leftTime; //剩余时间
@property (nonatomic, copy) NSString *longitude;    //经度
@property (nonatomic, copy) NSString *latitude; //纬度
@property (nonatomic, copy) NSNumber *status;   //0 未处理 1 同意 -1 拒绝
@property (nonatomic, copy) NSString *name; //位置名称
@property (nonatomic, copy) NSString *address;  //详细地址

@end

@interface ExVehiclePickupStatusResponse : ExResponse

@property (nonatomic, copy) ExVehiclePickupStatusData *data;

@end

NS_ASSUME_NONNULL_END
