//
//  ExVehicleRanking.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/13.
//

#import "BaseAPI.h"

@protocol ExVehicleRanking @end

@interface ExVehicleRanking : BaseModel

@property (nonatomic,  copy) NSString *vin;
@property (nonatomic,  copy) NSString *modelCode;
@property (nonatomic,assign) NSInteger orderNumber;
@property (nonatomic,assign) NSInteger sysTimestamp;
@property (nonatomic,assign) double    odometer;
@property (nonatomic,assign) double    aveFuelConsumption;
@property (nonatomic,  copy) NSString *userId;
@property (nonatomic,  copy) NSString *mobile;

@end
