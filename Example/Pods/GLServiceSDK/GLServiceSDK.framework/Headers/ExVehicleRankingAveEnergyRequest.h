//
//  ExVehicleRankingAveEnergyRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/9/1.
//

#import "ExRequest.h"
#import "ExVehicleRankingAveFuelResponse.h"

@interface ExVehicleRankingAveEnergyRequest : ExRequest

//全国
- (instancetype)initWithModelCode:(NSString *)modelCode
                              vin:(NSString *)vin;

//附近
- (instancetype)initWithModelCode:(NSString *)modelCode
                              vin:(NSString *)vin
                         latitude:(double)latitude
                        longitude:(double)longitude
                           radius:(double)radius
                             topn:(NSInteger)topn;

@end

@interface ExVehicleRankingAveEnergyResponse : ExVehicleRankingAveFuelResponse
@end
