//
//  ExVehicleRankingAveFuelRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/13.
//

#import "ExRequest.h"

@interface ExVehicleRankingAveFuelRequest : ExRequest

@property (nonatomic,copy) NSString *type;    //0:能耗 1:油耗

/**
 获取全国或附近平均油耗或电耗排名

 @param seriesCode 车系
 @param vin        车架号
 @param latitude   纬度   ,全国时0
 @param longitude  经度   ,全国时0
 @param radius     半径   ,全国时0
 @param topn       排名数 ,默认100
 */
- (instancetype)initWithSeriesCode:(NSString *)seriesCode
                               vin:(NSString *)vin
                          latitude:(double)latitude
                         longitude:(double)longitude
                            radius:(double)radius
                              topn:(NSInteger)topn;
/**
 获取全国或附近平均油耗或电耗排名(后续版本废弃)
 
 @param modelCode  车系
 @param vin        车架号
 @param latitude   纬度   ,全国时0
 @param longitude  经度   ,全国时0
 @param radius     半径   ,全国时0
 @param topn       排名数 ,默认100
 */
- (instancetype)initWithModelCode:(NSString *)modelCode
                              vin:(NSString *)vin
                         latitude:(double)latitude
                        longitude:(double)longitude
                           radius:(double)radius
                             topn:(NSInteger)topn;

@end
