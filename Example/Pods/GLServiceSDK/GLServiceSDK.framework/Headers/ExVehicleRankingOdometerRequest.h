//
//  ExVehicleRankingOdometerRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/13.
//

#import "ExRequest.h"

@interface ExVehicleRankingOdometerRequest : ExRequest

- (instancetype)initWithModelCode:(NSString *)modelCode
                              vin:(NSString *)vin
                         latitude:(double)latitude
                        longitude:(double)longitude
                           radius:(double)radius
                             topn:(NSInteger)topn;

@end
