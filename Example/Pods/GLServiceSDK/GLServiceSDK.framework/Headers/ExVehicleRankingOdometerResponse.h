//
//  ExVehicleRankingOdometerResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/3/13.
//

#import "ExResponse.h"
#import "ExVehicleRanking.h"

@interface ExVehicleRankingOdometerData : BaseModel

@property (nonatomic, copy) ExVehicleRanking          *myRanking;
@property (nonatomic, copy) NSArray<ExVehicleRanking> *topRanking;

@end

@interface ExVehicleRankingOdometerResponse : ExResponse

@property (nonatomic, copy) ExVehicleRankingOdometerData *data;

@property (nonatomic, copy) NSArray<ExVehicleRanking> *topRanking;
@property (nonatomic, copy) ExVehicleRanking          *myRanking;

@end
