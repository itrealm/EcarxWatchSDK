//
//  ExVehicleSendToCarRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/1/5.
//

#import "ExRequest.h"

@interface ExVehicleSendToCarRequest : ExRequest

/**
 发送到车

 @param vin vin
 @param latitude  纬度
 @param longitude 经度
 @param title     POI名称
 */
- (instancetype)initWithVin:(NSString *)vin
				   latitude:(double)latitude
				  longitude:(double)longitude
					  title:(NSString *)title OBJC_DEPRECATED("1.0.0");

/**
 发送到车

 @param vin vin
 @param latitude  纬度
 @param longitude 经度
 @param title     POI名称
 @param address   地址
 */
- (instancetype)initWithVin:(NSString *)vin
				   latitude:(double)latitude
				  longitude:(double)longitude
					  title:(NSString *)title
					address:(NSString *)address;

@end
