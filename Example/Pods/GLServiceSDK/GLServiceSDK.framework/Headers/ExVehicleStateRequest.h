//
//  ExVehicleStateRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/3/30.
//

#import "ExRequest.h"

@interface ExVehicleStateRequest : ExRequest

@property(nonatomic,copy,readonly) NSString *vin;

/**
 初始化

 @param userId 用户ID
 @param vin 车架号
 @return 请求对象
 */
- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin;

@end
