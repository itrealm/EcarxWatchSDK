//
//  ExVehicleStateResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/3/30.
//

#import "ExResponse.h"
#import "GLCspServiceSDK.h"

@interface ExVehicleStateData : GLVehicleState

@end

@interface ExVehicleStateResponse : ExResponse

@property (nonatomic, copy) ExVehicleStateData *data;

#pragma mark - Reformer
@property (nonatomic, copy) NSArray<GLVehicleState> *list;

@end
