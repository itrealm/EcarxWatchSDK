//
//  ExVehicleStatusHistoryResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "GLCspServiceSDK.h"

@interface ExVehicleStatusHistoryData : BaseModel

@property (nonatomic, copy) NSArray<GLVehicleStatus> *list;

@end

@interface ExVehicleStatusHistoryResponse : GLVehicleStatusHistoryResponse

@property (nonatomic, copy) ExVehicleStatusHistoryData *data;

@end
