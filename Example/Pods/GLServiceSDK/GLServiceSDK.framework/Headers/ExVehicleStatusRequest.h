//
//  ExVehicleStatusRequest.h
//  GLServiceSDK
//
//  Created by fish on 2017/11/16.
//

#import "ExRequest.h"

@interface ExVehicleStatusRequest : ExRequest

@property (nonatomic,copy) NSString *userId;
@property (nonatomic,copy) NSString *vin;
@property (nonatomic,copy) NSString *target;
@property (nonatomic,assign) BOOL latest;

/**
 查询车辆状态

 @param userId 用户ID
 @param vin    vin
 */
- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin OBJC_DEPRECATED("1.0.0");

/**
 查询车辆状态

 @param userId 用户ID
 @param vin    vin
 @param latest YES:先从TSC获取,如超过10秒,到TEM获取 NO:从TSC获取
 @return 请求
 */
- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin latest:(BOOL)latest;

/**
 查询车辆状态

 @param userId 用户ID
 @param vin    vin
 @param target basic,more
 @param latest YES:先从TSC获取,如超过10秒,到TEM获取 NO:从TSC获取
 */
- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin target:(NSString *)target latest:(BOOL)latest OBJC_DEPRECATED("1.0.6 Please use initWithUserId:vin:latest");

@end
