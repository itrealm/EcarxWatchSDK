//
//  ExVehicleStatusResponse.h
//  GLServiceSDK
//
//  Created by fish on 2017/11/20.
//

#import "ExResponse.h"
#import "GLCspServiceSDK.h"

@interface ExVehicleStatusData : BaseModel

@property (nonatomic, copy) GLTelematicsResult *result;
@property (nonatomic, copy) GLVehicleStatus    *vehicleStatus;

@end

@interface ExVehicleStatusResponse : ExResponse

@property (nonatomic,copy)ExVehicleStatusData *data;

//扩展CSP
@property (nonatomic,weak)GLTelematicsResult *result;
@property (nonatomic,weak)GLVehicleStatus    *vehicleStatus;

@end
