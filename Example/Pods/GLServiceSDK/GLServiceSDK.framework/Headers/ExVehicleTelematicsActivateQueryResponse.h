//
//  ExVehicleTelematicsActivateQueryResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "GLCspServiceSDK.h"

@interface ExVehicleTelematicsActivateQueryData : BaseModel

@property (nonatomic, assign) BOOL activate;

@end

@interface ExVehicleTelematicsActivateQueryResponse : GLVehicleTelematicsActivateQueryResponse

@property (nonatomic, copy) ExVehicleTelematicsActivateQueryData *data;

@end
