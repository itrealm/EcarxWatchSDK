//
//  ExVehicleTelematicsActivateResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "ExResponse.h"

@interface ExVehicleTelematicsActivateData : BaseModel

@property(nonatomic,  copy) NSString *platform;
@property(nonatomic,strong) NSNumber *operationResult;

@end

@interface ExVehicleTelematicsActivateResponse : ExResponse

@property(nonatomic,copy)ExVehicleTelematicsActivateData *data;

@property(nonatomic,strong,readonly) NSNumber *operationResult;

@end
