//
//  ExVehicleTelematicsRequest.h
//  GLServiceSDK
//
//  Created by fish on 2017/11/16.
//

#import "ExRequest.h"
#import "GLCspServiceSDK.h"

@interface ExVehicleTelematicsRequest : ExRequest

@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *serviceId;
@property (nonatomic, copy) NSString *command;
@property (nonatomic, copy) NSString *pin;
@property (nonatomic, copy) GLOperationSchedule *operationScheduling;
@property (nonatomic, copy) NSArray<GLServiceParameters> *serviceParameters;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *sessionId;
@property (nonatomic,assign) BOOL latest;

- (instancetype)initWithVin:(NSString *)vin serviceId:(NSString *)serviceId OBJC_DEPRECATED("1.0.0");

/**
 远程车控

 @param vin        车架号
 @param serviceId  命令ID
 @param command    命令
 @param schedule   预约参数
 @param parameters 配置参数
 */
- (instancetype)initWithVin:(NSString *)vin
				  serviceId:(NSString *)serviceId
					command:(NSString *)command
				   schedule:(GLOperationSchedule *)schedule
				 parameters:(NSArray<GLServiceParameters> *)parameters;

@end
