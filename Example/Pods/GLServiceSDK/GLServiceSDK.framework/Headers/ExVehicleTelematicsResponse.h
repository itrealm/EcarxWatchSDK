//
//  ExVehicleTelematicsResponse.h
//  GLServiceSDK
//
//  Created by fish on 2017/11/21.
//

#import "ExResponse.h"
#import "GLCspServiceSDK.h"

@interface ExVehicleTelematicsData : BaseModel

@property (nonatomic, copy) GLServiceResult *serviceResult;
@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *serviceId;
@property (nonatomic, copy) NSString *sessionId;
@property (nonatomic, copy) NSString *taskId;
@property (nonatomic, copy) NSString *diagnostics;
@property (nonatomic, copy) NSString *trackPoint;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *pollutionStatus;
@property (nonatomic, copy) NSString *group;
@property (nonatomic, copy) NSString *chargingStatus;
@property (nonatomic, copy) NSString *platform;

@end

@interface ExVehicleTelematicsResponse : ExResponse

@property(nonatomic,copy)ExVehicleTelematicsData *data;

#pragma mark -
@property (nonatomic, weak) NSString *vin;
@property (nonatomic, weak) NSString *serviceId;
@property (nonatomic, weak) NSString *sessionId;

@end
