//
//  ExVehivleOperationRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/20.
//

#import "ExRequest.h"

@interface ExVehivleOperationRequest : ExRequest

/**
 解绑车辆
 
 @param userId 用户ID
 */
- (instancetype)initUnBindWithUserId:(NSString *)userId vin:(NSString *)vin;

/**
 绑定车辆-申请
 
 @param userId        用户ID
 @param customerPhone 车主手机号码
 @param vin           待绑定车辆的VIN号（后8位）
 */
- (instancetype)initBindWithUserId:(NSString *)userId customerPhone:(NSString *)customerPhone vin:(NSString *)vin;

/**
 绑定车辆-确认
 
 @param userId        用户ID
 @param customerPhone 车主手机号码
 @param vin           待绑定车辆的VIN号（后8位）
 */
- (instancetype)initConfirmWithUserId:(NSString *)userId customerPhone:(NSString *)customerPhone vin:(NSString *)vin;

@end
