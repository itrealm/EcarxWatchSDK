//
//  ExVehivleOperationResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/20.
//

#import "ExResponse.h"

@interface ExVehivleOperationResponse : ExResponse

@property (nonatomic,strong) NSNumber *operationResult;

@end
