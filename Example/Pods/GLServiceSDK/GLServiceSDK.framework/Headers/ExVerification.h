//
//  ExVerification.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "BaseAPI.h"

typedef NSString *ExCaptchaPurposeName NS_EXTENSIBLE_STRING_ENUM;

extern ExCaptchaPurposeName const ExCodePurposeReg;			  //注册
extern ExCaptchaPurposeName const ExCodePurposeIdentity;
extern ExCaptchaPurposeName const ExCodePurposeResetPwd;	  //重置密码
extern ExCaptchaPurposeName const ExCodePurposeValidateDevice;
extern ExCaptchaPurposeName const ExCodePurposeBindVehicle;	  //绑定设备
extern ExCaptchaPurposeName const ExCodePurposeValidateApp;
extern ExCaptchaPurposeName const ExCodePurposeModifyMobile;  //修改手机号

@protocol ExVerification;

@interface ExVerification : BaseModel

//校验码
@property (nonatomic, copy) NSString *value;
//是否已经验证通过
@property (nonatomic, assign) BOOL verified;

@end
