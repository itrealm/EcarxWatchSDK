//
//  ExVerificationCheckRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"
#import "ExVerification.h"

@interface ExVerificationCheckRequest : ExRequest

/**
 验证短信验证码
 
 @param mobile           用户ID
 @param verificationcode 验证码
 @param purpose          验证码用途
 */
- (instancetype)initWithMobile:(NSString *)mobile
			  verificationcode:(NSString *)verificationcode
					   purpose:(NSString *)purpose;

@end
