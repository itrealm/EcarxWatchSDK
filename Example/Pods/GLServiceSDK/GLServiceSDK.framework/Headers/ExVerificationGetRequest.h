//
//  ExVerificationGetRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExRequest.h"
#import "ExVerification.h"

@interface ExVerificationGetRequest : ExRequest

/**
 通过手机号获取验证码
 
 @param mobile  手机号
 @param purpose 验证码申请用途
*/
- (instancetype)initWithMobile:(NSString *)mobile purpose:(NSString *)purpose;

@end
