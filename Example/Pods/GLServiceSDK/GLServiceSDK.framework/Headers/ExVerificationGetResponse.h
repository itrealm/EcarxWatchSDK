//
//  ExVerificationGetResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/17.
//

#import "ExResponse.h"
#import "ExVerification.h"

@protocol ExVerificationGet;

@interface ExVerificationGet : BaseModel

//校验码信息，只有在非生产环境下，才会返回
@property (nonatomic, copy) ExVerification *verification;

@end

@interface ExVerificationGetResponse : ExResponse

//校验码信息，只有在非生产环境下，才会返回
@property (nonatomic, copy) ExVerificationGet *data;

@end
