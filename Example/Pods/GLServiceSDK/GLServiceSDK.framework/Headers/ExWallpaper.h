//
//  ExWallpaper.h
//  GLServiceSDK
//
//  Created by 杨沁 on 2018/4/3.
//

#import "BaseAPI.h"

@protocol ExWallpaper @end;

@interface ExWallpaper : BaseModel

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSNumber *createTime;
@property (nonatomic, copy) NSNumber *updateTime;
@property (nonatomic, copy) NSString *filename;
@property (nonatomic, copy) NSNumber *size;
@property (nonatomic, copy) NSString *filetype;
@property (nonatomic, copy) NSString *uri;
@property (nonatomic, copy) NSString *status;   //"0"为正常,"1"为违规下架

@end
