//
//  ExWallpaperConfigRequest.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/8/15.
//

#import "ExRequest.h"

@interface ExWallpaperConfigRequest : ExRequest

- (instancetype)initWithModelCode:(NSString *)modelCode matCode:(NSString *)matCode vin:(NSString *)vin seriesCode:(NSString *)seriesCode;

@end
