//
//  ExWallpaperConfigResponse.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/8/15.
//

#import "ExResponse.h"

@protocol ExWallpaperConfigData @end

@interface ExWallpaperConfigData : BaseModel

@property (nonatomic, copy) NSString *modelCode;
@property (nonatomic, copy) NSString *configCode;
@property (nonatomic, copy) NSString *screenType;
@property (nonatomic, assign) BOOL enable;
@property (nonatomic, copy) NSString *previewUri;
@property (nonatomic, assign) NSInteger totalCapability;

@end

@interface ExWallpaperConfigResponse : ExResponse

@property (nonatomic, copy) ExWallpaperConfigData *data;

@end
