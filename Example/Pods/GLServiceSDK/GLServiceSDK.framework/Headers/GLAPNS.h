//
//  GLAPNS.h
//  Pods
//
//  Created by fish on 2017/4/17.
//
//

#import <Foundation/Foundation.h>
#import "GLPushNotification.h"

//操作结果
typedef NS_ENUM(NSUInteger, GLOperationResult) {
	GLOperationResultExecuteSuccess			= 0,  //命令执行成功
	GLOperationResultSubmitToCSP			= 1,  //命令发送CSP成功
	GLOperationResultExecuteFailure			= 2,  //命令执行失败
	GLOperationResultSendToTem				= 3,  //命令发送到TEM/TBOX
	GLOperationResultEngineRunning			= 4,  //引擎运行
	GLOperationResultEngineStop				= 5,  //引擎关闭
	GLOperationResultCarDiagnostics			= 6,  //车辆诊断状态更新
	GLOperationResultTrackingPoints			= 7,  //行车日志数据
	GLOperationResultActive					= 8,  //远程控制激活
	GLOperationResultLastmilenavi			= 9,  //最后一公里
	GLOperationResultCarOutOfGeoFence		= 10, //车辆超出电子围栏
	GLOperationResultFriendsAccept			= 11, //接好友接收
	GLOperationResultCarBind				= 12, //用户绑定车辆
	GLOperationResultImmobilizationCmdToCEM = 13, //汽车防盗命令到CEM
	GLOperationResultPM25Update             = 14, //PM2.5更新
	GLOperationResultUpdateStatusFromTSC    = 15, //车辆状态更新
	GLOperationResultChargingStatus         = 16, //预约充电状态
	GLOperationResultTemLogStop				= 17, //停止上传TEM日志
	GLOperationResultOtherDevice			= 18, //其它设备登录
	GLOperationResultAchievement			= 19, //完成成就
	GLOperationResultFriendRefuse			= 20, //接送好友拒绝

	GLOperationResultUpdateOTAHUVesrion = 1000,//OTA升级更新提示
    GLOperationResultUpdateOTAHUSuccess = 1001 //OTA升级完成
};

//控制命令
extern NSString * const kControlCmdSendSuccess;   //发送到TEM成功
extern NSString * const kControlCmdExecuteSuccess;//命令执行成功
extern NSString * const kControlCmdExecuteFailure;//命令执行失败

//车辆状态更新
extern NSString * const kVehicleStatusUpdate;
//PM2.5更新
extern NSString * const kPM25UpdatedNotification;

//异处登录
extern NSString * const kOtherClientLogin;
//成就达成通知
extern NSString * const kFinishAchievement;
//最后一公里
extern NSString * const kLastmilenaviArrived;

//接好友,好友接受
extern NSString * const kPickupFriendAccept;
//接好友,好友拒绝
extern NSString * const kPickupFriendRefuse;

//引擎状态开启与关闭
extern NSString * const kControlEngineStatueSwitch;

//行车日志开关
extern NSString * const kJouSwitchSendSuccess;   //发送成功
extern NSString * const kJouSwitchExecuteSuccess;//执行成功
extern NSString * const kJouSwitchExecuteFailure;//执行失败

//车辆诊断状态更新
extern NSString * const kCarDiagnosticsUpdate;

//泊车异常
extern NSString * const kWarningParkingAbnormal;

//所有推送
extern NSString * const kPushNotification;

extern NSString * const kOTAUpdateNewVersion;
extern NSString * const kOTAUpdateVersionSuccess;

@interface GLAPNS : NSObject

+ (GLPushNotification *)parse:(NSString *)content;

@end
