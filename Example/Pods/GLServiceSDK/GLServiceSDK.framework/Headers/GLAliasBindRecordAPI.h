//
//  GLAliasBindRecordAPI.h
//  Pods
//
//  Created by fish on 2017/5/19.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLAliasBindRecordRequest : GLRequest

@property (nonatomic,copy,readonly) NSString *userId;
@property (nonatomic,copy,readonly) NSString *alias;
@property (nonatomic,copy,readonly) NSString *registrationId;
@property (nonatomic,copy,readonly) NSString *platform;
@property (nonatomic,assign,readonly) NSInteger loginTime;

- (instancetype)initWithUserId:(NSString *)userId
						 alias:(NSString *)alias
				registrationId:(NSString *)registrationId
					  platform:(NSString *)platform
					 loginTime:(NSInteger)loginTime;

@end

@interface GLAliasBindRecordResponse : GLResponse

@end
