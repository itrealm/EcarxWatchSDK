//
//  GLCacheMessageRequest.h
//  GLServiceSDK
//
//  Created by fish on 2018/2/26.
//

#import "GLRequest.h"

@interface GLCacheMessageRequest : GLRequest

@property (nonatomic,copy,readonly)NSString *vin;
@property (nonatomic,copy,readonly)NSArray  *sessionIds;

- (instancetype)initWithVin:(NSString *)vin sessionIds:(NSArray *)sessionIds;

@end
