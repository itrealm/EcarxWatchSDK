//
//  GLCacheMessageResponse.h
//  GLServiceSDK
//
//  Created by fish on 2018/2/26.
//

#import "GLResponse.h"
#import "GLPushNotification.h"

@interface GLCacheTelematicsMessage : BaseModel

@property(nonatomic,copy)NSArray *alias;
@property(nonatomic,copy)GLPushNotification *telematicsResult;

@end

@interface GLCacheMessage : BaseModel

@property (nonatomic,copy)GLCacheTelematicsMessage *telematicsMessage;
@property (nonatomic,copy)NSNumber *createTime;

@end

@protocol GLCacheMessage @end;

@interface GLCacheMessageResponse : GLResponse

@property(nonatomic,copy)NSArray<GLCacheMessage> *list;

@end
