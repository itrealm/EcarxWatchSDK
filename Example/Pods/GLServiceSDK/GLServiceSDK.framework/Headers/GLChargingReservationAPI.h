//
//  GLChargingReservationAPI.h
//  GLServiceSDK
//
//  Created by fish on 2017/10/18.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLChargingReservationRequest : GLRequest

@property (nonatomic,  copy) NSString  *vin;
@property (nonatomic,assign) NSUInteger pageSize;
@property (nonatomic,assign) NSUInteger pageIndex;
@property (nonatomic,  copy) NSString  *sortField;
@property (nonatomic,  copy) NSString  *direction;

- (instancetype)initWithVin:(NSString *)vin;

@end

@interface GLChargingReservation : BaseModel

@property (nonatomic, copy)NSNumber *duration;//单位:分
@property (nonatomic, copy)NSString *eventId;
@property (nonatomic, copy)NSString *serviceCommand;
@property (nonatomic, copy)NSNumber *scheduledTime;
@property (nonatomic, copy)NSNumber *createTime;
@property (nonatomic, copy)NSString *vin;
@property (nonatomic, copy)NSString *id;

@end

@protocol GLChargingReservation @end

@interface GLChargingReservationResponse : GLResponse

@property(nonatomic,copy)NSArray <GLChargingReservation> *list;

@end
