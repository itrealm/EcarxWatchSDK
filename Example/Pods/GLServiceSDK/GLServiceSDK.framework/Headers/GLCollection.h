//
//  GLCollection.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "BaseModel.h"

@class GLMapInterestCollection;
@protocol GLCollection;

@interface GLCollection : BaseModel

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) GLMapInterestCollection *interestCollection;
@property (nonatomic, copy) NSNumber *createTime;
@property (nonatomic, copy) NSNumber *updateTime;

@end

@interface GLMapInterestCollection : BaseModel

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) double  lat;
@property (nonatomic, assign) double  lng;

@end
