//
//  GLCollectionAddAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@class GLMapInterestCollection;

@interface GLCollectionAddRequest : GLRequest

- (instancetype)initWithUserId:(NSString *)userId interestCollection:(GLMapInterestCollection *)collection;

@end

#pragma mark - Response

@interface GLCollectionAddResponse : GLResponse

@end
