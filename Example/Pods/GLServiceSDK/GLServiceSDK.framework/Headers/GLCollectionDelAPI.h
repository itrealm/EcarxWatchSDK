//
//  GLCollectionDelAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLCollectionDelRequest : GLRequest

- (instancetype)initWithUserId:(NSString *)userId mapCollectionId:(NSString *)mapCollectionId;

@end

@interface GLCollectionDelResponse : GLResponse

@end
