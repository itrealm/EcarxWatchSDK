//
//  GLCollectionListAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLCollection.h"

@interface GLCollectionListRequest : GLRequest

- (id)initWithUserId:(NSString *)userId;
@property (nonatomic,assign) NSUInteger pageSize;
@property (nonatomic,assign) NSUInteger pageIndex;

@end

@interface GLCollectionListResponse : GLResponse

@property (nonatomic, copy) NSArray<GLCollection> *list;

@end
