//
//  GLColor.h
//  Pods
//
//  Created by 杨沁 on 2017/9/15.
//
//

#import "BaseModel.h"

@protocol GLColor @end

@interface GLColor : BaseModel

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *value;

@end
