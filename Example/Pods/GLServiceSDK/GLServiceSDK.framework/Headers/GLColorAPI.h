//
//  GLColorAPI.h
//  Pods
//
//  Created by 杨沁 on 2017/9/15.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLColor.h"

@interface GLColorRequest : GLRequest

- (instancetype)initWithColorCode:(NSString *)colorCode;

@end

@interface GLColorResponse : GLResponse

@property (nonatomic, copy) NSArray<GLColor> *list;

@end
