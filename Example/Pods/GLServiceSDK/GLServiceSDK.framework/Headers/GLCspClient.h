//
//  GLCspClient.h
//  Pods
//
//  Created by ecarx on 2017/3/27.
//
//

#import "BaseAPI.h"
#import "GLServiceProtocol.h"

extern NSString * const kCspErrorDomain;

//用户语言
extern NSString * const kLanguageChinese;
extern NSString * const kLanguageEnglish;
extern NSString * const kLanguageMalay;

//平台
extern NSString * const kPlatformCMA;
extern NSString * const kPlatformNonCMA;

@class GLVehicle;

@interface GLCspClient : BaseAPI <GLServiceProtocol>

/**
 设置用户语言

 @param language kLanguage
 */
+ (void)setUserLanguage:(NSString *)language;

+ (instancetype)sharedClient;
+ (NSString *)userId;
+ (NSString *)accessToken;
+ (NSString *)tcToken;
+ (NSString *)idToken;
+ (NSString *)refreshToken;
+ (NSString *)expiresIn;
+ (NSString *)platform;
+ (NSString *)appId;
+ (NSString *)clientId;

+ (BOOL)isUserLogined;

@end
