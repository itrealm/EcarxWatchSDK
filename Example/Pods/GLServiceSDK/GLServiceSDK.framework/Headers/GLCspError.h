//
//  GLCspError.h
//  Pods
//
//  Created by fish on 2017/7/13.
//
//

typedef NS_ENUM(NSUInteger, GLCspError) {
	GLCspSuccess = 0,				 //成功
	GLCspErrorVehicleNotExist = 10006,//车辆不存在
	GLCspErrorVehicleUnbind   = 10031,//车辆被解绑
	GLCspErrorUserPwdError    = 30002,//用户或密码错误
	GLCspErrorSessionInvalid  = 30008,//Session失效
	GLCspErrorInvalidSession  = 30019 //Session失效
};
