//
//  GLCspFunction.h
//  GLServiceSDK
//
//  Created by ecarx on 2019/8/12.
//
//

#import <Foundation/Foundation.h>
#import "GLVehicleCapability.h"

typedef NSString *GLVehicleFunctionName NS_EXTENSIBLE_STRING_ENUM;

/************* 服务端配置定义 *************/
//闪光鸣笛
extern GLVehicleFunctionName const kFunctionHonkAndFlash;
//开启车窗
extern GLVehicleFunctionName const kFunctionWindowOpen;
//关闭车窗
extern GLVehicleFunctionName const kFunctionWindowClose;
//车窗一键透气
extern GLVehicleFunctionName const kFunctionOneKeyVentilate;
//开启后备箱
extern GLVehicleFunctionName const kFunctionTrunkOpen;
//解锁后备箱
extern GLVehicleFunctionName const kFunctionRemoteTrunkUnlock;
//开启/关闭引擎
extern GLVehicleFunctionName const kFunctionStartEngine;
//开启/关闭空调
extern GLVehicleFunctionName const kFunctionAirConditionSwitch;
//空调预调节(CMA)
extern GLVehicleFunctionName const kFunctionRemoteClimateControl;
//座椅预加热
extern GLVehicleFunctionName const kFunctionSeatPreheatSwitch;
//座椅透气
extern GLVehicleFunctionName const kFunctionSeatVentSwitch;
//开启/关闭PM2.5净化
extern GLVehicleFunctionName const kFunctionOpenPM25Purification;
//开启/关闭空气净化
extern GLVehicleFunctionName const kFunctionRemoteAirPurification;
//预约充电
extern GLVehicleFunctionName const kFunctionAppointmentCharging;
//车辆诊断
extern GLVehicleFunctionName const kFunctionDiagnostics;
//车门锁状态(中控锁),车门锁解锁/锁车
extern GLVehicleFunctionName const kFunctionCentraLockStatus;

/*************************************************/
//平均速度
extern GLVehicleFunctionName const kFunctionTheEverageSpeed;
//剩余油量
extern GLVehicleFunctionName const kFunctionFuelCapacity;
//油量可续航里程
extern GLVehicleFunctionName const kFunctionMileageLife;
//百公里油耗
extern GLVehicleFunctionName const kFunctionFuelConsumptionPerHundredKilometers;
//总里程
extern GLVehicleFunctionName const kFunctionTotalMileage;
//胎压
extern GLVehicleFunctionName const kFunctionTyrePressure;
//油压报警
extern GLVehicleFunctionName const kFunctionMotorOilAlarm;
//车门状态
extern GLVehicleFunctionName const kFunctionVehicleDoorsStatus;
//引擎盖
extern GLVehicleFunctionName const kFunctionEngineCompartmentCover;
//后备箱状态
extern GLVehicleFunctionName const kFunctionTrunkStatus;
//车门锁状态
extern GLVehicleFunctionName const kFunctionDoorLockSwitchStatus;
//车窗状态
extern GLVehicleFunctionName const kFunctionWindowsRollingStatus;
//天窗状态
extern GLVehicleFunctionName const kFunctionSunroofStatus;
//驻车标志
extern GLVehicleFunctionName const kFunctionParkingFlag;
//剩余保养里程
extern GLVehicleFunctionName const kFunctionDistanceService;
//车内PM2.5
extern GLVehicleFunctionName const kFunctionPM25Interior;
//车外PM2.5
extern GLVehicleFunctionName const kFunctionPM25Exterior;
//车内空气质量
extern GLVehicleFunctionName const kFunctionAirQualityLevel;

//电量可续航里程
extern GLVehicleFunctionName const kFunctionBatteryLife;
//平均百公里电耗
extern GLVehicleFunctionName const kFunctionEneryConsumptionPerHundredKm;
//充电状态
extern GLVehicleFunctionName const kFunctionChargingStatus;
//车内温度
extern GLVehicleFunctionName const kFunctionVehicleTemp;

/*************************************************/
//行车日志
extern GLVehicleFunctionName const kFunctionDrvingLog;
//单次行驶时间
extern GLVehicleFunctionName const kFunctionDrivingTimeSingleTime;
//单次行驶里程
extern GLVehicleFunctionName const kFunctionTripMileage;
//单程行驶平均速度
extern GLVehicleFunctionName const kFunctionAverageSpeedSingleTime;
//单程行驶油耗
extern GLVehicleFunctionName const kFunctionFuelConsumptionSingleTime;
//单程行驶电耗
extern GLVehicleFunctionName const kFunctionBetteryConsumptionSingleTime;

//行车日志开关切换
extern GLVehicleFunctionName const kFunctionJouSwitch;

/*************************************************/
//车辆位置显示
extern NSString* const kFunctionVehiclePosition;
//电子围栏
extern GLVehicleFunctionName const kFunctionGeographyFencing;
//扫码登录
extern GLVehicleFunctionName const kFunctionQrScan;
//发送到车方式
extern GLVehicleFunctionName const kFunctionSendToCar;
//发送到车功能
extern GLVehicleFunctionName const kFunctionSendToCarEnable;
//最后一公里导航
extern GLVehicleFunctionName const kFunctionLastKilometerNavigation;
//接送好友
extern GLVehicleFunctionName const kFunctionPickupFriends;
//远程控制激活转换
extern GLVehicleFunctionName const kFunctionRemoteControlActivateSwitch;
//PIN码
extern GLVehicleFunctionName const kFunctionPinSwitch;
//蓝牙钥匙
extern GLVehicleFunctionName const kFunctionBluetoothKey;

/************* 客户端本地定义 *************/
extern GLVehicleFunctionName const kFunctionSeatPreheatSwitchOpen;	  //关闭座椅加热
extern GLVehicleFunctionName const kFunctionSeatPreheatSwitchClose;	  //开启座椅加热
extern GLVehicleFunctionName const kFunctionAirConditionSwitchOpen;	  //空调开启
extern GLVehicleFunctionName const kFunctionAirConditionSwitchClose;  //空调关闭
extern GLVehicleFunctionName const kFunctionCentraLockStatusOpen;	  //中控开锁
extern GLVehicleFunctionName const kFunctionCentraLockStatusClose;    //中控锁车
extern GLVehicleFunctionName const kFunctionStartEngineOpen;	      //引擎开启
extern GLVehicleFunctionName const kFunctionStartEngineClose;	      //引擎关闭
extern GLVehicleFunctionName const kFunctionSunroofOpen;			  //天窗开启
extern GLVehicleFunctionName const kFunctionSunroofClose;			  //天窗关闭

extern GLVehicleFunctionName const kFunctionAirPurificationStart;	  //空气净化开启
extern GLVehicleFunctionName const kFunctionAirPurificationStop;	  //空气净化关闭

extern GLVehicleFunctionName const kFunctionOpenPM25PurificationStart;	//PM2.5净化开启
extern GLVehicleFunctionName const kFunctionOpenPM25PurificationStop;	//PM2.5净化关闭
extern GLVehicleFunctionName const kFunctionTrunkDoorOpen;			    //开启后备门(GE12)

@interface GLCspFunction : NSObject

//发送到车能力集
+ (BOOL)isEnabledSendToCar:(NSArray<GLVehicleCapability> *)capabilitys;

//是否启用对应能力
+ (BOOL)isEnabledFunction:(NSString *)functionId fromCapabilitys:(NSArray<GLVehicleCapability> *)capabilitys;
+ (GLVehicleCapability *)apabilityWithFunction:(NSString *)functionId
							   fromCapabilitys:(NSArray<GLVehicleCapability> *)capabilitys;

@end
