//
//  GLCspServiceParams.h
//  Pods
//
//  Created by fish on 2017/8/14.
//
//

#import <Foundation/Foundation.h>

//Service Id
extern NSString * const kServiceIdRCE;//NON-CMA远程空调
extern NSString * const kServiceIdRCC;//CMA远程空调
extern NSString * const kServiceIdRES;//发动机
extern NSString * const kServiceIdRPC;//PM2.5净化
extern NSString * const kServiceIdRPP;//PM2.5获取
extern NSString * const kServiceIdRSH;//座椅加热
extern NSString * const kServiceIdRSV;//座椅透气
extern NSString * const kServiceIdRVS;
extern NSString * const kServiceIdTRS;
extern NSString * const kServiceIdRVI;//车辆锁定
extern NSString * const kServiceIdJOU;//行车日志开关
extern NSString * const kServiceIdREC;//预约充电
extern NSString * const kServiceIdDIA;//车辆诊断
extern NSString * const kServiceIdRVL;//关闭后备箱
extern NSString * const kServiceIdRTU;//开启后备箱
extern NSString * const kServiceIdRHL;//闪灯鸣笛
extern NSString * const kServiceIdRDL;//解锁车门
extern NSString * const kServiceIdRDU;//锁车门
extern NSString * const kServiceIdRWS;//车窗

/** 车辆车窗和天窗 **/
extern NSString * const kRwsServiceParamAll;      //车窗和天窗
extern NSString * const kRwsServiceParamWindow;   //车窗
extern NSString * const kRwsServiceParamSunroof;  //天窗
extern NSString * const kRwsServiceParamVentilate;//一键透气

/** 闪灯鸣笛 **/
extern NSString * const kRhlHorn;           //鸣笛
extern NSString * const kRhlLightFlash;     //闪灯
extern NSString * const kRhlHornLightFlash; //闪灯鸣笛

extern NSString * const kCommandStart;
extern NSString * const kCommandStop;

@interface GLCspServiceParams : NSObject

@end
