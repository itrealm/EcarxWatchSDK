//
//  GLCspServiceSDK.h
//  Pods
//
//  Created by ecarx on 2017/3/20.
//
//

#ifndef GLCspServiceSDK_h
#define GLCspServiceSDK_h

//API
#import "GLCspError.h"
#import "GLCspClient.h"
#import "GLCspFunction.h"
#import "GLCspServiceParams.h"

//User API
#import "GLUserLoginAPI.h"
#import "GLUserLogoutAPI.h"
#import "GLUserProfileAPI.h"
#import "GLUserProfileModifyAPI.h"
#import "GLFileUploadAPI.h"
#import "GLUserPwdChangeAPI.h"
#import "GLUserPwdResetAPI.h"
#import "GLUserPwdVerifyAPI.h"
#import "GLUserRegisterAPI.h"
#import "GLUserMobileVerifyAPI.h"

#import "GLUserQrLoginAPI.h"

//绑定车辆
#import "GLUserBindCarAPI.h"
#import "GLUserBindMultiCarAPI.h"
#import "GLUserUnBindCarAPI.h"
#import "GLUserConfirmCarAPI.h"

//PIN码
#import "GLUserPinVerifyAPI.h"
#import "GLUserPinResetAPI.h"
#import "GLUserPinStatusAPI.h"


//UserSession API
#import "GLSessionUpdateAPI.h"

//Alias
#import "GLAliasBindRecordAPI.h"

//Vehicle API
#import "GLVehicleListAPI.h"
#import "GLVehicleModifyPlantNoAPI.h"
#import "GLColorAPI.h"

//接好友
#import "GLVehiclePickupAPI.h"
#import "GLVehiclePickupUpdateAPI.h"

//能力集
#import "GLVehicleCapabilityAPI.h"

//车辆状态
#import "GLVehicleStatusAPI.h"
#import "GLVehicleStatusHistoryAPI.h"

#import "GLVehicleStateAPI.h"
//车控
#import "GLVehicleTelematicsAPI.h"
#import "GLVehicleTelematicsActivateAPI.h"
#import "GLVehicleTelematicsActivateQueryAPI.h"
#import "GLVehicleControlWaitTimeAPI.h"
//行车日志
#import "GLVehicleJournalLogAPI.h"
#import "GLVehicleJournalLogSwitchAPI.h"
//PM2.5历史
#import "GLVehiclePM25API.h"

//车辆检测
#import "GLHealthDTCHistoryAPI.h"

//排名
#import "GLVehicleRankingOdometerAPI.h"
#import "GLVehicleRankingAveFuelAPI.h"

//G-Store
#import "GLStoreServicesAPI.h"
#import "GLPurchaseHistoryAPI.h"
#import "GLInUseServicesAPI.h"

//Map Collection API
#import "GLCollectionAddAPI.h"
#import "GLCollectionDelAPI.h"
#import "GLCollectionListAPI.h"

//Send to car
#import "GLSendToCarPOIAPI.h"
#import "GLSendToCarAPI.h"

//Dealer API
#import "GLDealerIDAPI.h"
#import "GLDealerSearchAPI.h"
#import "GLDealerSetAPI.h"

//Sms API
#import "GLSmsGetCodeAPI.h"
#import "GLSmsVerifyCodeAPI.h"

// Geo Fence
#import "GLGeoFenceDelAPI.h"
#import "GLGeoFenceListAPI.h"
#import "GLGeoFenceSaveAPI.h"
#import "GLGeoFenceUpdateAPI.h"

// 新能源预约充电
#import "GLChargingReservationAPI.h"

//Push
#import "GLAPNS.h"
#import "GLCacheMessageRequest.h"
#import "GLCacheMessageResponse.h"

#endif /* GLCspServiceSDK_h */
