//
//  GLDealer.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "BaseModel.h"

@protocol GLDealer;

@interface GLDealer : BaseModel

@property (nonatomic,  copy) NSNumber *id;
@property (nonatomic,  copy) NSString *dealerId;
@property (nonatomic,  copy) NSString *dealerCode;
@property (nonatomic,  copy) NSString *dealerFullname;
@property (nonatomic,  copy) NSString *dealerShortname;
@property (nonatomic,  copy) NSString *provinceCode;
@property (nonatomic,  copy) NSString *cityCode;
@property (nonatomic,  copy) NSString *address;
@property (nonatomic,  copy) NSString *phoneNum;
@property (nonatomic,  copy) NSString *hotline;
@property (nonatomic,  copy) NSString *webSeriese;
@property (nonatomic,  copy) NSString *operationStatus;
@property (nonatomic,  copy) NSString *brandName;
@property (nonatomic,  copy) NSString *coordinates;
@property (nonatomic,  copy) NSString *faxNo;
@property (nonatomic,  copy) NSString *bizAttribute;
@property (nonatomic,  copy) NSString *isEcSo;
@property (nonatomic,  copy) NSString *county;
@property (nonatomic,  copy) NSString *md5base64;
@property (nonatomic,assign) NSInteger type;
@property (nonatomic,assign) NSInteger createTime;
@property (nonatomic,assign) NSInteger updateTime;

@end
