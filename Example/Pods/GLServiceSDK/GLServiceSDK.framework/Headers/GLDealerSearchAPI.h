//
//  GLDealerSearchAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLDealer.h"

@interface GLDealerSearchRequest : GLRequest

- (instancetype)initWithName:(NSString *)name;

@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) NSUInteger pageIndex;

@end

@interface GLDealerSearchResponse : GLResponse

@property (nonatomic, copy) NSArray<GLDealer> *list;

@end
