//
//  GLDealerSetAPI.h
//  GLServiceSDK
//
//  Created by fish on 2018/4/10.
//

#import "GLUserProfileModifyAPI.h"

@interface GLDealerSetRequest : GLUserProfileModifyRequest

@property (nonatomic,copy,readonly) GLUser *userProfile;

@end

@interface GLDealerSetResponse : GLUserProfileModifyResponse

@end
