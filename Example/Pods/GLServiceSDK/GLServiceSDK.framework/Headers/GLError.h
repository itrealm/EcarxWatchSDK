//
//  GLError.h
//  Pods
//
//  Created by ecarx on 2017/3/31.
//
//

#import "BaseModel.h"

@interface GLError : BaseModel

@property (nonatomic, copy) NSNumber *code;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, copy) NSString *sysPlatformEnum;

@end
