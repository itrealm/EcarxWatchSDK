//
//  GLFileUploadAPI.h
//  Pods
//
//  Created by fish on 2017/5/26.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLFileUploadRequest : GLRequest

/**
 文件上传

 @param userId    用户ID
 @param string    文件BASE64字符串
 @param extension 文件扩展名
 @return 请求
 */
- (instancetype)initWithUser:(NSString *)userId fileBase64String:(NSString *)string fileExtension:(NSString *)extension;

@end

@interface GLFileUploadResponse : GLResponse

@end
