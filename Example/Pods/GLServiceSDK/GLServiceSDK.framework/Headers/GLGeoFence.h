//
//  GLGeoFence.h
//  Pods
//
//  Created by fish on 2017/9/6.
//
//

#import "BaseModel.h"

@protocol GLGeoFence @end

@interface GLGeoFence : BaseModel

@property (nonatomic,assign) NSInteger id;
@property (nonatomic,assign) NSInteger latitude;
@property (nonatomic,assign) NSInteger longitude;
@property (nonatomic,assign) NSInteger radius; 	  // 单位：km
@property (nonatomic,assign) BOOL status;
@property (nonatomic,assign) BOOL expired;
@property (nonatomic,  copy) NSString *vin;
@property (nonatomic,  copy) NSString *name;
@property (nonatomic,  copy) NSString *centerName;
@property (nonatomic,assign) long long startTime;
@property (nonatomic,assign) long long endTime;
@property (nonatomic,assign) long long createTime;
@property (nonatomic,assign) long long updateTime;

@end
