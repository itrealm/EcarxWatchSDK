//
//  GLGeoFenceDelAPI.h
//  Pods
//
//  Created by fish on 2017/9/6.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLGeoFenceDelRequest : GLRequest

@property (nonatomic,assign,readonly) NSInteger geoFenceId;

- (instancetype)initWithId:(NSInteger)Id;

@end

@interface GLGeoFenceDelResponse : GLResponse

@end
