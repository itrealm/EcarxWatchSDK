//
//  GLGeoFenceListAPI.h
//  Pods
//
//  Created by fish on 2017/9/6.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLGeoFence.h"

@interface GLGeoFenceListRequest : GLRequest

@property (nonatomic,copy,readonly) NSString *vin;

- (instancetype)initWithVin:(NSString *)vin;

@end

@interface GLGeoFenceListResponse : GLResponse

@property (nonatomic, copy) NSArray<GLGeoFence> *list;

@end
