//
//  GLGeoFenceSaveAPI.h
//  Pods
//
//  Created by fish on 2017/9/6.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLGeoFence.h"

@interface GLGeoFenceSaveRequest : GLRequest

@property (nonatomic,copy,readonly) GLGeoFence *data;

- (instancetype)initWithData:(GLGeoFence *)data;

@end

@interface GLGeoFenceSaveResponse : GLResponse

@end
