//
//  GLGeoFenceUpdateAPI.h
//  Pods
//
//  Created by fish on 2017/9/6.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLGeoFence.h"

@interface GLGeoFenceUpdateRequest : GLRequest

@property (nonatomic,copy,readonly) GLGeoFence *data;

- (instancetype)initWithData:(GLGeoFence *)data;

@end

@interface GLGeoFenceUpdateResponse : GLResponse

@end
