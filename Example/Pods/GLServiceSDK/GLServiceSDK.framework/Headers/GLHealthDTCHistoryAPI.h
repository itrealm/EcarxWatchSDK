//
//  GLHealthDTCHistoryAPI.h
//  Pods
//
//  Created by fish on 2017/9/22.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLVehicleHealthHistory.h"

@interface GLHealthDTCHistoryRequest : GLRequest

@property(nonatomic,assign)NSUInteger pageIndex;
@property(nonatomic,assign)NSUInteger pageSize;

@property(nonatomic,  copy,readonly)NSString  *userId;
@property(nonatomic,  copy,readonly)NSString  *vin;
@property(nonatomic,strong,readonly)NSDate    *startTime;
@property(nonatomic,strong,readonly)NSDate    *endTime;

- (instancetype)initWithUser:(NSString *)userId
						 vin:(NSString *)vin
				  startTime:(NSDate *)startTime
					endTime:(NSDate *)endTime;

@end

@interface GLHealthDTCHistoryResponse : GLResponse

@property (nonatomic, copy) NSArray<GLVehicleHealthHistory> *list;

@end
