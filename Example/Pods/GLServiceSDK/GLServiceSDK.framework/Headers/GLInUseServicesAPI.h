//
//  GLInUseServicesAPI.h
//  Pods
//
//  Created by fish on 2017/5/3.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLStoreService.h"

@interface GLInUseServicesRequest : GLRequest

@property (nonatomic,copy,readonly) NSString *vin;
@property (nonatomic,copy,readonly) NSString *userId;
@property (nonatomic,copy,readonly) NSString *langCode;

- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin langCode:(NSString *)lang;

- (instancetype)initWithVin:(NSString *)vin;

@end

@interface GLInUseServicesResponse : GLResponse

@property (nonatomic, assign) NSUInteger lastRow;
@property (nonatomic, assign) NSUInteger totalRows;
@property (nonatomic,   copy) NSArray<GLStoreService> *inUseServices;

@end
