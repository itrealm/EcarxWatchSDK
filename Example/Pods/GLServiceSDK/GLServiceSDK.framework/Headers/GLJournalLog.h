//
//  GLJournalLog.h
//  Pods
//
//  Created by ecarx on 2017/4/5.
//
//

#import "BaseModel.h"

@protocol GLJournalLog;
@protocol GLTrackPoint;
@protocol GLPosition;

@class    GLTrackPoint;
@class    GLPosition;

@interface GLJournalLog : BaseModel

@property (nonatomic, copy) NSNumber *tripId;
@property (nonatomic, copy) NSNumber *startTime;
@property (nonatomic, copy) NSNumber *startOdometer;
@property (nonatomic, copy) NSNumber *fuelConsumption;
@property (nonatomic, copy) NSNumber *traveledDistance;
@property (nonatomic, copy) NSArray<GLTrackPoint> *trackpoints;
@property (nonatomic, copy) NSArray<GLPosition>   *waypoints;
@property (nonatomic, copy) NSNumber *avgSpeed;
@property (nonatomic, copy) NSNumber *endTime;
@property (nonatomic, copy) NSNumber *endOdometer;
@property (nonatomic, copy) NSString *electricConsumption;
@property (nonatomic, copy) NSString *electricRegeneration;

@end

@interface GLTrackPoint : BaseModel

@property (nonatomic, copy) NSString   *systemTime;
@property (nonatomic, copy) GLPosition *position;
@property (nonatomic, copy) NSString   *speed;
@property (nonatomic, copy) NSString   *isSVT;
@property (nonatomic, copy) NSString   *odometer;
@property (nonatomic, copy) NSString   *travelDistanceSinceLastWaypoint;
@property (nonatomic, copy) NSString   *fuelConsumptionSinceLastWaypoint;
@property (nonatomic, copy) NSString   *electricConsumptionSinceLastSample;
@property (nonatomic, copy) NSString   *electricRegenerationSinceLastSample;
@property (nonatomic, copy) NSString   *direction;

@end
