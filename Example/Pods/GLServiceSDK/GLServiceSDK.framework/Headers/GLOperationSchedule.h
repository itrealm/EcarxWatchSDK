//
//  GLOperationSchedule.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "BaseModel.h"

@interface GLOperationSchedule : BaseModel

//调度执行操作的时间戳,单位：毫秒
@property (nonatomic,copy) NSString  *scheduledTime;
//命令执行时间,单位:10s,默认60(10分钟)
@property (nonatomic,assign) NSUInteger duration;
//间隔时间,单位:10s，默认0
@property (nonatomic,assign) NSUInteger interval;
//发生次数，-1表示没有限制，默认1次
@property (nonatomic,assign) NSUInteger occurs;
//是否重复执行,0:一次 1:重复,默认0
@property (nonatomic,assign) NSUInteger recurrentOperation;

@end
