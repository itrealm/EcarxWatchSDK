//
//  GLPosition.h
//  Pods
//
//  Created by fish on 2017/4/24.
//
//

#import "BaseModel.h"

@protocol GLPosition @end

@interface GLPosition : BaseModel

@property (nonatomic,strong) NSNumber *marsCoordinates;
@property (nonatomic,strong) NSNumber *carLocatorStatUploadEn;
@property (nonatomic,strong) NSNumber *posCanBeTrusted;

@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *altitude;

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSNumber *sysTimestamp;
@property (nonatomic,strong) NSNumber<Ignore> *isAvailable;

@end
