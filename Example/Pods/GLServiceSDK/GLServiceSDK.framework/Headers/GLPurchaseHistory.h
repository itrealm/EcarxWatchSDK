//
//  GLPurchaseHistory.h
//  Pods
//
//  Created by fish on 2017/5/3.
//
//

#import "BaseModel.h"

@protocol GLPurchaseHistory;

@interface GLPurchaseHistory : BaseModel

@property (nonatomic, copy) NSString *purchaseTime;
@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSString *itemType;
@property (nonatomic, copy) NSString *itemDescription;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSString *paymentMethod;
@property (nonatomic, copy) NSString *paymentAmout;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *paymentStatus;

@end



/*
{
	"operationResult": "0",
	"lastRow": 7,
	"totalRows": 7,
	"purchaseHistories": [{
		"purchaseTime": "2017-05-02T11:12:37.096+0800",
		"itemName": "2G流量包",
		"itemType": "telecom",
		"itemDescription": "",
		"price": "0.01",
		"currency": "CNY",
		"paymentMethod": "Unicom",
		"paymentAmout": "0.01",
		"userId": "ecarx_test02",
		"paymentStatus": "request sent and acknowledged"
	}, {
		"purchaseTime": "2017-05-02T11:12:03.823+0800",
		"itemName": "2G流量包",
		"itemType": "telecom",
		"itemDescription": "",
		"price": "0.01",
		"currency": "CNY",
		"paymentMethod": "Unicom",
		"paymentAmout": "0.01",
		"userId": "ecarx_test02",
		"paymentStatus": "request sent and acknowledged"
	}, {
		"purchaseTime": "2017-05-02T10:54:22.962+0800",
		"itemName": "10G Package",
		"itemType": "telecom",
		"itemDescription": "",
		"price": "0.0",
		"currency": "CNY",
		"paymentMethod": "Unicom",
		"paymentAmout": "0.0",
		"userId": "ecarx_test02",
		"paymentStatus": "paymentSkipped"
	}, {
		"purchaseTime": "2017-05-02T10:52:16.745+0800",
		"itemName": "10G Package",
		"itemType": "telecom",
		"itemDescription": "",
		"price": "0.0",
		"currency": "CNY",
		"paymentMethod": "Unicom",
		"paymentAmout": "0.0",
		"userId": "ecarx_test02",
		"paymentStatus": "paymentSkipped"
	}, {
		"purchaseTime": "2017-05-02T10:48:09.909+0800",
		"itemName": "地图",
		"itemType": "tservice",
		"itemDescription": "",
		"price": "0.0",
		"currency": "CNY",
		"paymentMethod": "Unicom",
		"paymentAmout": "0.0",
		"userId": "ecarx_test02",
		"paymentStatus": "paymentSkipped"
	}, {
		"purchaseTime": "2017-05-02T10:43:31.851+0800",
		"itemName": "1G流量包",
		"itemType": "telecom",
		"itemDescription": "",
		"price": "0.0",
		"currency": "CNY",
		"paymentMethod": "Unicom",
		"paymentAmout": "0.0",
		"userId": "ecarx_test02",
		"paymentStatus": "paymentSkipped"
	}, {
		"purchaseTime": "2017-05-02T10:43:06.942+0800",
		"itemName": "100M",
		"itemType": "telecom",
		"itemDescription": "",
		"price": "0.0",
		"currency": "CNY",
		"paymentMethod": "Unicom",
		"paymentAmout": "0.0",
		"userId": "ecarx_test02",
		"paymentStatus": "paymentSkipped"
	}]
}
*/
