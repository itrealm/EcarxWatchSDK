//
//  GLPurchaseHistoryAPI.h
//  Pods
//
//  Created by fish on 2017/5/3.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLPurchaseHistory.h"

@interface GLPurchaseHistoryRequest : GLRequest

@property (nonatomic,  copy,readonly) NSString *vin;
@property (nonatomic,  copy,readonly) NSString *userId;
@property (nonatomic,assign,readonly) NSInteger pageSize;
@property (nonatomic,assign,readonly) NSInteger pageIndex;

- (instancetype)initWithUserId:(NSString *)userId
						   vin:(NSString *)vin
					 pageIndex:(NSUInteger)index
					  pageSize:(NSUInteger)size;

@end

@interface GLPurchaseHistoryResponse : GLResponse

@property (nonatomic, assign) NSUInteger lastRow;
@property (nonatomic, assign) NSUInteger totalRows;
@property (nonatomic,   copy) NSArray<GLPurchaseHistory> *purchaseHistories;

@end
