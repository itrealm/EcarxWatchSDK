//
//  GLPushNotification.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2017/6/2.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "BaseModel.h"
#import "GLServiceResult.h"
#import "GLPosition.h"

@interface GLPushNotification : BaseModel

@property (nonatomic,  copy) NSString			 *vin;
@property (nonatomic,  copy) NSString			 *serviceId;
@property (nonatomic,  copy) NSString            *sessionId;
@property (nonatomic,  copy) GLServiceResult     *serviceResult;
@property (nonatomic,  copy) NSArray<GLPosition> *pois;
@property (nonatomic,  copy) NSString			 *content;
@property (nonatomic,  copy) NSString            *jsonContent;
@property (nonatomic,  copy) NSString			 *language;
@property (nonatomic,  copy) NSString			 *platform;
@property (nonatomic,assign) NSInteger			  group;
@property (nonatomic,  copy) NSNumber            *messageTimeStamp;

@end
