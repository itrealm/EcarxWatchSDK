//
//  GLRequest.h
//  Pods
//
//  Created by ecarx on 2017/4/10.
//
//

#import "BaseRequest.h"

@interface GLRequest : BaseRequest


/**
 是否需要登录
 */
@property (nonatomic,assign,readonly) BOOL isNeedAuth;

/**
 返回数据模型
 */
@property (nonatomic,strong,readonly) Class responseClass;

@end
