//
//  GLResponse.h
//  Pods
//
//  Created by ecarx on 2017/3/20.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "BaseResponse.h"
#import "GLError.h"

@class GLServiceResult,GLPagination;

@interface GLResponse : BaseResponse

// base response
@property (nonatomic,assign, readonly) BOOL      isSuccess;
@property (nonatomic,copy  ,readwrite) NSString *msg;
@property (nonatomic,assign,readwrite) NSInteger code;

// csp response
@property (nonatomic,strong) NSNumber        *operationResult;
@property (nonatomic,  copy) GLError         *error;
@property (nonatomic,  copy) GLServiceResult *serviceResult;
@property (nonatomic,  copy) GLPagination    *pagination;

@end

@interface GLCmdResponse : BaseResponse

@property (nonatomic, copy) GLServiceResult *serviceResult;

@end

@interface GLPagination : BaseModel

@property (nonatomic, copy) NSNumber *pageSize;
@property (nonatomic, copy) NSNumber *pageIndex;
@property (nonatomic, copy) NSString *sortField;
@property (nonatomic, copy) NSString *direction;
@property (nonatomic, copy) NSNumber *start;
@property (nonatomic, copy) NSNumber *totleSize;

@end


