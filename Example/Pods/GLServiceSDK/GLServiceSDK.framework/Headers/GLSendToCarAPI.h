//
//  GLSendToCarAPI.h
//  Pods
//
//  Created by fish on 2017/6/27.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLSendToCarRequest : GLRequest

@property(nonatomic,copy)NSString *address;

/**
 CMA平台 发送到车

 @param vin        vin
 @param latitude   纬度
 @param longitude  经度
 @param title	   POI名称
 @param sender	   sender
 @param messageId  messageId
 */
- (instancetype)initWithVin:(NSString *)vin
				   latitude:(double)latitude
				  longitude:(double)longitude
					  title:(NSString *)title
					 sender:(NSString *)sender
				  messageId:(NSString *)messageId;

@end

@interface GLSendToCarResponse : GLResponse

@end
