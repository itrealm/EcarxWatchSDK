//
//  GLSendToCarPOIAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/6.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLSendToCarPOIRequest : GLRequest

@property (nonatomic,strong,readonly) NSString *ihuId;
@property (nonatomic,assign,readonly) double    latitude;
@property (nonatomic,assign,readonly) double    longitude;
@property (nonatomic,copy  ,readonly) NSString *name;
@property (nonatomic,copy  ,readonly) NSString *address;
@property (nonatomic,copy ,readwrite) NSString *vin;

/**
 NonCMA平台 发送到车

 @param ihuId IHU ID
 @param name POI名称
 @param address POI地址
 @param latitude 纬度
 @param longitude 经度
 */
- (instancetype)initWithIhuId:(NSString *)ihuId
						 name:(NSString *)name
					  address:(NSString *)address
					 latitude:(double)latitude
					longitude:(double)longitude;

@end

@interface GLSendToCarPOIResponse : GLResponse

@end
