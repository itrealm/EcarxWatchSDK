//
//  GLServiceParameters.h
//  Pods
//
//  Created by ecarx on 2017/4/5.
//
//

#import "BaseModel.h"

@protocol GLServiceParameters @end

@interface GLServiceParameters : BaseModel

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *value;

@end
