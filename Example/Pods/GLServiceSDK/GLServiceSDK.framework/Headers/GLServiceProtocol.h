//
//  GLServiceProtocol.h
//  GLServiceSDK
//
//  Created by fish on 2018/7/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol GLServiceProtocol <NSObject>

@property (readonly,assign) BOOL isUserLogined;    //当前用户是否已登录
@property (readwrite, copy) id   loginData;   	   //登录数据
@property (readonly,  copy) NSString *userId;  	   //用户ID
@property (readonly,  copy) NSString *clientId;	   //设备别名
@property (readonly,  copy) NSString *accessToken;
@property (readonly,  copy) NSString *tcToken;
@property (readonly,  copy) NSString *refreshToken;//刷新token
@property (readonly,  copy) NSString *expiresIn;
@property (readonly,  copy) NSString *deviceId;    //设备ID

@property (readonly,assign) BOOL     isBindVehicle;
@property (readwrite, copy) id       bindVehicle;

@property (readwrite, copy) NSString  *appId;		 //应用ID
@property (readwrite, copy) NSString  *operatorCode; //运营商标识
@property (readwrite, copy) NSString  *hostEnv;	 	 //环境
@property (readwrite, copy) NSString  *platform; 	 //汽车平台
@property (readwrite, copy) NSString  *language;     //当前语言

- (void)clearAutoLogin;

@end

NS_ASSUME_NONNULL_END
