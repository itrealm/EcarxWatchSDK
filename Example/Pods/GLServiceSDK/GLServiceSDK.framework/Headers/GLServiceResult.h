//
//  GLServiceResult.h
//  Pods
//
//  Created by fish on 2017/6/12.
//
//

#import "BaseModel.h"
#import "GLError.h"

@interface GLServiceResult : BaseModel

@property (nonatomic, copy) NSNumber *operationResult;
@property (nonatomic, copy) GLError  *error;

@end
