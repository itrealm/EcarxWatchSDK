//
//  GLServiceSDK.h
//  GLServiceSDK
//
//  Created by fish on 2017/10/12.
//

#import <Foundation/Foundation.h>
#import "GLCspServiceSDK.h"
#import "GLTspServiceSDK.h"
#import "GLVehicleManager.h"

typedef NS_ENUM(NSUInteger, GLServiceModel) {
    GLServiceModelCsp  = 0,
    GLServiceModelTsp  = 1,
};

typedef NS_ENUM(NSUInteger, GLServiceEnv) {
    GLServiceEnvDev     = 0,
    GLServiceEnvTest    = 1,
    GLServiceEnvStage   = 2,
    GLServiceEnvProduct = 3
};

typedef NS_ENUM(NSUInteger, GLServiceLanguage) {
    GLServiceLanguageChinese  = 0,
    GLServiceLanguageEnglish  = 1,
    GLServiceLanguageMalay    = 2
};

//用户登录成功
FOUNDATION_EXPORT NSNotificationName const GLUserDidLoginNotification;
//用户退出成功[主动退出/被挤下线]
FOUNDATION_EXPORT NSNotificationName const GLUserDidLogoutNotification;

@interface GLServiceSDK : NSObject

@property (class,nonatomic,assign) GLServiceModel serviceModel;
//User
@property (class,assign,readonly) BOOL  isUserLogined;	//当前用户是否已登录
@property (class,copy  ,readonly) NSString *userId;		//用户ID
@property (class,copy  ,readonly) NSString *deviceId;   //设备ID
@property (class,assign,readonly) NSString *clientId;	//客户端ID,别名
@property (class,copy  ,readonly) NSString *accessToken;
@property (class,copy  ,readonly) NSString *tcToken;

//Vehicle
@property (class,assign,readonly) BOOL isBindVehicle;
@property (class, copy,readwrite) GLVehicle *bindVehicle;

//平台
@property (class,copy,readonly) NSString *appId;
@property (class,copy,readonly) NSString *operatorCode;
@property (class,copy,readonly) NSString *platform;

+ (void)setAppId:(NSString *)appId OBJC_DEPRECATED("Use startWithAppId:appSecret:operatorCode:platform:env: instead. 1.0.9+");
+ (void)setOperatorCode:(NSString *)operatorCode OBJC_DEPRECATED("Use startWithAppId:appSecret:operatorCode:platform:env: instead. 1.0.9+");
+ (void)setPlatform:(NSString *)platform OBJC_DEPRECATED("Use startWithAppId:appSecret:operatorCode:platform:env: instead. 1.0.9+");

//环境
@property (class,copy,readonly) NSString *hostUrl;
@property (class,copy,readwrite)NSString *hostEnv;
@property (class,copy,readonly) NSString *language;

#pragma mark - 初始化
/**
 初始化SDK
 
 @param appId  应用ID
 @param env      服务环境
 */
+ (void)startWithAppId:(NSString *)appId
                   env:(GLServiceEnv)env OBJC_DEPRECATED("Use startWithAppId:appSecret:operatorCode:platform:env: instead. 1.1.1+");
+ (void)startWithAppId:(NSString *)appId
             appSecret:(NSString *)appSecret
          operatorCode:(NSString *)operatorCode
              platform:(NSString *)platform
                   env:(GLServiceEnv)env;
/**
 切换语言
 
 @param language 语言
 */
+ (void)updateLanguage:(GLServiceLanguage)language;

#pragma mark - 验证码
/**
 获取验证码
 
 @param request 请求
 */
+ (RACSignal<ExResponse *> *)fetchCaptcha:(ExVerificationGetRequest *)request;

/**
 校验验证码
 
 @param request 请求
 */
+ (RACSignal<ExResponse *> *)checkCaptcha:(ExVerificationCheckRequest *)request;


#pragma mark - 用户
/**
 登录
 
 @param request 请求
 */
+ (RACSignal<ExLogin *> *)login:(ExLoginRequest *)request;

/**
 退出登录
 */
+ (RACSignal<ExResponse *> *)logout;

/**
 注册用户
 
 @param request 请求
 */
+ (RACSignal<ExResponse *> *)registerUser:(ExRegisterRequest *)request;

/**
 修改用户手机号
 
 @param request 请求
 */
+ (RACSignal<ExResponse *> *)modifyUserMobile:(ExUserMobileModRequest *)request;

/**
 修改用户密码
 
 @param request 请求
 */
+ (RACSignal<ExResponse *> *)modifyPassword:(ExUserPwdChangeRequest *)request;

/**
 重置用户密码
 
 @param request 请求
 */
+ (RACSignal<ExResponse *> *)resetPassword:(ExUserResetPwdRequest *)request;

/**
 获取用户信息
 */
+ (RACSignal<ExUser *> *)fetchUserProfile;

/**
 修改用户信息
 
 @param request 请求
 */
+ (RACSignal<ExResponse *> *)modifyUserProfile:(ExUserProfileModRequest *)request;

/**
 修改用户头像
 
 @param imageData 请求
 */
+ (RACSignal<ExResponse *> *)modifyUserAvatar:(NSData *)imageData;

#pragma mark - 车辆管理
/**
 车辆管理
 
 @return 车辆管理
 */
+ (GLVehicleManager *)defaultVehicleManager;

#pragma mark -
+ (RACSignal *)startRequest:(BaseRequest *)request;
+ (void)startRequest:(BaseRequest *)request success:(SuccessBlock)success failure:(FailureBlock)failure OBJC_DEPRECATED("Use startRequest: instead");
+ (void)clearAutoLogin;

//SDK版本号
+ (NSString *)sdkVersion;

@end
