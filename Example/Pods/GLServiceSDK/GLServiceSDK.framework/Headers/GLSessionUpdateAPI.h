//
//  GLSessionUpdateAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLSessionUpdateRequest : GLRequest
    
- (instancetype)initWithVin:(NSString *)vin token:(NSString *)token language:(NSString *)language;

- (instancetype)initWithVin:(NSString *)vin;

@end

@interface GLSessionUpdateResponse : GLResponse

@property(nonatomic,copy)NSString *alias;

@end
