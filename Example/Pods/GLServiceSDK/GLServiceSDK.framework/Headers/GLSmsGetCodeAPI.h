//
//  GLSmsGetCodeAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/5.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLSmsGetCodeRequest : GLRequest

@property (nonatomic,copy,readonly ) NSString *mobile;

/**
 通过手机号获取验证码

 @param mobile 手机号
 */
- (instancetype)initWithMobile:(NSString *)mobile language:(NSString *)language;

- (instancetype)initWithMobile:(NSString *)mobile;

@end

@interface GLSmsGetCodeRequest (Private)

@property (nonatomic, copy) NSString *purpose;

@end

@interface GLSmsGetCodeResponse : GLResponse

@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *serviceId;
@property (nonatomic, copy) NSString *sessionId;
@property (nonatomic, copy) NSString *taskId;
@property (nonatomic, copy) NSString *language;

@end
