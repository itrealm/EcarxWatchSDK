//
//  GLSmsVerifyCodeAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/5.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLSmsVerifyCodeRequest : GLRequest

@property (nonatomic,copy,readonly) NSString *mobile;
@property (nonatomic,copy,readonly) NSString *verifyCode;

/**
 验证短信验证码

 @param mobile 用户ID
 @param code   验证码
 */
- (instancetype)initWithMobile:(NSString *)mobile code:(NSString *)code;

@end

@interface GLSmsVerifyCodeRequest (Private)

@property (nonatomic,copy) NSString *purpose;

@end

@interface GLSmsVerifyCodeResponse : GLResponse

@end
