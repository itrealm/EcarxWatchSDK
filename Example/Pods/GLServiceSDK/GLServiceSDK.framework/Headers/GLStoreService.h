//
//  GLStoreService.h
//  Pods
//
//  Created by fish on 2017/5/3.
//
//

#import "BaseModel.h"

@protocol GLStoreService;

@interface GLStoreService : BaseModel

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *category;
@property (nonatomic, copy) NSString *expirationTime;
@property (nonatomic, copy) NSString *subscriptionTime;

@end

/*
{
	"operationResult": "0",
	"lastRow": 5,
	"totalRows": 11,
	"services": [{
		"id": "A-OLA-0pZ42jW",
		"type": "telecom",
		"name": "100M",
		"icon": "http://172.21.38.68:9000/repository/store/P-7jwzvOJeu4V/j/i/j/100M.png?action=access&assetId=A-OLA-0pZ42jW",
		"description": "100M",
		"category": "Telecom Service"
	}, {
		"id": "A-CLA-0pZ42jW",
		"type": "telecom",
		"name": "10G Package",
		"icon": "http://172.21.38.68:9000/repository/store/P-8jwzvOJeu4V/e/j/e/10G.png?action=access&assetId=A-CLA-0pZ42jW",
		"description": "10G流量包",
		"category": "Telematic Service"
	}, {
		"id": "A-7Nz-0pZ42jW",
		"type": "telecom",
		"name": "1G流量包",
		"icon": "http://172.21.38.68:9000/repository/store/P-7jwzvOJeu4V/d/f/e/1G.png?action=access&assetId=A-7Nz-0pZ42jW",
		"description": "1G流量包",
		"category": "Telecom Service"
	}, {
		"id": "A-fNz-0pZ42jW",
		"type": "telecom",
		"name": "500M",
		"icon": "http://172.21.38.68:9000/repository/store/P-6jwzvOJeu4V/c/h/f/500M.png?action=access&assetId=A-fNz-0pZ42jW",
		"description": "Unicom 500M 流量包",
		"category": "Telecom Service"
	}, {
		"id": "A-8Nz-0pZ42jW",
		"type": "telecom",
		"name": "2G流量包",
		"icon": "http://172.21.38.68:9000/repository/store/P-7jwzvOJeu4V/f/c/e/2G.jpg?action=access&assetId=A-8Nz-0pZ42jW",
		"description": "2G流量包",
		"category": "Telecom Service"
	}]
}
*/
