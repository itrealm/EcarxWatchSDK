//
//  GLStoreServicesAPI.h
//  Pods
//
//  Created by fish on 2017/5/3.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLStoreService.h"

@interface GLStoreServicesRequest : GLRequest

/**
 获取全部类型的商品
 */
- (instancetype)initWithUserId:(NSString *)userId
						   vin:(NSString *)vin
					  language:(NSString *)lang
					 pageIndex:(NSUInteger)index
					  pageSize:(NSUInteger)size;

/**
 获取指定类型下的商品

 @param userId userId
 @param vin    vin
 @param type   telecom:流量 tservice:服务 nil: 全部
 @param lang   用户语言
 @param index  页码
 @param size   每页数量
 */
- (instancetype)initWithUserId:(NSString *)userId
						   vin:(NSString *)vin
						  type:(NSString *)type
					  language:(NSString *)lang
					 pageIndex:(NSUInteger)index
					  pageSize:(NSUInteger)size;

@end

@interface GLStoreServicesResponse : GLResponse

@property (nonatomic, assign) NSUInteger lastRow;
@property (nonatomic, assign) NSUInteger totalRows;
@property (nonatomic,   copy) NSArray<GLStoreService> *services;

@end
