//
//  GLTelematicsResult.h
//  Pods
//
//  Created by ecarx on 2017/3/31.
//
//

#import "BaseModel.h"
#import "GLError.h"

@class GLServiceResult;

@interface GLTelematicsResult : BaseModel

@property (nonatomic, copy) GLServiceResult *serviceResult;

@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *serviceId;
@property (nonatomic, copy) NSString *sessionId;
@property (nonatomic, copy) NSString *taskId;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *platform;

@end
