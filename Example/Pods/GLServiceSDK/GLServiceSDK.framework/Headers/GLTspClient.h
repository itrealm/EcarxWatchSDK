//
//  GLTspClient.h
//  Pods
//
//  Created by fish on 2017/9/18.
//
//

#import "BaseAPI.h"
#import "GLServiceProtocol.h"

extern NSString * const kTspErrorDomain;

#define HOST_ENV_DEV     @"development"
#define HOST_ENV_TEST    @"testing"
#define HOST_ENV_STAGE   @"staging"
#define HOST_ENV_PRODUCT @"production"

//协议类型
typedef NS_ENUM(NSUInteger, ExProtocol) {
    ExProtocolExtended = 0, //扩展协议
    ExProtocolStandard = 1, //标准协议
};

@class ExVehicle,ExLogin;

@interface GLTspClient : BaseAPI <GLServiceProtocol>

@property (readonly,assign) BOOL isUserLogined;    //当前用户是否已登录
@property (readwrite, copy) ExLogin  *loginData;   //登录数据
@property (readonly,  copy) NSString *userId;  	   //用户ID
@property (readonly,  copy) NSString *clientId;	   //设备别名
@property (readonly,  copy) NSString *accessToken;
@property (readonly,  copy) NSString *tcToken;
@property (readonly,  copy) NSString *refreshToken;//刷新token
@property (readonly,  copy) NSString *expiresIn;
@property (readonly,  copy) NSString *deviceId;    //设备ecarxId

@property (readonly,assign) BOOL     isBindVehicle;
@property (readwrite, copy) ExVehicle *bindVehicle;

@property (readwrite, copy) NSString  *appId;		 //应用ID
@property (readwrite, copy) NSString  *appSecret;    //应用密钥
@property (readwrite, copy) NSString *operatorCode;	 //运营商标识
@property (readwrite, copy) NSString  *hostEnv;	 	 //环境
@property (readwrite, copy) NSString  *platform; 	 //汽车平台
@property (readwrite,assign)ExProtocol protocol;     //协议类型
@property (readwrite, copy) NSString  *language;     //语言

- (void)clearAutoLogin;

+ (instancetype)sharedClient;
+ (BOOL)isUserLogined;
+ (ExLogin *)loginData;
+ (NSString *)xClientId;
+ (NSString *)userId;
+ (NSString *)accessToken;
+ (NSString *)tcToken;
+ (NSString *)refreshToken;
+ (NSString *)expiresIn;
+ (NSString *)platform;
+ (NSString *)appId;
+ (NSString *)hostEnv;

@end
