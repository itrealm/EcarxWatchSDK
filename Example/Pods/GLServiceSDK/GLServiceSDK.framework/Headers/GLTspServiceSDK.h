//
//  GLTspServiceSDK.h
//  Pods
//
//  Created by fish on 2017/9/18.
//
//

#ifndef GLTspServiceSDK_h
#define GLTspServiceSDK_h

#import "GLTspError.h"
#import "GLTspClient.h"

//验证码
#import "ExVerificationGetRequest.h"
#import "ExVerificationGetResponse.h"
#import "ExVerificationCheckRequest.h"
#import "ExVerificationCheckResponse.h"
#import "ExCaptchaGetRequest.h"
#import "ExCaptchaCheckRequest.h"

//登录注册
#import "ExLoginRequest.h"
#import "ExLoginResponse.h"
#import "ExLogoutRequest.h"
#import "ExLogoutResponse.h"
#import "ExOpenLoginRequest.h"
#import "ExRegisterRequest.h"
#import "ExRegisterResponse.h"
#import "ExAccessTokenRequest.h"
#import "ExAccessTokenResponse.h"
#import "ExAuthLoginRequest.h"

//用户
#import "ExUserIdentityRequest.h"
#import "ExUserIdentityResponse.h"
#import "ExUserProfileModRequest.h"
#import "ExUserProfileModResponse.h"
#import "ExUserProfileRequest.h"
#import "ExUserProfileResponse.h"
#import "ExUserProfileV1Request.h"
#import "ExUserPwdChangeRequest.h"
#import "ExUserPwdChangeResponse.h"
#import "ExUserResetPwdRequest.h"
#import "ExUserResetPwdResponse.h"
#import "ExAvatarUploadRequest.h"
#import "ExAvatarUploadResponse.h"
#import "ExUserMobileModRequest.h"
#import "ExUserMobileModResponse.h"

//车机扫码登录
#import "ExQrLoginRequest.h"
#import "ExQrLoginResponse.h"

//车辆
#import "ExVehicleListRequest.h"
#import "ExVehicleListResponse.h"
#import "ExVehicleListV1Request.h"
#import "ExVehicleListV1Response.h"
#import "ExVehicleListByMobileRequest.h"
#import "ExVehicleListByMobileResponse.h"
#import "ExVehivleOperationRequest.h"
#import "ExVehivleOperationResponse.h"
#import "ExVehicleColorRequest.h"
#import "ExVehicleColorResponse.h"
#import "ExVehicleModifyPlateRequest.h"
#import "ExVehicleModifyPlateResponse.h"

//车控
#import "ExVehicleControlWaitTimeRequest.h"
#import "ExVehicleControlWaitTimeResponse.h"
#import "ExVehicleCapabilityRequest.h"
#import "ExVehicleCapabilityResponse.h"

#import "ExVehicleStatusRequest.h"
#import "ExVehicleStatusResponse.h"
#import "ExVehicleStatusHistoryRequest.h"
#import "ExVehicleStatusHistoryResponse.h"

#import "ExVehicleStateRequest.h"
#import "ExVehicleStateResponse.h"

#import "ExVehicleTelematicsRequest.h"
#import "ExVehicleTelematicsResponse.h"
#import "ExVehicleTelematicsActivateQueryRequest.h"
#import "ExVehicleTelematicsActivateQueryResponse.h"
#import "ExVehicleTelematicsActivateRequest.h"
#import "ExVehicleTelematicsActivateResponse.h"

//车辆诊断断历史
#import "ExVehicleHealthHistoryRequest.h"
#import "ExVehicleHealthHistoryResponse.h"

//预约充电历史
#import "ExChargingReservationRequest.h"
#import "ExChargingReservationResponse.h"

//行车日志
#import "ExVehicleJournalLogRequest.h"
#import "ExVehicleJournalLogResponse.h"
#import "ExVehicleJournalLogSwitchRequest.h"
#import "ExVehicleJournalLogSwitchResponse.h"

//发送到车
#import "ExVehicleSendToCarRequest.h"
#import "ExVehicleSendToCarResponse.h"
#import "ExVehicleSendToCarPOIRequest.h"
#import "ExVehicleSendToCarPOIResponse.h"

//电子围栏
#import "ExGeoFenceDelRequest.h"
#import "ExGeoFenceDelResponse.h"
#import "ExGeoFenceListRequest.h"
#import "ExGeoFenceListResponse.h"
#import "ExGeoFenceSaveRequest.h"
#import "ExGeoFenceSaveResponse.h"
#import "ExGeoFenceUpdateRequest.h"
#import "ExGeoFenceUpdateResponse.h"

//推送优化
#import "ExCacheMessageRequest.h"
#import "ExCacheMessageResponse.h"

//设备
#import "ExDeviceConfigRequest.h"
#import "ExDeviceConfigResponse.h"
#import "ExDeviceSessionUpdateRequest.h"
#import "ExDeviceSessionUpdateResponse.h"

//地区
#import "ExRegionCountryRequest.h"
#import "ExRegionZoneRequest.h"
#import "ExRegionCityRequest.h"
#import "ExRegionCitySearchRequest.h"
#import "ExRegionAddressRequest.h"

//车载流量
#import "ExFlowRequest.h"
#import "ExFlowResponse.h"

//违章
#import "ExPeccancyRequest.h"
#import "ExPeccancyResponse.h"

//接送好友
#import "ExVehiclePickupRequest.h"
#import "ExVehiclePickupResponse.h"
#import "ExVehiclePickupDeleteRequest.h"
#import "ExVehiclePickupDeleteResponse.h"
#import "ExVehiclePickupStatusRequest.h"
#import "ExVehiclePickupStatusResponse.h"

//首选经销商
#import "ExUserDealerRequest.h"
#import "ExUserDealerResponse.h"
#import "ExDealerSearchRequest.h"
#import "ExDealerSearchResponse.h"
#import "ExSetUserDealerRequest.h"
#import "ExSetUserDealerResponse.h"

//排名
#import "ExVehicleRankingAveFuelRequest.h"
#import "ExVehicleRankingAveFuelResponse.h"
#import "ExVehicleRankingOdometerRequest.h"
#import "ExVehicleRankingOdometerResponse.h"
#import "ExVehicleRankingAveEnergyRequest.h"

//车机壁纸
#import "ExAliyunInfoRequest.h"
#import "ExAliyunInfoResponse.h"
#import "ExSTSInfoRequest.h"
#import "ExSTSInfoResponse.h"
#import "ExGetWallpaperListRequest.h"
#import "ExGetWallpaperListResponse.h"
#import "ExDeleteWallpaperRequest.h"
#import "ExDeleteWallpaperResponse.h"
#import "ExWallpaperConfigRequest.h"
#import "ExWallpaperConfigResponse.h"

//服务
#import "ExServiceInUseRequest.h"
#import "ExServiceInUseResponse.h"
#import "ExServiceHistoryRequest.h"
#import "ExServiceHistoryResponse.h"

//PIN
#import "ExPinResetRequest.h"
#import "ExPinResetResponse.h"
#import "ExPinStatusRequest.h"
#import "ExPinStatusResponse.h"
#import "ExPinVerifyRequest.h"
#import "ExPinVerifyResponse.h"

#import "ExAuthSubSessionCodeRequest.h"
#import "ExAuthSubSessionCodeResponse.h"

//积分
#import "ExGrowEventRequest.h"

//充放电SOC
#import "ExChargingSocRequest.h"
#import "ExChargingSocResponse.h"

//香氛
#import "ExVehicleFragranceRequest.h"
#import "ExVehicleFragranceResponse.h"

#import "ExDisableMatCodeRequest.h"
#import "ExDisableMatCodeResponse.h"
#import "ExMatCodeFilterRequest.h"
#import "ExMatCodeFilterResponse.h"

/***************** CSP *****************/
//CSP车机数据同步
#import "ExDataSyncUserRequest.h"
#import "ExDataSyncUserResponse.h"
#import "ExDataSyncVehicleRequest.h"
#import "ExDataSyncVehicleResponse.h"
#import "ExCspSyncTokenRequest.h"
#import "ExCspSyncTokenResponse.h"

//CSP车机扫码登录
#import "ExCspQrLoginRequest.h"
#import "ExCspQrLoginResponse.h"

//别名,session
#import "ExAliasBindRecordRequest.h"
#import "ExAliasBindRecordResponse.h"

//CSP注册
#import "ExCspUserRegisterRequest.h"
#import "ExCspUserRegisterResponse.h"
#import "ExCspVerificationCheckRequest.h"
#import "ExCspVerificationCheckResponse.h"
#import "ExCspVerificationGetRequest.h"
#import "ExCspVerificationGetResponse.h"

//CSP重置密码
#import "ExCspGetCodeRequest.h"
#import "ExCspGetCodeResponse.h"
#import "ExCspVerifyCodeRequest.h"
#import "ExCspVerifyCodeResponse.h"
#import "ExCspResetPasswordRequest.h"
#import "ExCspResetPasswordResponse.h"

//CSP绑定车辆
#import "ExCspVehicleBindRequest.h"
#import "ExCspVehicleBindResponse.h"

#import "ExPoistarServiceSDK.h"

#import "LKAuthLoginRequest.h"

#endif /* GLTspServiceSDK_h */
