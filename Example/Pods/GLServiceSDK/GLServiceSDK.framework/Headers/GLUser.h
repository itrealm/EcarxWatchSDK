//
//  GLUser.h
//  GLCspServiceSDK
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "BaseAPI.h"

@protocol GLUser;

@interface GLUser : BaseModel

//用户ID
@property (nonatomic, copy) NSString *userId;

//车辆标识符
@property (nonatomic, copy) NSString *vin;

//姓别
@property (nonatomic, copy) NSString *gender;

//手机号
@property (nonatomic, copy) NSString *mobilePhone;

//昵称
@property (nonatomic, copy) NSString *name;

//用户联系地址
@property (nonatomic, copy) NSString *address;

//头像
@property (nonatomic, copy) NSString *avatarUri;

//邮箱
@property (nonatomic, copy) NSString *email;

//经销商
@property (nonatomic, copy) NSString *dealerKeyId;
@property (nonatomic, copy) NSString<Ignore> *dealerId;
@property (nonatomic, copy) NSString *dealerShortName;

//紧急联系人
@property (nonatomic, copy) NSString *emergencyContactName;
@property (nonatomic, copy) NSString *emergencyContactPhone;

@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *pin;
@property (nonatomic, copy) NSString *lockStatus;
@property (nonatomic, copy) NSString *tag;
@property (nonatomic, copy) NSString *privacySetting;

//创建时间,更新时间
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *updateTime;

@end
