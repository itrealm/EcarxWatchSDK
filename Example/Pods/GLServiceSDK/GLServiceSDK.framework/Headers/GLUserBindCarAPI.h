//
//  GLUserBindCarAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserBindCarRequest : GLRequest

/**
 绑定车辆

 @param userId		  用户ID
 @param vin			  VIN
 @param customerPhone 客户手机号
 */
- (id)initWithUserId:(NSString *)userId vin:(NSString *)vin customerPhone:(NSString *)customerPhone;

@end

@interface GLUserBindCarResponse : GLResponse

@end
