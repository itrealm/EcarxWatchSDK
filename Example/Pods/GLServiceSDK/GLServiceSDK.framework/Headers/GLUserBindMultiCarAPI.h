//
//  GLUserBindMultiCarAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/12.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserBindMultiCarRequest : GLRequest

/**
 绑定多辆车

 @param userId 用户ID
 @param registrationId 客户端标识
 @param vins [NSString]车辆身份标识
 */
- (instancetype)initWithUserId:(NSString *)userId
				registrationId:(NSString *)registrationId
						  vins:(NSArray *)vins;

@end

@protocol GLUserBindVehicleResult @end

@interface GLUserBindMultiCarResponse : GLResponse

@property (nonatomic,copy) NSArray<GLUserBindVehicleResult> *bindresults;

@end

@interface GLUserBindVehicleResult : BaseModel

@property (nonatomic, copy) NSNumber *operationResult;
@property (nonatomic, copy) GLError  *error;
@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *isExistingUser;

@end
