//
//  GLUserConfirmCarAPI.h
//  Pods
//
//  Created by fish on 2017/4/28.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserConfirmCarRequest : GLRequest

- (instancetype)initWithUserId:(NSString *)userId shortVin:(NSString *)shortVin clientId:(NSString *)clientId;

@end

@interface GLUserConfirmCarResponse : GLResponse

@end
