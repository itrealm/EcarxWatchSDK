//
//  GLUserLoginAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserLoginRequest : GLRequest

@property (nonatomic, copy) NSString *platform;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *registrationId;

/**
 用户登录

 @param user 用户名
 @param password 密码
 @param language 当前语言
 @param platform 平台
 */
- (instancetype)initWithUser:(NSString *)user
					password:(NSString *)password
					language:(NSString *)language
					platform:(NSString *)platform;

- (instancetype)initWithUser:(NSString *)user password:(NSString *)password;

@end

@interface GLUserLoginResponse : GLResponse

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, copy) NSString *refreshToken;
@property (nonatomic, copy) NSString *idToken;
@property (nonatomic, copy) NSString *tcToken;
@property (nonatomic, copy) NSNumber *expiresIn;
@property (nonatomic, copy) NSString *alias;
@property (nonatomic, copy) NSString *resultCode;
@property (nonatomic, copy) NSString *resultMessage;

@end
