//
//  GLUserLogoutAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserLogoutRequest : GLRequest

- (instancetype)initWithTcToken:(NSString *)tcToken;

@end

@interface GLUserLogoutResponse : GLResponse

@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *resultCode;
@property (nonatomic, copy) NSString *resultMessage;

@end
