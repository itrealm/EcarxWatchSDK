//
//  GLUserMobileVerifyAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserMobileVerifyRequest : GLRequest

/**
 验证手机号未注册
 - 成功：未注册
 - 失败：已注册
 @param mobile 手机号码
 */
- (instancetype)initWithMobile:(NSString *)mobile;

/**
 验证手机号与用户ID是否匹配

 @param userId 用户ID
 @param mobile 手机号码
 */
- (instancetype)initWithUser:(NSString *)userId mobile:(NSString *)mobile;

@end

@interface GLUserMobileVerifyResponse : GLResponse

@end
