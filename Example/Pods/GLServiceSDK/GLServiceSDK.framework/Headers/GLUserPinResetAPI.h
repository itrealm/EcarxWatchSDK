//
//  GLUserPinResetAPI.h
//  Pods
//
//  Created by fish on 2017/5/11.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserPinResetRequest : GLRequest

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userPin;

/**
 Pincode setting

 @param userId userId
 @param pin    pin code
 */
- (instancetype)initWithUser:(NSString *)userId pin:(NSString *)pin;

@end

@interface GLUserPinResetResponse : GLResponse

@end
