//
//  GLUserPinStatusAPI.h
//  Pods
//
//  Created by fish on 2017/6/13.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserPinStatusRequest : GLRequest

@property (nonatomic, copy) NSString *userId;

/**
 Pincode setting verify

 @param userId userId
 */
- (instancetype)initWithUser:(NSString *)userId;

@end

@interface GLUserPinStatusResponse : GLResponse

@property (nonatomic, assign) BOOL verifyResult;

@end
