//
//  GLUserPinVerifyAPI.h
//  Pods
//
//  Created by fish on 2017/5/11.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserPinVerifyRequest : GLRequest

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userPin;

/**
 Pincode verify

 @param userId 用户ID
 @param pin    PIN码
 */
- (instancetype)initWithUser:(NSString *)userId pin:(NSString *)pin;

@end

@interface GLUserPinVerifyResponse : GLResponse

@property (nonatomic, assign) BOOL verifyResult;

@end
