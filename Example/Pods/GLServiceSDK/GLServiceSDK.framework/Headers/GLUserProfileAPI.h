//
//  GLUserProfileAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLUser.h"

@interface GLUserProfileRequest : GLRequest

/**
 查询用户信息

 @param userId 用户ID
 */
- (id)initWithUserId:(NSString *)userId;

@end


@interface GLUserProfileResponse : GLResponse

@property (nonatomic, copy) NSArray<GLUser> *list;

@end
