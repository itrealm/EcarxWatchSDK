//
//  GLUserProfileModifyAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLUser.h"

@interface GLUserProfileModifyRequest : GLRequest

/**
 修改用户信息

 @param profile 用户信息
 */
- (instancetype)initWithUserProfile:(GLUser *)profile;

@end

@interface GLUserProfileModifyResponse : GLResponse

@end
