//
//  GLUserPwdChangeAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserPwdChangeRequest : GLRequest

/**
 修改用户密码

 @param userId      用户ID
 @param oldPassword 旧密码
 @param password    新密码
 */
- (id)initWithUserId:(NSString *)userId oldPassword:(NSString *)oldPassword password:(NSString *)password;

@end

@interface GLUserPwdChangeResponse : GLResponse

@property (nonatomic, copy) NSString *isExistingUser;
@property (nonatomic, copy) NSString *vin;

@end
