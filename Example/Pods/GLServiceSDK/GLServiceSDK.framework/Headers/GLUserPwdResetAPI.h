//
//  GLUserPwdResetAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserResetPwdRequest : GLRequest

/**
 重置用户密码

 @param userId		用户ID
 @param password	用户密码
 @param mobile		手机号
 @param captcha		验证码
 */
- (id)initWithUserId:(NSString *)userId password:(NSString *)password mobile:(NSString *)mobile captcha:(NSString *)captcha;

@end

@interface GLUserResetPwdResponse : GLResponse

@property (nonatomic, copy) NSString *isExistingUser;
@property (nonatomic, copy) NSString *vin;

@end
