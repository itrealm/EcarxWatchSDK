//
//  GLUserPwdVerifyAPI.h
//  Pods
//
//  Created by fish on 2017/5/11.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserPwdVerifyRequest : GLRequest

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *password;

/**
 验证密码

 @param userId   用户ID
 @param password 用户密码
 */
- (instancetype)initWithUser:(NSString *)userId password:(NSString *)password;

@end

@interface GLUserPwdVerifyResponse : GLResponse

@property (nonatomic, assign) BOOL verifyResult;

@end
