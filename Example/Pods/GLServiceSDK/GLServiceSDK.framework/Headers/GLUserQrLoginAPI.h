//
//  GLUserQrLoginAPI.h
//  Pods
//
//  Created by 杨沁 on 2017/7/14.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserQrLoginRequest : GLRequest

/**
 扫码登录
 
 @param qrString 扫码参数
 */
- (id)initWithQrString:(NSString *)qrString;

@end

@interface GLUserQrLoginResponse : GLResponse

@end
