//
//  GLUserRegisterAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserRegisterRequest : GLRequest

/**
 用户注册

 @param account  用户名
 @param mobile   手机号
 @param password 密码
 */
- (instancetype)initWithAccount:(NSString *)account mobile:(NSString *)mobile password:(NSString *)password;

/**
 用户注册

 @param password  密码
 @param mobile    手机号
 */
- (instancetype)initWithMobile:(NSString *)mobile password:(NSString *)password;


@end

@interface GLUserRegisterResponse : GLResponse

@end
