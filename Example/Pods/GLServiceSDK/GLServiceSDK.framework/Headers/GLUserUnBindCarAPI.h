//
//  GLUserUnBindCarAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/12.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLUserUnBindCarRequest : GLRequest

/**
 解绑车辆

 @param userId 用户ID
 @param vin    VIN
 */
- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin;

@end

@interface GLUserUnBindCarResponse : GLResponse

@end
