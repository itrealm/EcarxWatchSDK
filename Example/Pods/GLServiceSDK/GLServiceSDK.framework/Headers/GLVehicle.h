//
//  GLVehicle.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "BaseModel.h"

@protocol GLVehicle;

typedef enum : NSInteger {
	GLVehicleTypeBEV  = 1,
	GLVehicleTypePHEV = 2,
	GLVehicleTypeFCEV = 3,
	GLVehicleTypeNEV  = 98,//新能源
	GLVehicleTypeOIL  = 99,//汽油车
} GLVehicleType;

@interface GLVehicle : BaseModel

//车系名称
@property (nonatomic, copy) NSString *seriesName;
@property (nonatomic, copy) NSString *seriesCodeVs;
@property (nonatomic, copy) NSString *modelCode;
@property (nonatomic, copy) NSString *modelCodeAs;

/**
 车辆标识符
 */
@property (nonatomic,  copy) NSString *vin;
@property (nonatomic,  copy) NSString *matCode;
@property (nonatomic,  copy) NSString *engineNo;
@property (nonatomic,  copy) NSString *iccid;
@property (nonatomic,  copy) NSString *imsi;
@property (nonatomic,  copy) NSString *msisdn;
@property (nonatomic,  copy) NSString *colorCode;
@property (nonatomic,  copy) NSString *colorName;
@property (nonatomic,  copy) NSString *fccode;
@property (nonatomic,  copy) NSString *factoryCode;
@property (nonatomic,  copy) NSNumber *recordTime;//车辆信息记录时间
@property (nonatomic,  copy) NSNumber *updateTime;//车辆信息更新时间
@property (nonatomic,  copy) NSNumber *createTime;//车辆信息创建时间

/**
 车牌号
 */
@property (nonatomic,  copy) NSString *plateNo;

/**
 车辆类型
 */
@property (nonatomic,assign) GLVehicleType vehicleType;
@property (nonatomic,  copy) NSString *energyStorageType;
@property (nonatomic,  copy) NSString *driveEngineVolt;
@property (nonatomic,  copy) NSString *driveEngineSpeed;
@property (nonatomic,  copy) NSString *driveEngineTorque;
@property (nonatomic,  copy) NSString *driveEngineNumber;
@property (nonatomic,  copy) NSString *driveEngineType;
@property (nonatomic,  copy) NSString *driveEngineLocation;
@property (nonatomic,  copy) NSString *driveEngineCoolingType;

/**
 车辆平台类型 CMA/NON-CMD
 */
@property (nonatomic,  copy) NSString *vehiclePlatform;
@property (nonatomic,  copy) NSString *vehiclePhotoBig;
@property (nonatomic,  copy) NSString *vehiclePhotoSmall;
@property (nonatomic,  copy) NSString *dealerId;

/**
 油箱容量
 */
@property (nonatomic,  copy) NSString *fuelTankCapacity;

//If need IHU confirm when bind vehicle
@property (nonatomic,assign) BOOL isIHUConfirm;
@property (nonatomic,  copy) NSString *ihuId;
@property (nonatomic,  copy) NSString *ihuVendor;
@property (nonatomic,  copy) NSString *ihuVersion;
@property (nonatomic,  copy) NSString *ihuSystemVersion;

@property (nonatomic,  copy) NSString *temId;
@property (nonatomic,  copy) NSString *temType;
@property (nonatomic,  copy) NSString *temVendor;
@property (nonatomic,  copy) NSString *temVersion;
@property (nonatomic,  copy) NSString *temSystemVersion;

//[online,offline]
@property (nonatomic,  copy) NSString *ecuOnlineState;

@property (nonatomic,  copy) NSString *tboxPlatform;
@property (nonatomic,  copy) NSString *ihuPlatform;

- (BOOL)isAuthenticated;

#pragma mark - 用于整合版本
//CSP平台账号状态 0：未注册、1：已注册，没密码、2：已注册，有密码
@property (nonatomic,  copy) NSString *cspAccountState;
//车辆所属平台 csp、tsp
@property (nonatomic,  copy) NSString *carFromSoure;

@end
