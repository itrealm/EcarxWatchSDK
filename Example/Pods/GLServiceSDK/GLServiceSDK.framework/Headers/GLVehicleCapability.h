//
//  GLVehicleCapability.h
//  Pods
//
//  Created by ecarx on 2017/4/6.
//
//

#import "BaseModel.h"

@protocol GLVehicleCapability;

@interface GLVehicleCapability : BaseModel

@property (nonatomic, strong) NSNumber *id;
@property (nonatomic,   copy) NSString *platform;
@property (nonatomic,   copy) NSString *modelCode;
@property (nonatomic,   copy) NSString *configCode;
@property (nonatomic,   copy) NSString *functionCategory;
@property (nonatomic,   copy) NSString *functionId;
@property (nonatomic, assign) BOOL      valueEnable;
@property (nonatomic,   copy) NSString *valueRange;
@property (nonatomic,   copy) NSString *valueEnum;
@property (nonatomic, strong) NSDate   *createTime;
@property (nonatomic, strong) NSDate   *updateTime;
@property (nonatomic,   copy) NSString *seriesCodeVs;
@property (nonatomic,   copy) NSString *showType;

@end
