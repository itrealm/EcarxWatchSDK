//
//  GLVehicleCapabilityAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/6.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLVehicleCapability.h"

@interface GLVehicleCapabilityRequest : GLRequest


/**
 查询车辆能力集

 @param vin vin
 @param vehicleType 车辆类型
 */
- (instancetype)initWithVin:(NSString *)vin vehicleType:(NSInteger)vehicleType;

@end

@interface GLVehicleCapabilityResponse : GLResponse

@property (nonatomic, copy) NSArray<GLVehicleCapability> *list;

@end
