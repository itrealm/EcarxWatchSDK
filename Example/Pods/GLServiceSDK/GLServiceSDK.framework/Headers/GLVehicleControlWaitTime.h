//
//  GLVehicleControlWaitTime.h
//  Pods
//
//  Created by ecarx on 2017/4/12.
//
//

#import "BaseModel.h"

@protocol GLVehicleControlWaitTime;

@interface GLVehicleControlWaitTime : BaseModel

@property (nonatomic, copy) NSString *reswaitTime;
@property (nonatomic, copy) NSString *rpcwaitTime;
@property (nonatomic, copy) NSString *rhlwaitTime;
@property (nonatomic, copy) NSString *rcewaitTime;
@property (nonatomic, copy) NSString *rwswaitTime;
@property (nonatomic, copy) NSString *jouwaitTime;
@property (nonatomic, copy) NSString *rdlwaitTime;
@property (nonatomic, copy) NSString *rtlwaitTime;
@property (nonatomic, copy) NSString *pm25;
@property (nonatomic, copy) NSString *rshwaitTime;
@property (nonatomic, copy) NSString *rduwaitTime;
@property (nonatomic, copy) NSString *rtuwaitTime;
@property (nonatomic, copy) NSString *diawaitTime;
@property (nonatomic, copy) NSString *rccwaitTime;

@end
