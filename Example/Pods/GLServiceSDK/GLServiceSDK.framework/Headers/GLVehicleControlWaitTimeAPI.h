//
//  GLVehicleControlWaitTimeAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/12.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLVehicleControlWaitTime.h"

@interface GLVehicleControlWaitTimeRequest : GLRequest

@end

@interface GLVehicleControlWaitTimeResponse : GLResponse

@property (nonatomic, copy) NSArray<GLVehicleControlWaitTime> *list;

@end
