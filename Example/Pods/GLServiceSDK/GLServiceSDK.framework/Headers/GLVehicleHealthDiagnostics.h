//
//  GLVehicleHealthDiagnostics.h
//  Pods
//
//  Created by fish on 2017/9/22.
//
//

#import "BaseModel.h"

@protocol GLVehicleHealthECU @end
@protocol GLVehicleHealthDiagnostics @end

@interface GLVehicleHealthDiagnostics : BaseModel

@property (nonatomic,   copy) NSString  *ecu;
@property (nonatomic, assign) NSInteger diagnosticResult;
@property (nonatomic,   copy) NSArray<GLVehicleHealthECU> *dtcs;

@end

@interface GLVehicleHealthECU : BaseModel

@property (nonatomic, copy)   NSString *dtcNumber;
@property (nonatomic, copy)   NSString *dtcNumberHex;
@property (nonatomic, copy)   NSString *dtcDescription;
@property (nonatomic, copy)   NSString *dtcDescriptionCN;
@property (nonatomic, copy)   NSString *repairSuggestion;
@property (nonatomic, copy)   NSString *repairSuggestionCN;
@property (nonatomic, assign) NSInteger dtcPriority;

@end
