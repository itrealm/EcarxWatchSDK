//
//  GLVehicleHealthHistory.h
//  Pods
//
//  Created by fish on 2017/9/22.
//
//

#import "BaseModel.h"
#import "GLVehicleHealthDiagnostics.h"

@protocol GLVehicleHealthHistory @end

@interface GLVehicleHealthHistory : BaseModel

@property (nonatomic,assign)long long sysTimestamp;
@property (nonatomic,assign)NSInteger healthScore;
@property (nonatomic,  copy)NSArray<GLVehicleHealthDiagnostics> *diagnostics;

@end
