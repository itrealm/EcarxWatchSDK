//
//  GLVehicleJournalLogAPI.h
//  Pods
//
//  Created by ecarx on 2017/3/31.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLJournalLog.h"

@interface GLVehicleJournalLogRequest : GLRequest

- (id)initWithUserId:(NSString *)userId
				 vin:(NSString *)vin
		   startTime:(NSDate *)startTime
			 endTime:(NSDate *)endTime;

@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) NSUInteger pageIndex;

@end

@interface GLVehicleJournalLogResponse : GLResponse

@property (nonatomic, copy) NSArray<GLJournalLog> *list;

@end
