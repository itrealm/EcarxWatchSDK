//
//  GLVehicleJournalLogSwitchAPI.h
//  Pods
//
//  Created by fish on 2017/5/30.
//
//

#import "GLVehicleTelematicsAPI.h"

@interface GLVehicleJournalLogSwitchRequest : GLVehicleTelematicsRequest

@end

@interface GLVehicleJournalLogSwitchResponse : GLVehicleTelematicsResponse

@end
