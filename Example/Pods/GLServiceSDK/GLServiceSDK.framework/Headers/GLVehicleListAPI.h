//
//  GLVehicleListAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLVehicle.h"

@interface GLVehicleListRequest : GLRequest


/**
 查询我的车辆列表

 @param userId 用户ID
 */
- (instancetype)initWithUserId:(NSString *)userId;

/**
 查询我的车辆列表

 @param userId 用户ID
 @param type   类型:[all]
 */
- (instancetype)initWithUserId:(NSString *)userId type:(NSString *)type;

/**
 查询车辆信息

 @param vin vin
 @param vehicleType 车辆类型
 */
- (instancetype)initWithVin:(NSString *)vin vehicleType:(NSString *)vehicleType;


/**
 通过用户手机号查询车辆

 @param mobile 手机号
 */
- (instancetype)initWithCustomerMobileNo:(NSString *)mobile;

@end

@interface GLVehicleListResponse : GLResponse

@property (nonatomic, copy) NSArray<GLVehicle> *list;

@end
