//
//  GLVehicleManager.h
//  GLServiceSDK
//
//  Created by fish on 2019/2/26.
//

#import <Foundation/Foundation.h>
#import "GLVehicleManagerProtocol.h"
#import "GLVehicleService.h"
#import "GLVehicleService+AppConfig.h"

@class GLVehicleService,GLVehicle;

#define GLVehicleDefaultManager [GLVehicleManager defaultManager]
#define GLVehicleDefaultService GLVehicleDefaultManager.defaultVehicleService

@interface GLVehicleManager : NSObject

@property(nonatomic,  copy,readonly)GLVehicle         *defaultVehicle;
@property(nonatomic,strong,readonly)GLVehicleService  *defaultVehicleService;

#pragma mark -

+ (instancetype)defaultManager;

#pragma mark -
/**
 获取车辆列表
 */
- (RACSignal<ExVehicleListResponse *> *)fetchVehicleList;

/**
 切换默认车辆
 @param vehicle 车辆信息
 */
- (RACSignal<ExDeviceSessionUpdateResponse *> *)switchDefaultVehicle:(GLVehicle *)vehicle;

/**
 绑定车辆

 @param request 请求
 */
- (RACSignal<ExResponse *> *)bindVehicle:(ExVehivleOperationRequest *)request;

/**
 解绑车辆

 @param request 请求
 */
- (RACSignal<ExDeviceSessionUpdateResponse *> *)unbindVehicle:(ExVehivleOperationRequest *)request;

#pragma mark -
/**
 获取车辆服务实例

 @param vehicle 车辆数据
 */
- (GLVehicleService *)serviceWithVehicle:(GLVehicle *)vehicle;
- (BOOL)resetDefaultVehicleService;

@end
