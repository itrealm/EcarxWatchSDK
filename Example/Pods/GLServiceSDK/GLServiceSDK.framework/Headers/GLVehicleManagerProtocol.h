//
//  GLVehicleBindProtocol.h
//  GLServiceSDK
//
//  Created by fish on 2019/3/11.
//

#import <Foundation/Foundation.h>
#import "BaseAPI.h"

NS_ASSUME_NONNULL_BEGIN

/** 切换默认车辆 */
#define GLDefaultVehicleDidSwitchNotification @"kDefaultVehicleDidSwitchNotification"
/** 默认车辆信息更新 */
#define GLDefaultVehicleDidUpdateNotification @"kDefaultVehicleDidUpdateNotification"

/** 绑定或解绑车辆 */
#define GLDefaultVehicleDidBindedNotification @"kDefaultVehicleDidBindedNotification"
#define GLDefaultVehicleDidUnBindNotification @"kDefaultVehicleDidUnBindNotification"

@class GLVehicle;

@protocol GLVehicleManagerProtocol <NSObject>
//默认车辆
@property(nonatomic,  copy)GLVehicle *vehicle;
//车辆列表
@property(nonatomic,  copy)NSArray   *vehicleList;
//获取车辆列表的错误
@property(nonatomic,strong)NSError   *fetchError;
//更新车辆列表操作
@property(nonatomic,strong)RACCommand *updateVehicleCommand;

- (BOOL)hasBindVehicleError;
- (BOOL)hasBindVehicle;
- (BOOL)hasBindVehiclePlateNo;
- (BOOL)hasConnectedVehicle;

//显示文字
- (NSString *)statusString;

- (void)navigationFromViewController:(UIViewController *)vc;

@end

NS_ASSUME_NONNULL_END
