//
//  GLVehicleModifyPlantNoAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/13.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLVehicleModifyPlantNoRequest : GLRequest

/**
 修改车牌号

 @param vin  vin
 @param type 车辆类型
 @param plate 车牌号
 */
- (instancetype)initWithVin:(NSString *)vin
				vehicleType:(NSNumber *)type
					plateNo:(NSString *)plate;

@end


@interface GLVehicleModifyPlantNoResponse : GLResponse

@end
