//
//  GLVehiclePM25.h
//  Pods
//
//  Created by fish on 2017/5/5.
//
//

#import "BaseModel.h"

@interface GLVehiclePM25 : BaseModel

@property (assign, nonatomic) CGFloat diff;
@property (assign, nonatomic) CGFloat inner;
@property (assign, nonatomic) CGFloat outside;
@property (assign, nonatomic) NSInteger stayTime;

@property (assign, nonatomic) NSInteger date;

@end

@protocol GLVehiclePM25 @end;
