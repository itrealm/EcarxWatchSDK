//
//  GLVehiclePM25API.h
//  Pods
//
//  Created by fish on 2017/5/5.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLVehiclePM25.h"

@interface GLVehiclePM25Request : GLRequest

- (instancetype)initWithUser:(NSString *)userId startDate:(NSDate *)start endDate:(NSDate *)end;

@end

@interface GLVehiclePM25Response : GLResponse

@property (nonatomic,strong,readwrite) NSArray<GLVehiclePM25> *list;

@end
