//
//  GLVehiclePickup.h
//  Pods
//
//  Created by ecarx on 2017/4/6.
//
//

#import "BaseModel.h"

@protocol GLVehiclePickup;

@interface GLVehiclePickup : BaseModel

@property (nonatomic,   copy) NSString *id;
@property (nonatomic,   copy) NSString *vin;
@property (nonatomic,   copy) NSString *registrationId;
@property (nonatomic,   copy) NSString *link;
@property (nonatomic, assign) NSString *status;
@property (nonatomic, strong) NSNumber *createTime;
@property (nonatomic, strong) NSNumber *updateTime;
@property (nonatomic, strong) NSNumber *expiredTime;

@end
