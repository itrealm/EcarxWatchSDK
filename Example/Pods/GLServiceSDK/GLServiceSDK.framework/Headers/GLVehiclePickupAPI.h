//
//  GLVehiclePickupAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/6.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLVehiclePickup.h"

@interface GLVehiclePickupRequest : GLRequest

@property(nonatomic,copy)NSString *alias;
@property(nonatomic,copy)NSString *type;


/**
 接送朋友

 @param vin            vin
 @param registrationId JPUSH
 @param platform       platform
 */
- (instancetype)initWithVin:(NSString *)vin registrationId:(NSString *)registrationId platform:(NSString *)platform;

@end

@interface GLVehiclePickupResponse : GLResponse

@property (nonatomic, copy) NSArray<GLVehiclePickup> *list;

@end
