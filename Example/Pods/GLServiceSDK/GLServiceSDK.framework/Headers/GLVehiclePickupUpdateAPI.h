//
//  GLVehiclePickupUpdateAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/6.
//
//

#import "GLRequest.h"
#import "GLResponse.h"

typedef enum : NSUInteger {
	GLVehiclePickupStatusStart, //正在接送
	GLVehiclePickupStatusCancel,//取消接送
} GLVehiclePickupStatus;

@interface GLVehiclePickupUpdateRequest : GLRequest


/**
 更新接送朋友状态

 @param Id     接送朋友数据ID
 @param status 取消接送
 @return	   请求对象
 */
- (instancetype)initWithId:(NSString *)Id status:(GLVehiclePickupStatus)status;

@end

@interface GLVehiclePickupUpdateResponse : GLResponse

@end
