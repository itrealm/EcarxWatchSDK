//
//  GLVehicleRanking.h
//  Pods
//
//  Created by fish on 2017/5/3.
//
//

#import "BaseModel.h"

@protocol GLVehicleRanking @end;

@interface GLVehicleRanking : BaseModel

@property (nonatomic,  copy) NSString *vin;
@property (nonatomic,  copy) NSString *userId;
@property (nonatomic,  copy) NSString *modelCode;
@property (nonatomic,assign) NSInteger orderNumber;
@property (nonatomic,assign) NSInteger sysTimestamp;
@property (nonatomic,assign) double    odometer;
@property (nonatomic,assign) double    aveFuelConsumption;

@end

/*
{
	"vin": "ecarx111122220007",
	"userId": "ecarx_test03",
	"modelCode": "KC-1BT",
	"orderNumber": 9,
	"sysTimestamp": 1492413168000,
	"odometer": 50,
	"aveFuelConsumption": 5.0
}
 */
