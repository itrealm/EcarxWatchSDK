//
//  GLVehicleRankingAveFuelAPI.h
//  Pods
//
//  Created by fish on 2017/5/3.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLVehicleRanking.h"

@interface GLVehicleRankingAveFuelRequest : GLRequest

@property (nonatomic,copy) NSString *modelCode;
@property (nonatomic,copy) NSString *type;    //0:能耗 1:油耗

- (instancetype)initWithVin:(NSString *)vin;

- (instancetype)initWithVin:(NSString *)vin
				   latitude:(double)latitude
				  longitude:(double)longitude
				   altitude:(double)altitude
					 radius:(double)radius;

@end

@interface GLVehicleRankingAveFuelResponse : GLResponse

@property (nonatomic, copy) GLVehicleRanking          *myRanking;
@property (nonatomic, copy) NSArray<GLVehicleRanking> *topRanking;

@end
