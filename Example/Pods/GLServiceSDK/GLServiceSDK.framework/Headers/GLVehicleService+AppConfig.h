//
//  GLVehicleService+AppConfig.h
//  GLServiceSDK
//
//  Created by fish on 2019/3/25.
//

#import "GLVehicleService.h"

typedef NS_ENUM(NSUInteger, GLCarEnergyType) {
	GLCarEnergyTypeUnknown = 0,        //未知
	GLCarEnergyTypeOil,                //汽油
	GLCarEnergyTypeBattery,            //电动
	GLCarEnergyTypeOilBatteryMix    //油电混合
};

//控制车模图片
typedef NS_ENUM(NSUInteger, GLExFunctionCarModel) {
	GLExFunctionCarModelCar = 1,
	GLExFunctionCarModelSUV = 2
};

NS_ASSUME_NONNULL_BEGIN

@interface GLVehicleService (AppConfig)

#pragma mark -
+ (NSString *)conversionModelCode:(NSString *)modelCode;

/**
 油量

 @param modelCode modelCode description
 @return return value description
 */
+ (NSInteger)getOilMassForVehicle:(NSString *)modelCode;
+ (NSInteger)oilMassForVehicle:(GLVehicle *)vehicle withCapabilitys:(NSArray<GLVehicleCapability> *)capabilitys;

/**
 功能:显示车内温度
 
 @return return BOOL
 */
- (BOOL)hasFunctionTempInside;

/**
 功能:行车日志开关
 */
- (BOOL)hasFunctionDrivingLogSwitch;

/**
 功能:接好友

 @return return BOOL
 */
- (BOOL)hasFunctionShuttleFriend;

+ (GLCarEnergyType)energyTypeForVehicle:(NSString *)modelCode OBJC_DEPRECATED("Use energyTypeForCapability: instead. 1.1.3+");
/// 根据能力集判断新能源车/混动车/油车
/// @param vehicleCapabilitys 能力集
+ (GLCarEnergyType)energyTypeForCapability:(NSArray<GLVehicleCapability> *)vehicleCapabilitys;

/**
 汽车类型

 @param modelCode modelCode
 */
+ (GLExFunctionCarModel)carModelForVehicle:(NSString *)modelCode;

#pragma mark -状态判断
//判断车门是开门
+ (BOOL)isDoorOpenForStauts:(NSNumber *)doorStatus;

//判断车门锁是打开
+ (BOOL)isDoorLockOpenForStatus:(NSNumber *)doorLockStatus;

//判断车窗是开窗
+ (BOOL)isWindowOpenForStauts:(NSString *)winStatus;

//通过判断所有门锁显示中控锁
+ (BOOL)isDoorAllLockDoorLock:(GLVehicleStatusDrivingSafety *)safety;

//判断车窗不是关闭
+ (BOOL)isSunroofNotCloseForStatus:(NSString *)sunroofStatus;

//判断天窗未知
+ (BOOL)isSunroofUnkownForStatus:(NSString *)sunroofStatus;

//判断后备箱是开启
+ (BOOL)isTrunkOpenForStatus:(NSNumber *)trunkOpenStatus;

//判断后备箱倒计时不使能(结束倒计时)
+ (BOOL)isTrunkPreCloseForStatus:(NSNumber *)trunkLockStatus;

//判断引擎盖是开启
+ (BOOL)isEngineHoodOpenForStatus:(NSNumber *)engineHoodStatus;

//判断引擎是否开启
+ (BOOL)isEngineOpenForStatus:(NSString *)engineStatus;

#pragma mark -位置上报状态
+ (BOOL)isPositionUploadStateForVehicle:(GLVehicle *)vehicle
						   vehicleState:(GLVehicleState *)vehicleState
						vehiclePosition:(GLPosition *)vehiclePosition;

@end

NS_ASSUME_NONNULL_END
