//
//  GLVehicleService.h
//  GLServiceSDK
//
//  Created by fish on 2019/3/21.
//

#import <Foundation/Foundation.h>
#import "GLTspServiceSDK.h"
#import "GLVehicleServiceDelegate.h"
#import "GLCspFunction.h"

typedef NS_ENUM(NSUInteger, GLVehicleEnergyType) {
	GLVehicleEnergyTypeUnknown = 0,        //未知
	GLVehicleEnergyTypeOil,                //汽油
	GLVehicleEnergyTypeBattery,            //电动
	GLVehicleEnergyTypeOilBatteryMix    //油电混合
};

@class GLVehicle,GLPosition;

NS_ASSUME_NONNULL_BEGIN

@class GLVehicleState,GLVehicleStatus;

@interface GLVehicleService : NSObject

@property(nonatomic, weak) id<GLVehicleServiceDelegate> delegate;

/** 基本车辆数据 */
@property(nonatomic,copy,readonly) GLVehicle *vehicle;

/** 能源类型 */
@property(nonatomic,assign,readonly) GLVehicleEnergyType energyType;

/** 油箱油量(L) */
@property(nonatomic,assign,readonly) NSInteger fuelTankCapacity;

@property(nonatomic,copy,readonly) GLVehicleState  *vehicleState;
@property(nonatomic,copy,readonly) GLVehicleStatus *vehicleStatus;
@property(nonatomic,copy,readonly) NSArray<GLVehicleCapability> *vehicleCapabilitys;
@property(nonatomic,copy,readwrite) NSString *formattedAddress; //车辆位置

#pragma mark -
- (instancetype)initWithVehicle:(GLVehicle *)vehicle;

#pragma mark - PIN
/**
 获取PIN码状态
 */
- (RACSignal<ExPinStatusData*> *)fetchPinStatus;

/**
 校验PIN码或用户密码

 @param request 请求
 */
- (RACSignal<ExResponse *> *)verifyPin:(ExPinVerifyRequest *)request;

/**
 重置PIN码

 @param pin     新PIN码
 */
- (RACSignal<ExResponse *> *)resetPin:(NSString *)pin;

#pragma mark - 能力集
/**
 获取车辆能力集
 */
- (RACSignal<GLVehicleCapabilityResponse *> *)fetchCapabilitys;

/**
 获取对应的能力集

 @param functionId 能力集ID
 @return 能力集数据
 */
- (GLVehicleCapability *)apabilityWithFunction:(GLVehicleFunctionName)functionId;

#pragma mark - 车控
/**
 远程控制

 @param request 请求
 */
- (RACSignal<ExResponse *> *)remoteControl:(ExVehicleTelematicsRequest *)request;

/**
 更新车辆状态
 */
- (RACSignal<GLVehicleStatusResponse *> *)fetchVehicleStatusWithLatest:(BOOL)latest;

/**
 更新车辆状态(属性状态)
 */
- (RACSignal<GLVehicleState *> *)fetchVehicleState;

#pragma mark - 预约充电
/**
 获取预约充电信息
 */
- (RACSignal *)fetchChargingReservation;
#pragma mark - 激活高阶车控
/**
 激活或关闭激活状态

 @param activate YES:激活 NO:关闭激活
 */
- (RACSignal<ExResponse *> *)switchTelematicsActivate:(BOOL)activate;
/**
 获取激活状态
 */
- (RACSignal<ExVehicleTelematicsActivateData *> *)fetchTelematicsActivateStatus;

#pragma mark - 发送到车
/**
 发送到车

 @param request 请求
 */
- (RACSignal<ExResponse *> *)sendToCar:(ExVehicleSendToCarRequest *)request;

#pragma mark - 行车日志
/**
 获取行车日志

 @param request 请求
 */
- (void)fetchJournalLog:(ExVehicleJournalLogRequest *)request;

/**
 更新行车日志开关

 @param enabled YES: 开启 NO:关闭
 */
- (RACSignal<ExResponse *> *)updateJournalLogSwitch:(BOOL)enabled;

#pragma mark - 排名
/**
 获取平均油耗排名

 @param request 请求
 */
- (void)fetchFuelRanking:(ExVehicleRankingAveFuelRequest *)request;
/**
 获取总里程排名

 @param request 请求
 */
- (void)fetchMileageRanking:(ExVehicleRankingOdometerRequest *)request;
/**
 获取平均电耗排名

 @param request 请求
 */
- (void)fetchEnergyRanking:(ExVehicleRankingAveEnergyRequest *)request;

#pragma mark - 电子围栏
/**
 获取电子围栏列表
 */
- (RACSignal<GLGeoFenceListResponse *> *)fetchGeoFenceList;

/**
 新增电子围栏

 @param geoFence 数据
 */
- (RACSignal<ExResponse *> *)addGeoFence:(ExGeoFence *)geoFence;

/**
 更新电子围栏

 @param geoFence 数据
 */
- (RACSignal<ExResponse *> *)updateGeoFence:(ExGeoFence *)geoFence;

/**
 删除电子围栏

 @param Id 电子围栏Id
 */
- (RACSignal<ExResponse *> *)deleteGeoFence:(NSString *)Id;

#pragma mark -
/**
 获取流量信息
 */
- (RACSignal<ExFlowResponse *> *)fetchFlowInfo;

/**
 修改车牌号码

 @param plateNo 车牌号
 */
- (RACSignal<ExResponse *> *)modifyPlateNo:(NSString *)plateNo;

@end

NS_ASSUME_NONNULL_END
