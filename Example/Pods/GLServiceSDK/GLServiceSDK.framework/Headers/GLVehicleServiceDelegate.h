//
//  GLVehicleServiceDelegate.h
//  GLServiceSDK
//
//  Created by fish on 2019/3/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class GLVehicleService;

@protocol GLVehicleServiceDelegate <NSObject>

//车辆状态已更新
- (void)vehicleStatusDidUpdated:(GLVehicleService *)service;

@end

NS_ASSUME_NONNULL_END
