//
//  GLVehicleState.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "BaseModel.h"

@protocol GLVehicleState;

typedef enum : NSInteger {
	GLActivateStateNonActivated = 0,//非激活
	GLActivateStateActivated    = 1 //激活
} GLActivateState;

typedef enum : NSInteger {
	GLInhibitionStateDisable = 0,
	GLInhibitionStateEnable  = 1
} GLInhibitionState;

typedef enum : NSInteger {
	GLImmobilizationStateFalse = 0,
	GLImmobilizationStateTrue  = 1
} GLImmobilizationState;

typedef enum : NSInteger {
	GLEngineStateOff = 0,
	GLEngineStateRunning = 1
} GLEngineState;

typedef enum : NSInteger {
	GLPowerModeNormal = 0,
	GLPowerModeStandby,
	GLPowerModeSleep,
	GLPowerModeOff
} GLPowerMode;

typedef enum : NSInteger {
	GLSvtStateOff = 0,
	GLSvtStateOn,
} GLSvtState;

typedef enum : NSInteger {
	GLJourlogLogStateOff = 0,
	GLJourlogLogStateOn,
} GLJourlogLogState;

typedef enum : NSUInteger {
	GLPositionUploadStateOff = 0,
	GLPositionUploadStateOn
} GLPositionUploadState;

@interface GLVehicleState : BaseModel

@property (nonatomic,   copy) NSString 			   *vin;
@property (nonatomic,   copy) NSString 			   *pin;
@property (nonatomic,   copy) NSString             *svtStartTime;
@property (nonatomic,   copy) NSString             *svtEndTime;
@property (nonatomic, assign) GLActivateState       activateState;
@property (nonatomic, assign) GLInhibitionState     inhibitionState;
@property (nonatomic, assign) GLImmobilizationState immobilizationState;
@property (nonatomic, assign) GLSvtState            svtState;
@property (nonatomic, assign) GLJourlogLogState     journalLogState;
@property (nonatomic, assign) GLEngineState 		engineState;
@property (nonatomic, assign) GLPowerMode   		powerMode;
@property (nonatomic,   copy) NSString     		   *powerNextWakeupTime;
@property (nonatomic, assign) NSInteger 			maintenanceState;
@property (nonatomic, assign) NSInteger  			advancedRemoteControlState;
@property (nonatomic, assign) GLPositionUploadState positionUploadState;

@end
