//
//  GLVehicleStateAPI.h
//  Pods
//
//  Created by ecarx on 2017/4/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLVehicleState.h"

@interface GLVehicleStateRequest : GLRequest

/**
 获取车的动态状态查询

 @param userId 用户ID
 @param vin    车辆标识
 @return	   请求对象
 */
- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin;

@end

@interface GLVehicleStateResponse : GLResponse

@property (nonatomic, copy) NSArray<GLVehicleState> *list;

@end

