//
//  GLVehicleStatus.h
//  Pods
//
//  Created by ecarx on 2017/3/31.
//
//

#import "BaseModel.h"
#import "GLVehicleStatusAdditional.h"
#import "GLVehicleStatusBasic.h"

@protocol GLVehicleStatus
@end

@interface GLVehicleStatus : BaseModel

//上报状态时间,时间戳【毫秒】
@property (nonatomic, copy) NSNumber *updateTime;
@property (nonatomic, copy) GLVehicleStatusBasic      *basicVehicleStatus;
@property (nonatomic, copy) GLVehicleStatusAdditional *additionalVehicleStatus;

@end
