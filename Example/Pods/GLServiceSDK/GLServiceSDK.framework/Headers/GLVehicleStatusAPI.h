//
//  GLVehicleStatusAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLTelematicsResult.h"
#import "GLVehicleStatus.h"

@interface GLVehicleStatusRequest : GLRequest

/**
 查询车辆状态

 @param userId 用户ID
 @param vin    vin
 */
- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin;

/**
 查询车辆状态

 @param userId 用户ID
 @param vin    vin
 @param latest YES:先从TSC获取,如超过10秒,到TEM获取 NO:从TSC获取
 @return 请求
 */
- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin latest:(BOOL)latest;

/**
 查询车辆状态

 @param userId 用户ID
 @param vin    vin
 @param target basic,more
 @param latest YES:先从TSC获取,如超过10秒,到TEM获取 NO:从TSC获取
 */
- (instancetype)initWithUserId:(NSString *)userId vin:(NSString *)vin target:(NSString *)target latest:(BOOL)latest;

@end

@interface GLVehicleStatusResponse : GLResponse

@property (nonatomic, copy) GLTelematicsResult *result;
@property (nonatomic, copy) GLVehicleStatus    *vehicleStatus;

@end
