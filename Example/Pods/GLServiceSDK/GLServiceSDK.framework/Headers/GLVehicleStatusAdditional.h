//
//  GLVehicleStatusAdditional.h
//  Pods
//
//  Created by fish on 2017/4/24.
//
//

#import "BaseModel.h"
#import "GLVehicleStatusClimate.h"
#import "GLVehicleStatusElectricVehicle.h"
#import "GLVehicleStatusDrivingSafety.h"
#import "GLVehicleStatusMaintenance.h"
#import "GLVehicleStatusRunning.h"
#import "GLVehicleStatusClimate.h"
#import "GLVehicleStatusPollution.h"
#import "GLVehicleStatusDrivingBehaviour.h"

@interface GLVehicleStatusAdditional : BaseModel

//electric
@property (nonatomic, copy) GLVehicleStatusElectricVehicle *electricVehicleStatus;

//driving
@property (nonatomic, copy) GLVehicleStatusDrivingSafety *drivingSafetyStatus;

//main
@property (nonatomic, copy) GLVehicleStatusMaintenance *maintenanceStatus;

//running
@property (nonatomic, copy) GLVehicleStatusRunning *runningStatus;

//climate
@property (nonatomic, copy) GLVehicleStatusClimate *climateStatus;

//drving behaviore
@property (nonatomic, copy) GLVehicleStatusDrivingBehaviour *drivingBehaviourStatus;

//pm2.5
@property (nonatomic, copy) GLVehicleStatusPollution *pollutionStatus;

@end
