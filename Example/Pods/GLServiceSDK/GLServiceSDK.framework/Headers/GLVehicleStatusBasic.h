//
//  GLVehicleStatusBasic.h
//  Pods
//
//  Created by fish on 2017/4/24.
//
//

#import "BaseModel.h"
#import "GLPosition.h"



@interface GLVehicleStatusBasic : BaseModel

//当前定位
@property (nonatomic, copy) GLPosition *position;

//引擎状态["ENGINE_RUNNING","ENGINE_OFF"]
@property (nonatomic, copy) NSString *engineStatus;

//可续航里程
@property (nonatomic, copy) NSNumber *distanceToEmpty;

@end


