//
//  GLVehicleStatusClimate.h
//  Pods
//
//  Created by fish on 2017/4/24.
//
//

#import "BaseModel.h"

//座椅加热状态
typedef NS_ENUM(NSInteger, GLVehicleSeatHeatStatus) {
	GLVehicleSeatHeatStatusOff 	     = 0, //关闭
	GLVehicleSeatHeatStatusLevel1    = 1, //1级
	GLVehicleSeatHeatStatusLevel2    = 2, //2级
	GLVehicleSeatHeatStatusLevel3    = 3, //3级
	GLVehicleSeatHeatStatusNoRequest = 7
};

//座椅透气状态
typedef NS_ENUM(NSInteger, GLVehicleSeatVentStatus) {
	GLVehicleSeatVentStatusOff 	     = 0,//关闭
	GLVehicleSeatVentStatusLevel1    = 1,//1级
	GLVehicleSeatVentStatusLevel2    = 2,//2级
	GLVehicleSeatVentStatusLevel3    = 3,//3级
	GLVehicleSeatVentStatusNoRequest = 7
};

//空气净化状态
typedef NS_ENUM(NSInteger,GLVehicleAirCleanStatus) {
	GLVehicleAirCleanStatusOff = 0, //关闭
	GLVehicleAirCleanStatusOn  = 1  //打开
};

//空调状态
typedef NS_ENUM(NSInteger,GLVehiclePreClimateStatus) {
	GLVehiclePreClimateStatusOff = 0, //关闭
	GLVehiclePreClimateStatusOn  = 1  //打开
};

//天窗状态
typedef NS_ENUM(NSInteger, GLVehicleSunRoofStatus) {
	GLVehicleSunRoofStatusUnknow = 0,//Position unknow/invalid
	GLVehicleSunRoofStatusClosed = 1,//Completely closed
	GLVehicleSunRoofStatusOpened = 2 //Completely opened
};

//车窗状态
typedef NS_ENUM(NSInteger, GLVehicleWindowStatus) {
	GLVehicleWindowStatusMiddle  = 0,//Middle position
	GLVehicleWindowStatusOpened  = 1,//Complete open
	GLVehicleWindowStatusClosed  = 2,//Complete closed
	GLVehicleWindowStatusInvalid = 3 //Invalid
};

//遮阳帘状态
typedef NS_ENUM(NSInteger, GLVehicleCurtainStatus) {
    GLVehicleCurtainStatusUnkown = 0,
    GLVehicleCurtainStatusClosed = 1,
    GLVehicleCurtainStatusOpened = 2
};

@interface GLVehicleStatusClimate : BaseModel


@property (nonatomic,copy) NSString *interiorTemp;      //车内温度
@property (nonatomic,copy) NSString *exteriorTemp;      //车外温度

//车窗状态
@property (nonatomic,copy) NSString *sunroofOpenStatus;      //天窗开启状态
@property (nonatomic,copy) NSString *winStatusDriver;        //左前车窗状态
@property (nonatomic,copy) NSString *winStatusPassenger;     //右前车窗状态
@property (nonatomic,copy) NSString *winStatusPassengerRear; //右后车窗状态
@property (nonatomic,copy) NSString *winStatusDriverRear;    //左后车窗状态

//车窗开启百分比
@property (nonatomic,copy) NSString *sunroofPos;
@property (nonatomic,copy) NSString *winPosDriver;
@property (nonatomic,copy) NSString *winPosPassenger;
@property (nonatomic,copy) NSString *winPosPassengerRear;
@property (nonatomic,copy) NSString *winPosDriverRear;

/**
 车窗位置未关窗告
 0x0: invalid
 0x1: valid
 */
@property (nonatomic,copy) NSString *sunroofOpenStatusWarning;
@property (nonatomic,copy) NSString *winStatusDriverWarning;
@property (nonatomic,copy) NSString *winStatusPassengerWarning;
@property (nonatomic,copy) NSString *winStatusPassengerRearWarning;
@property (nonatomic,copy) NSString *winStatusDriverRearWarning;

//一键透气状态
@property (nonatomic,assign) BOOL ventilateStatus;

//座椅加热状态
@property (nonatomic,assign) GLVehicleSeatHeatStatus drvHeatSts;    	//driver seat heating status
@property (nonatomic,assign) GLVehicleSeatHeatStatus passHeatingSts;	//front passenger seat heating status
@property (nonatomic,assign) GLVehicleSeatHeatStatus rlHeatingSts;  	//left rear seat heating status
@property (nonatomic,assign) GLVehicleSeatHeatStatus rrHeatingSts;  	//right rear seat heating status

//座椅透气状态
@property (nonatomic,assign) GLVehicleSeatVentStatus drvVentSts;		//driver seat ventilation status
@property (nonatomic,assign) GLVehicleSeatVentStatus passVentSts;		//front passenger seat ventilation status
@property (nonatomic,assign) GLVehicleSeatVentStatus rrVentSts;			//right rear seat ventilation status
@property (nonatomic,assign) GLVehicleSeatVentStatus rlVentSts;			//left rear seat ventilation status

//空气净化状态
@property (nonatomic,assign) GLVehicleAirCleanStatus airCleanSts;

//空调开关状态(BOOL) YES:开启 NO:关闭
@property (nonatomic,assign) BOOL preClimateActive;

//遮阳帘状态(GLVehicleCurtainStatus)
@property (nonatomic,copy) NSNumber *curtainOpenStatus;
//遮阳帘开启百分比[0-100]
@property (nonatomic,copy) NSNumber *curtainPos;
//遮阳帘未关警告 0x0: invalid 0x1: valid
@property (nonatomic,copy) NSNumber *curtainWarning;

@end
