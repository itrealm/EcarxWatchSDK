//
//  GLVehicleStatusDrivingBehaviour.h
//  Pods
//
//  Created by fish on 2017/4/24.
//
//

#import "BaseModel.h"

@interface GLVehicleStatusDrivingBehaviour : BaseModel

//仪表显示的档位状态
@property (nonatomic,copy) NSNumber *transimissionGearPostion;
@property (nonatomic,copy) NSString *cruiseControlStatus;
@property (nonatomic,copy) NSNumber *engineSpeed;
@property (nonatomic,copy) NSNumber *brakePedalDepressed;

@end
