//
//  GLVehicleStatusDrivingSafety.h
//  Pods
//
//  Created by fish on 2017/4/24.
//
//

#import "BaseModel.h"

//车门状态
typedef NS_ENUM(NSInteger, GLVehicleDoorStatus) {
	GLVehicleDoorStatusClosed = 0,//Door closed
	GLVehicleDoorStatusAjar   = 1 //Door ajar
};

//车门锁门状态
//0x0: Unlocked
//0x1: Locked
//0x2: Reserved
//0x3: Error (Reserved
typedef NS_ENUM(NSInteger, GLVehicleDoorLock) {
	GLVehicleDoorUnlocked = 0,//未锁
	GLVehicleDoorLocked   = 1,//上锁
	GLVehicleDoorReserved = 2,//保留
	GLVehicleDoorError    = 3
};

//后备箱
typedef NS_ENUM(NSInteger, GLVehicleTrunkStatus) {
	GLVehicleTrunkStatusClosed = 0,//关闭
	GLVehicleTrunkStatusAjar   = 1 //半开
};

//引擎盖
typedef NS_ENUM(NSInteger,GLVehicleEngineHoodStatus) {
	GLVehicleEngineHoodStatusClosed = 0, //关闭
	GLVehicleEngineHoodStatusAjar   = 1, //半开
	GLVehicleEngineHoodStatusOpened = 1, //打开
	GLVehicleEngineHoodStautsUnkown = 2  //未知
};

@interface GLVehicleStatusDrivingSafety : BaseModel

/**
 中控锁状态
 */
@property (nonatomic,copy) NSString *centralLockingStatus;

/**
 车门开启状态
 */
@property (nonatomic,copy) NSString *doorOpenStatusDriver;
@property (nonatomic,copy) NSString *doorOpenStatusPassenger;
@property (nonatomic,copy) NSString *doorOpenStatusDriverRear;
@property (nonatomic,copy) NSString *doorOpenStatusPassengerRear;

/**
 车门打开百分比
 */
@property (nonatomic,copy) NSString *doorPosDriver;
@property (nonatomic,copy) NSString *doorPosPassenger;
@property (nonatomic,copy) NSString *doorPosDriverRear;
@property (nonatomic,copy) NSString *doorPosPassengerRear;

/**
 车门把手状态
 */
@property (nonatomic,copy) NSString *doorGripStatusDriver;
@property (nonatomic,copy) NSString *doorGripStatusPassenger;
@property (nonatomic,copy) NSString *doorGripStatusDriverRear;
@property (nonatomic,copy) NSString *doorGripStatusPassengerRear;

/**
 * 车门锁门状态
 */
@property (nonatomic,copy) NSString *doorLockStatusPassengerRear;
@property (nonatomic,copy) NSString *doorLockStatusDriverRear;
@property (nonatomic,copy) NSString *doorLockStatusPassenger;
@property (nonatomic,copy) NSString *doorLockStatusDriver;

/**
 * 后备箱状态
 */
@property (nonatomic,copy) NSString *trunkOpenStatus;

/**
 * 后备箱倒计时使能状态
 * 0: 使能(可以解锁）
 * 1: 不使能（倒计时结束）
 */
@property (nonatomic,copy) NSString *trunkLockStatus;

/**
 * 引擎盖
 */
@property (nonatomic,copy) NSString *engineHoodOpenStatus;

/**
 * 电子手刹状态
 * 0:未驻车
 * 1:已驻车
 * 2:驻车过程中
 */
@property (nonatomic,copy) NSString *electricParkBrakeStatus;

/**
 安全带
 */
@property (nonatomic,copy) NSString *seatBeltStatusDriver;
@property (nonatomic,copy) NSString *seatBeltStatusDriverRear;
@property (nonatomic,copy) NSString *seatBeltStatusPassengerRear;
@property (nonatomic,copy) NSString *seatBeltStatusPassenger;

/*
 私密锁状态
 0：invalid
 1：valid
 */
@property (nonatomic,copy) NSString *privateLockStatus;

/**
 加油盖状态(0..15)
 0:未知
 1:开
 2:关
 */
@property (nonatomic,copy) NSString *tankFlapStatus;

@end
