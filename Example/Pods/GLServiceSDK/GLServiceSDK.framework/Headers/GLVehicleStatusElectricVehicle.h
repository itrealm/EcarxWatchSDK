//
//  GLVehicleStatusElectricVehicle.h
//  AFNetworking
//
//  Created by 杨沁 on 2017/10/12.
//

#import "BaseModel.h"

typedef NS_ENUM(NSUInteger,GLChargingStatus) {
	GLChargingStatusDefault    = 0, //默认值
	GLChargingStatusCharging   = 1, //正在充电
	GLChargingStatusComplete   = 2, //充满电结束
	GLChargingStatusFailure    = 3, //充电故障
	GLChargingStatusHeating    = 4, //正在加热
	GLChargingStatusPause      = 5, //充电暂停
	GLChargingStatusWaitPreset = 6  //预约充电等待
};

//预约充电状态
typedef NS_ENUM(NSUInteger,GLChargeStatus) {
	GLChargeStatusNotAppoint    = 0,//未预约
	GLChargeStatusAppointing    = 1,//预约中
	GLChargeStatusAppointFinish = 2 //预约完成
};

//外部能量状态(CP信号)
typedef NS_ENUM(NSInteger,GLChargerConnection) {
	GLChargerConnectionNotConnetction = 0,           	//未连接
	GLChargerConnectionS2LookUpStateFailure = 1,	    //查找失败
	GLChargerConnectionPowAvailableButNotActivated = 2, //未激活
	GLChargerConnectionContectedWithPower = 3,		    //已通电
	GLChargerConnectionCPSignalFailure = 7			    //信号故障
};

@interface GLVehicleStatusElectricVehicle : BaseModel

//剩余电量百分比[0-100]
@property (nonatomic,copy) NSString *chargeLevel;
//电量续航(KM)
@property (nonatomic,copy) NSString *distanceToEmptyOnBatteryOnly;
//平均百公里能耗(kW·h/100km)
@property (nonatomic,copy) NSString *averPowerConsumption;
//正在充电
@property (nonatomic,copy) NSString *isCharging;
//已插充电枪状态
@property (nonatomic,copy) NSString *isPluggedIn;
//充电状态:GLChargingStatus
@property (nonatomic,copy) NSString *stateOfCharge;
//通电状态:GLChargerConnection
@property (nonatomic,copy) NSString *statusOfChargerConnection;
//预约充电状态:GLChargeStatus
@property (nonatomic,copy) NSString *chargeSts;
//充满剩余充电时间(分钟)
@property (nonatomic,copy) NSString *timeToFullyCharged;

//充电盖状态 0:未知 1:开 2:关
@property (nonatomic,copy) NSString *chargeLidStatus;
//前面充电盖状态 0:未知 1:开 2:关
@property (nonatomic,copy) NSString *chargeLidFrontStatus;
//后面充电盖状态 0:未知 1:开 2:关
@property (nonatomic,copy) NSString *chargeLidRearStatus;

//直流充电枪连接状态
@property (nonatomic,copy) NSString *dcDcConnectStatus;
//直流充电状态
@property (nonatomic,copy) NSString *dcChargeSts;
//直流充电电流
@property (nonatomic,copy) NSString *dcChargeIAct;

//交流充电电流
@property (nonatomic,copy) NSString *chargeIAct;
//交流充电电压
@property (nonatomic,copy) NSString *chargeUAct;

//放电连接状态
@property (nonatomic,copy) NSString *disChargeConnectStatus;
//放电状态
@property (nonatomic,copy) NSString *disChargeSts;
//放电电压
@property (nonatomic,copy) NSString *disChargeUAct;
//放电电流
@property (nonatomic,copy) NSString *disChargeIAct;

@end
