//
//  GLVehicleStatusHistoryAPI.h
//  Pods
//
//  Created by 杨沁 on 2017/9/20.
//
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLVehicleStatus.h"

@interface GLVehicleStatusHistoryRequest : GLRequest

@property (nonatomic, copy, readonly) NSString *vin;
@property (nonatomic, copy, readonly) NSNumber *startTime;
@property (nonatomic, copy, readonly) NSNumber *endTime;

- (id)initWithVin:(NSString *)vin
        startTime:(NSNumber *)startTime
          endTime:(NSNumber *)endTime;

@end

@interface GLVehicleStatusHistoryResponse : GLResponse

@property (nonatomic, copy) NSArray<GLVehicleStatus> *list;

@end
