//
//  GLVehicleStatusMainBattery.h
//  Pods
//
//  Created by fish on 2017/4/24.
//
//

#import "BaseModel.h"

@interface GLVehicleStatusMainBattery : BaseModel

//电量状态 0:正常 1:预警
@property (nonatomic,copy) NSNumber *stateOfCharge;
//电压(单位V)
@property (nonatomic,copy) NSNumber *voltage;
//电量百分比[0-100]
@property (nonatomic,copy) NSString *chargeLevel;
//健康状态 0:正常，1:有问题，7:未知
@property (nonatomic,copy) NSString *stateOfHealth;

@end
