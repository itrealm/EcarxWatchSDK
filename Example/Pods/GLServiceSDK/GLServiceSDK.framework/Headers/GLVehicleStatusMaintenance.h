//
//  GLVehicleStatusMaintenance.h
//  Pods
//
//  Created by fish on 2017/4/24.
//
//

#import "BaseModel.h"

@class GLVehicleStatusMainBattery;

@interface GLVehicleStatusMaintenance : BaseModel

/** 总里程,单位: km */
@property (nonatomic,copy) NSNumber *odometer;

/** 轮胎胎压告警 0:正常 1:预警 2:告警 */
@property (nonatomic,copy) NSNumber *tyrePreWarningDriver;
@property (nonatomic,copy) NSNumber *tyrePreWarningDriverRear;
@property (nonatomic,copy) NSNumber *tyrePreWarningPassenger;
@property (nonatomic,copy) NSNumber *tyrePreWarningPassengerRear;

/** 四轮轮胎胎压数字 */
@property (nonatomic,copy) NSNumber *tyreStatusDriver;        //左前
@property (nonatomic,copy) NSNumber *tyreStatusDriverRear;    //左后
@property (nonatomic,copy) NSNumber *tyreStatusPassenger;     //右前
@property (nonatomic,copy) NSNumber *tyreStatusPassengerRear; //右后

/** 轮胎温度 */
@property (nonatomic,copy) NSNumber *tyreTempDriver;
@property (nonatomic,copy) NSNumber *tyreTempPassenger;
@property (nonatomic,copy) NSNumber *tyreTempDriverRear;
@property (nonatomic,copy) NSNumber *tyreTempPassengerRear;

/** 轮胎温度告警 0:正常 1:预警 2:告警 */
@property (nonatomic,copy) NSNumber *tyreTempWarningDriver;
@property (nonatomic,copy) NSNumber *tyreTempWarningPassenger;
@property (nonatomic,copy) NSNumber *tyreTempWarningDriverRear;
@property (nonatomic,copy) NSNumber *tyreTempWarningPassengerRear;

@property (nonatomic,copy) NSString *washerFluidLevelStatus;
@property (nonatomic,copy) NSString *brakeFluidLevelStatus;

/** 保养剩余里程 */
@property (nonatomic,copy) NSNumber *distanceToService;
@property (nonatomic,copy) NSString *daysToService;
@property (nonatomic,copy) NSString *serviceWarningTrigger;
@property (nonatomic,copy) NSString *serviceWarningStatus;
@property (nonatomic,copy) NSString *engineHrsToService;

/** 主电池状态 */
@property (nonatomic,copy) GLVehicleStatusMainBattery *mainBatteryStatus;

@end
