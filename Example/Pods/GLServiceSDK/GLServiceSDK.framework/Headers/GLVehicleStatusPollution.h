//
//  GLVehicleStatusPollution.h
//  Pods
//
//  Created by fish on 2017/4/25.
//
//

#import "BaseModel.h"

@interface GLVehicleStatusPollution : BaseModel

/**
 1.车内空气PM2.5,单位:μg/m3
 2.车内空气质量 1.差 2.良 3.优
 */
@property (nonatomic, copy) NSNumber *exteriorPM25;

/**
 车外空气PM2.5,单位:μg/m3
 */
@property (nonatomic, copy) NSNumber *interiorPM25;
/**
 车外空气PM2.5质量等级
 */
@property (nonatomic, copy) NSNumber *exteriorPM25Level;

/**
 车外空气PM2.5质量等级
 */
@property (nonatomic, copy) NSNumber *interiorPM25Level;

/**
 二氧化碳浓度报警
 0:Off
 1:Lvl1
 2:Lvl2
 3:Lvl3
 */
@property (nonatomic, copy) NSNumber *interCO2Warning;

/**
 空气质量指数
 */
@property (nonatomic, copy) NSNumber *airQualityIndex;

/**
 空气粒子浓度
 */
@property (nonatomic, copy) NSNumber *airParticleConcentration;

/*
 空气湿度
 */
@property (nonatomic, copy) NSNumber *relHumSts;

@end
