//
//  GLVehicleStatusRunning.h
//  Pods
//
//  Created by fish on 2017/4/24.
//
//

#import "BaseModel.h"

@interface GLVehicleStatusRunning : BaseModel

@property (nonatomic,copy) NSString *bulbStatus;

/**
 机油健康 0:机油油压健康 1:机油油压告警
 */
@property (nonatomic,copy) NSString *engineOilPressureWarning;
@property (nonatomic,copy) NSString *engineOilTemperature;
@property (nonatomic,copy) NSString *engineOilLevelStatus;

//发动机冷却液温度 单位：摄氏度
@property (nonatomic,copy) NSNumber *engineCoolantTemperature;
//发动机冷却液液量 0:正常，1:低，2:很低，7:未知
@property (nonatomic,copy) NSString *engineCoolantLevelStatus;
@property (nonatomic,copy) NSString *engineCoolantTemperatureValidity;

/**
 平均百公里油耗,单位:L/100KM
 */
@property (nonatomic,copy) NSNumber *aveFuelConsumption;

/**
 最近一次基于总里程的平均油耗,单位:L/100KM
 */
@property (nonatomic,copy) NSNumber *aveFuelConsumptionInLatestDrivingCycle;

/**
 平均车速,单位:km/h
 */
@property (nonatomic,copy) NSNumber *avgSpeed;

/**
 剩余油量,单位:L
 */
@property (nonatomic,copy) NSNumber *fuelLevel;

/**
 剩余油量状态,
 0:正常，
 1:低，
 2:高，
 7:未知
 */
@property (nonatomic,copy) NSString *fuelLevelStatus;
/**
 小计里程1
 */
@property (nonatomic,copy) NSNumber *tripMeter1;

/**
 小计里程2
 */
@property (nonatomic,copy) NSString *tripMeter2;

/**
 低油量报警
 */
@property (nonatomic,copy) NSString *fuelLow1WarningDriver;
@property (nonatomic,copy) NSString *fuelLow2WarningDriver;

@end
