//
//  GLVehicleTelematicsAPI.h
//  EricssonTcGeelyProject
//
//  Created by ecarx on 2017/3/29.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"
#import "GLOperationSchedule.h"
#import "GLServiceParameters.h"

@interface GLVehicleTelematicsRequest : GLRequest

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *command;
@property (nonatomic, copy) NSString *sessionId;
@property (nonatomic, copy) NSString *pin;
@property (nonatomic, copy) GLOperationSchedule *operationScheduling;
@property (nonatomic, copy) NSArray<GLServiceParameters> *serviceParameters;
@property (nonatomic, assign) BOOL latest;

- (instancetype)initWithVin:(NSString *)vin serviceId:(NSString *)serviceId;

@end

@interface GLVehicleTelematicsResponse : GLCmdResponse

@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *serviceId;
@property (nonatomic, copy) NSString *sessionId;
@property (nonatomic, copy) NSString *taskId;
@property (nonatomic, copy) NSString *diagnostics;
@property (nonatomic, copy) NSString *trackPoint;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *pollutionStatus;
@property (nonatomic, copy) NSString *group;
@property (nonatomic, copy) NSString *chargingStatus;
@property (nonatomic, copy) NSString *platform;

@end
