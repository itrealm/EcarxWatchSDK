//
//  GLVehicleTelematicsActivateAPI.h
//  Pods
//
//  Created by ecarx on 2017/3/31.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLVehicleTelematicsActivateRequest : GLRequest

@property (nonatomic, copy,readonly) NSString *userId;
@property (nonatomic, copy,readonly) NSString *vin;
@property (nonatomic, copy,readonly) NSString *pin;
@property (nonatomic, copy,readonly) NSString *clientIdentity;
@property (nonatomic,assign,readonly) BOOL     activate;

/**
 切换远程控件开关

 @param userId    用户ID
 @param identity  客户端标识,GLUserLoginResponse中alias
 @param vin       vin
 @param pin       pin
 @param activate  YES:开启 NO:关闭
 @return          请求
 */
- (instancetype)initWithUserId:(NSString *)userId
				clientIdentity:(NSString *)identity
						   vin:(NSString *)vin
						   pin:(NSString *)pin
					  activate:(BOOL)activate;

@end

@interface GLVehicleTelematicsActivateResponse : GLResponse

@end
