//
//  GLVehicleTelematicsActivateQueryAPI.h
//  Pods
//
//  Created by ecarx on 2017/3/31.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLRequest.h"
#import "GLResponse.h"

@interface GLVehicleTelematicsActivateQueryRequest : GLRequest

@property (nonatomic,copy,readonly) NSString *vin;

- (id)initWithUserVin:(NSString *)vin;

@end

@interface GLVehicleTelematicsActivateQueryResponse : GLResponse

@property (nonatomic, assign) BOOL activate;

@end
