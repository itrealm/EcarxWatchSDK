//
//  LKAuthLoginRequest.h
//  GLServiceSDK
//
//  Created by fish on 2019/8/26.
//

#import "ExRequest.h"
#import "ExLoginResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface LKAuthLoginRequest : ExRequest

- (instancetype)initWithAuthCode:(NSString *)authCode redirectUri:(NSString *)redirectUri;

@end

NS_ASSUME_NONNULL_END

@interface LKAuthLoginResponse : ExLoginResponse

@end
