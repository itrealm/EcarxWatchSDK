//
//  GLTheme.h
//  Pods
//
//  Created by fish on 2017/7/14.
//
//

#import <Foundation/Foundation.h>

// 自定义字体
#define GLFONT(Size,Name) [UIFont fontWithName:(Name) size:(Size)]?:[UIFont systemFontOfSize:(Size)]

//Geely
#define GLLightFont(Size)           GLFONT(Size,@"GEELYLight20151114")
#define GLBoldFont(Size)            GLFONT(Size,@"GEELYBold20151114")
#define GLNormalFont(Size)          GLFONT(Size,@"GEELYNormal20151114")
#define GLNarrowBoldFont(Size)      GLFONT(Size,@"GEELYNarrowBold20151114")
#define GLNarrowRegularFont(Size)   GLFONT(Size,@"GEELYNarrowRegular20151114")
#define GLNotoSansFont(Size)        GLFONT(Size,@"NotoSansCJKsc-Regular")
#define GLNotoSansMediumFont(Size)  GLFONT(Size,@"NotoSansCJKsc-Medium")

//LYNK CO
#define GLYaHeiUIFont(Size)         GLFONT(Size,@"MicrosoftYaHeiUI")
#define GLYaHeiUILightFont(Size)    GLFONT(Size,@"MicrosoftYaHeiUILight")
#define GLYaHeiUIBoldFont(Size)     GLFONT(Size,@"MicrosoftYaHeiUI-Bold")
#define GLSofiaProBoldFont(Size)    GLFONT(Size,@"PingFangSC-Semibold")
#define GLSofiaProRegularFont(Size) GLFONT(Size,@"PingFangSC-Regular")

//系统默认UIFontWeightRegular
#define GLSystemRegularFont(x)    [UIFont systemFontOfSize:x * GLScreenScale weight:UIFontWeightRegular]

@interface GLTheme : NSObject

@end
