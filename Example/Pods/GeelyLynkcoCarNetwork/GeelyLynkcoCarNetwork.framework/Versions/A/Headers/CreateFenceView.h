//
//  CreateFenceView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2020/3/30.
//

#import <UIKit/UIKit.h>
#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^TimeSelectBlock)(id obj);

typedef void(^EditInfoBlock)(id obj);

typedef void(^JumpBlock)(void);

@interface CreateFenceView : UIView

@property (nonatomic, copy) TimeSelectBlock timeSelectBlock;

@property (nonatomic, copy) EditInfoBlock editInfoBlock;

@property (nonatomic, copy) JumpBlock jumpBlock;

@property (nonatomic, strong) ExGeoFence * fence;

@end

NS_ASSUME_NONNULL_END
