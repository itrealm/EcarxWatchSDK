//
//  ExDeviceVerifyViewModel.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2018/1/25.
//  Copyright © 2018年 ecarx. All rights reserved.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import <GLServiceSDK/GLServiceSDK.h>
#import "GLCountDownViewModel.h"

@interface ExDeviceVerifyViewModel : GLCountDownViewModel

//验证码
@property (nonatomic, copy) NSString *inputCode;

//验证验证码
@property (nonatomic,strong) RACCommand *verifyCodeCommand;

@end
