//
//  FenceInfoTableViewCell.h
//  AFNetworking
//
//  Created by 朱伟特 on 2020/3/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^FenceInfoSetBlock)(NSDictionary * setInfo);

typedef void(^JumpPageBlock)(void);

@interface FenceInfoTableViewCell : UITableViewCell

@property (nonatomic, copy) FenceInfoSetBlock infoSetBlock;

@property (nonatomic, copy) JumpPageBlock jumpPageBlock;

@property (nonatomic, strong) NSDictionary * data;

@end

NS_ASSUME_NONNULL_END
