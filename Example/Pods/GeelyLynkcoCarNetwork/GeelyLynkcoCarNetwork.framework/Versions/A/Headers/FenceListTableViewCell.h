//
//  FenceListTableViewCell.h
//  AFNetworking
//
//  Created by 朱伟特 on 2020/3/27.
//

#import <UIKit/UIKit.h>
#import "GLFormKit.h"
#import <GLServiceSDK/ExGeoFence.h>
NS_ASSUME_NONNULL_BEGIN

typedef void(^ReloadDataBlock)(void);

@interface FenceListTableViewCell : GLFormBaseCell

@property (nonatomic, strong) ExGeoFence * onFence;

@property (nonatomic, strong) ExGeoFence * fenceModel;

@property (nonatomic, copy) ReloadDataBlock reloadDataBlock;

@end

NS_ASSUME_NONNULL_END
