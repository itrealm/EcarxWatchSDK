//
//  FenceRemindTableViewCell.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/6/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FenceRemindTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString * title;

@end

NS_ASSUME_NONNULL_END
