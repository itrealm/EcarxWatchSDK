//
//  FenceSearchTableViewCell.h
//  AFNetworking
//
//  Created by 朱伟特 on 2020/3/30.
//

#import <UIKit/UIKit.h>
#import "GLLBSSearchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FenceSearchTableViewCell : UITableViewCell

@property (nonatomic, strong) GLLBSSearchModel * searchModel;

@end

NS_ASSUME_NONNULL_END
