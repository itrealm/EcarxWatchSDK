//
//  FenceTimeSetView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2020/3/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class FenceTimeSetView;

typedef NS_ENUM(NSUInteger, GLFenceTimeType){
    GLFenceTimeStart = 1,  //起始时间
    GLFenceTimeEnd   = 2,  //结束时间
};

typedef NS_ENUM(NSUInteger, GLFenceTimeAlignment){
    GLFenceTimeAlignmentLeft = 0,  //左对齐
    GLFenceTimeAlignmentRight,  //右对齐
};

typedef void(^FenceTimeSelectBlock)(FenceTimeSetView * setView);

@interface FenceTimeSetView : UIControl

@property (nonatomic, assign) GLFenceTimeType timeType;

@property (nonatomic, copy) FenceTimeSelectBlock timeSelectBlock;

@property (nonatomic, copy) NSString * timeStr;

//NSTextAlignmentLeft
-(instancetype)initWithAlignment:(GLFenceTimeAlignment)alignment;


@end

NS_ASSUME_NONNULL_END
