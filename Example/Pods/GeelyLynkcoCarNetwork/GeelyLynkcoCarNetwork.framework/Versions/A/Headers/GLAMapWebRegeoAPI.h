//
//  GLAMapWebRegeoAPI.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/4/24.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLServiceSDK/GLServiceSDK.h>
#import "GLAMapWebResponse.h"

@interface GLAMapWebRegeoRequest : BaseRequest

/**
 逆地理编码
 
 @param locations 经纬度坐标
 */
- (instancetype)initWithLocations:(NSArray *)locations;

@end

///地址组成要素
@interface GLAMapAddressComponent : BaseModel
///省/直辖市
@property (nonatomic, copy)   NSString         *province;
///市
@property (nonatomic, copy)   NSString         *city;
///城市编码
@property (nonatomic, copy)   NSString         *citycode;
///区
@property (nonatomic, copy)   NSString         *district;
///乡镇街道
@property (nonatomic, copy)   NSString         *township;
@end

@protocol GLAMapAddressComponent
@end
///逆地理编码
@interface GLAMapReGeocode : BaseModel
///格式化地址
@property (nonatomic, copy) NSString *formatted_address;
///地址组成要素
@property (nonatomic, copy) GLAMapAddressComponent *addressComponent;
@end

@protocol GLAMapReGeocode @end;

@interface GLAMapWebRegeoResponse : GLAMapWebResponse

@property (nonatomic, copy) NSArray<GLAMapReGeocode> *regeocodes;

@end
