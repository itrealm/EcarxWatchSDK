//
//  GLAMapWebResponse.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/4/24.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLServiceSDK/GLServiceSDK.h>

@interface GLAMapWebResponse : BaseResponse

@property (nonatomic, copy) NSString *status; // 1成功
@property (nonatomic, copy) NSString *info;   // OK成功
@property (nonatomic, copy) NSString *infocode;   // 10000成功

@end
