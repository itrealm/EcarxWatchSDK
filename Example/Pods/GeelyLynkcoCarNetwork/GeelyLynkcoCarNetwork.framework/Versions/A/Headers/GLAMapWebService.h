//
//  GLAMapWebService.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/4/24.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLServiceSDK/GLServiceSDK.h>

@interface GLAMapWebService : BaseAPI

/**
 发送请求
 
 @param request       HTTP请求
 @param responseClass 响应数据模型类
 @return			  信号
 */
+ (RACSignal *)startRequest:(BaseRequest *)request response:(Class)responseClass;

@end
