//
//  GLArticleApi.h
//  Pods
//
//  Created by yang.duan on 2019/12/13.
//

#import <Foundation/Foundation.h>

typedef void(^glsuccessBlock)(id _Nullable result);
typedef void(^glfailureBlock)(NSError * _Nullable error);

NS_ASSUME_NONNULL_BEGIN

@interface GLArticleApi : NSObject

+ (void)DraftAdd:(NSDictionary *)dic success:(glsuccessBlock)success fail:(glfailureBlock)fail;

+ (void)DraftUpdate:(NSDictionary *)dic success:(glsuccessBlock)success fail:(glfailureBlock)fail;

+ (void)DraftList:(NSDictionary *)dic success:(glsuccessBlock)success fail:(glfailureBlock)fail;
@end

NS_ASSUME_NONNULL_END
