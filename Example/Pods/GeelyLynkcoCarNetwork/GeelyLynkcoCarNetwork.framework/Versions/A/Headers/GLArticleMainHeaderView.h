//
//  GLArticleMainHeaderView.h
//  富文本
//
//  Created by LimboDemon on 2018/9/17.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SelectImageSuccess)(NSArray <UIImage *>*imageArray);

@interface GLArticleMainHeaderView : UIView

@property (nonatomic, copy) SelectImageSuccess successBlock;


/**
 删除
 */
@property (nonatomic, strong) UIButton *repealBtn;


/**
 撤销删除
 */
@property (nonatomic, strong) UIButton *unRepealBtn;

@end
