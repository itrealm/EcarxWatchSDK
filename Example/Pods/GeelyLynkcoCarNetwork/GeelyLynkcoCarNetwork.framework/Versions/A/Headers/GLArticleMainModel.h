//
//  GLArticleMainModel.h
//  富文本
//
//  Created by LimboDemon on 2018/9/13.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//typedef void(^FailureBlock)(id result);
typedef NS_ENUM(NSInteger, GLArticleMainState) {
    GLArticleMainStateNone,          // regular table view
    GLArticleMainStateUploading,        // sections are grouped together
    GLArticleMainStateSuccess,
    GLArticleMainStateFailed,
};

@interface GLArticleMainModel : NSObject<NSCopying,NSMutableCopying>

@property (nonatomic, copy) NSString *content;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign) CGFloat imageHeight;
@property (nonatomic, assign) CGFloat contentHeight;
@property (nonatomic, copy) NSString *contentH;
@property (nonatomic, copy) NSString *htmlString;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, assign) double loadProgress;
@property (nonatomic, assign) GLArticleMainState state;

/**
 文字修改时，修改当前changeLevel的等级为最高,暂时没用到
 */
@property (nonatomic, assign) NSInteger changeLevel;

- (void)uploadImage;

@end
