//
//  GLArticleMainTableViewCell.h
//  富文本
//
//  Created by LimboDemon on 2018/9/12.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLArticleMainModel.h"
/**
    咨询内容
        包含文字和图片
 */
@interface GLArticleMainTableViewCell : UITableViewCell

@property (nonatomic, weak) UITableView *tbView;

@property (nonatomic, weak) UIScrollView *bgScrollView;

@property (nonatomic, strong) NSMutableArray <GLArticleMainModel *>*dataArray;// 存放数据模型的数组

@property (nonatomic, strong) UITableView *mainContentTb;
@property (nonatomic, assign) CGFloat totalHeight;
@property (nonatomic, assign) BOOL isAddImageEnd;

- (void)reSetViewFrameAndData;

@end
