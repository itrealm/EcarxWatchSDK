//
//  GLCarBaseViewController.h
//  Mintour
//
//  Created by Fish on 15/11/13.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWCDVContainerViewController.h"


@interface GLCDVViewController : SWCDVContainerViewController
@property (nonatomic,assign)BOOL fullScreen;
@property (nonatomic,assign)BOOL hiddenNavigationBar; //default NO


@end
