//
//  GLCarBaseViewController.h
//  Mintour
//
//  Created by Fish on 15/11/13.
//  Copyright © 2015年 ecarx. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "BaseViewProtocol.h"
#import <GLMVVMKit/GLMVVMKit.h>
#import "GLBaseViewController.h"

@interface GLCarBaseViewController : GLBaseViewController
@property (nonatomic,strong) BaseViewModel *viewModel;

- (id<BaseViewProtocol>)initWithViewModel:(id)viewModel;

@end
