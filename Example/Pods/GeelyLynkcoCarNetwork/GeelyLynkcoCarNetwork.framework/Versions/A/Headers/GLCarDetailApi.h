//
//  GLCarDetailApi.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/10/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^SuccessBlock)(id result);

typedef void(^FailBlock)(NSError *error);


@interface GLCarDetailApi : NSObject

/*
 @param dic 参数
*/
+ (void)getCarMessage:(NSDictionary *)dic success:(SuccessBlock)success fail:(FailBlock)fail;

@end

NS_ASSUME_NONNULL_END
