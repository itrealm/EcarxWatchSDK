//
//  GLCarDetailBaseMessageCell.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ClickPlateNumberBtnBlock)(void);

typedef void(^ClickPlateProvinceBtnBlock)(void);

@interface GLCarDetailBaseMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *plateNumberProvinceTextField;

@property (weak, nonatomic) IBOutlet UITextField *plateNumberTextField;

@property (weak, nonatomic) IBOutlet UIView *platNumberBgView;

@property (nonatomic, copy)ClickPlateNumberBtnBlock clickPlateNumberBtnBlock;

@property (nonatomic, copy)ClickPlateProvinceBtnBlock clickPlateProvinceBtnBlock;

@property (nonatomic, copy)ClickPlateProvinceBtnBlock clickResetPwdBtnBlock;

@property (nonatomic, assign)BOOL hiddenResetPwdView;

- (void)addValueWithData:(id)data;


@end

NS_ASSUME_NONNULL_END
