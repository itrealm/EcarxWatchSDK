//
//  GLCarDetailModel.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLCarDetailModel : NSObject
@property (nonatomic, copy)NSString *seriesName;
@property (nonatomic, copy)NSString *vin;
@property (nonatomic, copy)NSString *engineNo;
@property (nonatomic, copy)NSString *vehiclePhotoSmall;
@property (nonatomic, copy)NSString *vehiclePhotoBig;
@property (nonatomic, copy)NSString *plateNumber;
@property (nonatomic, copy)NSString *color;
@property (nonatomic, copy)NSString *plateNumberProvince;
@property (nonatomic, assign)BOOL enableEdit;

@end

NS_ASSUME_NONNULL_END
