//
//  GLCarDetailViewController.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/17.
//

#import <UIKit/UIKit.h>
@class GLCarDetailViewModel;
NS_ASSUME_NONNULL_BEGIN

@interface GLCarDetailViewController : GLTableViewController

@property (strong, nonatomic) GLCarDetailViewModel *viewModel;

@end

NS_ASSUME_NONNULL_END

