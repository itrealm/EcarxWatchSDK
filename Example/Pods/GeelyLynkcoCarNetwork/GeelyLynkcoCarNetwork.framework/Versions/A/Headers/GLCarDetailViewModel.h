//
//  GLCarDetailViewModel.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/22.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import <GLServiceSDK/GLServiceSDK.h>


NS_ASSUME_NONNULL_BEGIN

typedef void(^ModifyPlateNoBlock)(NSString *plateNo);


@interface GLCarDetailViewModel : BaseViewModel

@property (nonatomic, assign) BOOL enableEdit;
@property (nonatomic, strong) GLVehicle *vehicle;
@property (nonatomic, strong) NSString *color;  // 车辆颜色
@property (nonatomic, strong) NSString *plateNumberProvince;  // 车辆车牌省份
@property (nonatomic, strong) NSString *plateNumber;  // 车辆车牌
// 车辆颜色的信号
@property (readonly, strong, nonatomic) RACSignal *colorSignal;
// 修改车牌的信号
@property (readonly, strong, nonatomic) RACSignal *modifyPlateNoSignal;

@end

NS_ASSUME_NONNULL_END
