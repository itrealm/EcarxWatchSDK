//
//  GLCarDownloadManager.h
//  Geelyconsumer
//
//  Created by 薛立恒 on 2018/8/21.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <Foundation/Foundation.h>



/** 下载的状态 */
typedef NS_ENUM(NSInteger, GLCarDownloadState) {
    GLCarDownloadStateNone = 0,     // 还没下载
    GLCarDownloadStateResumed,      // 下载中
    GLCarDownloadStateWait,         // 等待中
    GLCarDownloadStateStoped,       // 暂停中
    GLCarDownloadStateUnZip,        // 解压中
    GLCarDownloadStateCompleted,    // 已经完全下载完毕
    GLCarDownloadStateError         // 下载出错
};


@interface GLCarDownloadModel : NSObject
/** 下载状态 */
@property (assign, nonatomic) GLCarDownloadState state;
/** 文件名 */
@property (copy, nonatomic) NSString *filename;
/** 文件路径 */
@property (copy, nonatomic) NSString *fileSavePath;
@property (copy, nonatomic) NSString *tmpPath;
/** 文件url */
@property (copy, nonatomic) NSString *fileUrl;
/** 解压后路径 */
@property (copy, nonatomic) NSString *unzipPath;

/** 已下载的数量 */
@property (assign, nonatomic) NSUInteger totlalReceivedSize;
/** 文件的总大小 */
@property (assign, nonatomic) NSUInteger totalExpectedSize;

@property(assign,nonatomic)float progress;
/** 下载的错误信息 */
@property (strong, nonatomic) NSError *error;

@property (strong, nonatomic) void(^unZipBlock)(GLCarDownloadModel* m,void(^competionHandle)(BOOL success));



@end


@interface GLCarDownloadManager : NSObject

/** 最大同时下载数 */
@property (nonatomic, assign) int maxCount;

/** 当前正在下载的任务数(只读) */
@property (nonatomic,assign,readonly) NSInteger downloadingCount;




#pragma mark - ======== public action ========
- (NSArray *)getAllModels;
- (NSInteger)getDownloadingCount;

- (GLCarDownloadModel *)getModelByURL:(NSString *)url;
- (void)removeAllModels;
- (void)removeModel:(GLCarDownloadModel*)model;


- (void)resume:(GLCarDownloadModel *)model;
- (void)suspend:(GLCarDownloadModel *)model;
/** 全部文件暂停下载 */
- (void)suspendAll;
/** 全部文件开始\继续下载 */
- (void)resumeAll;

- (void)startTask:(GLCarDownloadModel *)task competion:(void(^)(BOOL success,NSURL* filePath,NSError* error))competionBlock;

- (void)registerUnZipHandle:(void(^)(GLCarDownloadModel* m,void(^competionHandle)(BOOL success)))unZipBlock;

@end
