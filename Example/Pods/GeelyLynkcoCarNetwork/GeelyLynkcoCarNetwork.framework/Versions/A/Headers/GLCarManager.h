//
//  GLMenuManager.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2018/9/5.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, GLCarCarOwner) {
    GLCarCarOwnerUnKnow,
    GLCarCarOwnerNO,
    GLCarCarOwnerUnbind,
    GLCarCarOwnerBind,
};
@class GLVehicle;
@class GLVehicleInformationViewModel;
@class GLMyCarViewModel;
@class LYLoginCaptchaViewModel;
@class GLVehicleViewModel;
@class GLVehicleControlItem;

@interface GLCarManager : NSObject
@property(nonatomic,readonly)GLVehicle* bindVehicle;
@property(nonatomic,readonly)GLVehicleInformationViewModel* currVehicleInfo;
@property(nonatomic,readonly)GLMyCarViewModel *carViewModel;
@property (nonatomic, readonly) LYLoginCaptchaViewModel *captchaViewModel;
@property (nonatomic, readonly) GLVehicleViewModel* vehicleModel;


@property(nonatomic,readonly)GLCarCarOwner isCarOwner;

@property(nonatomic,readonly)BOOL enable;

@property(nonatomic,weak)UIViewController* rootViewController;
@property (nonatomic, assign) NSInteger alertCount;
@property (nonatomic, strong) GLVehicleControlItem* currWebRemoteItem;


+ (instancetype)sharedManager;
/**
 获取车辆列表
 
 */
- (RACSignal *)fetchVehicleList;


- (void)bindVehicle:(GLVehicle *)vehicle competion:(void(^)(BOOL success))competionBlock;

- (void)getVehicleListRequest:(BOOL)needRequest competion:(void(^)(BOOL success,NSArray<GLVehicle*>* list))competionBlock;

- (void)isCarOwnerWithCompetion:(void(^)(GLCarCarOwner isCarOwner))competionBlock;

//退出登录
- (void)logout:(void (^)(void))completedBlock;

-(void)showErrorViewAuto;
- (void)showErrorViewWhenNoCarOwenr;
- (void)showErrorViewWhenNoBind;

- (void)autoLoginWithCallback:(void(^)(BOOL success,NSString* errStr))cb;
- (void)goToWeexPageByUrl:(NSString *)remoteUrl;
- (void)goToWeexPageByUrl:(NSString *)remoteUrl contentId:(NSString*)contentId;

- (BOOL)isWebHomePage;

- (void)clearInfo;

- (BOOL)canShowAlertByKey:(NSString*)key maxNum:(NSInteger)maxNum;


//下载车系素材
- (void)downloadCarMediaByUrl:(NSString*)url carCode:(NSString*)carCode colorCode:(NSString*)colorCode mediaVersion:(NSInteger)mediaVersion competion:(void(^)(BOOL success,id data))competionBlock;
//获取车系素材根目录
- (NSString*)getMediaRootPath;

- (NSString*)getCarNameByModelCode:(NSString*)code;
@end
