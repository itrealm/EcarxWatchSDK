//
//  GLCarMangeTableViewCell.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLCarMangeTableViewCell : UITableViewCell

@property(nonatomic,assign)BOOL carBind;
//@property(nonatomic,assign)BOOL canResetPwd;

-(void)updateData:(id)data;

@end

NS_ASSUME_NONNULL_END
