//
//  GLCarPlateNumberView.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^SelectPlateNumberBlock)(NSString *plateNumber);

typedef void(^HideBlock)(void);

@interface GLCarPlateNumberView : UIView

@property (nonatomic, copy) NSString *initializationPlateNumberStr;

@property (nonatomic, copy) SelectPlateNumberBlock selectPlateNumberBlock;

@property (nonatomic, copy) HideBlock hideBlock;

+ (CGFloat)viewHeight;

@end

NS_ASSUME_NONNULL_END
