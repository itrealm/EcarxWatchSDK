//
//  GLCarPlateProvinceView.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^SelectProvinceBlock)(NSString *province);

typedef void(^HideBlock)(void);

@interface GLCarPlateProvinceView : UIView

@property (nonatomic, copy, readonly)NSArray *dataSource;

@property (nonatomic, copy)SelectProvinceBlock selectProvinceBlock;

@property (nonatomic, copy)HideBlock hideBlock;

+ (CGFloat)viewHeight;


@end

NS_ASSUME_NONNULL_END
