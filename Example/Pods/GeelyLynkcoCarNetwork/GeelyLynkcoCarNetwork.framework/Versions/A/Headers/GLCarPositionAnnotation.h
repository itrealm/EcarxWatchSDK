//
//  GLCarPositionAnnotation.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/5/24.
//  Copyright © 2017年 ecarx. All rights reserved.
//


#if __has_include(<MAMapKit/MAMapKit.h>)
#import <MAMapKit/MAMapKit.h>
#else
#import <AMapNaviKit/MAMapKit.h>
#endif

@interface GLCarPositionAnnotation : MAPointAnnotation

@end
