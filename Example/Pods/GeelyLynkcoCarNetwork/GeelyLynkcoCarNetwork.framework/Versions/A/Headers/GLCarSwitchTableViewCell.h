//
//  GLCarSwitchTableViewCell.h
//  GeelyConsumerCarNetwork
//
//  Created by zhiyong.kuang on 2019/5/30.
//

#import <UIKit/UIKit.h>
#import "LYVerificationViewController.h"


NS_ASSUME_NONNULL_BEGIN

@interface GLCarSwitchTableViewCell : UITableViewCell
@property(nonatomic,assign)BOOL carSelected;
@property(nonatomic,assign)BOOL carBind;
//@property(nonatomic,assign)BOOL canResetPwd;

@property(nonatomic,assign)LYVerificationType verificationType;
@property(nonatomic,strong)void(^operationClickedBlock)(LYVerificationType type);


-(void)updateData:(id)data;
@end

NS_ASSUME_NONNULL_END
