//
//  GLCarSwitchViewController.h
//  GeelyConsumerCarNetwork
//
//  Created by zhiyong.kuang on 2019/5/30.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, GLVehicleBindStatus) {
    GLVehicleBindStatusTspRelevanced, //tsp已关联
    GLVehicleBindStatusTspUnRelevance, //tsp未关联
    GLVehicleBindStatusCspRelevanced, //csp已关联
    GLVehicleBindStatusCspUnRelevance,    //csp未关联
    GLVehicleBindStatusCspToActivate, //csp待激活
    GLVehicleBindStatusCspToVerify    //csp待验证
};



@interface GLCarSwitchViewController : GLTableViewController

@end

NS_ASSUME_NONNULL_END
