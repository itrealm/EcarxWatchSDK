//
//  GLCountDownViewModel.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/5/26.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLMVVMKit/GLMVVMKit.h>

@interface GLCountDownViewModel : BaseViewModel

//发送按钮标题
@property (nonatomic, copy) NSString *leftTimeTitle;
//手机号
@property (nonatomic, copy) NSString *mobile;
//验证码用途
@property (nonatomic, copy) NSString *purpose;

//获取验证码
- (RACSignal *)sendCodeSignal;
@property (nonatomic,strong) RACCommand *sendCodeCommand;

@end
