//
//  GLDataCenter.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2017/5/4.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLServiceSDK/GLServiceSDK.h>

@interface GLDataCenter : NSObject

// 当前用户
@property (nonatomic,copy) GLUser *user;

@property (nonatomic,copy) NSString *registrationId;

// 是否关联车辆
@property (nonatomic,assign) BOOL isBindVehicle;

// 关联的车辆
@property (nonatomic,strong) GLVehicle *bindVehicle;

// 关联车辆的能力集
@property (nonatomic,copy) NSArray<GLVehicleCapability> *bindVehicleCapability;

// 关联车辆的状态
//basicVehicleStatus->position
@property (nonatomic, copy) GLVehicleStatus *bindVehicleStatus;


//登录鉴权参数
@property (nonatomic, copy) GLUserLoginResponse *loginResponse;


// 当前用户语言
@property (nonatomic,copy) NSString *language;

+ (instancetype)sharedCenter;

// 清除数据
- (void)clearData;
- (void)updateBindVehicleInfo:(GLVehicle *)bindVehicle;
- (NSString *)realSeriesName:(NSString *)originalName;

@end
