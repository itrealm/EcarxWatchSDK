//
//  LYDateIntervalSelectorPicker.h
//  timeSelect
//
//  Created by yang.duan on 2020/3/31.
//  Copyright © 2020 yang.duan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol GLDateIntervalSelectorPickerDelegate <NSObject>

-(void)dateWithSelect:(NSDate *)date;

@end

@interface GLDateIntervalSelectorPicker : UIPickerView<UIPickerViewDelegate, UIPickerViewDataSource>

-(instancetype)initWithDatePickerView;

@property (nonatomic, assign) id<GLDateIntervalSelectorPickerDelegate> pvDelegate;

@property (nonatomic, strong, readonly) NSDate *date;

@end

NS_ASSUME_NONNULL_END
