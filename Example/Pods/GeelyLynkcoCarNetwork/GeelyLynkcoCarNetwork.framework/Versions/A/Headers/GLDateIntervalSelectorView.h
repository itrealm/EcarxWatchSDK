//
//  LYDateIntervalSelectorView.h
//  timeSelect
//
//  Created by yang.duan on 2020/3/31.
//  Copyright © 2020 yang.duan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLDateIntervalSelectorPicker.h"


NS_ASSUME_NONNULL_BEGIN

@interface GLDateIntervalSelectorView : UIView

//单例
+(GLDateIntervalSelectorView *)initClient;

-(void)datePickerCompleteBlock:(void (^)(NSDate *startDate))completeBlock;


@end

NS_ASSUME_NONNULL_END
