//
//  GLDrivingLogDetailViewModel.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/4/19.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import "GLJournalLogRegeoDetail.h"

@interface GLDrivingLogDetailViewModel : BaseTableViewModel

@property (nonatomic, strong) GLJournalLogRegeoDetail *regeoDetail;
@property (nonatomic, assign) BOOL showFuel;

@end
