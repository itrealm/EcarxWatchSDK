//
//  GLDrivingLogHeadeView.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2020/4/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^clickSwitchButtonBlock)(BOOL ison);

@interface GLDrivingLogHeadeView : UIView

@property(nonatomic,strong)UIView* bgView;
@property (nonatomic,strong)UIImageView *bgImageView;
@property(nonatomic,strong)UIView* topView;
@property(nonatomic,strong)UIView* bottomView;
@property(nonatomic,strong)UILabel* switchTitleLabel;
@property(nonatomic,strong)UILabel* distanceMeterLabel;
@property(nonatomic,strong)UILabel* distancetitleLabel;
@property(nonatomic,strong)UISwitch* switchButton;
@property (nonatomic,copy)clickSwitchButtonBlock block;


- (void)changeSwitchButtonStatus:(BOOL)ison;

-(instancetype)initWithShowSwitch:(BOOL)yesOrNot;

@end

NS_ASSUME_NONNULL_END
