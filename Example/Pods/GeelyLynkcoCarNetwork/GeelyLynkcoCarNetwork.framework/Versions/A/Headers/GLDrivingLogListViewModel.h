//
//  GLDrivingLogListViewModel.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/4/17.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import "GLJournalLogRegeoDetail.h"

@interface GLDrivingLogListViewModel : BaseTableViewModel

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;
@property (nonatomic, assign) NSInteger totalNum;

// 获取定位详细位置Signal
@property (readonly, nonatomic, strong) RACSignal *regeoSignal;

@end
