//
//  GLDrivingLogStatusViewModel.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2017/8/14.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLMVVMKit/GLMVVMKit.h>

@interface GLDrivingLogStatusViewModel : BaseViewModel

//行车日志
@property (nonatomic, assign) BOOL jouLogEnabled;
@property (nonatomic, assign) BOOL isJouLogSwitchLoading;
@property (nonatomic, strong) RACCommand *jouLogSwitchCommand;
@property (readonly, nonatomic, strong) RACCommand *updateVehicleStateCommand;

@end
