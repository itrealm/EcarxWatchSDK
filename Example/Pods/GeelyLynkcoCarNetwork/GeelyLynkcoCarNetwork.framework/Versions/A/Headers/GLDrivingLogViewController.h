//
//  GLDrivingLogViewController.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2020/3/30.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import "GLDrivingLogListViewModel.h"
#import "GLTableViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLDrivingLogViewController : GLTableViewController
@property (nonatomic, strong) GLDrivingLogListViewModel *viewModel;

@end

NS_ASSUME_NONNULL_END
