//
//  GLEditArticleBottomTableViewCell.h
//  富文本
//
//  Created by LimboDemon on 2018/9/19.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLFormBaseCell.h"

@interface GLEditArticleBottomTableViewCell : GLFormBaseCell

@property (nonatomic, copy) void(^goToNotice)(void);

@property (nonatomic, assign) BOOL isAgree;

@end
