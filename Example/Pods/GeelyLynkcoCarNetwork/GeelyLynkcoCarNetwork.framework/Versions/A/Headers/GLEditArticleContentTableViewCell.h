//
//  GLEditArticleContentTableViewCell.h
//  富文本
//
//  Created by LimboDemon on 2018/9/11.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "GLFormBaseCell.h"

typedef enum : NSUInteger {
    ArticleTitle,   // 标题
    ArticleSource,  // 信息来源
} ArticleType;


/**
 咨询标题等
 */
@interface GLEditArticleContentTableViewCell : GLFormBaseCell
@property (nonatomic, strong) UITextField *contentTX;

@property (nonatomic, assign) ArticleType articleType;

@property (nonatomic, strong) RACSignal *textSignal;

//区分首页进入投稿还是05时区投稿。以后可能会增加其他多种投稿方式
@property (nonatomic, copy) NSString * tagName;


@end
