//
//  GLEditArticleCoverTableViewCell.h
//  富文本
//
//  Created by LimboDemon on 2018/9/12.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLFormBaseCell.h"
typedef void(^LoadImageSuccess)(NSArray* imageUrls);

/**
 上传封面
 */
@interface GLEditArticleCoverTableViewCell : GLFormBaseCell

@property (nonatomic, strong)NSString* ossKey;
@property (nonatomic, weak) UITableView *tbView;

@property (nonatomic, copy) LoadImageSuccess succcessBlock;
@property (nonatomic, strong) void(^shouldSelectImage)(GLEditArticleCoverTableViewCell*cell);
- (void)addImageWithUrl:(NSString *)imageUrl;
-(void)setSelectedImage:(UIImage*)image;
@end
