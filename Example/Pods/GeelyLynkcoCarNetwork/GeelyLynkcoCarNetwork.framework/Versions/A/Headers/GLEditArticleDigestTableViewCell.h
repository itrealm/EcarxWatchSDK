//
//  GLEditArticleDigestTableViewCell.h
//  富文本
//
//  Created by LimboDemon on 2018/9/12.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>

/**
 * 摘要
 */
@interface GLEditArticleDigestTableViewCell : UITableViewCell


@property (nonatomic, strong) RACSignal *textSignal;

@end
