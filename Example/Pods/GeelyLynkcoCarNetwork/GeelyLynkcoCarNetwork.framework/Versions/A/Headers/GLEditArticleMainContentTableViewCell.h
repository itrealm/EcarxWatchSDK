//
//  GLEditArticleMainContentTableViewCell.h
//  富文本
//
//  Created by LimboDemon on 2018/9/13.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "GLArticleMainModel.h"
#import "YYTextView.h"

@protocol GLEditArticleMainContentTableViewCellDelegate<NSObject>

- (void)textViewDiffenceHeight:(CGFloat)diffence dif:(CGFloat)dif;

@end

@interface GLEditArticleMainContentTableViewCell : UITableViewCell

@property (nonatomic, strong) YYTextView *textView;
//@property (nonatomic, strong) UITextView *textView;
@property (nonatomic, assign) CGFloat textViewHeight;

@property (nonatomic, assign) CGFloat dif;

@property (nonatomic, assign) CGFloat diffence;

//@property (nonatomic, copy) NSString *content;

@property (nonatomic, strong)  RACSignal *signal;

@property (nonatomic, weak) GLArticleMainModel *model;

@property (nonatomic, weak) NSArray *dataArr;

@property (nonatomic, weak) id<GLEditArticleMainContentTableViewCellDelegate>delegate;

@end
