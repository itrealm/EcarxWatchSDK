//
//  GLEditArticleMainImageTableViewCell.h
//  富文本
//
//  Created by LimboDemon on 2018/9/13.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "GLArticleMainModel.h"

typedef void(^DeleteImageBlock)(GLArticleMainModel *model);

@interface GLEditArticleMainImageTableViewCell : UITableViewCell

@property (nonatomic, weak) UITableView *tableView;

@property (nonatomic, strong)  RACSignal *signal;

@property (nonatomic, copy) DeleteImageBlock deleteImageBlock;

@property (nonatomic, weak) GLArticleMainModel *model;

@end
