//
//  GLEndPositionAnnotation.h
//  Geelyconsumer
//
//  Created by 杨沁 on 2017/10/11.
//  Copyright © 2017年 Rdic. All rights reserved.
//

#if __has_include(<MAMapKit/MAMapKit.h>)
#import <MAMapKit/MAMapKit.h>
#else
#import <AMapNaviKit/MAMapKit.h>
#endif

@interface GLEndPositionAnnotation : MAPointAnnotation

@end
