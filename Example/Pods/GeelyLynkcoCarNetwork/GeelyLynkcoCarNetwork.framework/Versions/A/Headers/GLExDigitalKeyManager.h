//
//  ExViewModel.h
//  ExDigitalKeySDK_Example
//
//  Created by 杨沁 on 2019/4/22.
//  Copyright © 2019 yangqin. All rights reserved.
//



#if TARGET_IPHONE_SIMULATOR//模拟器
#else
#if __has_include(<ExDigitalKeyTspServiceSDK/ExDigitalKeyTspServiceSDK.h>)

#import <GLMVVMKit/GLMVVMKit.h>
#import <ExDigitalKeyTspServiceSDK/ExDigitalKeyTspServiceSDK.h>
#import <ExDigitalKeySDK/ExBluetoothConnectManager.h>

@interface GLExViewModel : BaseViewModel

//request param
@property (nonatomic, copy) NSString *vin;
@property (nonatomic, copy) NSString *temId;
@property (nonatomic, copy) NSString *shareId;
@property (nonatomic, copy) NSDate *startTime;
@property (nonatomic, copy) NSDate *endTime;
@property (nonatomic, assign) ExkeyTypeStatus keyType;
@property (nonatomic, assign) NSInteger times;
@property (nonatomic, copy) NSString *oldPhoneId;
@property (nonatomic, copy) NSString *phoneId;
@property (nonatomic, assign) ExAuthSharedDigitalkeyStatus authShareDKStatus;
@property (nonatomic, assign) ExCmdControlBleReqCode controlId;

//response param
@property (nonatomic, assign) ExActiveStatus activeStatus;
@property (nonatomic, copy) NSString *shareDigitalKeyId;
@property (nonatomic, copy) NSString *ownerDigitalKeyId;
@property (nonatomic, copy) ExDigitalkeyInfoModel *ownerDKInfo;

//获取车辆激活状态和车主钥匙Id
@property (nonatomic, strong) RACSignal *getActiveStatusSignal;

//更新车辆激活状态
@property (nonatomic, strong) RACSignal *updateActiveStatusSignal;

//远程激活车辆蓝牙数字钥匙功能
@property (nonatomic, strong) RACSignal *activeStatusSignal;

//创建分享的数字钥匙
@property (nonatomic, strong) RACSignal *createShareDKSignal;

//创建车主的数字钥匙
@property (nonatomic, strong) RACSignal *createOwnerDKSignal;

//更新车主的数字钥匙
@property (nonatomic, strong) RACSignal *updateOwnerDKSignal;

//重新认证数字钥匙
@property (nonatomic, strong) RACSignal *recertificationDKSignal;

//确认分享的数字钥匙
@property (nonatomic, strong) RACSignal *authShareDKSignal;

//获取所有被分享的数字钥匙
@property (nonatomic, strong) RACSignal *getAllSharedDKSignal;

//获取数字钥匙详细信息
@property (nonatomic, strong) RACSignal *getDKInfoSignal;

//获取所有分享的数字钥匙
@property (nonatomic, strong) RACSignal *getAllShareDKSignal;

@end

typedef NS_ENUM(NSUInteger, GLDeviceBluetoothStatus) {
    GLDeviceBluetoothStatusUnknow = 0,     //未知
    GLDeviceBluetoothStatusOpen,               //打开
    GLDeviceBluetoothStatusClose            //关闭
};


typedef NS_ENUM(NSUInteger, GLExBluetoothConnectState) {
    GLExBluetoothConnectStateDisConnected = 0,     //未连接
    GLExBluetoothConnectStateConnecting,            //连接中
    GLExBluetoothConnectStateConnected            //已连接
};


@interface GLExDigitalKeyManager : NSObject
@property(nonatomic,strong)GLExViewModel *viewModel;
@property(nonatomic,strong)ExBluetoothVehicleStateModel *carStatusModel;
@property (nonatomic,assign)GLDeviceBluetoothStatus blueToothStatus;
@property (nonatomic,readonly)GLExBluetoothConnectState blueToothConnectedState;



+ (instancetype)sharedManager;

-(void)checkDigitalKeyWithCompetion:(void(^)(BOOL success,id data))competionBlock;

//连接蓝牙并认证
-(void)startConnectBluetoothWithCompetion:(void(^)(BOOL success,id data))competionBlock;
//断开蓝牙
- (void)disconnectBluetooth;

-(void)autoTest;


//获取当前车辆状态
- (void)getCarStatus:(void(^)(BOOL success,id data))competionBlock;

#pragma mark - 车控指令


//寻车
- (void)findCar:(void(^)(BOOL success,id data))competionBlock;

//开锁
- (void)openDoor:(void(^)(BOOL success,id data))competionBlock;
//闭锁
- (void)closeDoor:(void(^)(BOOL success,id data))competionBlock;
//开后备箱
- (void)openTruck:(void(^)(BOOL success,id data))competionBlock;

//关闭后备箱
- (void)closeTruck:(void(^)(BOOL success,id data))competionBlock;
//开窗
- (void)openWindows:(void(^)(BOOL success,id data))competionBlock;
//关窗
- (void)closeWindows:(void(^)(BOOL success,id data))competionBlock;


- (void)log:(NSString*)str;


@end
#endif

#endif



