//
//  GLFavoriteAnnotation.h
//  EricssonTcGeelyProject
//
//  Created by Fish on 2017/3/27.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#if __has_include(<MAMapKit/MAMapKit.h>)
#import <MAMapKit/MAMapKit.h>
#else
#import <AMapNaviKit/MAMapKit.h>
#endif

@interface GLFavoriteAnnotation : MAPointAnnotation

@property (nonatomic, assign) NSUInteger tag;

@end
