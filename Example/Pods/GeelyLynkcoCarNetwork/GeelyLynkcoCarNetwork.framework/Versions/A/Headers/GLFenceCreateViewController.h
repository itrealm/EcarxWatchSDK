//
//  GLFenceCreateViewController.h
//  AFNetworking
//
//  Created by 朱伟特 on 2020/3/30.
//

#import "GLBaseViewController.h"

@class ExGeoFence;
NS_ASSUME_NONNULL_BEGIN

typedef void(^BackBlock)(void);

@interface GLFenceCreateViewController : GLBaseViewController

@property (nonatomic, assign) NSInteger fenceCount;

@property (nonatomic, copy) BackBlock backBlock;

@property (nonatomic, strong) ExGeoFence * fence;

@end

NS_ASSUME_NONNULL_END
