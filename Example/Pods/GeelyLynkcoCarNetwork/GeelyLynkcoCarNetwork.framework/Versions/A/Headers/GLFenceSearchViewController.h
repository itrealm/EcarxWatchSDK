//
//  GLFenceSearchViewController.h
//  AFNetworking
//
//  Created by 朱伟特 on 2020/3/30.
//

#import "GLBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^POISelectBlock)(id obj);

@interface GLFenceSearchViewController : GLBaseViewController

@property (nonatomic, copy) POISelectBlock poiSelectBlock;

@end

NS_ASSUME_NONNULL_END
