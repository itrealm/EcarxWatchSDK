//
//  GLJournalLogRegeoDetail.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/4/24.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import "GLAMapWebRegeoAPI.h"
#import <GLServiceSDK/GLServiceSDK.h>

@interface GLJournalLogRegeoDetail : GLJournalLog

@property (nonatomic, strong) GLAMapReGeocode *startReGeocode;
@property (nonatomic, strong) GLAMapReGeocode *endReGeocode;
- (instancetype)initWithJournalLog:(GLJournalLog *)journalLog;

@end
