//
//  GLLBSArchiverTool.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/21.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLLBSLocationCollectModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface GLLBSArchiverTool : NSObject

#pragma mark - SearchArchive

+ (void)archiveLBSSearchDataWithArray:(NSArray *)array;

+ (NSArray *)unarchiveLBSSearchData;

+ (void)cleanLBSSearchData;

#pragma mark - FenceArchive

+ (void)archiveFenceData:(id)obj;

+ (id)unarchiveFenceData;

+ (void)cleanFenceData;

#pragma mark - LocationStore

+ (void)archiveLocationData:(GLLBSLocationCollectModel *)obj;

+ (GLLBSLocationCollectModel *)unarchiveLocationData;

#pragma mark - NavigateHistory

+ (void)archiveLBSNavigateHistory:(NSArray *)array;

+ (NSArray *)unarchiveLBSNavigateHistory;

+ (void)cleanNavigateHistoryData;

@end

NS_ASSUME_NONNULL_END
