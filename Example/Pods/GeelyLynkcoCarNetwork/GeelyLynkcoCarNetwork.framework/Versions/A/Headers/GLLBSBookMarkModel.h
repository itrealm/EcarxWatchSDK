//
//  GLLBSBookMarkModel.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/6.
//

#import <Foundation/Foundation.h>

typedef void(^GLLoginSuccessBlock)(id result);

typedef void(^GLLoginErrorBlock)(id error);

NS_ASSUME_NONNULL_BEGIN

//@"poiId": _collection.poiId?:@"",
//@"lat": @(_collection.lat),
//@"lon": @(_collection.lon),
//@"name": _collection.name?:@"",
//@"address": _collection.address?:@"",
//@"phone": _collection.phone?:@""

@interface GLLBSBookMarkModel : NSObject


@end

NS_ASSUME_NONNULL_END
