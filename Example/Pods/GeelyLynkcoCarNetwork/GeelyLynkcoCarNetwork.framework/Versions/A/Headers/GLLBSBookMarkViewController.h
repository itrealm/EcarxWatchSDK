//
//  GLLBSBookMarkViewController.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/3.
//

#import "GLBaseViewController.h"

@class GLLBSSearchModel;
NS_ASSUME_NONNULL_BEGIN

typedef void(^SelectMarkPointBlock)(GLLBSSearchModel * selectModel);

typedef void(^DeleteMarkedPointBlock)(NSArray * newLocationArray);

@interface GLLBSBookMarkViewController : GLBaseViewController

@property (nonatomic, copy) SelectMarkPointBlock selectPointBlock;

@property (nonatomic, copy) DeleteMarkedPointBlock deleteMarkedPointBlock;

@end

NS_ASSUME_NONNULL_END
