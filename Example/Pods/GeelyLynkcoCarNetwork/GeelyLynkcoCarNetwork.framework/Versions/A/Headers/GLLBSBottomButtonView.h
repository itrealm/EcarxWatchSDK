//
//  GLLBSBottomButtonView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/6/26.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^LeftButtonAction)(void);

typedef void(^RightButtonAction)(void);

@interface GLLBSBottomButtonView : UIView

@property (nonatomic, copy) LeftButtonAction leftButtonBlock;

@property (nonatomic, copy) RightButtonAction rightButtonBlock;

@property (nonatomic, copy) NSString * leftButtonTitle;

@property (nonatomic, copy) NSString * rightButtonTitle;

@property (nonatomic, assign) BOOL leftButtonEnable;

@property (nonatomic, assign) BOOL rightButtonEnable;

@end

NS_ASSUME_NONNULL_END
