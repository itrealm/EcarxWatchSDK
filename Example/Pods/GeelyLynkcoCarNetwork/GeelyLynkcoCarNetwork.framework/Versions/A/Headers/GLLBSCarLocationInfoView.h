//
//  GLLBSCarLocationInfoView.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/20.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^WalkNaviBlock)(void);

typedef void(^ShareLocationBlock)(void);

typedef void(^ShowFenceBlock)(void);

typedef void(^StoreLocationBlock)(BOOL isCollected);

typedef void(^StartNavigateBlock)(void);

typedef void(^BackActionBlock)(void);

@interface GLLBSCarLocationInfoView : UIView

@property (nonatomic, copy) NSString * address;

@property (nonatomic, assign) BOOL isCollected;

@property (nonatomic, copy) WalkNaviBlock walkNaviBlock;

@property (nonatomic, copy) ShareLocationBlock shareLocationBlock;

@property (nonatomic, copy) StoreLocationBlock storeLocationBlock;

@property (nonatomic, copy) ShowFenceBlock showFenceBlock;

@property (nonatomic, copy) StartNavigateBlock startNavigateBlock;

@property (nonatomic, copy) BackActionBlock backActionBlock;

- (void)changeStyleToNormal:(BOOL)toNormal;

@end

NS_ASSUME_NONNULL_END
