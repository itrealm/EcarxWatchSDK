//
//  GLLBSFenceInfoModel.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/6/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLLBSFenceInfoModel : NSObject <NSCoding>

@property (nonatomic, copy) NSString * userAddress;

@property (nonatomic, assign) NSInteger fenceRadius;

@property (nonatomic, copy) NSString * fenceName;

@property (nonatomic, assign) NSTimeInterval createTime;

@end

NS_ASSUME_NONNULL_END
