//
//  GLLBSFenceListViewController.h
//  AFNetworking
//
//  Created by 朱伟特 on 2020/3/27.
//

#import "GLBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^UpdateFenceBlock)(void);

@interface GLLBSFenceListViewController : GLBaseViewController

@property (nonatomic, copy) UpdateFenceBlock updateFenceBlock;

@end

NS_ASSUME_NONNULL_END
