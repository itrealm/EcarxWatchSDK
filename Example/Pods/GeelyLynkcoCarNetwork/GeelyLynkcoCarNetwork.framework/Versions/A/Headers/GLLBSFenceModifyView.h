//
//  GLLBSFenceModifyView.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/21.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^GoFenceNoticePage)(void);

typedef void(^SaveFenceInfoBlock)(__nullable id fenceInfo);

typedef void(^UserLocationUpdateBlock)(void);

typedef void(^FenceRadiusChange)(NSInteger radius);

@interface GLLBSFenceModifyView : UIView

@property (nonatomic, copy) GoFenceNoticePage goFenceNoticeBlock;

@property (nonatomic, copy) SaveFenceInfoBlock saveBlock;

@property (nonatomic, copy) UserLocationUpdateBlock userLocationBlock;

@property (nonatomic, copy) FenceRadiusChange fenceRadiusChangeBlock;

@property (nonatomic, copy) NSString * userLocationAddress;

- (void)changeToHandleFence;//更改为开关围栏样式

- (void)changeToModifyFence;//更改为围栏信息修改样式

#pragma mark - FenceInfo
@property (nonatomic, strong) ExGeoFence * fenceInfo;

@end

NS_ASSUME_NONNULL_END
