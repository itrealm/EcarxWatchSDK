//
//  GLLBSFunctionView.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/17.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ParkingBlock)(void);

typedef void(^PetrolBlock)(void);

typedef void(^RestrauntBlock)(void);

typedef void(^CollectBlock)(void);

@interface GLLBSFunctionView : UIView

@property (nonatomic, copy) ParkingBlock parkingBlock;

@property (nonatomic, copy) PetrolBlock petrolBlock;

@property (nonatomic, copy) RestrauntBlock restrauntBlock;

@property (nonatomic, copy) CollectBlock collectBlock;

@end

NS_ASSUME_NONNULL_END
