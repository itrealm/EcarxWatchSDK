//
//  GLLBSHistoryTableViewCell.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/17.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

//#import <GeelyConsumerCore/GeelyConsumerCore.h>

#import "GeelyLynkcoCarNetwork.h"
#import "GLLBSSearchModel.h"
#import "GLLBSNavigateHistoryModel.h"
#import <GLServiceSDK/GLServiceSDK.h>
#import <GeelyLynkcoCore/GLFormBaseCell.h>
NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, HistoryCellType){
    LocationCell = 1, //定位类型的cell
    NaviCell          //导航类cell
};

typedef void(^NaviBlock)(void);

@interface GLLBSHistoryTableViewCell : GLFormBaseCell

@property (nonatomic, assign) HistoryCellType cellType;

@property (nonatomic, copy) NaviBlock naviBlock;

@property (nonatomic, strong) GLLBSSearchModel * searchModel;

@property (nonatomic, strong) GLLBSNavigateHistoryModel * historyModel;

@property (nonatomic, strong) ExCollection * collectionData;

@end

NS_ASSUME_NONNULL_END
