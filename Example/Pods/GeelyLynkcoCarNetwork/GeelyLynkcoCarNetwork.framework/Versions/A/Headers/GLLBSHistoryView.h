//
//  GLLBSHistoryView.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/17.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class GLLBSSearchModel, GLLBSNavigateHistoryModel;

typedef NS_ENUM(NSUInteger, GLLBSHistoryViewType){
    LBSHistoryType  = 0,//历史
    LBSNavigateType = 1,//导航
    LBSOtherType = 2,    //其他
    HistoryNavigateType //历史导航
};

typedef void(^HistoryPlaceSelectBlock)(GLLBSSearchModel * searchModel);

typedef void(^NavigateBlock)(GLLBSSearchModel * searchModel);

typedef void(^HistoryNavigateBlock)(GLLBSNavigateHistoryModel * naviagateModel);

@interface GLLBSHistoryView : UIView

@property (nonatomic, strong) NSMutableArray * dataArray;

@property (nonatomic, strong) NSMutableArray * navigateArray;

@property (nonatomic, assign) GLLBSHistoryViewType viewType;

@property (nonatomic, copy) HistoryPlaceSelectBlock historySelectBlock;

@property (nonatomic, copy) NavigateBlock navigateBlock;

@property (nonatomic, copy) HistoryNavigateBlock historyNavigateBlock;

- (void)setHistoryDataArray:(NSMutableArray *)dataArray;

- (void)setHistoryNaviagateArray:(NSMutableArray *)navigateArray;

@end

NS_ASSUME_NONNULL_END
