//
//  GLLBSLocationCollectModel.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/24.
//

#import <Foundation/Foundation.h>
#import <GLLBSSearchModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLLBSLocationCollectModel : NSObject<NSCoding>

@property (nonatomic, strong) GLLBSSearchModel * homeData;

@property (nonatomic, strong) GLLBSSearchModel * companyData;

@property (nonatomic, strong) NSArray <GLLBSSearchModel *>* collectionData;

@end

NS_ASSUME_NONNULL_END
