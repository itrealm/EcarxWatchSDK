//
//  GLLBSLocationCommonView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, LocationType){
    PersonalType = 1, //定位类型的cell
    LandBankType      //导航类cell
};

typedef void(^NavigateCommonBlock)(void);

typedef void(^ShareCommonBlock)(void);

typedef void(^StoreCommonBlock)(BOOL collected);

@interface GLLBSLocationCommonView : UIView

@property (nonatomic, copy) NSString * address;

@property (nonatomic, copy) NSString * name;

@property (nonatomic, assign) BOOL isCollected;

@property (nonatomic, copy) NavigateCommonBlock navigateBlock;

@property (nonatomic, copy) ShareCommonBlock shareBlock;

@property (nonatomic, copy) StoreCommonBlock storeBlock;

@end

NS_ASSUME_NONNULL_END
