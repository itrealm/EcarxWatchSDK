//
//  GLLBSMenuView.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/17.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ShowTrafficBlock)(BOOL show);

typedef void(^ShowCarLocationBlock)(BOOL showCarLocation);

typedef void(^CreateTrackBlock)(void);

typedef void(^ShowUserLocaionBlock)(void);

@interface GLLBSMenuView : UIView

@property (nonatomic, copy) ShowTrafficBlock showTrafficBlock;

@property (nonatomic, copy) ShowCarLocationBlock showCarLocationBlock;

@property (nonatomic, copy) CreateTrackBlock createTrackBlock;

@property (nonatomic, copy) ShowUserLocaionBlock showUserLocationBlock;


@end

NS_ASSUME_NONNULL_END
