//
//  GLLBSNaviRouteTableViewCell.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLLBSNaviRouteTableViewCell : UITableViewCell

@property (nonatomic, strong) UIView * lineView;

@property (nonatomic, strong) NSIndexPath * index;

@end

@interface GLLBSNaviCollectTableViewCell : UITableViewCell



@end

NS_ASSUME_NONNULL_END
