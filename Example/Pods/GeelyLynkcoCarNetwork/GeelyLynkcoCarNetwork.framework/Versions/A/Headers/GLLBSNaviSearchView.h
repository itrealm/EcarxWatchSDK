//
//  GLLBSNaviSearchView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/6/27.
//

#import <UIKit/UIKit.h>
#import "GLLBSSearchModel.h"


@class GLSearchTextField;
NS_ASSUME_NONNULL_BEGIN

typedef void(^NavigateSearchBlock)(GLLBSSearchModel * startModel, GLLBSSearchModel * desModel);

typedef void(^StartBlock)(NSString * str);//开始输入框开始输入时的回掉

typedef void(^DesBlock)(NSString * str);

typedef void(^TextFieldEditingBlock)(GLSearchTextField * editingTextField);

typedef void(^CreateNaviModelBlock)(GLLBSSearchModel * startModel, GLLBSSearchModel * desModel);

@interface GLLBSNaviSearchView : UIView

@property (nonatomic, strong) GLLBSSearchModel * startModel;

@property (nonatomic, strong) GLLBSSearchModel * desModel;

@property (nonatomic, strong) GLLBSSearchModel * targetModel;

@property (nonatomic, copy) StartBlock startEditBlock;

@property (nonatomic, copy) DesBlock destinationEditBlock;

@property (nonatomic, copy) TextFieldEditingBlock editingBlock;

@property (nonatomic, copy) NavigateSearchBlock navigateBlock;

@property (nonatomic, copy) CreateNaviModelBlock createNaviModelBlock;

- (void)setDesTextFieldEditing:(BOOL)editing;

- (void)initData;

@end

NS_ASSUME_NONNULL_END
