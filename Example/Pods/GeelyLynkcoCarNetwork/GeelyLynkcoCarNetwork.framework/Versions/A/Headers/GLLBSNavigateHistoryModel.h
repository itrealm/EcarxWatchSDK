//
//  GLLBSNavigateHistoryModel.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/29.
//

#import <Foundation/Foundation.h>
#import "GLLBSSearchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLLBSNavigateHistoryModel : NSObject<NSCoding>

//@property (nonatomic, copy) NSString * startAddress;
//
//@property (nonatomic, copy) NSString * desAddress;
//
//@property (nonatomic, assign) CGFloat startLatitude;
//
//@property (nonatomic, assign) CGFloat startLongitude;
//
//@property (nonatomic, assign) CGFloat desLatitude;
//
//@property (nonatomic, assign) CGFloat desLongitude;

@property (nonatomic, strong) GLLBSSearchModel * startModel;

@property (nonatomic, strong) GLLBSSearchModel * desModel;

@end

NS_ASSUME_NONNULL_END
