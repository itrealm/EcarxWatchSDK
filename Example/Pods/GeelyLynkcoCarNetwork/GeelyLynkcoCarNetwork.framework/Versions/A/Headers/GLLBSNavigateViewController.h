//
//  GLLBSNavigateViewController.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/29.
//

#import <GeelyLynkcoCore/GLBaseViewController.h>

@class GLLBSSearchModel;
NS_ASSUME_NONNULL_BEGIN

typedef void(^NavigatePageBlock)(GLLBSSearchModel * startModel, GLLBSSearchModel * desModel);

@interface GLLBSNavigateViewController : GLBaseViewController

@property (nonatomic, copy) NavigatePageBlock navigateBlock;

@property (nonatomic, strong) GLLBSSearchModel * startModel;

@property (nonatomic, strong) GLLBSSearchModel * desModel;

@end

NS_ASSUME_NONNULL_END
