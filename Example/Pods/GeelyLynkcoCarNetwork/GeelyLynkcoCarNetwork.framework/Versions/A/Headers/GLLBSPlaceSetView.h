//
//  GLLBSPlaceSetView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/3.
//

#import <UIKit/UIKit.h>
#import "GLLBSLocationCollectModel.h"

@class GLLBSSearchModel;
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, GLAddressSetType){
    GLAddressHomeSet      = 1,  //设置家的位置
    GLAddressCompanySet   = 2,  //设置公司的位置
    GLAddressOtherSet     = 3,  //设置其他位置
};
typedef void(^SetAddAddressBlock)(GLAddressSetType type);

typedef void(^CollectNavigateBlock)(GLLBSSearchModel * model);

typedef void(^GoFencePageBlock)(void);

@interface GLLBSPlaceSetView : UIView

@property (nonatomic, copy) NSString * homePlace;

@property (nonatomic, copy) NSString * companyPlace;

@property (nonatomic, assign) BOOL editing;

@property (nonatomic, assign) BOOL showFence;

@property (nonatomic, copy) SetAddAddressBlock setAddAddressBlock;

@property (nonatomic, copy)  CollectNavigateBlock collectNavigateBlock;

@property (nonatomic, copy) GoFencePageBlock goFenceBlock;

@property (nonatomic, strong) GLLBSLocationCollectModel * collectModel;

+ (GLLBSPlaceSetView *)createLBSPlaceSetViewWithCenter:(CGPoint)center;

//- (void)addCollectPlace:(GLLBSSearchModel *)collectPlace;

- (void)show;

@end

NS_ASSUME_NONNULL_END
