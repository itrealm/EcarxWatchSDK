//
//  GLLBSRecordView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/17.
//

#import <UIKit/UIKit.h>
@class GLDrivingLogDetailViewModel;
NS_ASSUME_NONNULL_BEGIN

@interface GLLBSRecordView : UIView
@property(nonatomic,strong)GLDrivingLogDetailViewModel* model;
@end

NS_ASSUME_NONNULL_END
