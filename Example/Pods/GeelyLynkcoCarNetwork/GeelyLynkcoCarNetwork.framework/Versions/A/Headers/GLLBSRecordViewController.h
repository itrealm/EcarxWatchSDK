//
//  GLLBSRecordViewController.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/17.
//

#import <GeelyLynkcoCore/GLBaseViewController.h>
@class GLDrivingLogDetailViewModel;
NS_ASSUME_NONNULL_BEGIN

@interface GLLBSRecordViewController : GLBaseViewController

@property (nonatomic, assign) BOOL showShareButton;
@property(nonatomic,strong)GLDrivingLogDetailViewModel* viewModel;
@property (nonatomic, assign) BOOL showFuelConsumptionSingleTime;

@end

NS_ASSUME_NONNULL_END
