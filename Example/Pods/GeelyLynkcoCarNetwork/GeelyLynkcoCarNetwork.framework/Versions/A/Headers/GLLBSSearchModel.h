//
//  GLLBSSearchModel.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/21.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLLBSSearchModel : NSObject<NSCoding>

@property (nonatomic, copy) NSString * searchType;//0,地址类型， 1，导航类型

@property (nonatomic, copy) NSString * searchName;

@property (nonatomic, copy) NSString * address;

@property (nonatomic, copy) NSString * uid;

@property (nonatomic, assign) double latitude;

@property (nonatomic, assign) double longitude;

@end

NS_ASSUME_NONNULL_END
