//
//  GLLBSSearchResultTableViewCell.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/3.
//

#import <UIKit/UIKit.h>
#import <AMapSearchKit/AMapSearchKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^NaviBlock)(id obj);

@interface GLLBSSearchResultTableViewCell : UITableViewCell

@property (nonatomic, copy) NaviBlock naviBlock;

@property (nonatomic, strong) AMapPOI * poi;

@end

NS_ASSUME_NONNULL_END
