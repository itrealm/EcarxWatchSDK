//
//  GLLBSSearchResultView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/2.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^AddressChooseBlock)(id obj);

typedef void(^SearchNavigateBlock)(id obj);

typedef void(^HideResultView)(void);

@interface GLLBSSearchResultView : UIView

@property (nonatomic, copy) AddressChooseBlock addressChooseBlock;

@property (nonatomic, copy) SearchNavigateBlock searchNavigateBlock;

@property (nonatomic, copy) HideResultView hideResultViewBlock;

@property (nonatomic, strong) NSArray * dataArray;

@end

NS_ASSUME_NONNULL_END
