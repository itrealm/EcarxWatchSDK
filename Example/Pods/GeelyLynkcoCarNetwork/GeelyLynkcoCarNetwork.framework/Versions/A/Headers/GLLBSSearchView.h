//
//  GLLBSSearchView.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/17.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLLBSSearchModel.h"
#import "GLLBSNaviSearchView.h"

@class GLSearchTextField;
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, GLLBSSearchViewType){
    SearchViewSearch  = 0,//搜索类型
    SearchViewNavi = 1,//导航类型
    SearchViewMogolia //导航类型带蒙层
};

typedef void(^GoBackBlock)(void);

typedef void(^SearchBlock)(NSString * searchContent);

typedef void(^EditingBlock)(NSString * editStr);

typedef void(^CancleBlock)(void);

typedef void(^BeginEditingBlock)(void);

#pragma mark - 复合TextField的block

typedef void(^StartEditingBlock)(NSString * editStr);

typedef void(^DesEditingBlock)(NSString * editStr);

typedef void(^MongoliaClick)(void);

typedef void(^ChangeNavigateRouteBlock)(GLLBSSearchModel * startModel, GLLBSSearchModel * desModel);

@interface GLLBSSearchView : UIView

@property (nonatomic, assign) GLLBSSearchViewType searchViewType;

@property (nonatomic, strong) GLSearchTextField * searchTextField;

@property (nonatomic, copy) GoBackBlock backBlock;

@property (nonatomic, copy) SearchBlock searchBlock;

@property (nonatomic, copy) CancleBlock cancleBlock;

@property (nonatomic, copy) BeginEditingBlock beginingEditBlock;

@property (nonatomic, copy) EditingBlock editingBlock;

@property (nonatomic, copy) NSString * searchName;

- (void)resignTextFieldFirstResponder;

#pragma mark - 复合textfield方法

@property (nonatomic, strong) GLSearchTextField * editingTextField;

@property (nonatomic, copy) StartEditingBlock startEditingBlock;

@property (nonatomic, copy) DesEditingBlock desEditingBlock;

@property (nonatomic, copy) MongoliaClick mongoliaClickBlock;

@property (nonatomic, copy) ChangeNavigateRouteBlock changeNavigateRouteBlock;

@property (nonatomic, copy) NavigateSearchBlock navigateBlock;

@property (nonatomic, strong) GLLBSSearchModel * targetModel;

@property (nonatomic, strong) GLLBSSearchModel * startModel;

@property (nonatomic, strong) GLLBSSearchModel * desModel;

- (void)changeToNaviStyleWithMongolia:(BOOL)withMongolia;

- (void)changeToNormalStyle;

- (void)setDesTextFieldEditing:(BOOL)editing;

@end

NS_ASSUME_NONNULL_END
