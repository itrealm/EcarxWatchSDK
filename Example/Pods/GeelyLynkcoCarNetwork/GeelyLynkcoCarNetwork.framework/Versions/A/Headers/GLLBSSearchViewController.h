//
//  GLLBSSearchViewController.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/7/11.
//

#import <GeelyLynkcoCore/GLBaseViewController.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^SearchPlaceBlock)(NSString * searchTitle, NSArray * searchResult);

typedef void(^SingleClickBlock)(id obj);

typedef NS_ENUM(NSUInteger, FromPage){
    FromSetAddressPage = 1,  //从设置地址页面进入
    FromSearchPage     = 2 //从搜索框进入
};

@interface GLLBSSearchViewController : GLBaseViewController

@property (nonatomic, copy) NSString * searchName;

@property (nonatomic, copy) SearchPlaceBlock searchPlaceBlock;

@property (nonatomic, copy) SingleClickBlock singleClickBlock;

@property (nonatomic, assign) FromPage fromPage;

@end

NS_ASSUME_NONNULL_END
