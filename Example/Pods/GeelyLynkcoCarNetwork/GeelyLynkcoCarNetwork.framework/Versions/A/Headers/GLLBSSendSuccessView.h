//
//  GLLBSSendSuccessView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/8/6.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLLBSSendSuccessView : UIView

- (void)show;

@end

NS_ASSUME_NONNULL_END
