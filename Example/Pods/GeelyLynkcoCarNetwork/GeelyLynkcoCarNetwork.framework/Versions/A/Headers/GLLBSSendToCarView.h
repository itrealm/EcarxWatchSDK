//
//  GLLBSSendToCarView.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/8/1.
//

#import <UIKit/UIKit.h>
//#import "<#header#>"

NS_ASSUME_NONNULL_BEGIN

typedef void(^SendToCarBlock)(void);

typedef void(^GaodeNavigateBlock)(void);

@interface GLLBSSendToCarView : UIView

@property (nonatomic, copy) SendToCarBlock sendToCarBlock;

@property (nonatomic, copy) GaodeNavigateBlock gaodeNavigateBlock;

@end

NS_ASSUME_NONNULL_END
