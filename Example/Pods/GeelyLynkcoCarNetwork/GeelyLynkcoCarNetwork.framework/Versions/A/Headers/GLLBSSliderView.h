//
//  GLLBSSliderView.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/20.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^SliderValueChanged)(NSInteger changedValue);

@interface GLLBSSliderView : UIControl

@property (nonatomic, copy) SliderValueChanged valueChangeBlock;

@property (nonatomic, assign) NSInteger scoreValue;

@end

NS_ASSUME_NONNULL_END
