//
//  GLLBSViewController.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/14.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import "GLBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLLBSViewController : GLBaseViewController

@property (nonatomic, assign) BOOL showCarLocation;

@property (nonatomic, strong) NSDictionary * paramDic;

@end

NS_ASSUME_NONNULL_END
