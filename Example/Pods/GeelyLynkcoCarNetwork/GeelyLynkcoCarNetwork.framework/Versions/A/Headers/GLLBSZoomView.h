//
//  GLLBSZoomView.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/20.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^MapZoomBlock)(BOOL minus);

@interface GLLBSZoomView : UIView

@property (nonatomic, copy) MapZoomBlock zoomBlock;

- (void)setLimitWithMinus:(BOOL)isMinus;

- (void)setNoLimitWithMinus:(BOOL)isMinus;

@end

NS_ASSUME_NONNULL_END
