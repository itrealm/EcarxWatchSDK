//
//  GLLShareScreenShotView.h
//  GeelyLynkcoCarNetwork
//
//  Created by shuaishuai on 2020/4/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLLShareScreenShotView : UIView
+ (instancetype)createShareScreenShotView:(NSString *)qrString;
@end

NS_ASSUME_NONNULL_END
