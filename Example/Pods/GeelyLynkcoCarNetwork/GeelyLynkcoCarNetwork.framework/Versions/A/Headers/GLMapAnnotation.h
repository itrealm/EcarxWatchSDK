//
//  GLMapAnnotation.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/3.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//


#if __has_include(<MAMapKit/MAMapKit.h>)
#import <MAMapKit/MAMapKit.h>
#else
#import <AMapNaviKit/MAMapKit.h>
#endif


NS_ASSUME_NONNULL_BEGIN

//该类为一个抽象类,定义了大头针的图片，不能直接使用，必须子类化之后才能使用
@interface GLMapAnnotation : MAPointAnnotation

@property (nonatomic, copy) NSString * image;

@property (nonatomic, assign) BOOL draggable;

@end

NS_ASSUME_NONNULL_END
