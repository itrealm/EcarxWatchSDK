//
//  GLMapFenceModel.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/5/30.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import "GLMapBaseModel.h"
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLMapFenceModel : GLMapBaseModel

@property (nonatomic, assign) NSInteger radius;

@property (nonatomic, assign) CLLocationCoordinate2D centerCoor;

- (void)createFenceWithCenter:(CLLocationCoordinate2D)coor radius:(NSInteger)radius;

@end

NS_ASSUME_NONNULL_END
