//
//  GLMapPushToastView.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/5/19.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLMapPushToastView : UIView

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *mapImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *commitBtn;
+ (instancetype)newView;

@end
