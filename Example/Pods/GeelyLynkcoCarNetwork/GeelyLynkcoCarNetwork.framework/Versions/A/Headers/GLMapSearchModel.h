//
//  GLMapSearchModel.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/3.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import "GLMapBaseModel.h"
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^MapSearchResult)(BOOL success, NSArray * result);

typedef void(^MapGeoSearchBlock)(NSArray * geocods);

typedef void(^MapRegeoSearchBlock)(id regeoObj);

typedef void(^GenerateShareBlock)(NSString * shareURL);

@interface GLMapSearchModel : GLMapBaseModel

//@property (nonatomic, strong) CLLocation * currentLocation;
@property (nonatomic, assign) CLLocationCoordinate2D currentLocation;

//根据关键字和半径搜索
- (void)searchAroundWithRound:(NSInteger)round city:(NSString *)city WithKeyWords:(NSString *)keyWords block:(MapSearchResult)result;

//根据关键字和城市搜索（类似高德地图）
- (void)searchPoiAroundWithKeyWords:(NSString *)keyWords city:(NSString *)city block:(MapSearchResult)result;

//地理编码
- (void)geoCodeWithAddress:(NSString *)address result:(MapGeoSearchBlock)geoBlock;

//逆地理编码
- (void)reGeoCodeWithLocation:(CLLocationCoordinate2D)coor result:(MapRegeoSearchBlock)regeoBlock;

//生成短串
- (void)generateShareCodeWithName:(NSString *)name location:(CLLocationCoordinate2D)coor block:(GenerateShareBlock)block;

//模糊搜索
- (void)onInputTipSearch:(NSString *)keyWords inCity:(NSString *)city block:(MapSearchResult)result;

@end

NS_ASSUME_NONNULL_END
