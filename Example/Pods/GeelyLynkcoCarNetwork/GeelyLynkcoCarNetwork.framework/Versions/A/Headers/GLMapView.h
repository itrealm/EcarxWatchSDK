//
//  GLMapView.h
//  Pods
//
//  Created by 朱伟特 on 2019/5/29.
//

#import <UIKit/UIKit.h>

#if __has_include(<MAMapKit/MAMapKit.h>)
#import <MAMapKit/MAMapKit.h>
#else
#import <AMapNaviKit/MAMapKit.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface GLMapView : MAMapView


@end

NS_ASSUME_NONNULL_END
