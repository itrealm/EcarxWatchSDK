//
//  GLMenuEditViewController.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2018/9/5.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import "GLBaseViewController.h"

@interface GLMenuItemHeaderViewCell : UICollectionReusableView
@property (nonatomic, copy) NSString * title;
@end


@interface GLMenuItemCollectionViewCell : UICollectionViewCell


@property (nonatomic, copy) NSString * imageName;
@property (nonatomic, strong) UIImage * image;
@property (nonatomic, copy) NSString * title;

@property (nonatomic, assign) BOOL enable;
@property (nonatomic, assign) BOOL isInHome;

@property (nonatomic, strong) GLBadgeItem* badgeItem;

@property (nonatomic, strong) void(^operationButtonClicked)(void);

@end

@interface GLMenuEditViewController : GLBaseViewController

@end
