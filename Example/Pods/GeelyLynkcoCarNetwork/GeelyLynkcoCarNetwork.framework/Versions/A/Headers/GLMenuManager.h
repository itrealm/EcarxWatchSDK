//
//  GLMenuManager.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2018/9/5.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLBadgeLabel.h"

typedef NS_ENUM(NSInteger, GLMenuItemType) {
    GLMenuItemTypeNative = 0,
    GLMenuItemTypeH5,
    GLMenuItemTypeWeex,
    GLMenuItemTypeRouter,
    GLMenuItemTypeOther,
};

extern NSString * const kGLMenuManagerMenuUpdatedNotification;


@interface GLMenuItem : NSObject
@property(nonatomic,strong)NSString* title;
@property(nonatomic,strong)NSString* image;
@property(nonatomic,strong)NSString* selectedImage;

@property(nonatomic,assign)GLMenuItemType type;
@property(nonatomic,assign)BOOL needToken;
@property(nonatomic,assign)BOOL needCarOwner;

@property(nonatomic,strong)NSString* url;


@property(nonatomic,strong)NSString* key;
@property(nonatomic,assign)NSInteger tag;

@property(nonatomic,strong)NSNumber* sort;

@property(nonatomic,strong)NSNumber* defaultSort;

@property(nonatomic,strong)void(^actionBlock)(void);

@property(nonatomic,assign)BOOL needLogin;
@property(nonatomic,assign)BOOL navigationBarHidden;


@property(nonatomic,assign)BOOL isMore;
@property(nonatomic,assign)BOOL isInHome;

@end

@interface GLMenuSection : NSObject
@property(nonatomic,strong)NSString* title;
@property(nonatomic,strong)NSArray<GLMenuItem*>* items;
@property(nonatomic,assign)NSInteger rowNum;

@property(nonatomic,strong)NSString* key;
@property(nonatomic,assign)NSInteger tag;

@end


@interface GLMenuManager : NSObject
@property(nonatomic,weak)UIViewController* viewController;
@property(nonatomic,assign)NSUInteger homeMaxNum;
@property(nonatomic,readonly)NSArray* datas;

+ (GLMenuManager*)sharedManager;


-(GLMenuSection*)homeMenuSection;

-(void)actionByViewController:(UIViewController*)ctl item:(GLMenuItem*)item;

-(void)updateMenuData:(NSArray<GLMenuItem*>*)datas;

- (UIViewController *)createWeexHost:(NSString *)remoteUrl;
- (UIViewController *)createWeexHost:(NSString *)remoteUrl contentId:(NSString*)contentId;
@end
