//
//  GLMyCarViewModel.h
//  EricssonTcGeelyProject
//
//  Created by Bias.Xie on 17/2/17.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import <GLServiceSDK/GLServiceSDK.h>

@interface GLMyCarViewModel : BaseTableViewModel

@property (nonatomic, assign) BOOL firstLogin;
// 关联按钮响应
@property (readonly, strong, nonatomic) RACCommand *relevanceCommand;
- (RACSignal *)vehicleListSignal;
@end
