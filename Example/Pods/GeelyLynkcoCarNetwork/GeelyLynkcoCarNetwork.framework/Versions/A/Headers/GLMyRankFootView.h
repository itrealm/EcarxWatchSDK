//
//  GLMyRankFootView.h
//  GeelyConsumerCarNetwork
//
//  Created by yang.duan on 2019/7/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLMyRankFootView : UIView
@property (strong, nonatomic) UIImageView *imgView;
@property (strong, nonatomic) UILabel *nameLabel;
@property (strong, nonatomic) UILabel *rankLabel;
@property (strong, nonatomic) UILabel *detailLabel;
@property (strong, nonatomic) UILabel *distanceLabel;
@property (strong, nonatomic) UILabel *oilLabel;

@end

NS_ASSUME_NONNULL_END
