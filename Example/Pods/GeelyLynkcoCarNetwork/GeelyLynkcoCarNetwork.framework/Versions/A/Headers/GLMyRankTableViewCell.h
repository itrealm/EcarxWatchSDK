//
//  GLMyRankTableViewCell.h
//  GeelyConsumerCarNetwork
//
//  Created by yang.duan on 2019/7/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLMyRankTableViewCell : UITableViewCell


//  @{@"time":@"03-13 13:23:10",@"rank":@"敬爱的车主您好，您的爱车y忘记锁门了，再不去看，爱车就要被偷走了！",@"name":@"查看详情",@"totalDistance":@"查看详情",@"totalOil":@"查看详情",@"isTotalOil":@""},

- (void)addValueWithData:(id)data;

@end

NS_ASSUME_NONNULL_END
