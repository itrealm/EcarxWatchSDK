//
//  GLMyRankingListViewModel.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/5/3.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import <GLServiceSDK/GLServiceSDK.h>

typedef enum : NSUInteger {
    GLRankingOdometerType, //总里程
    GLRankingAveFuelType,//燃料消耗
} GLRankingType;

typedef enum : NSUInteger {
    GLRankingRadius30, //附近30km
    GLRankingRadiusNationwide,//全国
} GLRankingRadius;

@interface GLMyRankingListViewModel : BaseTableViewModel

@property (nonatomic, assign) GLRankingType rankingType;
@property (nonatomic, assign) GLRankingRadius rankingRadius;
@property (nonatomic, strong) GLVehicleRanking *myRanking;
@property (nonatomic, strong, readonly) GLVehicleRankingOdometerResponse *odometerResponse;
@property (nonatomic, strong, readonly) GLVehicleRankingAveFuelResponse *aveFuelResponse;
// 用户信息
@property (nonatomic, copy, readonly) GLUser *user;
// 排名接口
@property (nonatomic, strong, readonly) RACCommand *requestCommand;

@end
