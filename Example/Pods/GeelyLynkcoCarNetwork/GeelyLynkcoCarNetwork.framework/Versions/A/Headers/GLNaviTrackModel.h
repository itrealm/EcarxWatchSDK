//
//  GLNaviTrackModel.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/3.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import "GLMapBaseModel.h"
#import <AMapNaviKit/AMapNaviKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, GLNaviTrackType){
    GLNaviTrackByCar  = 0,//开车
    GLNaviTrackByRide = 1,//骑车
//    GLNaviTrackByBus  = 2,//公交
    GLNaviTrackByWalk     //走路
};

typedef void(^RoutePlanResult)(id result);

@interface GLNaviTrackModel : GLMapBaseModel

@property (nonatomic, assign) GLNaviTrackType naviType;//导航方式，default is GLNaviTrackByCar

- (void)trackFromPoint:(CLLocationCoordinate2D)startPoint toPoint:(CLLocationCoordinate2D)endPoint trackType:(GLNaviTrackType)trackType result:(RoutePlanResult)result;

//- (void)


@end

NS_ASSUME_NONNULL_END
