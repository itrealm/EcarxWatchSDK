//
//  GLNavigateTool.h
//  Geelyconsumer
//
//  Created by 朱伟特 on 2018/8/28.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, NavigateMapType){
    MapGaode   = 1,  //高德地图客户端
    MapSystem  = 2,  //iOS自带高德地图
    MapWeb     = 3,  //网页高德地图(打开网页之后，会自动检查有没有安装高德地图客户端，如果安装了会提示跳转到高德地图客户端)
};


typedef NS_ENUM(NSUInteger, GLNavigateTravelType){
    GLTravelDriving   = 0,  //开车
    GLTravelWalking   = 1,  //走路
    GLTravelTransit   = 2,  //公交
};

@interface GLNavigateTool : NSObject

/*单例*/
+ (id)shareInstance;

/**
 从某个地方导航到某个地方，使用高德地图，默认NavigateMapType = MapGaode
 @param coor 开始的坐标
 @param desCoor 目的地坐标
 @return automatic，如果用户手机上没有安装高德地图客户端，是否自动切换到iOS自带的高德地图中或者使用网页高德地图,默认YES。依照NavigateMapType从小到大依次尝试
 */
- (void)navigateFrom:(CLLocationCoordinate2D)coor fromAddress:(NSString *)fromAddress To:(CLLocationCoordinate2D)desCoor destination:(NSString *)des travelType:(GLNavigateTravelType)travelType automatic:(BOOL)automatic;

/**
 从某个地方导航到某个地方，使用高德地图
 @param coor 开始的坐标
 @param desCoor 目的地坐标
 @param type 地图客户端选取方式
 @return automatic，如果用户手机上没有安装高德地图客户端，是否自动切换到iOS自带的高德地图中或者使用网页高德地图,默认YES。依照NavigateMapType从小到大依次尝试
 */
- (void)navigateFrom:(CLLocationCoordinate2D)coor fromAddress:(NSString *)fromAddress To:(CLLocationCoordinate2D)desCoor destination:(NSString *)des travelType:(GLNavigateTravelType)travelType withMapType:(NavigateMapType)type automatic:(BOOL)automatic;

/**
 跳转到高德地图并显示定位点
 @param coor 开始的坐标
 @param addressName 定位点名称
 @param type 地图客户端选取方式
 @return automatic，如果用户手机上没有安装高德地图客户端，是否自动切换到iOS自带的高德地图中或者使用网页高德地图,默认YES。依照NavigateMapType从小到大依次尝试
 */
- (void)jumpToGaodeWithCoor:(CLLocationCoordinate2D)coor addressName:(NSString *)addressName withMapType:(NavigateMapType)type automatic:(BOOL)automatic;


+(double)distanceFromDist:(CLLocationCoordinate2D)location1 to:(CLLocationCoordinate2D)location2;

/**
 跳转第三方地图搜索POI
 @param poiName 搜索内容
 */
- (void)searchPOIInThirdMap:(NSString*)poiName;

@end
