//
//  GLNewDrivingLogTableViewCell.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2020/4/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class GLJournalLogRegeoDetail;

@interface GLNewDrivingLogTableViewCell : UITableViewCell

@property (strong, nonatomic) GLJournalLogRegeoDetail *model;
@property (assign, nonatomic) BOOL showFuel;

@end

NS_ASSUME_NONNULL_END
