//
//  GLNewMapManager.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/5/29.
//

#import <Foundation/Foundation.h>
#import "GLMapView.h"
#import "GLNaviTrackModel.h"
#import "GLMapSearchModel.h"
#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

#define kMapManager [GLNewMapManager sharedManager]

typedef void(^MapBlock)(void);

typedef void(^CarAddressBlock)(NSString * address);

typedef void(^SearchResultBlock)(BOOL success, NSArray * results);

typedef void(^ManualUpdateUserLocation)(NSString * address);

typedef void(^TouchPoiBlock)(id obj);

typedef void(^TapBlankBlock)(CLLocationCoordinate2D coor);

typedef void(^PoiClickBlock)(id obj);

typedef void(^ShareSearchBlock)(NSString * shareURL);

typedef void(^UserLocationBlock)(CLLocationCoordinate2D userCoor,NSError* error);

@interface GLNewMapManager : NSObject

+ (GLNewMapManager *)sharedManager;

@property (nonatomic, strong) GLMapView * mapView;

@property (nonatomic, assign) BOOL showUserLocation;

@property (nonatomic, assign) CLLocationCoordinate2D carLocation;//车辆坐标

@property (nonatomic, copy) NSString * carLocationAddress;//车辆位置信息

@property (nonatomic, assign) CLLocationCoordinate2D userCoor;//用户位置坐标

@property (nonatomic, copy) NSString * userLocationName;//用户位置名

@property (nonatomic, copy) NSString * userLocationAddress;//用户位置地址

@property (nonatomic, weak) UIViewController * viewController;

@property (nonatomic, assign) NSInteger fenceRadius;//围栏半径

@property (nonatomic, strong, readonly) NSArray * annoArray;//最多包含两个大头针

//初始化地图
- (void)initMapView;
//带回调的地图初始化方法
- (void)initMapViewWithBlock:(MapBlock)block;
//设置地图定位点
- (void)setCenterCoor:(CLLocationCoordinate2D)coor withAnno:(nullable MAPointAnnotation *)anno animate:(BOOL)animate;
//是否显示交通情况
- (void)showTraffic:(BOOL)showTraffic;
//是否显示缩放按键
- (void)showScaleView:(BOOL)show;
//获取用户位置
- (void)getUserLocation:(UserLocationBlock)userLocationBlock;
//是否显示缩放按键
- (void)showScaleView:(BOOL)show withPosition:(CGPoint)position;
//让地图恢复原样，只显示用户定位
- (void)mapViewSetStateNormal;
//移除某个大头针
- (void)mapViewRemoveAnno:(MAPointAnnotation *)anno;
//移除所有大头针
- (void)mapViewRemoveAllAnnos;
//移除所有overlay
- (void)mapViewRemoveAllOverlays;
- (void)mapViewRemoveAllOverlaysExclude:(NSArray<Class>*)classes;
//显示用户定位大头针
- (void)showUserLocation:(BOOL)show;
//显示用户定位大头针
- (void)showUserLocationWithAnimate:(BOOL)animate;
//手动更新用户定位
- (void)manualUpdateUserLocation:(ManualUpdateUserLocation)updateBlock;
//显示车辆定位大头针,是否第一次显示。第一次的话会保持用户位置和车辆位置保持在一个页面上。不是第一次的话会将b车辆和用户位置保持在一个页面上的同时将车辆大头针居中地图
- (void)showCarLocationWithFirstTime:(BOOL)firstTime;
//显示车辆定位大头针,是否第一次显示。第一次的话会保持用户位置和车辆位置保持在一个页面上。不是第一次的话会将b车辆和用户位置保持在一个页面上的同时将车辆大头针居中地图，并返回地址信息
- (void)showCarLocationWithFirstTime:(BOOL)firstTime address:(nullable CarAddressBlock)address;
//显示车辆定位大头针
- (void)showCarLocation:(BOOL)show;
//显示车辆定位大头针，并返回地址信息
- (void)showCarLocation:(BOOL)show address:(nullable CarAddressBlock)address;
//添加一组大头针
- (void)addAnomationWithArray:(NSArray *)array;
//添加一组大头针并且设置UIEdgeInset
- (void)addAnomationWithArray:(NSArray *)array inset:(UIEdgeInsets)insets;
//添加一个大头针（车子）
- (void)addCarAnomationWithCoor:(CLLocationCoordinate2D)coor;
//根据关键字和范围(米)搜索附近,并以大头针的方式展现在地图上
- (void)searchAroundWithKeyWords:(NSString *)keywords round:(NSUInteger)round showOnMap:(BOOL)showOnMap;
//根据关键字和范围(米)搜索附近
- (void)searchAroundWithKeyWords:(NSString *)keywords round:(NSUInteger)round showOnMap:(BOOL)showOnMap searchResult:(nullable SearchResultBlock)results;
//根据关键字模糊搜索
- (void)searchWithKeyWords:(NSString *)keyWords searchResult:(nullable SearchResultBlock)results;
//根据关键字搜索Poi地址
- (void)searchWithPoiKeyWords:(NSString *)keyWords searchResult:(nullable SearchResultBlock)results;
//路径规划
- (void)routerPlanFrom:(CLLocationCoordinate2D)startPoint to:(CLLocationCoordinate2D)endPoint inset:(UIEdgeInsets)inset type:(GLNaviTrackType)trackType;
//根据一组定位坐标绘制一条路线
- (void)createPolyLineWithPoints:(CLLocationCoordinate2D *)coors count:(NSInteger)count edgePadding:(UIEdgeInsets)insets bindingRoad:(BOOL)binding;
//创建围栏
- (void)createFenceWithCenter:(CLLocationCoordinate2D)coor radius:(NSInteger)radius withAnno:(BOOL)withAnno;
//更改围栏半径
- (void)updateFenceWithRadius:(NSInteger)radius;
//删除围栏
- (void)deleteAllFence;
//点击地图上的Poi
- (void)didTouchPoiWithBlock:(TouchPoiBlock)touchBlock;
//点击地图空白地方
- (void)didTouchBlankWithBlock:(TapBlankBlock)tapBlock;
//根据某个坐标转为地址
- (void)reGeoCodeWithLocation:(CLLocationCoordinate2D)coor result:(MapRegeoSearchBlock)regeoBlock;
//根据某个地址转为坐标
- (void)geoCodeWithAddress:(NSString *)address result:(MapGeoSearchBlock)geoBlock;
//点击某个大头针
- (void)clickPoiAnnoWithBlock:(PoiClickBlock)poiClickBlock;
//计算两个点之间的距离
+ (CLLocationDistance)distanceBetweenPoint:(CLLocationCoordinate2D)firstPoint toPoint:(CLLocationCoordinate2D)secondPoint;


- (void)showAllAnnos;
//将GPS定位点转为高德定位点
+ (CLLocationCoordinate2D)convertPoint:(CLLocationCoordinate2D)coor;
//根据type将定位点转为高德定位点
+ (CLLocationCoordinate2D)convertPoint:(CLLocationCoordinate2D)coor withType:(AMapCoordinateType)coorType;

//生成坐标短串
- (void)generateShareSearchStringWithAddress:(NSString *)address Location:(CLLocationCoordinate2D)coor block:(ShareSearchBlock)shareBlock;

    /**
     GPS转换成高德坐标
     */
+ (CLLocationCoordinate2D)aMapCoordinateConvert:(GLPosition *)position;
    
    /**
     静态地图——接送好友弹窗
     
     @param size 大小
     @param longitude 经度
     @param latitude 纬度
     @return 地图图片地址
     */
+ (NSString *)staticMapPickUpFriendWithSize:(CGSize)size longitude:(CLLocationDegrees)longitude latitude:(CLLocationDegrees)latitude;
    
    /**
     静态地图
     
     @param size 大小
     @param longitude 经度
     @param latitude 纬度
     @return 地图图片地址
     */
+ (NSString *)staticMapWithSize:(CGSize)size longitude:(CLLocationDegrees)longitude latitude:(CLLocationDegrees)latitude;
    
    /**
     静态地图
     
     @param size 大小
     @return 地图图片地址
     */
+ (NSString *)staticMapWithSize:(CGSize)size trackPoint:(NSArray *)trackPoint wayPoint:(NSArray *)wayPoint;
    
    
+ (BOOL)isAvailableForCoordinate:(CLLocationCoordinate2D)coordinate;


//销毁单例
- (void)destoryManager;
    
    

@end

NS_ASSUME_NONNULL_END
