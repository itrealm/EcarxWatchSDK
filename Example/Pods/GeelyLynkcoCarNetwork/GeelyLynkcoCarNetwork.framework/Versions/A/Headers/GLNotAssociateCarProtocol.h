//
//  GLNotAssociateCarProtocol.h
//  EricssonTcGeelyProject
//
//  Created by 杨沁 on 2017/6/21.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIScrollView+EmptyDataSet.h"

@interface GLNotAssociateCarProtocol : NSObject <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

+ (instancetype)sharedManager;

@end
