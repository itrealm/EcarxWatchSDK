//
//  GLNotifiInfoViewController.h
//  GeelyConsumerCarNetwork
//
//  Created by yang.duan on 2019/7/9.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import "GLCarBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLNotifiInfoViewController : GLCarBaseViewController

@end

NS_ASSUME_NONNULL_END
