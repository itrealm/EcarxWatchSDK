//
//  GLNotifiListTableViewCell.h
//  GeelyConsumerCarNetwork
//
//  Created by yang.duan on 2019/7/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class GLNotification;
@interface GLNotifiListTableViewCell : UITableViewCell

-(void)updateData:(GLNotification *)data;

@end

NS_ASSUME_NONNULL_END
