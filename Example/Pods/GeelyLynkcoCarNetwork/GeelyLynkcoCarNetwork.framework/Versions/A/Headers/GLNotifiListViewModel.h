//
//  GLNotifiListViewModel.h
//  GeelyConsumerCarNetwork
//
//  Created by yang.duan on 2019/7/9.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import "GLNotification.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLNotifiListViewModel : BaseTableViewModel
@property (nonatomic, copy) NSString *notificationType;


+ (void)initComponent;
+(BOOL)insertModel:(GLNotification*)model;
+(NSArray<GLNotification*>*)getNotificationsByType:(NSInteger)type onlyUnRead:(BOOL)yesOrNot;
+(NSArray<GLNotification*>*)getNotificationsByType:(NSInteger)type key:(NSString*)key value:(NSString*)value;
//删除数据
- (void)refreshData;

/* 存储通知消息 */
+ (BOOL)saveNotification:(NSString *)title detail:(NSString *)detail time:(NSString *)time type:(NSString *)type;
+ (BOOL)saveNotificationModel:(GLNotification*)model;


//已读未读提示
+(BOOL)setNotificationRead:(BOOL)yesOrNot type:(NSInteger)type;
+ (void)setHasUnreadNotification:(BOOL)hasUnread;
+ (BOOL)hasUnreadNofitcaiton;
@end

NS_ASSUME_NONNULL_END
