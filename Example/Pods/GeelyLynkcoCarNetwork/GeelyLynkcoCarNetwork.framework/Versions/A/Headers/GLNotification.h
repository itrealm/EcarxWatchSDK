//
//  GLNotification.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2017/6/1.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLServiceSDK/GLServiceSDK.h>

//通知TAG
typedef NS_ENUM(NSUInteger, GLNotificationTag) {
	GLNotificationTagAdvertisement = 1, //资讯
	GLNotificationTagRemoteControl = 2, //遥控
	GLNotificationTagWaring = 3,		//告警
	GLNotificationTagOther  = 4,		//其它
    GLNotificationTagShare  = 5,        //车分享
};

@interface GLNotification : BaseModel

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, assign) NSInteger type;
@property (nonatomic, strong) NSString *time;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *vin;
@property (nonatomic, strong) NSString *jsonContent;


@property (nonatomic, assign) NSTimeInterval timeInterval;

@property (nonatomic, assign) BOOL readFlag;

+(NSString*)typeStringByType:(GLNotificationTag)type;
@end
