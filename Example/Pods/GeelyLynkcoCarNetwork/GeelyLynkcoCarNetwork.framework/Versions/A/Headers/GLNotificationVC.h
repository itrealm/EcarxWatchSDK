//
//  GLNotificationVC.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/8/9.
//

#import "GLCarBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLNotificationVC : GLCarBaseViewController
@property(nonatomic,assign)NSNumber* defaultSelectedType;
@end

NS_ASSUME_NONNULL_END
