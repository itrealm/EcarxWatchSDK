//
//  GLUpLoadImageTools.h
//  Geelyconsumer
//
//  Created by LimboDemon on 2018/9/21.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, GLOSSImageTaskState) {
    GLOSSImageTaskStateNone,          // regular table view
    GLOSSImageTaskStateUploading,        // sections are grouped together
    GLOSSImageTaskStateSuccess,
    GLOSSImageTaskStateFailed,
};

@interface GLOSSImageTask : NSObject
@property(nonatomic,strong)UIImage* image;
@property(nonatomic,strong)NSString* url;
@property(nonatomic,strong)NSString* path;
@property(nonatomic,assign)CGFloat progress;
@property(nonatomic,assign)GLOSSImageTaskState state;
@property(nonatomic,strong)NSString* key;
@property(nonatomic,strong)void(^competionBlock)(GLOSSImageTaskState state,NSString* url);
@property(nonatomic,strong)void(^successBlock)(id result);
@property(nonatomic,strong)void(^failedBlock)(NSError* error);

@end

@interface GLOSSImageManager : NSObject
+ (instancetype)sharedManager;
-(BOOL)startTask:(GLOSSImageTask*)task;
-(void)removeTaskByKey:(NSString*)key;
-(GLOSSImageTask*)taskByKey:(NSString*)key;
@end
