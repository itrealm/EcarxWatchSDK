//
//  GLEditArticleMainImageTableViewCell.h
//  富文本
//
//  Created by LimboDemon on 2018/9/13.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GLOSSUploadImageState) {
    GLOSSUploadImageStateNone = 0,
    GLOSSUploadImageStateUploading,
    GLOSSUploadImageStateUploadFalied,
    GLOSSUploadImageStateUploadSuccess,
};

@interface GLOSSUploadImageView : UIImageView

@property (nonatomic, copy) void(^imageDeleteBlock)(GLOSSUploadImageView* imageView);
@property (nonatomic, assign) BOOL showDeleteButton;
@property (nonatomic, strong) NSString* url;

@property (nonatomic, assign) GLOSSUploadImageState uploadState;

@end
