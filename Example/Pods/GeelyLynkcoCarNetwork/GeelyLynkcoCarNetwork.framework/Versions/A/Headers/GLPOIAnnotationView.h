//
//  GLMapPOIAnnotationView.h
//  EricssonTcGeelyProject
//
//  Created by Fish on 2017/3/20.
//  Copyright © 2017年 ecarx. All rights reserved.
//


//#import <MAMapKit/MAMapKit.h>
#if __has_include(<MAMapKit/MAMapKit.h>)
#import <MAMapKit/MAMapKit.h>
#else
#import <AMapNaviKit/MAMapKit.h>
#endif

#import <AMapSearchKit/AMapSearchKit.h>

@interface GLMapPOIAnnotationView : MAAnnotationView

@property (nonatomic,assign) NSInteger badge;

@end

@interface GLMapPOIAnnotation : MAPointAnnotation

@property (nonatomic, assign) NSUInteger tag;

@property (nonatomic, strong) AMapPOI * poi;

@end
