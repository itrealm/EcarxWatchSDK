//
//  GLPullMenuCell.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/23.
//

#import <UIKit/UIKit.h>
#import "GLPullMenuModel.h"

NS_ASSUME_NONNULL_BEGIN
 @interface GLPullMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuTitleLab;
/**
 *  model
 */
@property (nonatomic, strong) GLPullMenuModel *menuModel;
/**
 * 最后一栏cell
 */
@property (nonatomic, assign) BOOL isFinalCell;
/**
 *  pullMenu样式
 */
@property (nonatomic, assign) GLPullMenuStyle glPullMenuStyle;

@end

NS_ASSUME_NONNULL_END
