//
//  GLPullMenuModel.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, GLPullMenuStyle) {
    GLPullMenuStyleDark = 0,  //类微信、黑底白字
    GLPullMenuStyleLight      //类支付宝、白底黑字
};

@interface GLPullMenuModel : NSObject
/**
 * 文字
 */
@property (nonatomic, copy) NSString *title;
/**
 * 图片
 */
@property (nonatomic, copy) NSString *imageName;

/**
 是否被选中
 */
@property (nonatomic, assign) BOOL isSelect;

@end

NS_ASSUME_NONNULL_END
