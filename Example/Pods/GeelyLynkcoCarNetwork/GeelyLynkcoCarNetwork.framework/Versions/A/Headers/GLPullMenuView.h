//
//  GLPullMenuView.h
//  GeelyLynkcoCarNetwork
//
//  Created by yang.duan on 2019/7/23.
//

#import <UIKit/UIKit.h>
#import "GLPullMenuModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^BlockSelectedMenu)(NSInteger menuRow);

@interface GLPullMenuView : UIView
/**
 *  文字
 */
@property (nonatomic, copy) NSArray *titleArray;
/**
 *  图片
 */
@property (nonatomic, copy) NSArray *imageArray;
/**
 *  是否被选中
 */
@property (nonatomic, copy) NSArray *selectArray;
/**
 *  图文Model
 */
@property (nonatomic, copy) NSArray<GLPullMenuModel *> *menuArray;
/**
 *  蒙层背景color
 */
@property (nonatomic, strong) UIColor *coverBgColor;
/**
 *  主样式color
 */
@property (nonatomic, strong) UIColor *menuBgColor;
/**
 *  cel高度
 */
@property (nonatomic, assign) CGFloat menuCellHeight;
/**
 *  小三角高度
 *  45°等腰三角形
 */
@property (nonatomic, assign) CGFloat triangleHeight;
/**
 *  pullMenu样式
 */
@property (nonatomic, assign) GLPullMenuStyle glPullMenuStyle;
/**
 *  click
 */
@property (nonatomic, copy) BlockSelectedMenu blockSelectedMenu;
/**
 *  anchorView：下拉依赖视图[推荐初始化]
 *  箭头指向依赖视图
 *  titleArray:文字
 *  imageArray:icon
 *  menuArray:图文Model
 */
+ (instancetype)pullMenuAnchorView:(UIView *)anchorView;
+ (instancetype)pullMenuAnchorView:(UIView *)anchorView titleArray:(NSArray *)titleArray;
+ (instancetype)pullMenuAnchorView:(UIView *)anchorView titleArray:(NSArray *)titleArray imageArray:(NSArray *)imageArray;
+ (instancetype)pullMenuAnchorView:(UIView *)anchorView titleArray:(NSArray *)titleArray imageArray:(NSArray *)imageArray selectArray:(NSArray *)selectArray;

+ (instancetype)pullMenuAnchorView:(UIView *)anchorView menuArray:(NSArray<GLPullMenuModel *> *)menuArray;

/**
 *  anchorView：下拉依赖绝对坐标
 *  指定坐标下拉
 *  箭头指向点
 *  titleArray:文字
 *  imageArray:icon
 *  menuArray:图文Model
 */
+ (instancetype)pullMenuAnchorPoint:(CGPoint)anchorPoint;
+ (instancetype)pullMenuAnchorPoint:(CGPoint)anchorPoint titleArray:(NSArray *)titleArray;
+ (instancetype)pullMenuAnchorPoint:(CGPoint)anchorPoint titleArray:(NSArray *)titleArray imageArray:(NSArray *)imageArray;
+ (instancetype)pullMenuAnchorPoint:(CGPoint)anchorPoint menuArray:(NSArray<GLPullMenuModel *> *)menuArray;


@end


NS_ASSUME_NONNULL_END
