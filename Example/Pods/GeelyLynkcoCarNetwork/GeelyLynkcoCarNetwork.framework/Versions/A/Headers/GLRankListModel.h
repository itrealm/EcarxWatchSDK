//
//  GLRankListModel.h
//  GeelyConsumerCarNetwork
//
//  Created by yang.duan on 2019/7/5.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import <GLServiceSDK/GLServiceSDK.h>

typedef enum : NSUInteger {
    GLRankingOdometerType, //总里程
    GLRankingAveFuelType,//燃料消耗
} GLRankingType;

typedef enum : NSUInteger {
    GLRankingRadius30, //附近30km
    GLRankingRadiusNationwide,//全国
} GLRankingRadius;

NS_ASSUME_NONNULL_BEGIN

@interface GLRankListModel : BaseTableViewModel

@property (nonatomic, assign) GLRankingType rankingType; //排行类型
@property (nonatomic, assign) GLRankingRadius rankingRadius; //排行位置范围
@property (nonatomic, strong) GLVehicleRanking *myRanking; //排行的model
@property (nonatomic, strong, readonly) GLVehicleRankingOdometerResponse *odometerResponse;
@property (nonatomic, strong, readonly) GLVehicleRankingAveFuelResponse *aveFuelResponse;
// 用户信息
@property (nonatomic, copy, readonly) GLUser *user;
// 排名接口
@property (nonatomic, strong, readonly) RACCommand *requestCommand;

@end

NS_ASSUME_NONNULL_END
