//
//  GLRichTextTableViewCell.h
//  GLCoreKit-Example
//
//  Created by 匡志勇 on 2019/11/1.
//  Copyright © 2019年 匡志勇. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLFormBaseCell.h"
@class YYTextView;

UIKIT_EXTERN NSString *const GLRichTextDidBeginEditingNotification;
UIKIT_EXTERN NSString *const GLRichTextDidChangeNotification;
UIKIT_EXTERN NSString *const GLRichTextDidEndEditingNotification;

@interface GLRichTextTableViewCell : GLFormBaseCell

@property (nonatomic, strong) NSString *gl_attachment_key;

@property (nonatomic, readonly) YYTextView *textView;
@property (nonatomic, assign) CGFloat minHeight;

@property (nonatomic, assign) CGFloat spaceFromKeyboard;

@property (nonatomic, assign) NSInteger limitOfTextNumber;
@property (nonatomic, strong) void(^reachMaxTextNumberCallback)(YYTextView* textView);

@property (nonatomic, strong) void(^textViewDidChange)(YYTextView*textView);

@property (nonatomic, strong) void(^textViewTextChanged)(YYTextView*textView);


-(void)insertImage:(UIImage*)image forKey:(NSString*)key;
-(void)insertImage:(UIImage*)image size:(CGSize)size forKey:(NSString*)key;
-(void)insertAttachment:(UIView*)attachment size:(CGSize)size forKey:(NSString*)key;
-(void)insertAttributeText:(NSString *)text;


-(void)removeAttachmentForKey:(NSString*)key;
-(UIView*)getAttachmentForKey:(NSString*)key;
-(void)asyncUpdataFrame;
- (BOOL)resignFirstResponder;
@end


@interface NSObject (GLRichText)
@property (nonatomic, strong) NSString *gl_attachment_key;

@end
