//
//  GLSearchTextField.h
//  CarNetwork_Example
//
//  Created by 朱伟特 on 2019/6/17.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLSearchTextField : UITextField

@end

NS_ASSUME_NONNULL_END
