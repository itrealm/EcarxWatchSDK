//
//  timeChooseSelectTableViewCell.h
//  timeSelect
//
//  Created by yang.duan on 2020/3/31.
//  Copyright © 2020 yang.duan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLTimeChooseSelectTableViewCell : UITableViewCell

@property (nonatomic, assign)BOOL anyFinishTime; /**是否是结束时间*/
@property (nonatomic, copy)NSString *timeString; /**从新设置时间*/
@property (nonatomic, copy)void(^selectTimeBlock)(BOOL finishTime);

@end

NS_ASSUME_NONNULL_END
