//
//  timeChooseSelectViewController.h
//  timeSelect
//
//  Created by yang.duan on 2020/3/31.
//  Copyright © 2020 yang.duan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^selectTimeBlock)(NSDate *begin,NSDate *end);
@interface GLTimeChooseSelectViewController : GLBaseViewController

@property (nonatomic, copy)NSDate* startDate;
@property (nonatomic, copy)NSDate* endDate;

@property (nonatomic, copy)selectTimeBlock block;
@end

NS_ASSUME_NONNULL_END
