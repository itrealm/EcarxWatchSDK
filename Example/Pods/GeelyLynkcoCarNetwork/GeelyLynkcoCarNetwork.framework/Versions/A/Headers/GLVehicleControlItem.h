//
//  GLVehicleControlItem.h
//  Pods
//
//  Created by fish on 2017/4/11.
//
//

#import <Foundation/Foundation.h>
#import <GLServiceSDK/GLServiceSDK.h>

@protocol GLVehicleControlItem;

@interface GLVehicleControlItem : NSObject

@property (nonatomic,strong) UIFont   *titleFont;
@property (nonatomic,strong) UIColor  *titleColor;
@property (nonatomic, assign) BOOL     selected;

@property (nonatomic,  copy) NSString *title;
@property (nonatomic,  copy) NSString *iconNormalImage;
@property (nonatomic,  copy) NSString *iconDisableImage;
@property (nonatomic,  copy) NSString *iconSelectedImage;

@property (nonatomic,  copy) NSString *pin;
@property (nonatomic,  copy) NSString *functionId;
@property (nonatomic,  copy) NSString *command;
@property (nonatomic,assign) NSInteger waitTime;
@property (nonatomic,  copy) NSString *serviceId;
@property (nonatomic,  copy) NSArray<GLServiceParameters> *serviceParameters;
@property (nonatomic,assign) NSUInteger duration;
@property (nonatomic,  copy) NSNumber* scheduledTime;



@property (nonatomic, copy) GLVehicleCapability *capability;

@end
