 //
//  GLVehicleInformationViewModel.h
//  GeelyConsumer
//
//  Created by 杨沁 on 2017/8/1.
//  Copyright © 2017年 Rdic. All rights reserved.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import <GLServiceSDK/GLServiceSDK.h>
#import "GLDrivingLogStatusViewModel.h"

@class ExFlowData;

@interface DashBoardData : BaseViewModel

@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *value;
@property (nonatomic, copy) NSString *unit;

@end

@interface GLVehicleInformationViewModel : BaseTableViewModel

// 充电数据
//@property (readonly, nonatomic, copy) GLVehicleStatusElectricVehicle *electricVehicleStatus;
// 仪表盘数据
@property (readonly, nonatomic, copy) NSArray<DashBoardData *> *dashBoardDataSource;
@property (readonly, nonatomic, copy) NSDictionary<NSString*,DashBoardData *> *dashBoardInfo;

// 剩余流量数据
@property (readonly, nonatomic, strong) ExFlowData* flowData;

@property (readonly, nonatomic, assign) NSInteger usedFlow;


// 行车日志状态ViewModel
@property (strong, nonatomic) GLDrivingLogStatusViewModel *drivingLogStatusViewModel;

@end
