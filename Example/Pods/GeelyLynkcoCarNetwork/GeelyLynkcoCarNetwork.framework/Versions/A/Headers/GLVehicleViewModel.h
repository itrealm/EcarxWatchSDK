//
//  GLVehicleViewModel.h
//  Pods
//
//  Created by Fish on 2017/3/20.
//
//

#import <GLMVVMKit/GLMVVMKit.h>

@class GLVehiclePartItem,GLVehicleCapability,GLVehicleControlItem;

@interface GLVehicleViewModel : BaseViewModel

//当前关联的车辆
@property (nonatomic,  copy) NSString  *curVehicleVin;
@property (nonatomic,assign) NSInteger  curVehicleType;

//当前操作项
@property (nonatomic,strong) GLVehicleControlItem *curControlItem;



//更新车辆状态
@property (nonatomic,strong) RACCommand *updateVehicleStatusCommand;
//更新车辆能力集
@property (nonatomic,strong) RACCommand *updateVehicleCapabilityCommand;

//远程控制车辆,input :GLVehicleControlItem
@property (nonatomic,strong) RACCommand *controlVehicleCommand;
//命令正在执行
@property (atomic,assign) BOOL isCommandExecuting;
@property (atomic,assign) NSInteger cmdExecutingProgress;

//激活车辆控制权限
@property (nonatomic, strong) RACCommand *telematicsActivateCommand;

@property (nonatomic, copy) NSString *updateTime;
@property (nonatomic, assign) NSTimeInterval updateTimetimeInterval;

//该车是被分享过来的
@property (nonatomic, assign) BOOL isBeenShared;

@end
@interface GLShareVehicleViewModel : GLVehicleViewModel

- (void)initialize;
@end
