//
//  InternationalControl.h
//  G-netlink
//
//  Created by shane west on 16/8/9.
//  Copyright © 2016年 Ericsson. All rights reserved.
//

#import <Foundation/Foundation.h>

// 跟随系统语言
#define kLanguageAuto @"languageAuto"
// 中文字符串
#define kHans @"zh-Hans"
// 英文字符串
#define kEnglish @"en"

// 语言切换通知
#define kLanguageChange   @"languageChange"
// 保存语言key
#define kCurrentLanguage  @"currentLanguage"
// 语言string文件表明

#define kDefaultType      @"languageDefault" // 默认key
#define kChineseType      @"languageChinese" // 中文
#define kEnglishType      @"languageEnglish" // 英文

// ============================= 本地化字符串 =========================== //
#define GLLocalizedString(Key) [[InternationalControl bundle] localizedStringForKey:Key value:nil table:@"G-NETLINK"]
#define XLocalizedString(Key) [[InternationalControl bundle] localizedStringForKey:Key value:nil table:@"GLServiceSDK"]
#define GLVehicleLocalizedString(Key) [[InternationalControl bundle] localizedStringForKey:Key value:nil table:@"LYVehicleModule"]
#define GLVehicleImageNamed(name)     [UIImage imageNamed:(name)]

// 语言切换通知
extern NSString *const LanguageChangeNotification;

@interface InternationalControl : NSObject

/** 获取当前资源文件 */
+(NSBundle *)bundle;


/** 初始化语言文件 */
+(void)initUserLanguage;


/** 获取应用当前语言 */
+(NSString *)userLanguage;


/** 设置当前语言 */
+(void)setUserlanguage:(NSString *)language;


/** 获取系统语言 */
+ (NSString *)systemLanguage;


/**
 *  获取当前语言是否为英文
 *
 *  @return YES:英文； NO：中文
 */
+ (BOOL)english;

+ (BOOL)chinese;

+ (void)loadBundle;

@end






extern NSString * const kVehicleLanguageChange;

@interface NSBundle (Vehicle)
    
+ (NSBundle *)vehicleResBundle;
+ (UIImage  *)vehicleImageNamed:(NSString *)name;
+ (NSString *)vehicleLocalizedStringForKey:(NSString *)key;
    
+ (void)updateUserlanguage:(NSString *)language;
    
@end
