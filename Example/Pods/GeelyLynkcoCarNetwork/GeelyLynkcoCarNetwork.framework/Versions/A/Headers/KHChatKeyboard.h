//
//  KHKeyboard.h
//  KHChart
//
//  Created by 匡志勇 on 2017/6/29.
//  Copyright © 2017年 匡志勇. All rights reserved.
//



#import <UIKit/UIKit.h>

@interface KHChatKeyboardItem : NSObject
@property(nonatomic,strong)NSString* title;
@property(nonatomic,strong)NSString* key;
@property(nonatomic,strong)UIImage* image;
@end

NS_INLINE KHChatKeyboardItem* GLKeyboardItemMake(UIImage* image, NSString* title,NSString* key)
{
    KHChatKeyboardItem* item = [[KHChatKeyboardItem alloc]init];
    item.image = image;
    item.title = title;
    item.key = key;
    return item;
}

@interface KHChatKeyboard : UIView
@property(nonatomic,assign)CGFloat customHeight;
@property(nonatomic,strong)NSArray* datas;
@property(nonatomic,strong)void(^didWhenItemClicked)(KHChatKeyboardItem* item);

-(void)showInView:(UIView*)superView;



@end
