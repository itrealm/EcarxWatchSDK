//
//  LMClickImage.h
//  Test_ClickImage
//
//  Created by 王朝阳 on 16/4/8.
//  Copyright © 2016年 LimboDemon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface LMClickImage : NSObject
/**
 * @brief 浏览头像
 *
 * @param oldImageView 头像所在的imageView
 */
+(void)showImage:(UIImageView *)avatarImageView;
@end
