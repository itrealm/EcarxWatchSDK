//
//  LMSectorProgress.h
//  富文本
//
//  Created by LimboDemon on 2018/9/20.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LMSectorProgress : UIView

/** 进度 */
@property (nonatomic, assign) CGFloat progress;
/** 填充颜色 */
@property (nonatomic, strong) UIColor *fillColor;

/** 初始化 */
- (instancetype)initWithFrame:(CGRect)frame progress:(CGFloat)progress;

@end
