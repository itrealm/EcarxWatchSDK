//
//  LYActivateVehicleViewModel.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/4/13.
//

#import <GLMVVMKit/GLMVVMKit.h>

//激活车辆
@interface LYActivateVehicleViewModel : BaseViewModel

//用户名
@property (nonatomic, copy) NSString *username;
//验证码
@property (nonatomic, copy) NSString *code;
//密码
@property (nonatomic, copy) NSString *password;
//发送按钮标题
@property (readonly, nonatomic, copy) NSString *leftTimeTitle;
//获取验证码
@property (readonly, nonatomic, strong) RACCommand *sendCodeCommand;
//验证验证码
@property (readonly, nonatomic, strong) RACCommand *checkCodeCommand;
//激活
@property (readonly, nonatomic, strong) RACCommand *registerCommand;

@end
