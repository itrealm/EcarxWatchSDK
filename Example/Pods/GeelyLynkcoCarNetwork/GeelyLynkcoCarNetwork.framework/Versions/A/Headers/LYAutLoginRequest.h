//
//  LYAutLoginRequest.h
//  LYVehicleModule
//
//  Created by 杨沁 on 2018/12/11.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface LYAutLoginRequest : ExRequest

/**
 会员中心授权 - 授权登录（领克 OAuth2）
 
 @param authCode    第三方授权code
 @param redirectUri 调用第三方接口指定的回调地址
 */
- (instancetype)initWithAuthCode:(NSString *)authCode redirectUri:(NSString *)redirectUri;

@end

NS_ASSUME_NONNULL_END
