//
//  LYAutLoginResponse.h
//  LYVehicleModule
//
//  Created by 杨沁 on 2018/12/11.
//

#import <GLServiceSDK/GLServiceSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface LYAutLoginResponse : ExLoginResponse

@end

NS_ASSUME_NONNULL_END
