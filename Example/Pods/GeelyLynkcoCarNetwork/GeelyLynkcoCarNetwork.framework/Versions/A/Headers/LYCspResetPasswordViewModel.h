//
//  LYCspResetPasswordViewModel.h
//  LYCarNetWorking
//
//  Created by 杨沁 on 2018/4/23.
//

#import <GLMVVMKit/GLMVVMKit.h>

@interface LYCspResetPasswordViewModel : BaseViewModel

//发送按钮标题
@property (nonatomic, copy) NSString *leftTimeTitle;
//手机号
@property (nonatomic, copy) NSString *mobile;

//验证码
@property (nonatomic, copy) NSString *inputCode;
//新密码
@property (nonatomic, copy) NSString *password;
//发送验证吗
@property (nonatomic, strong, readonly) RACCommand *sendCodeCommand;
//验证验证码
@property (nonatomic, strong, readonly) RACCommand *verifyCodeCommand;
//重置密码
@property (nonatomic, strong, readonly) RACCommand *resetPwdCommand;

@end
