//
//  LYLoginCaptchaViewModel.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/4/13.
//

#import "GLCountDownViewModel.h"
#import "LYAutoLoginService.h"

//手机验证
@interface LYLoginCaptchaViewModel : GLCountDownViewModel

//是否已经登录
@property (nonatomic,assign,readonly) BOOL isLogined;

//是否非首次登录进行邦车操作
@property (nonatomic,assign) BOOL isNotFirstLoginBinded;

#pragma mark - 验证码登录
@property (nonatomic,copy) NSString *code;
@property (nonatomic,strong,readonly) RACCommand *loginBySMSCommand;

#pragma mark - LynkCo LID登录
@property (nonatomic,copy) NSString *authCode;	 // 第三方授权code
@property (nonatomic,copy) NSString *redirectUri;// 调用第三方接口指定的回调地址
@property (nonatomic,strong,readonly) RACCommand *loginByLidCommand;
@property (nonatomic, strong,readonly) RACCommand *loginByLidCodeCommand;

//绑定推送别名
@property (nonatomic,strong,readonly) RACCommand *bindAliasCommand;

//绑定JPush(csp)别名
- (void)updateJPushAlias:(NSString *)alias;

@end
