//
//  LYLoginViewModel.h
//  EricssonTcGeelyProject
//
//  Created by fish on 2017/5/15.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import <GLServiceSDK/GLServiceSDK.h>

@interface LYLoginViewModel : BaseViewModel

@property (nonatomic,copy) NSString *username; //用户名
@property (nonatomic,copy) NSString *password; //密码
@property (nonatomic,assign) BOOL isRememberPassword; //是否记住密码

//是否已经登录
@property (nonatomic,assign) BOOL isLogined;

//是否非首次登录进行邦车操作
@property (nonatomic,assign) BOOL isNotFirstLoginBinded;

//是否注册过
@property (nonatomic,assign) NSInteger registerIndex;   // 0:接口未获取; 1:已注册; -1:未注册

//登录
@property (nonatomic,strong,readonly) RACCommand *loginCommand;
//退出登录
@property (nonatomic,strong,readonly) RACCommand *logoutCommand;

//绑定推送别名
@property (readonly, strong, nonatomic) RACCommand *bindAliasCommand;

//验证手机号Command
@property (readonly, strong, nonatomic) RACCommand *userMobileVerifyCommand;

@end
