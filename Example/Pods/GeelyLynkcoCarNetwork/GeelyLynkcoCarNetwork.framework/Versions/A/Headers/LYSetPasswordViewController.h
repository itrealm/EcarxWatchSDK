//
//  LYSetPasswordViewController.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/4/13.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import "GLCarBaseViewController.h"
#import "LYVerificationViewController.h"
@interface LYSetPasswordViewController : GLCarBaseViewController

@property (assign, nonatomic) LYVerificationType type;

@end
