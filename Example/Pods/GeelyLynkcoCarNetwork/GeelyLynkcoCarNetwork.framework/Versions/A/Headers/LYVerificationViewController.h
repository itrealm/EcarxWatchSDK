//
//  LYVerificationViewController.h
//  AFNetworking
//
//  Created by 杨沁 on 2018/4/13.
//

#import <GLMVVMKit/GLMVVMKit.h>
#import "GLCarBaseViewController.h"
typedef NS_ENUM(NSInteger, LYVerificationType) {
    LYVerificationTypeNone,
    LYVerificationTypeActivateVehicle,  //激活车辆
    LYVerificationTypeVerifyVehicle,   //验证车辆
    LYVerificationTypeResetIhuPwd    //重置车机登录密码
};

@interface LYVerificationViewController : GLCarBaseViewController

@property (assign, nonatomic) LYVerificationType type;

@end
