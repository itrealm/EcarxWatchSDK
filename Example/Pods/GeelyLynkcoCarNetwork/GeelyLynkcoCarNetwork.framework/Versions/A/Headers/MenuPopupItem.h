//
//  MenuPopupItem.h
//  G-netlink
//
//  Created by shane west on 16/8/26.
//  Copyright © 2016年 Ericsson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuPopupItem : NSObject
/** 标题 */
@property (copy, nonatomic) NSString *title;
/** 图片名称 */
@property (copy, nonatomic) NSString *imgName;

- (instancetype)initWithTitle:(NSString *)title imageName:(NSString *)imgName;
@end
