//
//  AboutMenuView.h
//  G-netlink
//
//  Created by shane west on 16/8/26.
//  Copyright © 2016年 Ericsson. All rights reserved.
//

#import <UIKit/UIKit.h>

// 消息
typedef void(^mapMenuClean)();
// 方向
typedef NS_ENUM(NSUInteger, MenuDirection){
    MenuDirectionLeft = 0,
    MenuDirectionRight = 1
};

@interface MenuPopupView : UIView

/** 消失block */
@property (copy, nonatomic) mapMenuClean setClean;

/**
 *  初始化
 *
 *  @param dataArray 数据
 *  @param origin    弹出位置
 *  @param width     宽
 *  @param height    高
 *  @param direction 方向
 */
- (instancetype)initWithDataArray:(NSArray *)dataArray
						   origin:(CGPoint)origin
							width:(CGFloat)width
						   height:(CGFloat)height
						direction:(MenuDirection)direction;

/**
 *  显示
 */
- (void)show;


/**
 *  消失
 */
- (void)dismiss;

- (void)selectedMenu:(NSIndexPath *)indexPath;

@end
