//
//  MeterLabel.h
//  IntelligentShoes
//
//  Created by 朱伟特 on 2017/6/20.
//  Copyright © 2017年 朱伟特. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeterLabel : UIView

@property (nonatomic, copy) NSString * meterStr;

@end
