//
//  NSString+Valite.h
//  EricssonTcGeelyProject
//
//  Created by danny on 2017/4/10.
//  Copyright © 2017年 ecarx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Valite)

/**
 *  验证手机号码
 *
 */
+ (BOOL)valiteTelephoneWithString:(NSString *)telephone;

/**
 *  验证邮箱格式
 *
 */
+ (BOOL)valiteEmailWithString:(NSString *)email;

/**
 *  验证密码格式
 *
 */
+ (BOOL)valitePasswordWithString:(NSString *)password;

/**
 *  验证name格式
 *
 */
+ (BOOL)valiteNameWithString:(NSString *)name;

/**
 *  验证address格式
 *
 */
+ (BOOL)valiteAddressWithString:(NSString *)address;
@end
