//
//  ViewController.h
//  富文本
//
//  Created by LimboDemon on 2018/9/11.
//  Copyright © 2018年 LimboDemon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLBaseViewController.h"


/**
 没有继承基类的VC，因为和基类的tableview有冲突
 */
@interface PublishArticleViewController : GLBaseViewController

/* NSDictionary *dic = @{@"title" : self.articleTitle,
                         @"desc" : digestStr?:@"",
                         @"detail":self.htmlString,
                         @"material":self.articleCover
                         };

*/
@property (nonatomic, strong)NSDictionary *dataDic;

@end

