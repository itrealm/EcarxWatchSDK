//
//  UIImage+RadiusImage.h
//  test_shootSc
//
//  Created by xpp on 2020/4/2.
//  Copyright © 2020 xpp. All rights reserved.
//



#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (RadiusImage)


- (UIImage *)imageWithCornerRadius:(CGFloat)radius;
+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)newsize;
- (UIImage *)addBorderToImage:(UIImage *)image bordWidth:(CGFloat)bordWidth;

@end

NS_ASSUME_NONNULL_END
