//
//  UILabel+Space.h
//  Pods
//
//  Created by fish on 2017/10/10.
//
//

#import <UIKit/UIKit.h>

@interface UILabel (Space)

- (void)setLineSpace:(float)space;
- (void)setWordSpace:(float)space;
- (void)setLineSpace:(float)lineSpace wordSpace:(float)wordSpace;

@end
