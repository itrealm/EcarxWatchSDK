//
//  UILabel+Util.h
//  FlowWallet
//
//  Created by YangQin on 14-12-25.
//  Copyright (c) 2014年 YangQin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Util)

// 获取字符串单行显示时所需要的长度
- (float)getWidthInOneLine;

// 固定宽度，返回size
- (CGSize)getSizeStretchPortrait:(CGSize)size;

// 固定宽度，自定义高度
- (void)stretchPortrait:(CGRect)frame;

// 固定宽度，自定义高度，返回size
- (CGSize)stretchPortraitCGSize:(CGSize)size;

// 固定宽度为屏幕宽度
- (void)stretchPortraitWidthStatic:(CGRect)frame;

//最后一行截取掉offset距离
- (void)cutAtLastLine:(CGFloat) offset;

@end
