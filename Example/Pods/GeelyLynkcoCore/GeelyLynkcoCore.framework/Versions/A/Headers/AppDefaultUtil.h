//
//  AppDefaultUtil.h
//  YSgeely
//
//  Created by dong on 2017/6/13.
//  Copyright © 2017年 dong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppDefaultUtil : NSObject

/**
 单例模式，实例化对象
 */
+ (instancetype)sharedInstance;

/**
 单例模式，实例化对象
 */
-(NSString *)getCurrentDeviceModel;
/**
  获取现在的日历时间
 */
- (NSString *) getCurrentTime;

- (NSString *) getCurrentTimeWithWeek;

/*
  验证电话号码
 */

- (BOOL)validateCellPhoneNumber:(NSString *)cellNum;
/*
  打电话
 */
-(void)callPhoneWithNum:(NSString *)number;
/**
 获取当前显示视图
 
 @return 当前显示视图
 */
+ (GLBaseViewController *)getCurrentVC;

@end
