//
//  CommonMacro.h
//  AppFrame
//
//  Created by GXJ on 2017/2/24.
//  Copyright © 2017年 GXJ. All rights reserved.
//
#import "GLBaseNetWork.h"


#ifndef CommonMacro_h
#define CommonMacro_h

#pragma mark- 字符串为空判断

static inline BOOL GLIsObjectEmpty(id obj) { // 空对象
    return obj == nil || [obj isEqual:[NSNull null]] || ([obj respondsToSelector:@selector(length)] && [(NSData *) obj length] == 0) ||
    ([obj respondsToSelector:@selector(count)] && [(NSArray *) obj count] == 0);
}

static inline BOOL GLIsStringEmpty(NSString *string) { // 空字符串
    return string == nil || [string isEqual:[NSNull null]] || string.length == 0 || [string isEqualToString:@"<null>"] ||
    [string isEqualToString:@"(null)"];
}

static inline NSString* GLTransformEmptyString(NSString *string) { // 补全空字符串
    return GLIsStringEmpty(string) ? @"" : string;
}

#pragma mark- 运行环境相关
/** 运行环境 */
#define kRunEnvironment @"runEnvironment"
// dev环境
#define kDEV @"dev"
// sit 环境
#define kSIT @"sit"
// UAT 环境
#define kUAT @"uat"
// 预发布环境
#define kRELEASE @"tra"
// 生产环境
#define kPRODUCTION @"production"

#pragma mark- 日志打印
/**********************/
#if DEBUG

#if __has_include("GLLogHelper.h")
#import "GLLogHelper.h"
//#define NSLog(format,...)  GLLogInfo(format,## __VA_ARGS__)

#endif

#else
//#define NSLog(format,...)

#endif

/**********************/

#pragma mark- 字符串国际化
#define NSLocalString(key)   NSLocalizedString(key, nil)

//#define GLY_CORE_LocStr(key) NSLocalizedStringFromTable(key, @"GeelyNECoreModuleLocalizable", nil)
#define GLY_CORE_LocStr(key) NELocalizedString((key), @"GeelyNECoreModuleLocalizable")



#pragma mark- 弱引用
#define WEAKSELF __weak __typeof__(self) weakSelf = self;
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;



/**系统版本号判断*/
#pragma mark- 系统版本号判断
#define IOS14_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 14.0)
#define IOS13_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 13.0)
#define IOS12_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 12.0)
#define IOS11_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
#define IOS10_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
#define IOS9_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define IOS8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IOS7_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_BANGS_SCREEN (IOS11_OR_LATER && ([[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0))

#pragma mark- 设备型号判断
/* 设备型号 */
//判断是否是ipad
#define isPad ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
//判断iPhone4系列
#define IS_IPHONE4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhone5系列
#define IS_IPHONE5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhone6系列
#define IS_IPHONE6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iphone6+系列
#define IS_IPHONE6P ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneX
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPHoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs
#define IS_IPHONE_Xs ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)
//判断iPhoneXs Max
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) && !isPad : NO)




/** 设置图片 */
#define kImageName(name) [UIImage imageNamed:name]



/** 设置错误消息 */
//#define kShowErrorMessage(message) [[GLErrorMessageView shareRemindView]setMessageMessage:message]
#define kShowErrorMessage(message) [SVProgressHUD showErrorWithStatus:message]


// 获取当前环境中的 host
#define kGETHOST(APIKey) [[GLNetworkEnvConfig sharedInstance]getCurHost:APIKey]


#endif
