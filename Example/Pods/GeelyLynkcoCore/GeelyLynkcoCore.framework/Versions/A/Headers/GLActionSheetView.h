//
//  GLActionSheetView.h
//  
//
//  Created by zhiyong.kuang on 2018/11/5.
//

#import <UIKit/UIKit.h>

#if __has_include("GLFormKit.h")
#import "GLFormKit.h"
#endif

@interface GLActionSheetView : UIView
//头部视图
@property(nonatomic,readonly)UIView* headerView;
//头部分割线
@property(nonatomic,readonly)UIView* headerLine;
//弹出框的最底部视图,如果有就传,没有就不传
@property(nonatomic,strong)UIView* bottomView;

//头部视图标题
@property(nonatomic,readonly)UILabel* titleLabel;
//头部视图中右边的控制视图关闭的按钮
@property(nonatomic,readonly)UIButton* operationButton;

//设置底部视图的高度
@property(nonatomic,assign) CGFloat bottomHeight;
//设置背景颜色
@property(nonatomic,strong)UIColor* tintBackColor;
//设置视图占屏幕比例(默认0.6)
@property (nonatomic,assign) CGFloat viewHeightPercent;
//设置背景视图的透明度(默认0.3)
@property (nonatomic,assign) CGFloat backViewAlpha;
//动画执行时间(默认0.2)
@property (nonatomic,assign) CGFloat animationTime;

- (instancetype)initWithBottomView:(UIView *)bottomView;

/**
 隐藏视图
 */
- (void)hidden;

/**
 展示视图

 @param content content 支持 UIView\UIViewController\GLForm
 @param title 标题
 */
+ (void)showContent:(id)content title:(NSString*)title;
+ (void)showContent:(id)content contentHeight:(CGFloat)height configUI:(void(^)(GLActionSheetView* actionSheet))configBlock didWhenCanceled:(void(^)(void))cancelBlock;
+ (void)showInView:(UIView*)view withContent:(id)content contentHeight:(CGFloat)height configUI:(void(^)(GLActionSheetView* actionSheet))configBlock didWhenCanceled:(void(^)(void))cancelBlock;



@end
