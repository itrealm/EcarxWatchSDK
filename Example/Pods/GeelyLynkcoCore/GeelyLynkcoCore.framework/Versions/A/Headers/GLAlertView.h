//
//  GLCustomAlertView.h
//  Geelyconsumer
//
//  Created by 朱伟特 on 2018/10/23.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, GLAlertActionStyle) {
    GLAlertActionStyleDefault = 0,
    GLAlertActionStyleCancel,
    GLAlertActionStyleDestructive
};

typedef NS_ENUM(NSInteger, GLAlertActionTag) {
    GLAlertActionTagDefault = 0,
    GLAlertActionTagCancel,
    GLAlertActionTagDestructive,
    
};

typedef NS_ENUM(NSInteger, GLAlertViewAnimationType) {
    GLAlertViewAnimationTypeDefault = 0,
    GLAlertViewAnimationTypeScale,

    GLAlertViewAnimationTypeLeft,
    GLAlertViewAnimationTypeRight,
    GLAlertViewAnimationTypeTop,
    GLAlertViewAnimationTypeBottom,
};

typedef NS_ENUM(NSInteger, GLAlertViewStyle) {
    GLAlertViewStyleSystem = 0,
    GLAlertViewStyleLynkco,
    GLAlertViewStyleNew,

    GLAlertViewStyleActionSheet,
    GLAlertViewStyleLynkcoActionSheet,
    GLAlertViewStyleMax,
};

typedef NS_ENUM(NSInteger, GLAlertLogoPosition) {
    GLAlertLogoPositionTop = 0,
    GLAlertLogoPositionBottomOfTitle,
};

@class GLAlertView;

@protocol GLAlertViewStyleDelegate <NSObject>
@required
-(void)initAlertView:(GLAlertView*)alertV;

-(void)updateConstraintsWithAlertView:(GLAlertView*)alertV;
@optional
-(void)updateRootViewPosition:(GLAlertView*)alertV;
-(BOOL)showAnimationWithAlertView:(GLAlertView*)alertV animationType:(GLAlertViewAnimationType)type duration:(NSTimeInterval)duration finished:(void(^)(BOOL finished))finishedBlock;
-(BOOL)hiddenAnimationWithAlertView:(GLAlertView*)alertV animationType:(GLAlertViewAnimationType)type duration:(NSTimeInterval)duration finished:(void(^)(BOOL finished))finishedBlock;
@end



@interface GLAlertAction : NSObject

+ (GLAlertAction*)actionWithTitle:(NSString *)title style:(GLAlertActionStyle)style handler:(void (^)(GLAlertAction *action))handler;

@property (nonatomic, readonly) NSString *title;

@property (nonatomic, strong) NSAttributedString *attributedText;

@property (nonatomic, strong) UIColor *color;
@property (nonatomic, strong) UIFont *font;

@property (nonatomic, assign) NSTextAlignment alignment;

@property (nonatomic, readonly) GLAlertActionStyle style;

@property (nonatomic, readonly) GLAlertActionTag tag;
@property (nonatomic, readonly) void(^configUIBlock)(UIButton* actionButton);

@end


@interface GLAlertView : UIView

@property (nonatomic, assign) NSTextAlignment alignment;
@property (nonatomic, strong) UIColor *tintBackColor;
@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, assign) CGFloat titleYoffset;

+ (void)showAlertViewWithTitle:(NSString *)title
                       message:(NSString *)message
                      configUI:(void(^)(GLAlertView* alertV))configUIBlock
                 clickedEvents:(void(^)(GLAlertAction*action))clickedEvents
                determineTitle:(NSString *)determineTitle
                   cancelTitle:(NSString *)cancelTitle
                    otherTitle:(NSString*)otherTitle, ...;

+ (void)showAlertViewWithTitle:(NSString *)title
                   contentView:(UIView *)contentView
                 contentHeight:(CGFloat)contentHeight
                      configUI:(void(^)(GLAlertView* alertV))configUIBlock
                 clickedEvents:(void(^)(GLAlertAction*action))clickedEvents
                determineTitle:(NSString *)determineTitle
                   cancelTitle:(NSString *)cancelTitle
                    otherTitle:(NSString*)otherTitle, ...;

- (void)addAction:(GLAlertAction *)action;
@property (nonatomic, readonly) NSArray<GLAlertAction *> *actions;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSTextAlignment titleAlignment;
@property (nonatomic, copy) NSString *message;

/** logo
 */
@property (nonatomic, strong) UIImage *logo;
@property (nonatomic, assign) CGPoint logoOffset;
@property (nonatomic, assign) CGSize logoSize;
@property (nonatomic, assign) GLAlertLogoPosition logoPosition;

@property (nonatomic, assign) UIEdgeInsets contentViewEdgeInsets;

@property (nonatomic, assign) BOOL tapBackgroundToClose; //default NO
@property (nonatomic, assign) NSInteger titleNumberOfLines; //default 2



@property (nonatomic, assign) GLAlertViewAnimationType animationType;
@property (nonatomic, assign) GLAlertViewStyle style;
@property (nonatomic, strong) void(^configUIBlock)(GLAlertView* alertV);
@property (nonatomic, readonly) UIView * rootView;
@property (nonatomic, readonly) UIView * backView;
@property (nonatomic, readonly) UILabel * titleLabel;
@property (nonatomic, readonly) UILabel * contentLabel;

@property (nonatomic, strong) UIView * contentView;
@property (nonatomic, assign) CGFloat contentHeight;

@property (nonatomic, assign) BOOL bindKeyboard;


- (void)show;
- (void)dismiss;
- (void)hideWidow;
- (void)showWindow;
+ (BOOL)registerDelegate:(Class<GLAlertViewStyleDelegate>)delegate style:(NSInteger)styleNum;

@end
