/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLAnimateButton.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,DDtimingFunction) {
    DDtimingFunctionLinear,
    DDtimingFunctionEaseOut
};

typedef void(^clickActionBlock)();
@interface GLAnimateButton : UIView

/*boder边框从(0,0)开始顺时针计算一整圈是0-1*/

/**
 边框boder线结束的百分比 (取值0-1,默认0.6)
 */
@property (nonatomic,assign) CGFloat lineEndPercent;

/**
 边框boder线开始的百分比 (取值0-1,并且start <= end,默认0.15)
 */
@property (nonatomic,assign) CGFloat lineStartPercent;

/**
 边框boder每一圈动画的时长 (默认1秒)
 */
@property (nonatomic,assign) CGFloat lineAnimateduration;

/**
 边框boder动画的循环次数 (默认1次)
 */
@property (nonatomic,assign) CGFloat lineAnimateRepeatCount;

/**
 边框boder的颜色 (默认红色)
 */
@property (strong,nonatomic) UIColor *lineColor;

/**
 边框线的宽度 (默认2)
 */
@property (nonatomic,assign) CGFloat lineWidth;

/**
 标题文字的颜色 (默认红色)
 */
@property (strong,nonatomic) UIColor *buttonTitleColor;

/**
 标题文字的字体大小 (默认20)
 */
@property (nonatomic,assign) CGFloat buttonTitleFont;


/**
 标题文字的字体
 */
@property (nonatomic, strong) UIFont *titleFont;

/**
 标题的文字 
 */
@property (nonatomic,copy) NSString *buttonTitle;

/**
 标题的富文本文字  注意:这里使用了富文本的话,就不要设置buttonTitle,以及titleFont等一些文字属性的设置.
 */
@property (nonatomic,copy) NSMutableAttributedString *buttonAttributeTitle;

/**
 点击事件blcok回调
 */
@property (nonatomic,copy) clickActionBlock clickAction;

/**
 动画的timingFounction (默认linear)
 */
@property (nonatomic,assign) DDtimingFunction timingFounction;
/** 是否可用 */
@property (nonatomic, getter=isEnable) BOOL enable;

/** 动画是否可用 */
@property (nonatomic, assign) BOOL animateEnable;

/**
 初始化方法
 
 @param title 标题文字
 @param action 点击的动作
 @return ddbutton
 */
+ (instancetype)DDbuttonWithTitle:(NSString *)title tapAction:(clickActionBlock)action;

/**
 初始化方法
 
 @param attriTitle 标题富文本文字
 @param action 点击的动作
 @return ddbutton
 */
+ (instancetype)DDbuttonWithAttributeTitle:(NSMutableAttributedString *)attriTitle tapAction:(clickActionBlock)action;

@end
