//
//  GLAppAuthorizationStatus.h
//  GeelyLynkcoCore
//
//  Created by shuaishuai on 2020/3/3.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, GLAuthorizationType) {

    GLAuthorizationTypePhoto,  //相册
    GLAuthorizationTypeCamera,   //相机
    GLAuthorizationTypeAudio  //麦克风
};

@interface GLAppAuthorizationStatus : NSObject


/// 检测是否有权限
/// @param authorType 要检测的权限类型
/// @return  0: 无权限或不支持的authType类型，1.：未确定   2：有权限
+ (int)checkAuthorizationStatus:(GLAuthorizationType)authType;


/// 请求授权异步
/// @param authType 权限类型
/// @param permission 是否允许授权回调（会在主线程）
+ (void)authorizationStatusAction:(GLAuthorizationType)authType
                       permission:(void (^)(BOOL granted))permission;


+ (UIAlertController *)showAlerView:(NSString *)title
                               desc:(NSString *)desc
                            okBlock:(void(^)(void))okBlock
                        cancelBlock:(void(^)(void))cancelBlock;


@end


