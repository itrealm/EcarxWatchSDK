//
//  GLBadgeLabel.h
//  KTools
//
//  Created by zhiyong.kuang on 2017/9/22.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易
 
#import <UIKit/UIKit.h>

@class GLBadgeLabel;

@interface GLBadgeCenter : NSObject
+(instancetype)defaultCenter;

-(void)registerObserver:(GLBadgeLabel*)label forKeyPath:(NSString*)keypath;
-(void)removeObserver:(GLBadgeLabel*)label forKeyPath:(NSString*)keypath;

-(void)setBadgeValue:(NSUInteger)value forKeyPath:(NSString*)keyPath;
-(NSUInteger)badgeValueForKeyPath:(NSString*)keyPath;


@end


typedef NS_ENUM(NSInteger, GLBadgeStyle) {
    
    GLBadgeStyleNum = 0,        //最大显示maxNum(99)，超过将显示maxString(...)
    GLBadgeStyleDot,            //仅在有值时，显示一个小点
    GLBadgeStyleString,         //仅在有值时，显示字符串，需设置 defaultStrig或defaultAttributedString
    GLBadgeStyleStringCustom,   //仅在有值时，显示字符串，无corner背景
    GLBadgeStyleCustom,         //不做任何限制
};

//center在父视图相对位置
typedef NS_ENUM(NSInteger, GLBadgeCenterPosition) {
    
    GLBadgeCenterPositionRT = 0,        //右上角
    GLBadgeCenterPositionLT,            //左上角
    GLBadgeCenterPositionRB,         //右下角
    GLBadgeCenterPositionLB,   //左下角
    GLBadgeCenterPositionM,     //中间
    GLBadgeCenterPositionRM,     //右侧中间
    GLBadgeCenterPositionTM,     //上侧中间
    GLBadgeCenterPositionLM,     //左侧中间
    GLBadgeCenterPositionBM,     //底部中间
    GLBadgeCenterPositionCustom,
};



@interface GLBadgeItem : NSObject
@property (nonatomic, strong) NSString* bindKeyPath;
@property (nonatomic, strong) UIColor* backgroundColor;
@property (nonatomic, strong) UIFont* font;
@property (nonatomic, strong) UIColor* color;
@property (nonatomic, assign) CGPoint centerOffset;
@property (nonatomic, assign) GLBadgeStyle style;
@property (nonatomic, assign) GLBadgeCenterPosition centerPosition;
@property (nonatomic, strong) void(^positionCustomBlock)(GLBadgeLabel* badgeLabel);
/**
 仅在GLBadgeStyleNum模式下生效
 */

@property (nonatomic, assign) NSInteger maxNum;
@property (nonatomic, strong) NSString* maxString;

/**
 defaultAttributedString
 自定义字符串，优先级高于defaultString
 */
@property (nonatomic, copy) NSAttributedString* defaultAttributedString;
@property (nonatomic, copy) NSString* defaultString;
@end

@interface GLBadgeLabel : UILabel

@property (nonatomic, strong) NSString* bindKeyPath;
@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@property (nonatomic, assign) CGFloat cornerRadius;
@property (nonatomic, assign) CGPoint centerOffset;
@property (nonatomic, assign) GLBadgeStyle style;
@property (nonatomic, assign) GLBadgeCenterPosition centerPosition;
@property (nonatomic, strong) void(^positionCustomBlock)(GLBadgeLabel* badgeLabel);
/**
 仅在GLBadgeStyleNum模式下生效
 */
@property (nonatomic, assign) NSInteger maxNum;
@property (nonatomic, strong) NSString* maxString;
/**
 defaultAttributedString
 自定义字符串，优先级高于defaultString
 */
@property (nonatomic, strong) NSAttributedString* defaultAttributedString;
@property (nonatomic, strong) NSString* defaultString;


/**
 设置显示值

 @param value NSNumber\NSValue\NSString
 */
-(void)configValue:(id)value;

-(void)setBadgeItem:(GLBadgeItem*)item;

-(void)updatePosition;
/**
 image tool
 */
+(NSAttributedString*)attributedStringByImage:(UIImage*)image bounds:(CGRect)bounds;

@end




@interface UIView (GLBadgeLabel)

/*
 @description 默认为nil，需调用show方法后有值
 */
@property(nonatomic,strong)GLBadgeLabel* badgeLabel;


-(void)showBadge:(NSString*)badgeStr centerOffset:(CGPoint)centerOffset badgeColor:(UIColor*)color;
-(void)showBadgeBindKeyPath:(NSString*)keyPath centerOffset:(CGPoint)centerOffset badgeColor:(UIColor*)color;
-(void)showBadgeStyle:(GLBadgeStyle)style bindKeyPath:(NSString*)keyPath centerOffset:(CGPoint)centerOffset badgeColor:(UIColor*)color;
-(void)showBadgeStyle:(GLBadgeStyle)style value:(NSString*)value bindKeyPath:(NSString*)keyPath centerOffset:(CGPoint)centerOffset badgeColor:(UIColor*)color;
-(void)showBadgeAndConfig:(void(^)(GLBadgeLabel* badgeLabel))configBlock;
-(void)showBadgeWithItem:(GLBadgeItem*)item;

-(void)hideBadge;
-(void)removeBadge;

@end

@interface UIBarItem (GLBadgeLabel)
@property (nonatomic, strong) GLBadgeItem* gl_badgeItem;
@end

@interface UITabBar (GLBadgeLabel)
@end
