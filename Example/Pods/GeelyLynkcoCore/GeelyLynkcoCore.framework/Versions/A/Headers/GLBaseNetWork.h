//
//  ZHNbaseNetWrok.h
//  ZHNnetWorkIng
//
//  Created by zhn on 16/10/12.
//  Copyright © 2016年 zhn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLNetWrokEngine.h"
#import "GLNetworkEnvConfig.h"

@class GLBaseNetWork;
#define GLNetWorkManager [GLBaseNetWork shareInstance]

typedef void(^GLNetWorkErrorHandleBlock)(GLBaseEngine *engine,NSInteger statusCode,NSError *error);
typedef BOOL(^GLNetWorkWillStartHandleBlock)(GLBaseNetWork* network,GLBaseEngine *engine);
typedef BOOL(^GLNetWorkResponseHandleBlock)(GLBaseEngine *engine,NSInteger statusCode,id data,NSError* error);


typedef NSString*(^GLNetWorkParamReplaceHandleBlock)(NSString* key);


@interface GLBaseNetWork : NSObject
/**
 单例的初始化方法

 @return 实例
 */
+ (instancetype)shareInstance;
@property (nonatomic,readonly) AFHTTPSessionManager * sessionManager;
@property (nonatomic, assign) BOOL netState;

@property (nonatomic, strong) BOOL(^networkShouldRunByApiGatway) (NSString* host);

- (void)startMonitoringNetWorkingState;
/**
 基础发起请求方法

 @param workEngine 请求对象

 @return 请求的id
 */
- (NSNumber *)startRequest:(GLBaseEngine *)workEngine;

/**
 取消网络请求

 @param requestID 网络请求的id
 */
- (void)cancleRequsetWithRequsetID:(NSNumber *)requestID;



- (GLBaseEngine*)getEngineInQueueForKey:(NSString*)key;
- (void)enumerateEngineInQueueUsingBlock:(void(^)(id key,id obj,BOOL* stop))block;

/**
 添加删除通用请求错误处理handler
 **/
- (void)addObserverHandleError:(GLNetWorkErrorHandleBlock)handleBlock forKey:(NSString*)key;
- (void)removeObserverHandleErrorForKey:(NSString*)key;

/**
 添加通用网络请求处理策略，判断某请求是否可以执行
 */
- (void)registerObserverWillStartHandle:(GLNetWorkWillStartHandleBlock)handleBlock;

/**
添加通用网络请求处理策略，根据返回结果判断某请求是否继续运行回调，可以执行token刷新等操作
*/
- (void)registerObserverForResponseHandle:(GLNetWorkResponseHandleBlock)handleBlock;



//通过主机url替特定的主机服务器添加指定header
-(void)addBaseHeader:(NSString*)value key:(NSString*)key forHostKey:(NSString*)hostKey;
-(void)addBaseHeader:(NSDictionary*)headers forHostKey:(NSString*)hostKey;

//通过主机url替特定的主机服务器添加指定AFHTTPResponseSerializer、AFHTTPRequestSerializer
//仅对非API网关请求生效
-(void)setRequestSerializer:(AFHTTPRequestSerializer*)requestSerializer forBaseUrl:(NSString*)url;
-(void)setResponseSerializer:(AFHTTPResponseSerializer*)responseSerializer forBaseUrl:(NSString*)url;
-(AFHTTPRequestSerializer*)getRequestSerializerForBaseUrl:(NSString*)url;
-(AFHTTPResponseSerializer*)getResponseSerializerForBaseUrl:(NSString*)url;

//通过主机HostKey替特定的主机服务器添加指定AFHTTPResponseSerializer、AFHTTPRequestSerializer
//仅对非API网关请求生效
-(void)setRequestSerializer:(AFHTTPRequestSerializer*)requestSerializer forHostKey:(NSString*)hostKey;
-(void)setResponseSerializer:(AFHTTPResponseSerializer*)responseSerializer forHostKey:(NSString*)hostKey;
-(AFHTTPRequestSerializer*)getRequestSerializerForHostKey:(NSString*)hostKey;
-(AFHTTPResponseSerializer*)getResponseSerializerForHostKey:(NSString*)hostKey;


// priority:(NSInteger)priority
-(void)registerObserverWithParamReplaceHandle:(GLNetWorkParamReplaceHandleBlock)handler;

-(NSString*)requestUrlAfterReplaceParamByOriginUrl:(NSString*)originUrl;

-(NSDictionary*)dictionaryByRelplaceTmpValueFromDict:(NSDictionary*)dict;

@end
