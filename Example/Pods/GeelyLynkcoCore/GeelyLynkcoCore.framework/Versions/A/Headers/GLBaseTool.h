/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLBaseTool.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */
#import <Foundation/Foundation.h>
typedef void(^GetSelectAreaNameBlock)(NSString *selectAreaName, NSArray *dataArr);

typedef void(^FinishCleanCacheBlock)(BOOL success);

@interface GLBaseTool : NSObject

/**
 获取当前显示视图

 @return 当前显示视图
 */
+ (UIViewController *)getCurrentVC;
+ (UINavigationController *)getCurrentNavigationController;
+ (UIViewController *)getTopVC;

+ (NSAttributedString *)setLabelAttstrWithText:(NSString *)text andAttributedStringArr:(NSArray<NSString *> *)attributedStringArr andFontArr:(NSArray<UIFont*> *)fontArr andTextColorArr:(NSArray<UIColor *> *)textColorArr;

+ (NSString *)getUserNameWithOriginalName:(NSString *)orgName;

+ (NSString *)getTime:(NSInteger)second WithFormat:(NSString *)format;

//正则去除标签
+ (NSString *)getZZwithString:(NSString *)string;
// 清空 cookie
+ (void)clearCookies;
+ (NSString *)getCurrentTimeWithFormat:(NSString *)format;
+ (NSString *)getCurrentTimeAfterDays:(NSInteger)dis withFormat:(NSString *)format;
/** 时间戳转化成标准时间 */
+ (NSString *)getStandardTimeWithTimestamp:(NSInteger)timestamp andFormat:(NSString *)format;
/** 时间转化成时间戳 */
+(NSInteger)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format;
/** 比较两个日期谁大谁小 a大于b 返回-1 小于返回1 等于返回0 */
+ (NSInteger)compareDate:(NSString*)aDate withDate:(NSString*)bDate withFormat:(NSString *)format;

//获得沙盒中Cache下文件总大小
+ (NSUInteger)getCacheSize;
//清除Cache下的文件
+ (void)cleanCacheWithBlock:(FinishCleanCacheBlock)cleanBlock;


@end
