//
//  GLBaseViewController.h
//  GLCoreKit-Example
//
//  Created by kzy on 2018/7/22.
//  Copyright © 2018年 kzy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLBadgeLabel.h"
#import "GLNavigationBar.h"
#import "GLHButton.h"
#import "UIViewController+GLController.h"


@interface GLBaseViewController : UIViewController

@property(nonatomic,strong)UIColor* titleColor;
@property(nonatomic,readonly)GLNavigationBar* glNavigationBar;

@property(nonatomic,assign)NSInteger maxCountWhenShowMoreButton;
@property(nonatomic,strong)NSString* moreButtonTitle;
@property(nonatomic,strong)UIImage* moreButtonImage;

/**
 是否允滑动返回
 */
@property(nonatomic,assign)BOOL canSideBack;

/**
 使用自定义导航栏
 */
@property(nonatomic,assign)BOOL useCustomNavigationBar;
/**
 自定义导航栏高度
 仅当useCustomNavigationBar为YES时，只对自定义导航栏生效
 */
@property(nonatomic,assign)CGFloat customNavigationBarHeight;


@property(nonatomic,readonly)CGFloat startYOffset;
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated;

#pragma mark- 添加导航栏按钮


/**
 添加导航按钮

 @param style 左侧或右侧按钮
 @param title 按钮标题
 @param image 按钮显示源，可以是以下类型UIImage，NSString(网络地址、本地地址、图片名称)，NSURL
 @param target 按钮点击事件target
 @param selector 按钮点击事件selector
 @param badgeItem 按钮角标badge配置
 */

- (GLNavigationButton*)addBarButotn:(GLBaseBarButtonStyle)style title:(NSString*)title image:(id)image target:(id)target selector:(SEL)selector badgeItem:(GLBadgeItem*)badgeItem forKey:(NSString*)key;
- (GLNavigationButton*)addBarButotn:(GLBaseBarButtonStyle)style image:(id)image target:(id)target selector:(SEL)selector;

/**
 添加导航按钮
 
 @param style 左侧或右侧按钮
 @param title 按钮标题
 @param image 按钮显示源，可以是以下类型UIImage，NSString(网络地址、本地地址、图片名称)，NSURL
 @param badgeItem 按钮角标badge配置
 @param block 按钮点击事件block
 */
- (GLNavigationButton*)addBarButotn:(GLBaseBarButtonStyle)style title:(NSString*)title image:(id)image badgeItem:(GLBadgeItem*)badgeItem forKey:(NSString*)key action:(void(^)(id sender,id vc))block;
- (GLNavigationButton*)addBarButotn:(GLBaseBarButtonStyle)style title:(NSString*)title image:(id)image forKey:(NSString*)key action:(void(^)(id sender,id vc))block;



- (void)addCustomBarItem:(UIView*)view style:(GLBaseBarButtonStyle)style forKey:(NSString*)key;
- (void)addCustomBarItem:(UIView*)view style:(GLBaseBarButtonStyle)style badgeItem:(GLBadgeItem*)badgeItem forKey:(NSString*)key;


/**
 设置按钮黑色半透明圆形背景

 @param alpha 不透明值
 */
- (void)setBarButtonBackgroundMaskAlpha:(CGFloat)alpha;

/**
 返回指定的导航栏按钮

 @param style 左侧或右侧按钮
 @param key 按钮标识符
 @return 按钮UIView
 */
- (UIView*)customBarItemForStyle:(GLBaseBarButtonStyle)style forKey:(NSString*)key;

- (void)removeBarItemForStyle:(GLBaseBarButtonStyle)style forKey:(NSString*)key;

- (void)setNavigationTitleView:(UIView*)titleView;

@end


