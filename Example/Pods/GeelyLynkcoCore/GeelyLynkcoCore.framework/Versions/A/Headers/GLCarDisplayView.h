//
//  GLCarDisplayView.h
//  CarDisplay
//
//  Created by 朱伟特 on 2018/8/3.
//  Copyright © 2018年 geely. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^EndRotation)(void);

typedef void(^BeginRotation)(void);

@interface GLCarDisplayView : UIView

@property (nonatomic, copy) BeginRotation beginRotation;//开始旋转回调

@property (nonatomic, copy) EndRotation endRotation;//结束旋转

@property (nonatomic, strong) NSArray * imageArray;//图片数组 ，传入类型为NSString或者UIImage类型

@property (nonatomic, assign) CGRect imageFrame;//内置UIImageView的frame

- (instancetype)initWithFrame:(CGRect)frame andImageArray:(NSArray *)imageArray;//构造方法

@end
