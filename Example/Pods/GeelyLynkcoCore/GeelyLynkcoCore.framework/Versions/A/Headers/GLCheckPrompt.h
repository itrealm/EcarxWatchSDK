//
//  GLCheckPrompt.h
//  GeelyConsumerMain
//
//  Created by yang.duan on 2018/8/8.
//

#import <Foundation/Foundation.h>

typedef void (^RegistPromptResultBlock)(BOOL allowPrompt);

@interface GLCheckPrompt : NSObject

/**
 相册权限
 
 @return 回调 YES 开启 NO 未开启
 */
+ (BOOL)photoAlbumPrompt;

/**
 注册相册权限
 
 @param result 回调
 */
+ (void)registPhotoAlbumPrompt:(RegistPromptResultBlock)result;

/**
 相机的授权
 
 @return 回调 YES 开启 NO 未开启
 */
+ (BOOL)cameraPrompt;

/**
 注册相机权限
 
 @param result 回调
 */
+ (void)registCameraPrompt:(RegistPromptResultBlock)result;

/**
 麦克风权限
 
 @return 回调 YES 开启 NO 未开启
 */
+ (BOOL)microphonePrompt;

/**
 注册麦克风权限
 
 @param result 回调
 */
+ (void)registMicrophonePrompt:(RegistPromptResultBlock)result;

/**
 通讯录权限
 
 @return 回调 YES 开启 NO 未开启
 */
+ (BOOL)contactPrompt;

/**
 注册通讯录权限
 
 @param result 回调
 */
+ (void)registContactPrompt:(RegistPromptResultBlock)result;


@end
