//
//  GLGoodsImageDisplayView.h
//  Geelyconsumer
//
//  Created by 朱伟特 on 2018/12/11.
//  Copyright © 2018 Rdic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLImageDisplayController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLCommentImageModel : GLImageDisplayModel
@property (nonatomic, strong)NSString* authorStr;
@property (nonatomic, strong)NSString* commentStr;
@end

@interface GLCommentCoverContentView : UIView<GLImageDisplayContentDelegate>
@end

NS_ASSUME_NONNULL_END
