/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLCountManager.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#import <Foundation/Foundation.h>

@interface GLCountTask : NSOperation

/** 计时中回调 */
@property(nonatomic,copy) void(^countingDownBlock)(NSTimeInterval timeInterval);

/** 计时结束回调 */
@property(nonatomic,copy) void(^finishedBlock)(NSTimeInterval timeInterval);

/** 计时剩余时间 */
@property(nonatomic,assign) NSTimeInterval leftTimeInterval;

/** 后台任务标示,确保任务进入后台依然能够计时 */
@property(nonatomic,assign) UIBackgroundTaskIdentifier taskIdentifier;

/** `NSOperation`的`name`属性只在iOS8+中存在，这里定义一个属性，用来兼容 iOS7 */
@property(nonatomic,copy) NSString *operationName;

@end




/** 定时器管理类,属于全局类 */
@interface GLCountManager : NSObject

typedef void(^GLCountBlock)(NSTimeInterval timerInterval);

//本类拥有一个线程池（也叫并发操作队列，规定队列中最多只允许存在 20 个并发线程），每分配一个计时器（即创建一个子线程）就将其放入池中，计时器跑完以后会自动从池子里销毁。
/** 线程池 */
@property(nonatomic,strong) NSOperationQueue *pool;

/** 单例对象 */
+ (instancetype)standardCountDown;


/**
 *  开始倒计时，如果倒计时管理器里具有相同的key，则直接开始回调。
 *
 *  @param aKay         任务key，用于标示唯一性
 *  @param timeInterval 倒计时总时间，受操作系统后台时间限制，倒计时时间规定不得大于 120 秒.
 *  @param countingDown 倒计时，会多次回调，提供当前秒数
 *  @param finished     倒计时结束时调用，提供当前秒数，值恒为 0
 *  @param isCreate     不存在此计时器线程时,是否要创建
 */
- (void)scheduledCountDownWithKey:(NSString *)aKay timeInterval:(NSTimeInterval)timeInterval create:(BOOL)isCreate countingDown:(GLCountBlock)countingDown finished:(GLCountBlock)finished;

/** 删除线程池里面名字为 aKey 的定时器 */
- (void)removeTaskWithKey:(NSString *)aKey;

@end
