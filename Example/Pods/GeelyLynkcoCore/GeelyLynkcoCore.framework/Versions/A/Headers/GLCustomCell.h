//
//  GLCustomCell.h
//  GLCustomCell
//
//  Created by 匡志勇 on 17/10/30.
//  Copyright © 2015年 匡志勇. All rights reserved.
//



#import <UIKit/UIKit.h>

#if __has_include("GLFormBaseCell.h")
#import "GLFormBaseCell.h"
#endif

#ifndef GLS_FONT_CELL_TITLE
#define GLCustomCell_Title_Font [UIFont systemFontOfSize:16]
#else
#define GLCustomCell_Title_Font GLS_FONT_CELL_TITLE
#endif
typedef NSString GLComponentKey;


//可根据此key给CELL本身设置属性，仅供设置属性
UIKIT_EXTERN NSString*_Nonnull GLCustomCellSelf;
/*
 可添加组件 GLCustomCellTextKey
 */

/*
 @class GLCustomLabel
 默认keypath:.text
 value:@NSString
 */
UIKIT_EXTERN NSString*_Nonnull GLCustomCellText;    //文字
UIKIT_EXTERN NSString*_Nonnull GLCustomCellMutableLineText;    //多行文字

#if __has_include("YYText.h")
UIKIT_EXTERN NSString*_Nonnull GLCustomCellYYLabelText;    //YYLabel
#endif

UIKIT_EXTERN NSString*_Nonnull GLCustomCellGrayText;    //文字
UIKIT_EXTERN NSString*_Nonnull GLCustomCellText2;    //文字
UIKIT_EXTERN NSString*_Nonnull GLCustomCellCenterText;    //文字 居中
UIKIT_EXTERN NSString*_Nonnull GLCustomCellSubText;    //文字
UIKIT_EXTERN NSString*_Nonnull GLCustomCellCatpionText;          // 文字
/*
 @class GLCustomImageView
 默认keypath:.image
 value:@NSString @UIImage
 */
UIKIT_EXTERN NSString*_Nonnull GLCustomCellIcon;         //图标
UIKIT_EXTERN NSString*_Nonnull GLCustomCellIcon2;      //图标2
/*
 @class UISwitch
 默认keypath:.on
 value:@NSNumber
 */
UIKIT_EXTERN NSString*_Nonnull GLCustomCellSwitch;  //开关
/*
 @class UITextField
 默认keypath:.text
 value:@NSString
 */
UIKIT_EXTERN NSString*_Nonnull GLCustomCellInput;      //输入框
UIKIT_EXTERN NSString*_Nonnull GLCustomCellLeftNoEditInput;      //输入框 不可编辑 靠左排列
UIKIT_EXTERN NSString*_Nonnull GLCustomCellRightNoEditInput;      //输入框 不可编辑 靠右排列
/*
 @class UITextView
 默认keypath:.text
 value:@NSString
 */
UIKIT_EXTERN NSString*_Nonnull GLCustomCellTextView;      //多行显示textView
/*
 @class GLBadgeLabel
 默认keypath:.text
 value:@NSString
 */
UIKIT_EXTERN NSString*_Nonnull GLCustomCellBadge;      //badge角标
/*
 @class GLCustomButton
 默认keypath:.title .image
 value:@NSString @UIImage
 */
UIKIT_EXTERN NSString*_Nonnull GLCustomCellButton;      //按钮1
UIKIT_EXTERN NSString*_Nonnull GLCustomCellButton2;      //按钮2
UIKIT_EXTERN NSString*_Nonnull GLCustomCellButton3;      //按钮3
UIKIT_EXTERN NSString*_Nonnull GLCustomCellButton4;      //按钮4
UIKIT_EXTERN NSString*_Nonnull GLCustomCellButton5;      //按钮5

UIKIT_EXTERN NSString*_Nonnull GLCustomCellActivityIndicator;      //加载

//cell自带
UIKIT_EXTERN NSString*_Nonnull GLCustomCellArrow;         // 箭头
UIKIT_EXTERN NSString*_Nonnull GLCustomCellRadio;         // 勾

/*********************************************************/


/* defaultAttribute
 GLCustomCell默认组件属性
 可调用updateComponentAttributeByDictionary进行默认值修改，全局调用一次即可
 
 
 默认值如下：
  @{
         
         GLCustomCellText:@{
                 @"font":GLCustomCell_Title_Font,
                 @"textColor":[UIColor blackColor],
                 },
         GLCustomCellGrayText:@{
                 @"font":GLCustomCell_Title_Font,
                 @"textColor":[UIColor grayColor],
                 },
         GLCustomCellText2:@{
                 @"font":GLCustomCell_Title_Font,
                 @"textColor":[UIColor blackColor],
                 },
         GLCustomCellCenterText:@{
                 @"font":GLCustomCell_Title_Font,
                 @"textColor":[UIColor blackColor],
                 @"textAlignment":@(NSTextAlignmentCenter),
                 },
         GLCustomCellSubText:@{
                 @"font":[UIFont systemFontOfSize:14],
                 @"textColor":[UIColor blackColor],
                 },
         GLCustomCellCatpionText:@{
                 @"font":GLCustomCell_Title_Font,
                 @"textColor":[UIColor grayColor],
                 @"textAlignment":@(NSTextAlignmentRight),
                 },
         //textField
         GLCustomCellInput:@{
                 @"font":GLCustomCell_Title_Font,
                 @"clearButtonMode":@(UITextFieldViewModeWhileEditing),
                 @"placeholder":@"请输入",
                 },
         GLCustomCellLeftNoEditInput:@{
                 @"font":GLCustomCell_Title_Font,
                 @"clearButtonMode":@(UITextFieldViewModeWhileEditing),
                 @"placeholder":@"请选择",
                 @"enabled":@(NO),
                 },
         GLCustomCellRightNoEditInput:@{
                 @"font":GLCustomCell_Title_Font,
                 @"clearButtonMode":@(UITextFieldViewModeWhileEditing),
                 @"placeholder":@"请选择",
                 @"textAlignment":@(NSTextAlignmentRight),
                 @"enabled":@(NO),
                 },
         //textView
         GLCustomCellTextView:@{
                 @"font":GLCustomCell_Title_Font,
                 },
         //badge
         GLCustomCellBadge:@{
                 @"font":[UIFont systemFontOfSize:14],
#ifdef GLS_COLOR_RED_E0563D
                 @"backgroundColor":GLS_COLOR_RED_E0563D,
#else
                 @"backgroundColor":[UIColor redColor],
#endif
                 },
         //button
         GLCustomCellButton:@{
                 @"titleLabel.font":[UIFont systemFontOfSize:14],
                 @"imageView.contentMode":@(UIViewContentModeScaleAspectFit),
                 @"titleColor":[UIColor blackColor],
                 },
         GLCustomCellButton2:@{
                 @"titleLabel.font":[UIFont systemFontOfSize:14],
                 @"imageView.contentMode":@(UIViewContentModeScaleAspectFit),
                 @"titleColor":[UIColor blackColor],
                 },
         GLCustomCellButton3:@{
                 @"titleLabel.font":[UIFont systemFontOfSize:14],
                 @"imageView.contentMode":@(UIViewContentModeScaleAspectFit),
                 @"titleColor":[UIColor blackColor],
                 },
         GLCustomCellButton4:@{
                 @"titleLabel.font":[UIFont systemFontOfSize:14],
                 @"imageView.contentMode":@(UIViewContentModeScaleAspectFit),
                 @"titleColor":[UIColor blackColor],
                 },
         GLCustomCellButton5:@{
                 @"titleLabel.font":[UIFont systemFontOfSize:14],
                 @"imageView.contentMode":@(UIViewContentModeScaleAspectFit),
                 @"titleColor":[UIColor blackColor],
                 },
         };
*/
@class GLCustomCell;

typedef void (^GLCustomCellAttributionRegister)(GLCustomCell*_Nonnull cell);

typedef void (^GLCustomCellRegister)(GLCustomCell *_Nonnull cell,id _Nonnull model);


@class GLFormBaseCell;

#if __has_include("GLFormBaseCell.h")
@interface GLCustomCell : GLFormBaseCell
#else
@interface GLCustomCell : UITableViewCell
#endif
@property(nonatomic,strong,nullable)GLComponentKey* valueKey;
@property(nonatomic,readonly,nullable,class)NSMutableDictionary* customComponentDict;

//内容边距 仅针对GLCustomCell生效
@property (nonatomic,assign) UIEdgeInsets contentEdge;
//GLCustomCell组件间间距
@property (nonatomic,assign) CGFloat itemHorizontalSpace;

/**
 更新GLCustomCell组件默认属性，全局调用一次

 @param dict 属性字典
 例子
 [GLCustomCell updateComponentAttributeByDictionary:@{
     GLCustomCellCenterText:@{
     @"font":GLCustomCell_Title_Font,
     @"textColor":[UIColor redColor],
     @"textAlignment":@(NSTextAlignmentCenter),
     },
     GLCustomCellButton:@{
     @"titleLabel.font":[UIFont systemFontOfSize:14],
     @"imageView.contentMode":@(UIViewContentModeScaleAspectFit),
     @"titleColor":[UIColor redColor],
     }
 }];
 */
+ (void)updateComponentAttributeByDictionary:(nonnull NSDictionary*)dict;

+ (void)registerCustomComponentByKey:(nonnull NSString*)key size:(CGSize)size component:(nonnull UIView*(^)(void))customComponentBlock;


- (nonnull instancetype)initWithCustom:(nonnull NSArray*)componentKeys reuseIdentifier:(nonnull NSString *)reuseIdentifier;


/**
 通过KVC设置各组件属性

 @param value 组件值，如果不加keyPath，默认值如下
 GLCustomLabel
     keypath:.text
     value:@NSString
 GLCustomButton
     keypath:.title .image
     value:@NSString @UIImage
 GLCustomImageView
     keypath:.image
     value:@NSString @UIImage @NSString
 UISwitch
     keypath:.on
     value:NSNumber
 UITextField
     keypath:.text
     value:@NSString
 UITextView
     keypath:.text
     value:@NSString
 GLBadgeLabel
     keypath:.text
     value:@NSString
 @param ckey GLComponentKey 支持@"GLCustomCellText.textColor"样式
 */
- (void)setComponentValue:(nullable id)value forComponenyKey:(nonnull GLComponentKey *)ckey;
- (void)setComponentValue:(nullable id)value forComponenyKey:(nonnull GLComponentKey *)ckey forKeyPath:(nonnull NSString*)key;

/**
 通过KVC设置各组件属性

 @param dict kvc字典
 示例
 [cell setComponentValuesForKeys:@{
                                 GLCustomCellIcon:[UIImage imageNamed:iconName],
                                 GLCustomCellText:title,
                                 GLCustomCellCatpionText:sub,
                                 }];
 */
- (void)setComponentValuesForKeys:(nonnull NSDictionary*)dict;

- (nullable id)getComponentValueByKey:(nonnull GLComponentKey*)key;

@end

@interface GLCustomCell (GLIdentifier)

/**
 根据identifier注册全局cell，更方便调用，需注册数据绑定BLOCK
 全局仅调用一次即可
 @param componentKeys 注册的组件
 @param identifier 唯一标识符，重用标识
 @param defaultConfig 默认属性设置
 @param bindBlock 对cell与数据源进行绑定
 示例：
-------------------->注册
[GLCustomCell registerCell:@[GLCustomCellIcon,GLCustomCellText,GLCustomCellCatpionText] identifier:GLCustomCellIdentifierCell2 defaultAttributeConfig:^(GLCustomCell *cell) {
    //设置GLCustomCellText各属性，仅在CELL初始化时调用一次
    [cell setComponentValue:[UIColor blackColor] forComponenyKey:GLCustomCellText forKeyPath:@"textColor"];
    [cell setComponentValue:[UIFont systemFontOfSize:15] forComponenyKey:GLCustomCellText forKeyPath:@"font"];
    //设置GLCustomCellCatpionText各属性，仅在CELL初始化时调用一次
    [cell setComponentValue:[UIColor grayColor] forComponenyKey:GLCustomCellCatpionText forKeyPath:@"textColor"];
    [cell setComponentValue:[UIFont systemFontOfSize:15] forComponenyKey:GLCustomCellCatpionText forKeyPath:@"font"];
} bindData:^(GLCustomCell *cell, id model) {
    NSDictionary* dict = (NSDictionary*)model;
    NSString* iconName = dict[@"AppIcon"];
    NSString* title = dict[@"title"];
    NSString* sub = dict[@"sub"];
 
     //设置各组件值，cell初始化及复用均会调用
    [cell setComponentValuesForKeys:@{
                                      GLCustomCellIcon:[UIImage imageNamed:iconName],
                                      GLCustomCellText:title,
                                      GLCustomCellCatpionText:sub,
                                      }];
}];

-------------------->使用
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    GLCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:GLCustomCellIdentifierCell2];
    if (!cell) {
        cell = [[GLCustomCell alloc]initWithIdentifier:GLCustomCellIdentifierCell2];
    }
    NSDictionary* dict = [self.datas objectAtIndex:indexPath.row];
    [cell updateCellByModel:dict];
    // Configure the cell...
    
    return cell;
}
*/
+ (void)registerCell:(nonnull NSArray*)componentKeys identifier:(nonnull NSString*)identifier defaultAttributeConfig:(nullable GLCustomCellAttributionRegister)defaultConfig bindData:(nonnull GLCustomCellRegister)bindBlock;
+ (void)registerCell:(nonnull NSArray*)componentKeys identifier:(nonnull NSString*)identifier bindData:(nonnull GLCustomCellRegister)bindBlock;


/**
 使用通过registerCell注册过的CELL

 @param identifier 唯一标识符，重用标识
 @return 实例
 */
- (nonnull instancetype)initWithIdentifier:(nonnull NSString *)identifier;


/**
 更新cell数据
 需使用registerCell注册过
 需使用initWithIdentifier初始化
 @param obj 数据model，需在registerCell时绑定的数据类型
 */
- (void)updateCellByModel:(nonnull id)obj;

@end




@interface GLCustomImageView : UIImageView
@property(nonatomic,weak,nullable)NSURL* url;
@property(nonatomic,weak,nullable)NSString* imageName;
@property(nonatomic,weak,nullable)NSString* imagePath;
@property(nonatomic,assign)CGSize size;
@end


@interface GLCustomButton : UIButton
@property(nonatomic,weak,nullable)UIImage* image;
@property(nonatomic,weak,nullable)NSString* imageName;
@property(nonatomic,weak,nullable)NSString* imagePath;

@property(nonatomic,weak,nullable)UIImage* selectedImage;
@property(nonatomic,weak,nullable)NSString* selectedImageName;
@property(nonatomic,weak,nullable)NSString* selectedImagePath;

@property(nonatomic,weak,nullable)NSString* title;
@property(nonatomic,weak,nullable)UIColor* titleColor;

@property(nonatomic,weak,nullable)NSString* selectedTitle;
@property(nonatomic,weak,nullable)UIColor* selectedTitleColor;
@property(nonatomic,assign)CGSize size;
@end

@interface GLCustomLabel : UILabel
@property(nonatomic,assign)NSInteger limitWordNumber;
@property(nonatomic,assign)CGSize size;
@end

@interface GLActivityIndicatorView : UIActivityIndicatorView
@property(nonatomic,assign)BOOL animating;
@end

