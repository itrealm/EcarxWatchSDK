/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLCustomColor.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#ifndef GLCustomColor_h
#define GLCustomColor_h

#import "UIColor+GLHex.h"
/** 颜色函数缩写 */
#define GLColorRGB(r,g,b)    [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define GLColorRGBA(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
#define GLColorHex(_hex_)   [UIColor gl_colorWithHexStr:_hex_]


#define KCOLOR_CLEAR [UIColor clearColor]

/** 黑色 */
#define KCOLOR_BLACK [UIColor blackColor]
/** 白色 */
#define KCOLOR_WHITE [UIColor gl_colorWithHexStr:@"ffffff"]
/** 红色 */
#define KCOLOR_RED [UIColor gl_colorWithHexStr:@"F95566"]

/** 粉色 */
#define KCOLOR_PINK [UIColor gl_colorWithHexStr:@"f54359"]
/** 绿色 */
#define KCOLOR_GREEN [UIColor gl_colorWithHexStr:@"00c7b2"]
/** 深灰色 */
#define KCOLOR_DARK_GRAY [UIColor gl_colorWithHexStr:@"565a5c"]
/** 灰色 */
#define KCOLOR_GRAY [UIColor gl_colorWithHexStr:@"1D252F"]
/** 亮灰色 */
#define KCOLOR_LIGHT_GRAY [UIColor gl_colorWithHexStr:@"343B44"]
/** 深蓝色 */
#define KCOLOR_DARK_BLUE [UIColor gl_colorWithHexStr:@"111c24"]

/** 黄色 */
#define KCOLOR_YELLO_COLOR [UIColor gl_colorWithHexStr:@"e7e600"]






#endif /* GLCustomColor_h */
