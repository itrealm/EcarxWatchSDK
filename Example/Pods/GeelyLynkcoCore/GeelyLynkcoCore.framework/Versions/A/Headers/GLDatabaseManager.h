//
//  GLSqliteDataBase.h
//  Geelyconsumer
//
//  Created by 朱伟特 on 2018/8/29.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/FMDB.h>



@interface GLDatabaseManager : NSObject

+ (GLDatabaseManager *)sharedManager;//单例构造方法
#pragma mark- 线程安全

/**
 线程安全执行方法
 
 @param block 具体执行操作
 */
+(void)safeExec:(void(^)(GLDatabaseManager* manager))block;

#pragma mark- 非线程安全

/**
 执行sql语句
 
 @param sql 标准sql语句
 @return 结果
 */
- (BOOL)db_executeUpdate:(NSString *)sql;


/**
 执行sql查询语句
 
 @param sql 标准sql语句
 @return 查询结果
 */
- (FMResultSet*)db_executeQuery:(NSString *)sql;


/**
 根据model字段去创建table，如果已经存在，则不在创建
 
 @param tableName tableName
 @param modelClass model Class
 @param primaryKey 指定主key,可以为nil
 @return 执行结果
 */
- (BOOL)db_createTable:(NSString *)tableName model:(Class)modelClass primaryKey:(NSString *)primaryKey;


- (BOOL)db_tableExists:(NSString*)tableName;

/**
 查找models
 
 @param modelClass model Class
 @param tableName 表名
 @param keyString 列的字段名称
 @param valueString 列的值
        等同于'where keyString = valueString'
 @return 查找结果
 */
- (NSMutableArray *)db_searchAllModel:(Class)modelClass tableName:(NSString *)tableName withKey:(NSString *)keyString value:(NSString *)valueString;
/**
 查找models
 
 @param modelClass model Class
 @param tableName 表名
 @return 查找结果
 */
- (NSMutableArray *)db_searchAllModel:(Class)modelClass tableName:(NSString *)tableName;

/**
 查找models
 
 @param modelClass model Class
 @param tableName 表名
 @param conditionSql 条件语句，示例：'where age>=30'
 @return 查找结果
 */
- (NSMutableArray *)db_searchAllModel:(Class)modelClass tableName:(NSString *)tableName withConditionSql:(NSString*)conditionSql;



/**
 在表中插入model
 
 @param model model
 @param tableName tableName
 @return 执行结果
 */
- (BOOL)db_insterModel:(id)model toTable:(NSString *)tableName;

/**
 在表中插入model
 
 @param modelArray model数组
 @param tableName tableName
 @return 执行结果
 */
- (BOOL)db_insterModelArray:(NSArray *)modelArray toTable:(NSString *)tableName;

/**
 删除整张表
 
 @param tableName 表名
 @return 执行结果
 */
- (BOOL)db_dropTable:(NSString *)tableName;


/**
 删除表中所有所有的数据，但不删除表
 
 @param tableName 表名
 @return 执行结果
 */
- (BOOL)db_removeAllDataFromTable:(NSString *)tableName;

/**
 删除表中字段{keyString}值为{valueString}的数据
 
 @param tableName 表名
 @param keyString 字段名称
 @param valueString 字段值
        等同于'where keyString = valueString'
        如果keyString和valueString为空，则将删除所有所数据
 @return 执行结果
 */
- (BOOL)db_deleteModelFromTable:(NSString *)tableName key:(NSString *)keyString value:(NSString *)valueString;

/**
 删除表中符合条件{conditionSql}的数据
 
 @param tableName 表名
 @param conditionSql 条件语句，示例：'where age >= 30'
        如果条件语句为空，则将删除所有所数据
 @return 执行结果
 */
- (BOOL)db_deleteModelFromTable:(NSString *)tableName withConditionSql:(NSString*)conditionSql;

@end

