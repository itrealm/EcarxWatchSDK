//
//  GLRouter
//
//  Created by kzy on 2018/7/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GLDevice : NSObject


+ (instancetype)currentDevice;

@property(nonatomic,readonly,strong) NSString    *name;              // e.g. "My iPhone"
@property(nonatomic,readonly,strong) NSString    *model;             // e.g. @"iPhone", @"iPod touch"
@property(nonatomic,readonly,strong) NSString    *localizedModel;    // localized version of model
@property(nonatomic,readonly,strong) NSString    *systemName;        // e.g. @"iOS"
@property(nonatomic,readonly,strong) NSString    *systemVersion;     // e.g. @"4.0"
@property(nonatomic,readonly) UIDeviceOrientation orientation;       // return current device orientation.  this will return UIDeviceOrientationUnknown unless device orientation notifications are being generated.

@property(nullable, nonatomic,readonly,strong) NSUUID      *identifierForVendor;      // a UUID that may be used to uniquely identify the device, same across apps from a single vendor.

@property(nonatomic,readonly) UIDeviceBatteryState          batteryState;  // UIDeviceBatteryStateUnknown if monitoring disabled
@property(nonatomic,readonly) float                         batteryLevel;  // 0 .. 1.0. -1.0 if UIDeviceBatteryStateUnknown
@property(nonatomic,readonly) UIUserInterfaceIdiom userInterfaceIdiom;

#pragma mark-

@property(nonatomic,readonly) CGFloat                         aspectRatio;  // 屏膜宽高比

@property(nonatomic,readonly) CGFloat                         navBarHeight;
@property(nonatomic,readonly) CGFloat                         tabBarHeight;



-(CGFloat)yOffsetScaleInIPhone7WithTabBarByOrginYOffset:(CGFloat)yOffset;

-(CGFloat)yOffsetScaleForSize:(CGSize)size cutScreenHeight:(CGFloat)cutHeight orginYOffset:(CGFloat)yOffset;
@end



