/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLErrorMessageView.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#import <UIKit/UIKit.h>

/**
 顶部错误消息视图(自动弹出)
 */
@interface GLErrorMessageView : UIView
+ (GLErrorMessageView *)shareRemindView;

/**
 设置提醒框的类型及提醒内容
 
 @param message 消息内容
 */
- (void)setMessageMessage:(NSString *)message;

@end
