//
//  GLKTableView.h
//  KTools
//
//  Created by zhiyong.kuang on 2017/9/28.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, GLErrorCode) {
    GLErrorCodeNone = 0,
    GLErrorCodeValueNil,
    GLErrorCodeValueValid,
};



#pragma mark- class GLFormError

@interface GLFormError : NSError
@property(nonatomic,copy)NSString* tag;
@end

@class GLFormSection;
@class GLFormRow;
@class GLFormModule;

typedef void(^GLFormEventClicked)(GLFormRow*row,NSIndexPath* indexPath,id sender,BOOL isSub,NSString* subTag);
typedef void(^GLFormValueChanged)(GLFormRow*row,NSIndexPath* indexPath,id newValue);

@interface GLForm : NSObject
@property(nonatomic,readonly)NSMutableArray* modules;
@property(nonatomic,readonly)NSMutableArray* sectioins;
@property(nonatomic,copy)NSString* tag;
@property (nonatomic, weak) UITableView *gl_tableView;

@property (nonatomic, strong) UIView *formHeaderView;
@property (nonatomic, strong) UIView *formFooterView;
@property (nonatomic, assign) BOOL showCorner;
@property (nonatomic, assign) UITableViewStyle tableStyle;
@property (nonatomic, assign) UITableViewCellSeparatorStyle separatorStyle;//系统原生分割线
@property (nonatomic, strong) UIColor *backgroundColor;

@property (nonatomic, strong) GLFormEventClicked formClicked;
@property (nonatomic, strong) GLFormValueChanged formValueChanged;

+(instancetype)formWithTag:(NSString*)tag;

-(void)addFormModule:(GLFormModule*)module;

-(void)addFormSection:(GLFormSection*)section;


-(void)removeFormSectionIndex:(NSInteger)index;
-(void)removeFormSection:(GLFormSection*)section;
-(void)removeFormModule:(GLFormModule*)module;

- (void)insertFormSection:(GLFormSection*)section atIndex:(NSUInteger)index;
- (void)insertFormSections:(NSArray<GLFormSection*> *)sections atIndexes:(NSIndexSet *)indexes;


-(NSArray<GLFormRow *> *)formRowsWithTag:(NSString *)tag;
-(GLFormRow *)formRowAtIndex:(NSIndexPath *)indexPath;
-(NSIndexPath *)indexPathForFormRow:(GLFormRow *)formRow;
-(GLFormSection *)formSectionAtIndex:(NSUInteger)index;
-(NSInteger)indexformSection:(GLFormSection*)section;

-(GLFormModule *)formModuleWithTag:(NSString *)tag;


-(NSDictionary *)formValues;
-(GLFormError*)validateFormValues;




- (void)reloadFormSections:(NSArray<GLFormSection*> *)sections withRowAnimation:(UITableViewRowAnimation)animation;
- (void)reloadFormRows:(NSArray<GLFormRow *> *)rows withRowAnimation:(UITableViewRowAnimation)animation;
- (void)reloadData;

@end





