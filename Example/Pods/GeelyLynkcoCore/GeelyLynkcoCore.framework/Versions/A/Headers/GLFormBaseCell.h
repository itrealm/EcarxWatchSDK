//
//  GLFormBaseCell.h
//  KTools
//
//  Created by zhiyong.kuang on 2017/10/24.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GLFormRow;
@interface GLFormBaseCell : UITableViewCell
@property(nonatomic,weak)GLFormRow* formRow;
@property(nonatomic,assign)BOOL isRequired;
@property(nonatomic,assign)CGFloat edgeMargin;
@property(nonatomic,assign)CGFloat contentMargin;
//separator
@property(nonatomic,assign)BOOL glSeparatorHidden;
@property (nonatomic) UIColor* glSeparatorColor;
@property (nonatomic) UIEdgeInsets glSeparatorInset;


/*
 * 需要重写，外部value变化后，会调用此值，请更新value
 */

-(void)setRowValue:(id)value;
/*
 * 需要调用
 */
//cell vlaue变化时，调用[super valueDidChanged:newValue]，通知GLForm
-(void)valueDidChanged:(id)newValue;
//cell 内部子控件有点击事件时，调用
-(void)clicked:(id)sender subTag:(NSString*)tag;
@end
