//
//  GLFormModule.h
//  KTools
//
//  Created by zhiyong.kuang on 2017/9/28.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易

#import <UIKit/UIKit.h>
@class GLForm;
@class GLFormSection;
@class GLFormRow;

@protocol GLFormModuleDelegate<NSObject>
@optional

-(void)setup;//init时调用
-(void)shouldUpdateData;
-(void)formRowDidSelected:(GLFormRow*)row isSub:(BOOL)isSub sender:(id)sender subTag:(NSString*)subTag;
-(void)formRowValueChanged:(GLFormRow*)row newValue:(id)newValue;
@end

typedef NS_ENUM(NSInteger, GLFormModuleLoadTime) {
    GLFormModuleLoadTimeDefault = 0,//注册时即会调用shouldUpdateData
    GLFormModuleLoadTimeWhenShow,//加载显示时才调用shouldUpdateData
};


@interface GLFormModule : NSObject<GLFormModuleDelegate>
@property(nonatomic,strong)NSString* tag;
@property(nonatomic,assign)GLFormModuleLoadTime loadTime;
@property (nonatomic, weak)UIViewController* viewController;
@property(nonatomic,readonly)NSMutableArray* sectioins;
@property(nonatomic,weak)GLForm* form;

+(instancetype)formModuleWithTag:(NSString*)tag;

/**
 加载中占位行高
 仅在loadTime为GLFormModuleLoadTimeWhenShow时生产，默认为400
 */
@property(nonatomic,assign)CGFloat loadingRowHeight;

-(void)addFormSection:(GLFormSection*)section;
-(void)removeFormSectionIndex:(NSInteger)index;
-(void)removeAllFormSections;


-(void)shouldRequestInitData;


-(void)reloadData;
-(void)reloadDataWithAnimate:(BOOL)yesOrNot;

-(void)shouldCacheData;

/**
 真实数据加载前的占位section
 可重写后返回自定义
 */
-(GLFormSection*)defaultLoadSection;
-(GLFormSection*)defaultEmptySection;

@end





