//
//  GLFormRow.h
//  KTools
//
//  Created by zhiyong.kuang on 2017/10/24.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GLFormBaseCell;
@class GLFormRow;
@class GLFormModule;
@class GLForm;

typedef NSString* (^GLFormRowValidateBlock)(GLFormRow* row,id value);


/**
 当row配置action时各种操作

 - GLFormRowActionNone: 无
 - GLFormRowActionPicker: 显示Picker供选择数据
 - GLFormRowActionPushViewController: 进入下级业面，需赋值pushViewController
 - GLFormRowActionPushOption: push进入下级界面进行选择
 - GLFormRowActionPushSwitch: push进入下级界面switch
 - GLFormRowActionPushInput: push进入下级界面进行输入
 */
typedef NS_ENUM(NSInteger, GLFormRowAction) {
    GLFormRowActionNone = 0,
    GLFormRowActionPicker,
    GLFormRowActionPushViewController,
    GLFormRowActionPushOption,
//    GLFormRowActionPushSwitch,
    GLFormRowActionPushInput,
};

typedef NS_ENUM(NSInteger, GLFormRowSeparatorStyle) {
    GLFormRowSeparatorStyleGloble = 0,
    GLFormRowSeparatorStyleShow,
    GLFormRowSeparatorStyleHide,
};


@interface GLFormRow : NSObject


/**
 实例方法，该方法会使用指定的cellClass去创建cell

 @param cellClass cell Class
 @param tag 识别及复用标识符
 @return GLFormRow实例
 */
+(instancetype)formRowByRegisterClass:(Class)cellClass withTag:(NSString*)tag;
+(instancetype)formRowByRegisterClass:(Class)cellClass withTag:(NSString*)tag updateContent:(void(^)(id cell))block;

/**
 每一个row对应的标识字符串，用于tableView复用以及最后表单value的识别
 */
@property(nonatomic,copy)NSString* tag;

/**
 每一个row对应的高度
 default 44
 */
@property(nonatomic,assign)CGFloat rowHeight;
@property(nonatomic,assign)CGFloat estimatedRowHeight;
/**
 //缓存高度，自适应情况下，优先返回缓存高度
 如果布局变化，需要调用clearRowHeightCache清除缓存
 */
@property(nonatomic,assign)CGFloat rowHeightCache;

-(void)clearRowHeightCache;


/**
 每一个row仅有一个value，默认值
 */
@property(nonatomic,strong)id value;


/**
 row对应的cell class
 */
@property(nonatomic,readonly)Class cellClass;

@property(nonatomic,weak)GLFormBaseCell* cell;
@property(nonatomic,weak)GLForm* form;
@property(nonatomic,weak)GLFormModule* formModule;
/**
 //separator
 */
//separator
@property(nonatomic,assign)GLFormRowSeparatorStyle separatorStyle;
@property (nonatomic,strong) UIColor* separatorColor;
@property (nonatomic,assign) UIEdgeInsets separatorInset;

//cell 选择样式，默认不设置值，由cell本身决定
@property(nonatomic,assign)UITableViewCellSelectionStyle selectionStyle;


/**
 isRequried 是否是必填项，如果YES，cell前会有小红点标识
 */
@property(nonatomic,assign)BOOL isRequried;

/**
 requriedMsg 没有填写时，GLFormViewController调用 validateFormValues返回的错误信息
 */
@property(nonatomic,copy)NSString* requriedMsg;


/**
 仅在初始化时调用一次 通过KVC赋值
 [cell setValue:value forKeyPath:key]
 */
-(void)addDefaultValue:(id)value forKeyPath:(NSString*)keyPath;

-(void)addDefaultValuesByDictionary:(NSDictionary*)dict;


/**
 初始化及复用时，均调用 通过KVC赋值
 [cell setValue:value forKeyPath:key]
 */
-(void)addConfigValue:(id)value forKeyPath:(NSString*)keyPath;
-(id)configValueForKeyPath:(NSString*)keyPath;

-(void)addConfigValuesByDictionary:(NSDictionary*)dict;

/**
 设置row的Value,会同步修改cell
 为外部值变化，同步到表单CELL
 @param newValue 新value
 */
-(void)setRowValue:(id)newValue;

-(void)setCellValue:(id)newValue forKeyPath:(NSString*)keyPath;

-(void)updateCellContent:(void(^)(id cell))block;
/**
 校验表单value时，会调用该block去校验vlaue是否合法，合法返回nil,否则返回错误信息
 */
@property(nonatomic,strong)GLFormRowValidateBlock validateValueBlock;

/**
 当点击CELL所发生的动作
 */
@property(nonatomic,assign)GLFormRowAction action;
/**
 当action有选值操作时，会展示该数组中的操作
 */
@property(nonatomic,strong)NSArray* options;
/**
 当action为GLFormRowActionPushViewController时，
 push进入下一级界面，类名NSString或Class
 */
@property(nonatomic,strong)id pushViewController;

/**
 生成实例CELL

 @return 生成实例CELL
 */
-(GLFormBaseCell*)cellForForm:(GLForm*)form tableView:(UITableView*)tableView;


@end


@interface GLFormRow (GLCustomCell)

/**
 需要指定一个componentKey来作为表单的value
 */
@property(nonatomic,strong)NSString* valueKey;
/**
 实例方法，该方法会默认调用 GLCustomCell去创建cell
 
 @param componentKeys GLCustomCell的组件数组
 @param tag 识别及复用标识符
 @return GLFormRow实例
 */
+(instancetype)formRowByComponentKeys:(NSArray*)componentKeys withTag:(NSString*)tag;



-(void)addConfigValue:(id)value componentKey:(NSString*)ckey forKeyPath:(NSString*)keyPath;
-(void)addDefaultValue:(id)value componentKey:(NSString*)ckey forKeyPath:(NSString*)keyPath;

//需使用registerCell注册过
//需使用initWithIdentifier初始化
+(instancetype)formRowWithCustomCellIdentifier:(NSString*)tag;
//需使用initWithIdentifier初始化
- (void)updateCellByModel:(id)obj;

//内容边距 仅针对GLCustomCell生效
@property (nonatomic,assign) UIEdgeInsets contentEdge;
//GLCustomCell组件间间距
@property (nonatomic,assign) CGFloat itemHorizontalSpace;

@end
