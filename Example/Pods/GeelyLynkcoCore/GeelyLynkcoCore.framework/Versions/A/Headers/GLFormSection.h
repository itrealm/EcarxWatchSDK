//
//  GLFormSection.h
//  KTools
//
//  Created by zhiyong.kuang on 2017/10/24.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GLFormRow;
@interface GLFormSection : NSObject
@property(nonatomic,copy)NSString* tag;
@property(nonatomic,readonly)NSMutableArray* rows;
@property(nonatomic,assign)CGFloat HeaderHeight;
@property(nonatomic,assign)CGFloat footerHeight;

@property(nonatomic,strong)UIView* headerView;
@property(nonatomic,strong)UIView* footerView;

+(instancetype)formSectionWithTag:(NSString*)tag;

//data
- (void)addFormRow:(GLFormRow*)formRow;
- (void)addFormRowsFromArray:(NSArray<GLFormRow*> *)otherRows;

- (void)insertFormRow:(GLFormRow*)formRow atIndex:(NSUInteger)index;
- (void)insertFormRows:(NSArray<GLFormRow*> *)formRows atIndex:(NSUInteger)index;
- (void)insertFormRows:(NSArray<GLFormRow*> *)formRows atIndexes:(NSIndexSet *)indexes;

- (void)removeLastFormRow;
- (void)removeFormRowAtIndex:(NSUInteger)index;
- (void)removeAllFormRows;
- (void)removeFormRow:(GLFormRow*)formRow;
- (void)removeFormRowsInArray:(NSArray<GLFormRow*> *)otherRows;
- (void)removeFormRowsInRange:(NSRange)range;

- (void)replaceFormRowAtIndex:(NSUInteger)index withFormRow:(GLFormRow*)formRow;

#pragma mark- api
-(NSArray *)formRowsWithTag:(NSString *)tag;
-(GLFormRow *)formRowAtIndex:(NSInteger)index;
@end
