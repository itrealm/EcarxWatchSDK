//
//  GLGridView.h
//  KTools
//
//  Created by zhiyong.kuang on 2017/9/22.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易

#import <UIKit/UIKit.h>

typedef void (^GLGridViewDidSelectBlock)(UICollectionView*collectView,NSIndexPath*indexPath,id value);
typedef void (^GLGridViewDidMovedBlock)(UICollectionView*collectView,NSArray*newData);
typedef void (^GLGridViewDidAddBlock)(UICollectionView*collectView);


typedef void (^GLGridViewSupplementaryElementBindBlock)(UICollectionReusableView* cell,NSString *kind,NSInteger section);

typedef void (^GLGridViewBindBlock)(UICollectionViewCell* cell,NSIndexPath *indexPath,id value);

typedef NSMutableArray* (^GLGridViewDatasOfSection)(NSInteger section);
typedef NSInteger (^GLGridViewNumberOfSections)(void);


typedef NS_ENUM(NSInteger, GLGridViewOperationType) {
    GLGridViewOperationTypeNone,//无
    GLGridViewOperationTypeAways,//功能一直存在
    GLGridViewOperationTypeEditing,//编辑状态才有该功能
};

@protocol GLGridViewCellProtocol <NSObject>

-(void)updateCurrentEditable:(NSNumber*)editable;
-(void)shouldHiddenCellWhenBeenMove:(NSNumber*)isMoving;

@end
#pragma mark- CLASS GLGridView

@interface GLGridView : UIView
@property(nonatomic, readonly) UICollectionView *collectionView;
//是否处于编辑状态
@property(nonatomic, assign)BOOL editable;
/*
 @required
 dataArray 数据源
*/

@property(nonatomic, strong)GLGridViewNumberOfSections numOfSectionsBlock;
@property(nonatomic, strong)GLGridViewDatasOfSection datasOfSectionBlock;

/* 
 @required
 @description 注册数据CELL以及绑定
 @prama
    cellClass 需要展示的UICollectionViewCell
    dataBind 将dataArray数组中的ITEM与CELL进行绑定展示
*/
-(void)registerCell:(Class)cellClass bind:(GLGridViewBindBlock)dataBind;
-(void)registerHeaderView:(Class)headerClass bind:(GLGridViewSupplementaryElementBindBlock)dataBind;
-(void)registerFooterView:(Class)footerClass bind:(GLGridViewSupplementaryElementBindBlock)dataBind;
#pragma mark
-(void)registerCell:(Class)cellClass regularKey:(NSString*)regularKey bind:(GLGridViewBindBlock)dataBind;
-(void)registerHeaderView:(Class)headerClass regularKey:(NSString*)regularKey bind:(GLGridViewSupplementaryElementBindBlock)dataBind;
-(void)registerFooterView:(Class)footerClass regularKey:(NSString*)regularKey bind:(GLGridViewSupplementaryElementBindBlock)dataBind;

/*
 itemHeight 元素高度，default:60
 */
@property(nonatomic, assign) CGFloat itemHeight;
@property(nonatomic, assign) CGFloat sectionHeaderHeight;
@property(nonatomic, assign) CGFloat sectionFooterHeight;

@property(nonatomic, assign) CGFloat itemSpaceVertical;
@property(nonatomic, assign) CGFloat itemSpaceHorizontal;

/**
 emptyData Setting
 */
@property(nonatomic, copy) NSString* emptyAlertString;
@property(nonatomic, assign) CGFloat emptyYOffset;
@property(nonatomic, strong) NSAttributedString* emptyAlertAttributedString;


/**
 内容边距
 */
@property(nonatomic,assign)UIEdgeInsets contentEdge;
/*
 rowCount 每行格子数 default:4
 */
@property(nonatomic, assign) NSInteger rowCount;
/*
 lineWidth 网格线宽 default 最小
 */
@property(nonatomic, assign) CGFloat lineWidth;
/*
 lineColor 网格颜色 default 
 */
@property(nonatomic, strong) UIColor *lineColor;
/*
 showLine 是否显示网格 default YES
 */
@property(nonatomic, assign) BOOL showLine;

/*
 当不满一行时，是否需要填充空白行
 */
@property(nonatomic, assign) BOOL shouldFillRow;

@property(nonatomic, strong) UIView* headerView;

/*
 是否可跨区移动， default YES
 */
@property(nonatomic, assign) BOOL itemCanSpannedMove;

/*
 限定仅某些区域可移动，无刚不限制
 */
@property(nonatomic, strong) NSIndexSet *itemdMoveSectionLimit;

/*
 canMoveItem 网格视图移动类型
 */
@property(nonatomic, assign) GLGridViewOperationType itemMoveType;
/*
 canMoveItem 网格视图删除类型
 */
@property(nonatomic, assign) GLGridViewOperationType itemDeleteType;
/*
 canMoveItem 网格视图添加类型
 */
@property(nonatomic, assign) GLGridViewOperationType itemAddType;

/*
 @description 数据源切换后及首次初始化后，需要调用，重新加载数据
 */
-(void)reloadData;


/*
 @description 网格点击事件回调
 */
@property(nonatomic, copy)GLGridViewDidSelectBlock didWhenSelectItem;

/*
 @description 排序变更
 */
@property(nonatomic, copy)GLGridViewDidMovedBlock didWhenMovedItem;

@property(nonatomic, copy)GLGridViewDidMovedBlock didWhenDeletedItem;
@property(nonatomic, copy)GLGridViewDidAddBlock didWhenAddClickedItem;

@property(nonatomic, assign)CGSize delBtnSize;
@property(nonatomic, strong)UIImage* delImage;

@property(nonatomic, assign)CGSize addBtnSize;
@property(nonatomic, strong)UIImage* addImage;
@end



