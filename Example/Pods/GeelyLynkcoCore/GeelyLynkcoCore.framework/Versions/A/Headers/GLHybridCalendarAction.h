//
//  DTDeviceCalendarAction.h
//  Geelyconsumer
//
//  Created by wx-5102 on 2018/5/7.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLHybridCommand.h"

@interface GLHybridCalendarAction : NSObject

- (void)addCalendar:(GLHybridCommand*)command;

- (void)searchCalendar:(GLHybridCommand*)command;

- (void)deleteCalendar:(GLHybridCommand*)command;

@end
