///
//  GLHybridCommand.h
//  GLHybird-Example
//
//  Created by 匡志勇 on 2018/8/4.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GLWebViewController.h"


@interface GLHybridCommand : NSObject

@property (nonatomic, strong) NSString *handleName;
@property (nonatomic, strong) NSString *moduleName;
@property (nonatomic, strong) NSString *className;
@property (nonatomic, strong) NSString *methodName;
@property (nonatomic, strong) NSDictionary *params;
@property (nonatomic, weak) UIView *webView;
@property (nonatomic, weak) GLWebViewController *controller;


-(void)excuteSuccessCallBack:(id)result;
-(void)excuteFailedCallBack:(id)result;


@end



@interface GLHybridWkCommand : GLHybridCommand
@property (nonatomic, strong) void(^responseCallback)(id);
@end


