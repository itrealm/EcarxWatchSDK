//
//  GLHybridExecuter.h
//  GLHybird-Example
//
//  Created by 匡志勇 on 2018/8/4.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLHybridCommand.h"

@interface GLHybridExecuter : NSObject

+(id)executerObjForKeyPath:(NSString*)keyPath;

+(void)registerExecuter:(Class)aclass forKeyPath:(NSString*)keyPath;
+(void)removeExecuter:(Class)aclass forKeyPath:(NSString*)keypath;

-(void)exectue:(GLHybridCommand*)command;



@end
