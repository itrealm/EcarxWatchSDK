//
//  GLHybridNavigationAction.h
//  PersonalManager
//
//  Created by hsd on 16/8/10.
//  Copyright © 2016年 Haiyun.Qian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLHybridCommand.h"


@interface GLHybridNavigationAction : NSObject

- (void)getToken:(GLHybridCommand*)command;
- (void)setTitle:(GLHybridCommand*)command;
- (void)close:(GLHybridCommand*)command;
- (void)back:(GLHybridCommand*)command;

- (void)hiddenCloseButton:(GLHybridCommand*)command;

//隐藏导航栏
- (void)hiddenNavigationBar:(GLHybridCommand*)command;
/** 隐藏状态栏 */
- (void)hiddenStatusBar:(GLHybridCommand*)command;

/** 新的设置左边按钮(新的设置左边按钮的功能,推荐使用.传入的参数可以是网络图片,也可以是静态资源图片,如果传入的是静态资源图片,只用传入相应的图片名即可) */
- (void)addLeftBarButton:(GLHybridCommand*)command;

/** 新的设置右边按钮(新的设置左边按钮的功能,推荐使用.传入的参数可以是网络图片,也可以是静态资源图片,如果传入的是静态资源图片,只用传入相应的图片名即可) */
- (void)addRightBarButton:(GLHybridCommand*)command;
//返回到主页
- (void)goToHomePage:(GLHybridCommand*)command;


/**
 设置导航栏进度条颜色

 @param command command
 */
- (void)setProgressColor:(GLHybridCommand *)command;

/**
 设置webview背景颜色
 
 @param command command
 */
- (void)setBackgroundColor:(GLHybridCommand *)command;
//启用bounce
-(void)setWebviewBounceEnable:(GLHybridCommand *)command;

/**
 路由系统
 */
- (void)router:(GLHybridCommand *)command;


#pragma mark- 老接口，不建议使用
/** 设置右边按钮 */
- (void)setRight:(GLHybridCommand*)command;

/** 设置左边按钮*/
- (void)setLeft:(GLHybridCommand*)command;

- (void)call:(GLHybridCommand*)command;

- (void)setNavigationLeftBar:(GLHybridCommand *)command;
- (void)setNavigationRightBar:(GLHybridCommand *)command;
- (void)hiddenNavigationImage:(GLHybridCommand*)command;


@end
