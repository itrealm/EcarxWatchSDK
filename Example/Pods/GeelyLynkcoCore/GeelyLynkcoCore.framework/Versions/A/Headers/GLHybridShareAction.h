//
//  DTBizShare.h
//  AFNetworking
//
//  Created by 薛立恒 on 2018/8/3.
//

#import <Foundation/Foundation.h>
#import "GLHybridCommand.h"

@interface GLHybridShareAction : NSObject

/** 分享 */
- (void)share:(GLHybridCommand *)command;

@end
