//
//  GLHybridUserAction.h
//  AFNetworking
//
//  Created by 薛立恒 on 2018/8/2.
//

#import <Foundation/Foundation.h>
#import "GLHybridCommand.h"

@interface GLHybridUserAction : NSObject

/** 获取用户信息 */
- (void)getUserInfo:(GLHybridCommand*)command;
/** 获取token */
- (void)getToken:(GLHybridCommand*)command;

@end
