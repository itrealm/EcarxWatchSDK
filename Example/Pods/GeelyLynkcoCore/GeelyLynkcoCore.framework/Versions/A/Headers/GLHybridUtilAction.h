//
//  GLHybridUtilAction.h
//  PersonalManager
//
//  Created by user on 16/8/15.
//  Copyright © 2016年 Haiyun.Qian. All rights reserved.
//

#import <Foundation/Foundation.h>
@class GLHybridCommand;

@interface GLHybridUtilAction : NSObject

- (void)scan:(GLHybridCommand *)command;
//分享
- (void)share:(GLHybridCommand *)command;

- (void)openLink:(GLHybridCommand *)command;

- (void)notifiy:(GLHybridCommand *)command;
@end
