//
//  GLGoodsImageDisplayView.h
//  Geelyconsumer
//
//  Created by 朱伟特 on 2018/12/11.
//  Copyright © 2018 Rdic. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSInteger, GLImageDisplayDownloadTaskState) {
    GLImageDisplayDownloadTaskStateNone = 0,
    GLImageDisplayDownloadTaskStateDownloading,
    GLImageDisplayDownloadTaskStateSuccess,
    GLImageDisplayDownloadTaskStateFailed,
};

@interface GLImageDisplayDownloadTask : NSObject
@property (nonatomic, strong)NSString* url;

@property (nonatomic, assign)GLImageDisplayDownloadTaskState state;
@property (nonatomic, readonly)CGFloat progress;
@property (nonatomic, readonly)id result;


@property (nonatomic, readonly)void(^progressBlock)(CGFloat progress);
@property (nonatomic, readonly)void(^competionBlock)(BOOL success,id result);

@end



@interface GLImageDisplayModel : NSObject
@property (nonatomic, readonly)GLImageDisplayDownloadTask* downloadTask;

-(BOOL)downloadTask:(GLImageDisplayDownloadTask*)task;
@end

@interface GLImagePhotoDisplayModel : GLImageDisplayModel
@property (nonatomic, strong)NSString* imgUrl;
@property (nonatomic, strong)UIImage* image;
@property (nonatomic, weak)UIImageView* imageView;
@property (nonatomic, assign)CGFloat maxZoomScale; //default 3
@property (nonatomic, assign)CGFloat minZoomScale; //default 1

+(instancetype)modelWithImage:(UIImage*)image orImageUrl:(NSString*)imageUrl imageView:(UIImageView*)imageView;
@end

@interface GLImageVideoDisplayModel : GLImageDisplayModel
@property (nonatomic, strong)NSURL* videoUrl;
@property (nonatomic, weak)UIImageView* coverImageView;
@property (nonatomic, strong)UIImage* coverImage;

@end



@protocol GLImageDisplayContentDelegate<NSObject>
@required
-(void)displayImageContentShouldUpdateByDatas:(NSArray<GLImageDisplayModel*>*)datas currIndex:(NSInteger)index;
-(void)shouldHiddenContent:(BOOL)yesOrNot animated:(BOOL)animated;
@end


typedef NS_ENUM(NSInteger, GLImageDisplayMode) {
    GLImageDisplayModeWidthScaleAspectFit = 0, //宽度适配屏幕宽度，高度等比例缩放，适合很高的图片
    GLImageDisplayModeHeightScaleAspectFit, //高度适配屏幕高度，宽度等比例缩放，适合很长的图片
};

typedef void(^shouldLoadMoreDatasCompetionBlock)(NSArray<GLImageDisplayModel*>* datas);
@interface GLImageDisplayController : UIViewController

@property (nonatomic, strong) void(^shouldLoadMoreDatas)(shouldLoadMoreDatasCompetionBlock cometion);
@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, assign) BOOL imageCanSaveToSystemPhoto; //default YES

/**
 展示详情图时，是否需要把原图隐藏掉
 */
@property (nonatomic, assign) BOOL hiddenOriginImageViewWhenAnimated; //default NO
/**
 图片展示模式
 */
@property (nonatomic, assign) GLImageDisplayMode displayMode; //default

/**
 单击是否图片退出
 */
@property (nonatomic, assign) BOOL tapQuit; //default YES

/**
 是否需要显示导航栏
 */
@property (nonatomic, assign) BOOL showDefaultNavigationView; //default YES

/**
 覆盖图层，自定义，可添加作为评论层，需实现GLImageDisplayContentDelegate协议
 */
@property (nonatomic, strong) Class <GLImageDisplayContentDelegate> contentViewClass;

@property (nonatomic, strong) BOOL(^shouldDownloadTask)(GLImageDisplayController *controller, GLImageDisplayDownloadTask* task);


+(instancetype)showWithDatas:(NSArray <GLImageDisplayModel*>*)datas currIndex:(NSInteger)index;

+(instancetype)showWithDatas:(NSArray <GLImageDisplayModel*>*)datas currIndex:(NSInteger)index shouldLoadMoreDatas:(void(^)(shouldLoadMoreDatasCompetionBlock competion))shouldLoadMoreDatas;

/**
 显示并展示图片详情

 @param datas 数据源
 @param index 当前显示图片序号
 @param configBlock 可以配置GLImageDisplayView属性及样式
 @param shouldLoadMoreDatas 追加数据回调
 */
+(instancetype)showWithDatas:(NSArray <GLImageDisplayModel*>*)datas currIndex:(NSInteger)index config:(void(^)(GLImageDisplayController* displayView))configBlock shouldLoadMoreDatas:(void(^)(shouldLoadMoreDatasCompetionBlock competion))shouldLoadMoreDatas;
@end


NS_ASSUME_NONNULL_END
