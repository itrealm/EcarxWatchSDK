//
//  GLLogManager.h
//  GeelyConsumerCarNetwork
//
//  Created by zhiyong.kuang on 2019/5/22.
//

#import <Foundation/Foundation.h>

@class CACommonRequest;
@class CACommonResponse;

@interface GLLogManager : NSObject

/**
 单例的初始化方法

 @return 实例
 */
+ (instancetype)sharedManager;

/**
 日志开关,默认为关
 */
@property(nonatomic,assign)BOOL logEnabled;
@property(nonatomic,assign)BOOL printInfConsole; //default YES in DEBUG;NO in RELEASE

@property(nonatomic,strong)NSString* account;
@property(nonatomic,strong)NSString* logStoreName;


-(void)registerNetworkHost:(NSString*)hostStr;


-(BOOL)registerApp:(NSString*)endpoint accessKeyID:(NSString*)accessKeyID accessKeySecret:(NSString*)accessKeySecret projectName:(NSString*)projectName;


-(void)logContent:(NSDictionary*)info;
-(void)log:(NSString*)logStr;
-(void)logCARequest:(CACommonRequest*)request response:(CACommonResponse*)response;

//-(void)logRequest:(NSURLRequest*)urlRequest data:(NSData*)data response:(NSHTTPURLResponse*)response error:(NSError*)error;
@end
