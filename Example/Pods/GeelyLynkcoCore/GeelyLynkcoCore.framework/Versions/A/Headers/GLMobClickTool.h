//
//  GLMobClickTool.h
//  Geelyconsumer
//  埋点相关的
//  Created by yang.duan on 2018/9/17.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface GLMobClickTool : NSObject
/**
 埋点
 
 @param event 埋点字段
 */
+ (void)recordEvent:(NSString *)event;

/**
 埋点
 
 @param event 埋点字段
 @param attributes 附加信息
 */
+ (void)recordEvent:(NSString *)event attributes:(id)attributes;

/**
 页面统计-获取页面停留时长（begin和end，必须成对调用，否则该统计不被纳入统计范围）
 
 @param event 埋点字段
 */
+ (void)startEvent:(NSString *)event;

+ (void)endEvent:(NSString *)event;

@end

NS_ASSUME_NONNULL_END
