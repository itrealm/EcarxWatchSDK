//
//  GLNavigationBar.h
//  GLCoreKit-Example
//
//  Created by kzy on 2019/5/06.
//  Copyright © 2019年 kzy. All rights reserved.

#import <UIKit/UIKit.h>

@interface GLNavigationBar : UIView


@property (nonatomic, strong)   NSString *title;
@property (nonatomic, strong)   UIView *titleView;


@property (nonatomic, strong) UIView  *leftBarButtonCustomView;
@property (nonatomic, strong) UIView  *rightBarButtonCustomView;

@property (nonatomic, readonly) CGFloat bottom;


+ (instancetype)navigationBarWithHeight:(CGFloat)height;

- (void)gl_setBottomLineHidden:(BOOL)hidden;
- (void)gl_setBackgroundAlpha:(CGFloat)alpha;
- (void)gl_setBarButtonItemsAlpha:(CGFloat)alpha;
- (void)gl_setTintColor:(UIColor *)color;


- (void)gl_setTitleColor:(UIColor *)color;
- (void)gl_setTitleFont:(UIFont *)font;
- (void)gl_setBackgroundColor:(UIColor *)color;
- (void)gl_setBackgroundImage:(UIImage *)image;



@end













