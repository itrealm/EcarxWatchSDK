//
//  GLNavigationController.h
//  GLCoreKit-Example
//
//  Created by kzy on 2018/7/22.
//  Copyright © 2018年 kzy. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GLNavigationController : UINavigationController
+ (UINavigationController *)getCurrNavigationController;
@end



@interface UINavigationController (GLNavigation)
@property(nonatomic,assign)BOOL gl_baseNavControllerEnable;
/// The gesture recognizer that actually handles interactive pop.
@property (nonatomic, strong, readonly) UIPanGestureRecognizer *gl_fullscreenPopGestureRecognizer;

- (void)gl_setNeedsNavigationBarUpdateForBarTintColor:(UIColor *)barTintColor;
- (void)gl_setNeedsNavigationBarUpdateForTitleColor:(UIColor *)titleColor;
- (void)gl_setNeedsNavigationBarUpdateForTitleFont:(UIFont *)font;
- (void)gl_setNeedsNavigationBarUpdateForShadowImageHidden:(BOOL)hidden;
- (void)gl_setNeedsNavigationBarUpdateForHidden:(BOOL)hidden;
- (void)gl_setNeedsNavigationBarUpdateForTintColor:(UIColor *)tintColor;
- (void)gl_setNeedsNavigationBarUpdateForBarBackgroundImage:(UIImage *)backgroundImage;
- (void)gl_setNeedsNavigationBarUpdateForBarBackgroundAlpha:(CGFloat)barBackgroundAlpha;


@end
