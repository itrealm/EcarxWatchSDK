//
//  ZHNnetWrokEngnine.h
//  ZHNnetWorkIng
//
//  Created by zhn on 16/10/16.
//  Copyright © 2016年 zhn. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AFNetworking/AFNetworking.h>

typedef NS_ENUM(NSInteger,GLRequestType) {
    GLRequestTypeGET,
    GLRequestTypePOST
} __attribute__((deprecated("已弃用.")));

typedef NS_ENUM(NSInteger,GLResultType) {
    GLResultTypeJson,       //NSDictionary
    GLResultTypeData,       //NSData
    GLResultTypeString,     //NSString
};

typedef void(^GLNetSuccessBlock)(id result);
typedef void(^GLNetFailureBlock)(NSError * error,id result);
typedef void(^GLNetProgressBlock)(NSProgress *progress);
typedef void(^GLNetConstructingBodyBlock)(id <AFMultipartFormData> formData);



@interface GLBaseEngine : NSObject

/**
 基础路径key
 */
@property (strong,nonatomic) NSString *hostKey;

/**
 请求的路径（必填,和hostKey拼接成完整的路径）
 */
@property (nonatomic,copy) NSString * requestURL;

/**
 请求类型 POST GET
 */
@property (nonatomic,copy) NSString * requestType;

@property (nonatomic,copy) GLNetConstructingBodyBlock bodys;
/**
 成功的回调
 */
@property (nonatomic,copy) GLNetSuccessBlock success;

/**
 成功的回调
 */
@property (nonatomic,strong) NSMutableArray *otherSuccess;
/**
 上传的百分比的回调
 */
@property (nonatomic,copy) GLNetProgressBlock uploadProgress;
/**
 下载的百分比的回调
 */
@property (nonatomic,copy) GLNetProgressBlock downloadProgress;
/**
 失败的回调
 */
@property (nonatomic,copy) GLNetFailureBlock failure;
@property (nonatomic,strong) NSMutableArray *otherFailure;


/**
 是否需要自动出来error的回调 (可不填，默认是都需要。如果DDnetwork 配置了错误处理，这里可以控制单个请求是否需要)
 */
@property (nonatomic,assign) BOOL isNeedAutoDealError;

/** 请求结果类型 */
@property (nonatomic, assign) GLResultType resultType;


/**
 http header 字典
 */
@property (nonatomic,strong) NSMutableDictionary* httpHeaders;

/**
 请求Url中参数字典
 */
@property (nonatomic,strong) NSMutableDictionary* queryParameters;

/**
 请求body中数据
 */
@property (nonatomic,strong) NSMutableDictionary* postBodys;

/**
 支持请求参数为模板，如{@"access_token":@"{{token}}"},在GLBaseNetWork 通过registerObserverWithParamReplaceHandle 注册全局监听，当value为{{xxxxx}}类型字符串时，会调用回调，由监听者返回 xxxxx 的真实值，并进行请求
 以下参数支持替换
 httpHeaders queryParameters postBodys
 */

/**
 设置请求头

 @param value vaule
 @param header key
 */
-(void)setValue:(NSString*)value forHTTPHeaderField:(NSString*)header;

/**
 设置请求url中参数

 @param value value
 @param key key
 */
-(void)addQueryParameter:(NSString*)value key:(NSString*)key;

/**
 设置body中参数

 @param value value
 @param key key
 */
-(void)addPostBody:(NSString*)value key:(NSString*)key;


/**
 获取GLBaseEngine拼接好的url

 @return url string
 */
-(NSString*)getFullRequestUrl;


/**
 根据参数返回GET请求实例

 @param hostKey 当前host关键字，请求是会根据当前环境替换为真实地址
 @param requestUrl 请求url，如果有hostKey，会与替换后的真实地址拼接
 @param queryParams url中的请求参数
 @param success 成功回调
 @param failure 失败回调
 @return 请求实例
 */
+ (instancetype)GET_hostKey:(NSString *)hostKey
                 requestUrl:(NSString *)requestUrl
                queryParams:(NSDictionary *)queryParams
                    success:(GLNetSuccessBlock)success
                    failure:(GLNetFailureBlock)failure;

/**
 根据参数返回POST请求实例

 @param hostKey 当前host关键字，请求是会根据当前环境替换为真实地址
 @param requestUrl 请求url，如果有hostKey，会与替换后的真实地址拼接
 @param queryParams url中的请求参数
 @param postBodys body中字典
 @param success 成功回调
 @param failure 失败回调
 @return 请求实例
 */
+ (instancetype)POST_hostKey:(NSString *)hostKey
                  requestUrl:(NSString *)requestUrl
                 queryParams:(NSDictionary *)queryParams
                   postBodys:(NSDictionary *)postBodys
                     success:(GLNetSuccessBlock)success
                     failure:(GLNetFailureBlock)failure;


+ (instancetype)PUT_hostKey:(NSString *)hostKey
                     requestUrl:(NSString *)requestUrl
                    queryParams:(NSDictionary *)queryParams
                      postBodys:(NSDictionary *)postBodys
                        success:(GLNetSuccessBlock)success
                        failure:(GLNetFailureBlock)failure;

+ (instancetype)PATCH_hostKey:(NSString *)hostKey
                     requestUrl:(NSString *)requestUrl
                    queryParams:(NSDictionary *)queryParams
                      postBodys:(NSDictionary *)postBodys
                        success:(GLNetSuccessBlock)success
                        failure:(GLNetFailureBlock)failure;

+ (instancetype)DELETE_hostKey:(NSString *)hostKey
                     requestUrl:(NSString *)requestUrl
                    queryParams:(NSDictionary *)queryParams
                      postBodys:(NSDictionary *)postBodys
                        success:(GLNetSuccessBlock)success
                        failure:(GLNetFailureBlock)failure;

+ (instancetype)HEAD_hostKey:(NSString *)hostKey
                     requestUrl:(NSString *)requestUrl
                    queryParams:(NSDictionary *)queryParams
                      postBodys:(NSDictionary *)postBodys
                        success:(GLNetSuccessBlock)success
                        failure:(GLNetFailureBlock)failure;
/**
 初始化方法
 
 @param baseUrl 请求的base url
 @param requestUrl 请求的路径
 @param requestType 请求的类型
 @param requestParams 请求的参数
 @param success 请求成功的回调
 @param failure 请求失败的回调
 */
+ (instancetype)engineWithBaseUrl:(NSString *)baseUrl
                       requestUrl:(NSString *)requestUrl
                      requestType:(GLRequestType)requestType
                    requestParams:(NSDictionary *)requestParams
                          success:(GLNetSuccessBlock)success
                          failure:(GLNetFailureBlock)failure __attribute__((deprecated("已弃用.")));

+ (instancetype)engineWithHostKey:(NSString *)hostKey
                       requestUrl:(NSString *)requestUrl
                      requestType:(GLRequestType)requestType
                    requestParams:(NSDictionary *)requestParams
                          success:(GLNetSuccessBlock)success
                          failure:(GLNetFailureBlock)failure __attribute__((deprecated("已弃用.")));
@end



@interface GLNetWrokEngine : GLBaseEngine

@end



@interface GLUploadEngine : GLBaseEngine
+ (instancetype)uploadEngineWithImg:(UIImage*)uploadImage
                    compressPercent:(CGFloat)compressPercent
                            hostKey:(NSString *)hostKey
                            baseUrl:(NSString *)baseUrl
                         requestUrl:(NSString *)requestUrl
                           progress:(GLNetProgressBlock)progress
                            success:(GLNetSuccessBlock)success
                            failure:(GLNetFailureBlock)failure;
/**
 初始化方法
 

 @param baseUrl 基础路径
 @param requestUrl 请求路径
 @param progress 上传的百分比的回调
 @param success 上传成功的回调
 @param failure 上传失败的回调
 @return engine
 */
+ (instancetype)uploadEngineHostKey:(NSString *)hostKey
                            baseUrl:(NSString *)baseUrl
                         requestUrl:(NSString *)requestUrl
                   constructingBody:(GLNetConstructingBodyBlock)body
                           progress:(GLNetProgressBlock)progress
                            success:(GLNetSuccessBlock)success
                            failure:(GLNetFailureBlock)failure;


@end

