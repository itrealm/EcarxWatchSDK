//
//  GLNetworkEnvConfig.h
//  hnGovernment
//
//  Created by zhn on 2017/3/18.
//  Copyright © 2017年 zhn. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^GLNetworkEnvConfigBlock)(NSString* newEnvKey);

extern NSString* dRunEnvironmentSaveKey;

@interface GLNetworkEnvConfig : NSObject
/**
 单例初始化
 
 @return 单例对象
 */
+ (instancetype)sharedInstance;

- (void)addHosts:(NSDictionary*)hosts envTitle:(NSString*)title envKey:(NSString*)envKey;

- (void)configDefaultEnvKey:(NSString*)envKey;

- (NSString*)getCurEnvKey;
- (NSString*)getCurHost:(NSString*)hostkey;
- (NSString*)getHost:(NSString*)hostkey envKey:(NSString*)envKey;

- (NSString*)getCurUrlByHostKey:(NSString*)hostkey path:(NSString*)path;

/**
 展示切换的menu
 */
- (void)showSwitchMenu;
- (void)clearUserSelectedEnvKey;

-(void)addObserverBlock:(GLNetworkEnvConfigBlock)block forKey:(NSString*)key;
-(void)removeObserverBlockForKey:(NSString*)key;

@end
