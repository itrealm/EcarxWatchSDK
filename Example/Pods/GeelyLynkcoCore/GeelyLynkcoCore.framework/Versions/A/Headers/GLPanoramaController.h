//
//  GLPanoramaController.h
//  VRPanoramaKit
//
//  Created by yang.duan on 2018/8/7.
//  Copyright © 2018年 geely. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <CoreMotion/CoreMotion.h>

@interface GLPanoramaController : GLKViewController

@property (nonatomic, copy  ) NSString        *imageName;

@property (nonatomic, copy  ) NSString        *imageNameType;

@property (nonatomic, strong) GLKView         *panoramaView;

@property (nonatomic) BOOL orientToDevice; //是否可以自动旋转，先设置完图片再设置这个属性
///**
// 是否启用滑动手势
// */
//@property (nonatomic) BOOL touchToPan;
///**
// 是否可以放大缩小
// */
//@property (nonatomic) BOOL pinchToZoom;


/**
 启动全景图
 */
- (void)startPanoramViewMotion;
/**
 初始化全景控制器
 
 @param imageName 全景图名字
 @param type 全景图类型，默认是jpg
 */
- (void)addImageName:(NSString *)imageName imageType:(NSString *)type;

@end
