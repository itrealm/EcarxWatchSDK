//
//  GLShowPickerView.h
//  KTools
//
//  Created by 匡志勇 on 2017/9/25.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//
//                            _ooOoo_
//                           o8888888o
//                           88" . "88
//                           (| -_- |)
//                            O\ = /O
//                        ____/`---'\____
//                      .   ' \\| |// `.
//                       / \\||| : |||// \
//                     / _||||| -:- |||||- \
//                       | | \\\ - /// | |
//                     | \_| ''\---/'' | |
//                      \ .-\__ `-` ___/-. /
//                   ___`. .' /--.--\ `. . __
//                ."" '< `.___\_<|>_/___.' >'"".
//               | | : `- \`.;`\ _ /`;.`/ - ` : | |
//                 \ \ `-. \_ __\ /__ _/ .-` / /
//         ======`-.____`-.___\_____/___.-`____.-'======
//                            `=---='
//
//         .............................................
//                  佛祖镇楼                  BUG辟易

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, GLPickerEvent) {
    GLPickerEventCancel = 10,
    GLPickerEventSure,
};

typedef NS_ENUM(NSInteger, GLPickerStyle) {
    GLPickerStyleDark = 0,
    GLPickerStyleLight,
};


@class GLPickerView;

typedef void(^GLPicerConfigUI)(GLPickerView *picker);


typedef NSString*(^GLPickerStringCovertBlock)(id model);
typedef void(^GLPickerCompetionBlock)(GLPickerEvent event,id value);

//多列

typedef NSString*(^GLPickerStringMutableCovertBlock)(id model,NSInteger component);
typedef void(^GLPickerMutableCompetionBlock)(GLPickerEvent event,NSArray* values);


@interface GLPickerView : UIView
@property(nonatomic,readonly)UIView *header;
@property(nonatomic,readonly)UIView *headerLine;
@property(nonatomic,readonly)UIButton *cancleBtn;
@property(nonatomic,readonly)UIButton *sureBtn;
@property(nonatomic,readonly)UILabel *titleLabel;

@property(nonatomic,assign)GLPickerStyle style;
@property(nonatomic,strong)UIColor *cellTitleColor;
@property(nonatomic,strong)UIColor *singleLineColor;

@property(class,nonatomic,assign)GLPickerStyle defaultStyle;

#pragma mark- data
//单列数据


/**
 单列数据选择器

 @param datas 数据数组
 @param convert 数据转换器，如果数组item不是NSString，需调用者转换，返回需要显示的字符，如item为一个model，需返回model.name
 @param competion 选择结果调用，需先判断event是取消事件还是点击确定事件，value为数组item
 */
+ (void)showDatas:(NSArray*)datas configUI:(GLPicerConfigUI)configUI convert:(GLPickerStringCovertBlock)convert competion:(GLPickerCompetionBlock)competion;

//多列数据


/**
 多列数据选择器

 @param datas 数据数组
 @param component 需要显示的列数
 @param keys 返回子列数组的key值
 @param convert 数据转换器，如果数组item不是NSString，需调用者转换，返回需要显示的字符，如item为一个model，需返回model.name
 @param competion 选择结果调用，需先判断event是取消事件还是点击确定事件，value为数组
 */
+ (void)showDatas:(NSArray*)datas component:(NSInteger)component subArrayKey:(NSArray*)keys configUI:(GLPicerConfigUI)configUI  convert:(GLPickerStringMutableCovertBlock)convert competion:(GLPickerMutableCompetionBlock)competion;

/**
 多列数据选择器

 @param datas 数据数组
 @param component 需要显示的列数
 @param weights 每列显示宽度的权重数组，如3列，宽度为1:2:3则传值@[@1,@2,@3]
 @param keys 返回子列数组的key值
 @param convert 数据转换器，如果数组item不是NSString，需调用者转换，返回需要显示的字符，如item为一个model，需返回model.name
 @param competion 选择结果调用，需先判断event是取消事件还是点击确定事件，value为数组
 */
+ (void)showDatas:(NSArray*)datas component:(NSInteger)component weights:(NSArray*)weights subArrayKey:(NSArray*)keys configUI:(GLPicerConfigUI)configUI  convert:(GLPickerStringMutableCovertBlock)convert competion:(GLPickerMutableCompetionBlock)competion;
#pragma mark- noLikeageData

/**
 多列数据选择器
 
 @param datas 数据数组 @[@[...],@[...],@[...]]
 @param component 需要显示的列数
 @param weights 每列显示宽度的权重数组，如3列，宽度为1:2:3则传值@[@1,@2,@3]
 @param convert 数据转换器，如果数组item不是NSString，需调用者转换，返回需要显示的字符，如item为一个model，需返回model.name
 @param competion 选择结果调用，需先判断event是取消事件还是点击确定事件，value为数组
 */
+ (void)showNoLinkageDatas:(NSArray*)datas component:(NSInteger)component weights:(NSArray*)weights configUI:(GLPicerConfigUI)configUI convert:(GLPickerStringMutableCovertBlock)convert competion:(GLPickerMutableCompetionBlock)competion;

@end




typedef NS_ENUM(NSInteger, GLDatePickerMode) {
    GLDatePickerModeYear, //年
    GLDatePickerModeYearAndMonth, //年 月
    GLDatePickerModeDate, //年 月 日
    GLDatePickerModeDateHourMinute, //年 月 日 时 分
    GLDatePickerModeDateHourMinuteSecond, //年 月 日 时 分 秒
    GLDatePickerModeTime, //时 分
    GLDatePickerModeTimeAndSecond, //时 分 秒
    GLDatePickerModeMaxNumber,
};

typedef NS_ENUM(NSInteger, GLDatePickerTime) {
    GLDatePickerTimeYear = NSCalendarUnitYear,
    GLDatePickerTimeMonth = NSCalendarUnitMonth,
    GLDatePickerTimeDay = NSCalendarUnitDay,
    GLDatePickerTimeHour = NSCalendarUnitHour,
    GLDatePickerTimeMinute = NSCalendarUnitMinute,
    GLDatePickerTimeSecond = NSCalendarUnitSecond,
};

typedef void(^GLDatePickerCompetionBlock)(GLPickerEvent event,NSDateComponents* value);


@interface GLDatePickerView : GLPickerView
+ (NSDate *)setYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second ;
+ (NSDate *)setYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day;
+ (NSDateComponents *)dateComponentsForDate:(NSDate*)date;
/**
 日期选择器
 
 @param mode 同UIDatePicker
 @param competion 选择结果调用，需先判断event是取消事件还是点击确定事件，value为NSDate
 */
+ (void)showDate:(GLDatePickerMode)mode configUI:(GLPicerConfigUI)configUI competion:(GLDatePickerCompetionBlock)competion;
+ (void)showDate:(GLDatePickerMode)mode minDate:(NSDate*)minDate maxDate:(NSDate*)maxDate selectdDate:(NSDate*)date configUI:(GLPicerConfigUI)configUI competion:(GLDatePickerCompetionBlock)competion;


@end
