//
//  GLPopMenuView.h
//  KTools
//
//  Created by zhiyong.kuang on 2018/8/22.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLMenuModel : NSObject

@property (nonatomic,copy) NSString *imageName;
@property (nonatomic,strong) UIImage *image;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,assign)NSInteger tag;
+ (instancetype)updateWithDict:(NSDictionary *)dict;
@end

@interface GLPopMenuViewCell : UITableViewCell
@property (nonatomic,strong) GLMenuModel * menuModel;
@end

typedef void(^ItemsClickBlock)(GLMenuModel *m, NSInteger index);


@interface GLPopMenuView : UIView

+ (void)showMenuWithSize:(CGSize)size displayPoint:(CGPoint)point dataArray:(NSArray *)dataArray itemsClickBlock:(ItemsClickBlock)itemsClickBlock;
+ (void)showMenuInView:(UIView*)view dataArray:(NSArray *)dataArray itemsClickBlock:(ItemsClickBlock)itemsClickBlock;
+ (void)hidden;

@end
