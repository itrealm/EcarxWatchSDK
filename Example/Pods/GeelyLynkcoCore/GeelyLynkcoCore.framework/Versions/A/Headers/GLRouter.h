//
//  GLRouter
//
//  Created by kzy on 2018/7/17.
//  Copyright © 2018年 kzy All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const GLRouterSuccessBlockKey;
extern NSString* const GLRouterFailedBlockKey;
extern NSString* const GLRouterBlockKey;

#define Router [[GLRouter alloc]init]

@interface GLRouter : NSObject

#pragma mark- register
/** 路由注册
 Example：
     Router.registerHost(CarServiceTargetHost,@"CarServiceTarget").
            registerPath(@"ysViewController"                        ,@"ysViewController:").
            registerPath(@"myAppointmentVC"                         ,@"myAppointmentVC:").
            registerPath(@"mySettlementVC"                          ,@"mySettlementVC:").;
 */

-(GLRouter *(^)(NSString* host,NSString* target))registerHost;
-(GLRouter *(^)(NSString* path,NSString*selector))registerPath;
-(GLRouter *(^)(NSString* host,NSString* path,NSString* selector))registerPathForHost;


#pragma mark- openUrl
/** 路由调用
 Example:
     id returunValue = Router.openUrl(@"lynkco://login/login")
                             .appendKeyValue(@"username",@"xxx")
                             .appendKeyValue(@"pwd",@"xxx")
                             .appendSuccessBlock(^(id result){
                                //success todo
                             })
                             .appendFailedBlock(^(NSError* error){
                                //failed todo
                             })
                             .run();
    //returunValue do something
 */


/**
 传入路由url
 */
-(GLRouter *(^)(NSString*))openUrl;
-(GLRouter *(^)(NSString*))openUrlWithParams;
/**
 传入参数字典
 */
-(GLRouter *(^)(NSDictionary*))appendInfo;
/**
 传入参数 key-value
 */
-(GLRouter *(^)(NSString* key,id value))appendKeyValue;
/**
 传入参数 成功block
 key 固定为GLRouterSuccessBlockKey
 */
-(GLRouter *(^)(void(^)(id result)))appendSuccessBlock;
/**
 传入参数 失败block
 key 固定为GLRouterFailedBlockKey
 */
-(GLRouter *(^)(void(^)(NSError* error)))appendFailedBlock;


/**
 执行调用路由
 */
-(id (^)(void))run;


@end



