//
//  GLScanLayer.h
//  Geelyconsumer
//
//  Created by 朱伟特 on 2018/10/9.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface GLScanLayer : CAShapeLayer

@property (nonatomic, assign) CGRect scanRect;

@end
