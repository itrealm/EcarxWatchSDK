//
//  ScanView.h
//  PersonalManager
//
//  Created by user on 16/8/16.
//  Copyright © 2016年 Haiyun.Qian. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! 扫描成功发送通知（在代理实现的情况下不发送）*/
extern NSString * const LXDSuccessScanQRCodeNotification;
/*! 通知传递数据中存储二维码信息的关键字*/
extern NSString * const LXDScanQRCodeMessageKey;

typedef void(^WSLFlashSwitchBlock)(BOOL open);

@class GLScanView;
@protocol GLScanViewDelegate <NSObject>

- (void)scanView:(GLScanView *)scanView codeInfo:(NSDictionary *)codeInfo;

@end

@interface GLScanView : UIView
/**
 打开/关闭闪光灯的回调
 */
@property (nonatomic,copy) WSLFlashSwitchBlock flashSwitchBlock;
/*! 扫描回调代理人*/
@property (nonatomic, weak) id<GLScanViewDelegate> delegate;

/*! 创建扫描视图，建议使用LXDScanCodeController*/
+ (instancetype)scanViewShowInController: (UIViewController *)controller;

@property (nonatomic, strong) UILabel * remind;

@property (nonatomic, assign) BOOL isScanning;

/*! 开始扫描*/
- (void)start;
/*! 结束扫描*/
- (void)stop;

@end
