//
//  GLScanViewController.h
//  PersonalManager
//
//  Created by user on 16/8/15.
//  Copyright © 2016年 Haiyun.Qian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLBaseViewController.h"

typedef void(^ScanVCResultBlock)(NSDictionary *resultDic);

@interface GLScanViewController : GLBaseViewController
@property (nonatomic,class, readonly,) NSMutableArray * handles;

/** 扫描完毕之后回调的block */
@property (nonatomic,copy) ScanVCResultBlock scanVCResultBlock;

+(void)addHandler:(BOOL (^)(GLScanViewController* viewController,NSString*result))block priority:(NSInteger)priority;
@end
