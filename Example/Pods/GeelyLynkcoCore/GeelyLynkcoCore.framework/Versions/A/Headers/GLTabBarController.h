//
//  GLTabBarController.h
//  CodeDemo
//
//  Created by kzy on 2018/7/22.
//  Copyright © 2018年 kzy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLBadgeLabel.h"

@interface GLTabPlusButton : UIButton
@end


@interface GLTabBar : UITabBar
@end



@interface GLTabBarController : UITabBarController

- (void)addViewController:(UIViewController *)vc withTitle:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName;
- (void)addViewController:(UIViewController *)vc withTitle:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName badgeItem:(GLBadgeItem*)badgeItem;


- (void)setPlusButtonWithTitle:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage clickedBlock:(void(^)(void))block;
- (void)setPlusButtonWithTitle:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage viewController:(UIViewController*)vc;

@end
