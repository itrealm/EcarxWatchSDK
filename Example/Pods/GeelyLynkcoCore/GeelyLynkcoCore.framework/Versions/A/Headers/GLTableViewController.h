//
//  GLTableViewController.h
//  GLCoreKit-Example
//
//  Created by 匡志勇 on 2018/7/22.
//  Copyright © 2018年 匡志勇. All rights reserved.
//

#import "GLBaseViewController.h"

@interface GLTableViewController : GLBaseViewController



/** tableview属性,公开是为了方便以后继承的修改 */
@property(nonatomic,strong) UITableView *tableView;


/** 列表视图是否可以点击 */
@property (nonatomic, getter=isTableViewCanClick) BOOL tableViewCanClick;



- (instancetype)initWithStyle:(UITableViewStyle)style;



- (void)setRefreshEnable:(BOOL)enable;
//上拉加载更多
- (void)setPullUpRefreshEnable:(BOOL)enable;
//下拉刷新
- (void)setPullDownRefreshEnable:(BOOL)enable;
/** MJRefresh 上拉加载 */
- (void)loadMoreDataSource;
/** MJRefresh 下拉刷新 */
- (void)refreshDataSource;



/**
 显示视图数据为空的提醒视图
 */
- (void)showEmptyViewWithTitle:(NSString *)tipText;

/**
 隐藏为空提醒视图
 */
- (void)hiddenEmptyView;


@end
