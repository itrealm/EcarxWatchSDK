//
//  GLTextViewViewController.h
//  Geelyconsumer
//
//  Created by 朱伟特 on 2018/10/6.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import "GLBaseViewController.h"

@interface GLTextViewViewController : GLBaseViewController

@property (nonatomic, copy) NSString * text;

@end
