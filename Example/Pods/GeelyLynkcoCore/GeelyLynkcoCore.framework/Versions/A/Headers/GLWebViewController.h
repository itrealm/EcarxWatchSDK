//
//  GLWebViewController.h
//  GeelyConsumer
//
//  Created by dev001 on 2017/8/21.
//  Copyright © 2017年 Rdic. All rights reserved.
//

#import "GLBaseViewController.h"
#import <WebKit/WebKit.h>

typedef NS_ENUM(NSInteger, GLWebViewProgressStyle) {
    GLWebViewProgressStyleNone = 0,
    GLWebViewProgressStyleCar = 1 << 0,
    GLWebViewProgressStyleLine = 1 << 1,
};

@interface GLWebViewController : GLBaseViewController

//  加载的url
@property (nonatomic,copy) NSString *urlString;
@property (nonatomic,copy) NSString *allowingReadAccessToURL;


@property (nonatomic,strong)UIColor* progessColor;
/**
 progessStyle 加载进度样式，当progessStyle未设置GLWebViewProgressStyleLine时，progessColor不生效
 */
@property (nonatomic,assign)GLWebViewProgressStyle progessStyle;

@property (nonatomic,assign)BOOL debugEnable;
@property (nonatomic,assign)BOOL fullScreen;
@property (nonatomic,assign)CGFloat navigationBarAlpha; //default 1
@property (nonatomic,assign)BOOL hiddenNavigationBar; //default NO
@property (nonatomic,readonly)WKWebView* webView;

@property (nonatomic,strong,class) UIColor* defaultBackgroundColor;

@property (nonatomic,strong) void(^configUIWhenLoad)(GLWebViewController* webCtl);
@property (nonatomic,strong,class) void(^defaultConfigUIWhenLoad)(GLWebViewController* webCtl);

/**
 自定义标题，如果不设置话，将读取html document.title作为标题
 */
@property (nonatomic,copy)NSString* customTitle;

/**
 隐藏导航栏，仅限viewDidLoad后使用
 */
- (void)hiddenNavigation:(BOOL)hidden;

- (void)evaluateJavaScriptCallbackById:(NSString*)callBackId params:(NSDictionary*)params;
- (void)evaluateJavaScript:(NSString*)jsStr;
- (void)reloadUrl;

@end
