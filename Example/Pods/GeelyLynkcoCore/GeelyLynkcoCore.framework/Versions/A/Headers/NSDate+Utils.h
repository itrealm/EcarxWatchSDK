//
//  NSDate+Utils.h
//  Pods
//
//  Created by yang.duan on 2019/12/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/*
 yy: 年的后2位
 yyyy: 完整年
 MM: 月，显示为1-12
 MMM: 月，显示为英文月份简写,如 Jan
 MMMM: 月，显示为英文月份全称，如 Janualy
 dd: 日，2位数表示，如02
 d: 日，1-2位显示，如 2
 EEE: 简写星期几，如Sun
 EEEE: 全写星期几，如Sunday
 aa: 上下午，AM/PM
 H: 时，24小时制，0-23
 K：时，12小时制，0-11
 m: 分，1-2位
 mm: 分，2位
 s: 秒，1-2位
 ss: 秒，2位
 S: 毫秒
 */

/**
 *  时间NSDate扩展类别
 */
@interface NSDate (Utils)

#pragma mark - Format Date

/**
 *  获取当前是上午还是下午还是晚上
 *
 *  @return 返回  上午 ，下午， 晚上
 */
+ (NSString *)getDayStatus;

/**
 *  获取当前是星期几，用英文简写
 *
 *  @return 星期几
 */
+ (NSString *)currentWeekdayStringInEnglish;

/**
 *  返回当前月份和日期，月份是英文简写，日期是数字
 *
 *  @return 返回当前月份和日期，月份是英文简写，日期是数字
 */
+ (NSString *)currentMonthAndDayStringInEnglish;

/**
 *  当前时间字符串，年、月、日
 *
 *  @return 时间字符串
 */
+ (NSString *)currentTimeString;

/**
 *  当前时间字符串，除了年、月和日，还包含小时、分钟和秒
 *
 *  @return 时间字符串
 */
+ (NSString *)currentFullTimeString;

/**
 *  当前时间字符串，只包含小时、分钟和秒
 *
 *  @return 时间字符串
 */
+ (NSString *)currentDetailTimeString;

/**
 *  通过一定格式的字符串转换为NSDate时间对象
 *
 *  @param dateString          时间字符串
 *  @param dateFormatterString 时间格式字符串
 *
 *  @return 时间对象
 */
+ (NSDate *)dateWithString:(NSString *)dateString formatString:(NSString *)dateFormatterString;

/**
 *  通过'yyyy-MM-dd HH:mm:ss' 格式的字符串获取NSDate时间对象
 *
 *  @param str 时间字符串
 *
 *  @return 时间对象
 */
+ (NSDate *)dateWithDateString:(NSString *)str;

/**
 *  通过'yyyy-MM-dd HH:mm:ss' 格式的字符串获取NSDate时间对象
 *
 *  @param str 时间字符串
 *
 *  @return 时间对象
 */
+ (NSDate *)dateWithDateTimeString:(NSString *)str;

/**
 *  将时间戳修改为时间对象
 *
 *  @param timestamp 时间戳
 *
 *  @return 时间对象 yyyy-MM-dd HH:mm:ss
 */
+ (NSString *)dateFormTimestampString:(NSString *)timestamp;

/**
 毫秒字符串转换为 yyyy-MM-dd HH:mm:ss 格式
 
 @param timestamp 毫秒时间戳
 @return yyyy-MM-dd HH:mm:ss
 */
+ (NSString *)dateFormTimestampDivisionThousandString:(NSString *)timestamp;
/**
 毫秒字符串转换为 yyyy-MM-dd HH:mm 格式
 
 @param timestamp 毫秒时间戳
 @return yyyy-MM-dd HH:mm:ss
 */
+ (NSString *)changtimestampToTimeString:(CGFloat)timestamp;
/**
*  将时间戳修改为时间对象
*
*  @param timestamp 时间戳
*
*  @return 时间对象 yyyy年MM月dd日
*/
+ (NSString *)changtimestampToYearMonthDay:(CGFloat)timestamp;
/**
 *  将时间戳修改为时间对象
 *
 *  @param timestamp 时间戳
 *
 *  @return 时间对象 yyyy-MM
 */
+ (NSString *)dateForTimestampString:(NSString *)timestamp;

/**
 *  将时间戳修改为时间对象
 *
 *  @param timestamp 时间戳
 *
 *  @return 时间对象 HH:mm:ss
 */
+ (NSString *)dateHMSForTimestampString:(NSString *)timestamp;

/**
 *  将秒数转换为天、小时、分和秒；例如111230转换为3天5小时42分12秒
 *
 *  @param count 秒
 *
 *  @return 描述文字
 */
+ (NSString *)dateFormmterFormSecond:(int)count;

/**
 *  将时间修改为刚刚、2分钟前、2小时前和2天前描述的字符串
 *
 *  @return 结果字符串
 */
- (NSString *)formattedExactRelativeDate;

/**
 *  未来的多少天
 *
 *  @param pageDays 天数
 *
 *  @return 时间描述字符串
 */
+ (NSString *)addDateString:(int)pageDays;

/**
 *  返回英文格式的年月日
 *
 *  @return 返回英文格式的年月日字符串
 */
- (NSString *)englishFormatString;

/**
 *  返回英文格式的年月日小时和分钟
 *
 *  @return 返回英文格式的年月日小时和分钟字符串
 */
- (NSString *)englishFormatWithHourandrMinuteString;

/**
 *  自1970的时间搓字符串
 *
 *  @return 自1970的时间搓字符串
 */
+ (NSString *)timeStampStringSince1970;

/**
 *  当前时间字符串，除了年、月和日，还包含小时、分钟、上午下午
 *
 *  @return 时间字符串
 */
- (NSString *)currentFullTimeStringWithAA;

/**
 *  当前时间字符串，除了年、月和日，星期几
 *
 *  @return 时间字符串
 */
- (NSString *)currentTimeStringWithEEEE;
/**
 iOS比较日期大小默认会比较到秒
 **/
+ (int)compareOneDay:(NSDate *)oneDay withAnotherDay:(NSDate *)anotherDay;
+ (NSDate *)changeDateFormateWithDate:(NSDate *)oneDate;
+ (NSString *)currentHourAndMinuteWithDate:(NSDate *)date;
+ (NSDate *)changeHourAndMinuteToZeroWithDate:(NSDate *)date;

/**
 返回一个2017-02-12类型的字符串
 */
+ (NSString *)currentYearAndMonthWithDate:(NSDate *)date;

/**
 返回一个2017-02类型的字符串
 */
+ (NSString *)getYearAndMonthWithDate:(NSDate *)date;

@end


NS_ASSUME_NONNULL_END
