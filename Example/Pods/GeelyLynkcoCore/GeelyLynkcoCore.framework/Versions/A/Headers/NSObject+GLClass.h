//
//  UIColor+Hex.h
//  TaoTuMall
//
//  Created by ZhouX on 16/5/4.
//  Copyright © 2016年 HYcompany. All rights reserved.
//

#import <UIKit/UIKit.h>

#define GLArchiveImplementation \
- (instancetype)initWithCoder:(NSCoder *)aDecoder {\
if (self = [super init]) {\
[self gl_decode:aDecoder];\
}\
return self;\
}\
\
- (void)encodeWithCoder:(NSCoder *)aCoder {\
[self gl_encode:aCoder];\
}\
- (id)copyWithZone:(nullable NSZone *)zone{\
    return [self gl_copyWithZone:zone];\
}


@interface NSObject (GLClass)

- (BOOL)isKindOfClassString:(NSString*)aClassStr;
- (BOOL)isKindOfClassInStringArray:(NSArray*)classStrs;

- (BOOL)isMemberOfClassString:(NSString*)aClassStr;
- (BOOL)isMemberOfClassInStringArray:(NSArray*)classStrs;
@end



@interface NSObject (GLArchive)

- (void)gl_encode:(NSCoder *)aCoder;
- (void)gl_decode:(NSCoder *)aDecoder;
- (id)gl_copyWithZone:(nullable NSZone *)zone;
@end
