//
//  NSObject+GLKVO.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2018/9/11.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>


#define glkeypath(OBJ, PATH) \
(((void)(NO && ((void)OBJ.PATH, NO)), # PATH))

#define glckeypath(CLASS, PATH) \
(((void)(NO && ((void)[CLASS new].PATH, NO)), # PATH))

#define _GLObserve(TARGET, KEYPATH) \
({ \
__weak id target_ = (TARGET); \
[self gl_bindValueForTarget:target_ forKeyPath:@glkeypath(TARGET,KEYPATH)]; \
})

#if __clang__ && (__clang_major__ >= 8)
#define GLObserve(TARGET, KEYPATH) _GLObserve(TARGET, KEYPATH)
#else
#define GLObserve(TARGET, KEYPATH) \
({ \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Wreceiver-is-weak\"") \
_GLObserve(TARGET, KEYPATH) \
_Pragma("clang diagnostic pop") \
})
#endif


typedef void (^GLKVOCallBack)(id oldValue,id newValue);

#define gltargetpath(OBJ, PATH) \
(((void)(NO && ((void)OBJ.PATH, NO)), [GLKVOTargetPath target:OBJ keyPath:@(# PATH)]))

@interface GLKVOTargetPath : NSObject
@property(nonatomic,readonly)id target;
@property(nonatomic,readonly)NSString* keyPath;

+(GLKVOTargetPath*)target:(id)target keyPath:(NSString*)keyPath;
@end

@interface GLKVOObject : NSObject
-(void)action:(GLKVOCallBack)callBack;
-(void)bindSelfKeyPath:(NSString*)keyPath;
@end


@interface NSObject (GLKVOHelper)
-(GLKVOObject*)gl_bindValueForObject:(GLKVOTargetPath*)obj;
-(void)gl_bindValueForObject:(GLKVOTargetPath*)obj callback:(GLKVOCallBack)callback;

-(GLKVOObject*)gl_bindValueForTarget:(NSObject*)target forKeyPath:(NSString *)keyPath;
-(void)gl_bindValueForTarget:(NSObject*)target forKeyPath:(NSString *)keyPath callback:(GLKVOCallBack)callback;
-(void)gl_unBindObserver;

@end

