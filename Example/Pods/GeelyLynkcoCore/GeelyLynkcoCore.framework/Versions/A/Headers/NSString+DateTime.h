//
//  NSString+DateTime.h
//  Geelyconsumer
//
//  Created by yang.duan on 2018/10/6.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DateTime)

/*
 根据NSDate获取时间戳
 */
+ (NSString *)featchTimeStampFromDate:(NSDate *)date;

/*
 将NSDate修改为日期字符串
 */
+ (NSString *)changeTimestampToString:(NSInteger)timestamp;

+ (NSString *)getTimeStampFromDate:(NSDate*)date;

+ (NSString *)time_timestampToString:(NSInteger)timestamp;

/**
 *  NSDate转NSString
 */
+ (NSString *)stringFormDate:(NSDate *)date formatter:(NSString *) formatterString;
/**
 *  NSString转NSDate
 */
+ (NSDate *)dateFormString:(NSString *)str formatterString:(NSString *)formatterString;
/**
 *  NSTimeInterval转NSString
 */
+ (NSString *)stringFormTimeInterval:(NSTimeInterval)timeInterval formatter:(NSString *) formatterString;
+ (NSString *)oneWeekAgoDate;
//比较两个日期大小
+ (NSInteger)compareDate:(NSString*)startDate withDate:(NSString*)endDate;

+ (NSString *)getNowTimeTimestamp;
@end
