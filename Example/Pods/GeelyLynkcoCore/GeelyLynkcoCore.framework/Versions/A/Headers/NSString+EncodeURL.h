//
//  NSString+EncodeURL.h
//  GeelyConsumer
//
//  Created by dev001 on 2017/7/19.
//  Copyright © 2017年 Rdic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EncodeURL)

+ (NSString *)encodeToPercentEscapeString:(NSString *) input;

+ (NSString *)formatURL:(NSString *)urlString;

@end
