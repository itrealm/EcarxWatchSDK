//
//  NSString+SHA1.h
//  Gassistant
//
//  Created by zhiyong.kuang on 2017/10/6.
//  Copyright © 2017年 121. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encryption)

#pragma mark - BASE64
- (NSString*)encodeBase64;
- (NSString*)decodeBase64;
#pragma mark - SHA1
- (NSString *)md5;
#pragma mark - SHA1
- (NSString*)SHA1;
- (NSString*) SHA256;
#pragma mark - AES加密


@end
