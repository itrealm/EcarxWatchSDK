//
//  NSString+SizeFont.h
//  QiuYouQuan
//
//  Created by 123 on 15/6/4.
//  Copyright (c) 2015年 QYQ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SizeFont)
/**
 *  返回字符串所占用的尺寸
 *
 *  @param font    字体
 *  @param maxSize 最大尺寸
 */
- (CGSize)sizeWithFont:(UIFont *)font maxSize:(CGSize)maxSize;


- (CGSize)sizeWithFont:(UIFont *)font andLineSpacing:(CGFloat)spacing maxSize:(CGSize)maxSize;

+ (CGFloat)labelHeightForOneLineWithFont:(UIFont *)font;//获取一行label的高度

+ (CGFloat)getWidthWithString:(NSString *)content font:(UIFont *)font;//获取一串字符串的宽度

//限制最大字符长度
+ (void)restrictionInputTextField:(UITextField *)inputClass maxNumber:(NSInteger)maxNumber showErrorCallback:(void(^)(void))errorCallback;
+ (void)restrictionInputTextView:(UITextView *)inputClass maxNumber:(NSInteger)maxNumber showErrorCallback:(void(^)(void))errorCallback;
@end
