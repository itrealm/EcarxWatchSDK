//
//  NSString+Verify.h
//  AFNetworking
//
//  Created by 薛立恒 on 2018/8/7.
//

#import <Foundation/Foundation.h>

@interface NSString (Verify)

/** 验证字符串是否是URL */
- (BOOL)isValidURLString;

- (BOOL)isChinese;//判断是否是纯汉字

- (BOOL)includeChinese;//判断是否含有汉字


@end
