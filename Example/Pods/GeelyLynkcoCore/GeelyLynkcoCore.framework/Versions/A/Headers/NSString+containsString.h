//
//  NSString+containsString.h
//  HZBus
//
//  Created by Rdic on 16/11/8.
//  Copyright © 2016年 user. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (containsString)

- (BOOL) containsString:(NSString *)str;

+ (BOOL) isEmpty: (NSString *)str;

@end
