//
//  RegularRestrictions.h
//  51life
//
//  Created by yixiaoshan on 16/1/30.
//  Copyright © 2016年 HYcompany. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegularRestrictions : NSObject

/**
 判断是否为价格（不允许0开头小数点后最多两位）

 @param strPrice 判断内容
 @return 判断结果
 */
+ (BOOL)IsPriceNotBeginningZero:(NSString *)strPrice;

/**
 判断是否为正整数

 @param strWholeNumber 判断内容
 @return 判断结果
 */
+ (BOOL)IsWholeNumber:(NSString *)strWholeNumber;

/**
 判断是否为价格（允许0开头小数点后最多两位）

 @param strPrice 判断内容
 @return 判断结果
 */
+ (BOOL)IsPrice:(NSString *)strPrice;

/**
 判断是否为邮箱格式

 @param strEmail 判断内容
 @return 判断结果
 */
+ (BOOL)IsEmail:(NSString *)strEmail;

/**
 判断是否为手机号码

 @param strPhoneNumber 判断内容
 @return 判断结果
 */
+ (BOOL)IsPhoneNumber:(NSString *)strPhoneNumber;

/**
 判断是否为固定电话

 @param strLandlinePhone 判断内容
 @return 判断结果
 */
+ (BOOL)IsLandlinePhone:(NSString *)strLandlinePhone;

/**
 判断是否为身份证

 @param strIDCard 判断内容
 @return 判断结果
 */
+ (BOOL)IsIDCard:(NSString *)strIDCard;

/**
 判断是否为数字

 @param strNum 判断内容
 @return 判断结果
 */
+ (BOOL)IsNum:(NSString *)strNum;

/**
 判断是否为浮点型数据

 @param strFloat 判断内容
 @return 判断结果
 */
+ (BOOL)IsFloat:(NSString *)strFloat;

/**
 判断该文本是否是有效密码

 @param text 密码
 @return 结果
 */
+ (BOOL)isValidPassword:(NSString *)text;
/**
 判断用户昵称是否合法
 
 @param text 昵称
 @return 结果
 */
+ (BOOL)isValidNickName:(NSString *)text;
+ (BOOL)isValidGeelyName:(NSString *)text;
/**
 判断姓名是否合法
 
 @param text 姓名
 @return 结果
 */
+ (BOOL)isValidName:(NSString *)text;

/**
 判断身份证是否合法

 @param text 身份证号码
 @return 结果
 */
+ (BOOL)isdentityCard:(NSString *)text;

/**
 判断税务号是否合法

 @param text 税务号
 @return 结果
 */
+ (BOOL)isVaildTaxNumber:(NSString *)text;

/**
 格式化手机号码

 @param phone 原手机号码
 @return 格式化完成之后的手机号码
 */
+ (NSString *)formatPhoneNumber:(NSString *)phone;

/**
 判断纳税识别号
 
 @param text 文本
 @return 结果
 */
+ (BOOL)isVaildReceiptNUmber:(NSString *)text;

/**
 判断车牌号
 
 @param text 文本
 @return 结果
 */
+ (BOOL)isValidCarNumber:(NSString *)text;

@end
