//
//  UIColor+GLHex.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2018/9/11.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (GLHex)

+ (UIColor*)gl_colorWithRGB:(NSUInteger)hex alpha:(CGFloat)alpha;

+ (UIColor *)gl_colorWithHexStr:(NSString *)color;

//从十六进制字符串获取颜色，
//color:支持@“#123456”、 @“0X123456”、 @“123456”三种格式
+ (UIColor *)gl_colorWithHexStr:(NSString *)color alpha:(CGFloat)alpha;

@end
