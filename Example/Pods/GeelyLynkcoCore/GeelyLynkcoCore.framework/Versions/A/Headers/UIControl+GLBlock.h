//
//  UIControl+GLBlock.h
//  KTools
//
//  Created by zhiyong.kuang on 2018/08/12.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^GLActionBlock)(id sender);

@interface UIControl (GLBlock)
- (void)gl_addAction:(GLActionBlock)block;
@end



@interface UIButton (GLBlock)
- (void)gl_addAction:(GLActionBlock)block;
@end



@interface UIBarButtonItem (GLBlock)
- (instancetype)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style andBlock:(GLActionBlock)block;
- (instancetype)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style andBlock:(GLActionBlock)block;
@end
