//
//  UIFont+PingFangSC.h
//  GeelyNewEnergyHomeExample
//
//  Created by zhiyong.kuang on 2019/3/4.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (PingFangSC)

//苹方-简 常规体
//font-family: PingFangSC-Regular, sans-serif;
+ (UIFont *)pf_regularFontOfSize:(CGFloat)fontSize;
//苹方-简 极细体
//font-family: PingFangSC-Ultralight, sans-serif;
+ (UIFont *)pf_ultralightFontOfSize:(CGFloat)fontSize;
//苹方-简 细体
//font-family: PingFangSC-Light, sans-serif;
+ (UIFont *)pf_lightFontOfSize:(CGFloat)fontSize;
//苹方-简 纤细体
//font-family: PingFangSC-Thin, sans-serif
+ (UIFont *)pf_thinFontOfSize:(CGFloat)fontSize;
//苹方-简 中黑体
//font-family: PingFangSC-Medium, sans-serif;
+ (UIFont *)pf_mediumFontOfSize:(CGFloat)fontSize;
//苹方-简 中粗体
//font-family: PingFangSC-Semibold, sans-serif;
+ (UIFont *)pf_semiboldFontOfSize:(CGFloat)fontSize;

//苹方-简 粗体
//font-family: PingFang-SC-Bold, sans-serif;
+ (UIFont *)pf_boldFontOfSize:(CGFloat)fontSize;
@end
