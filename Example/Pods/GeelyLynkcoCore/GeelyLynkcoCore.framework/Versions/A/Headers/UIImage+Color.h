//
//  UIImage+Color.h
//  Geelyconsumer
//
//  Created by wx-5102 on 2017/11/9.
//  Copyright © 2017年 Rdic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

+ (UIImage *)imageWithLinearGradientStartColor:(UIColor *)startColor endColor:(UIColor *)endColor size:(CGSize)size vertical:(BOOL)isVertical;
+ (UIImage *)imageWithColor:(UIColor *)color rect:(CGRect)rect cornerRadius:(CGFloat)cornerRadius;
+ (UIImage *)imageWithColor:(UIColor *)color rect:(CGRect)rect;
+ (UIImage *)imageWithColor:(UIColor *)color;

-(UIImage *)updateImageWithTintColor:(UIColor*)color alpha:(CGFloat)alpha rect:(CGRect)rect;

/** 图片置灰*/
-(UIImage *)grayImage;
@end
