//
//  UIImage+Cut.h
//  Geelyconsumer
//
//  Created by wx-5102 on 2017/11/9.
//  Copyright © 2017年 Rdic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Cut)
//裁剪图片
+ (UIImage *)handleImage:(UIImage *)originalImage withSize:(CGSize)size;

+ (UIImage *)trimImageWithImage:(UIImage *)image;

/**
 *  压缩图片到指定尺寸大小
 *
 *  @param image 原始图片
 *  @param size  目标大小
 *
 *  @return 生成图片
 */
+ (UIImage *)compressOriginalImage:(UIImage *)image toSize:(CGSize)size;

/**
 *  压缩图片到指定文件大小
 *
 *  @param image 目标图片
 *  @param size  目标大小（最大值）
 *
 *  @return 返回的压缩后的图片
 */
+ (UIImage *)compressOriginalImage:(UIImage *)image toMaxDataSizeKBytes:(CGFloat)size;


/** 等比压缩图片到指定大小 */
+ (UIImage *)compressPropertyImageForSize:(UIImage *)sourceImage targetSize:(CGSize)size;


@end
