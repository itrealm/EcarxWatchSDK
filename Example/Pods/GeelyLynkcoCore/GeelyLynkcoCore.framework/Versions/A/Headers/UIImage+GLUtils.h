//
//  UIImage+GLUtils.h
//  Geelyconsumer
//
//  Created by yang.duan on 2018/11/5.
//  Copyright © 2018年 Rdic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (GLUtils)
/**
 @brief 通过指定颜色生成一张图片

 @param color 颜色
 @return 创建的图片
 */
+ (UIImage *)GL_imageWithColor:(UIColor *)color;

/**
 @brief 通过指定颜色生成一张图片

 @param color 颜色
 @param size 尺寸
 @return 创建的图片
 */
+ (UIImage *)GL_imageWithColor:(UIColor *)color withSize:(CGSize)size;

/**
 画出一张圆形图片

 @param color 颜色
 @param size 尺寸
 @return 返回一张圆形图片
 */
+ (UIImage *)GL_circleImageWithColor:(UIColor *)color withSize:(CGSize)size;

/**
 画出一张圆形图片

 @param color 图片背景颜色
 @param size 圆形图片尺寸
 @param name 图片上水印的文字内容
 @return 返回一张圆形图片
 */
+ (UIImage *)GL_circleImageWithColor:(UIColor *)color withSize:(CGSize)size withName:(NSString *)name;

/**
 @brief 群组二维码图片生成

 @param data 群组数据
 @param width 生产二维码图片的宽度
 @return 生成的二维码图片
 */
+ (UIImage *)GL_imageWithQRCodeData:(NSString *)data imageWidth:(CGFloat)width;

/**
 根据视频文件的一个地址获取第一帧图

 @param videoURL 视频文件的地址
 @return 返回第一针图
 */
+ (UIImage *)GL_GetVideoImage:(NSString *)videoURL;

/**
 传入一个view截取截取生成一张图片

 @param shotView 传入要截图的view
 @return 返回一张图片
 */
+ (UIImage *)GL_screenshotWithView:(UIView *)shotView;

/**
 传入一个view截取截取生成一张图片

 @param shotView 传入要截图的view
 @param shotSize 截取的范围
 @return 返回一张图片
 */
+ (UIImage *)GL_screenshotWithView:(UIView *)shotView shotSize:(CGSize)shotSize;

/**
 @brief 压缩图片

 @param image 待压缩图片
 @param viewsize 需要压缩图片的size
 @return 压缩后图片
 */
+ (UIImage *)GL_compressImage:(UIImage *)image withSize:(CGSize)viewsize;


/**
 根据scale等比例压缩当前图片并返回新图片

 @param img 原始图片
 @param scale 压缩比例
 @return 新图片
 */
+ (UIImage *)gl_scaleImg:(UIImage *)img scale:(float)scale;

/**
 @brief 转换图片方向

 @param aImage 原始图片
 @return 转换后生成的目标图片
 */
+ (UIImage *)GL_fixOrientation:(UIImage *)aImage;
@end
