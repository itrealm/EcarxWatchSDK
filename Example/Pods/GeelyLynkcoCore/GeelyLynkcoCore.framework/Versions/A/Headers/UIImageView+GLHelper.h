//
//  UIImageView+MJWebCache.h
//  FingerNews
//
//  Created by mj on 13-10-2.
//  Copyright (c) 2013年 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
@interface UIImageView (GLHelper)

/**单击进入图片浏览模式*/
@property(nonatomic,assign)BOOL gl_browserEnable;

@property(nonatomic,strong)NSString* gl_browserLinkKey;

@property(nonatomic,assign)BOOL gl_imageFadeAnimationEnable;


+(UIImage*)gl_placeholderImageWithSize:(CGSize)size;
/**/
-(void)gl_showPlaceholderWithColor:(UIColor*)color;
-(void)gl_showPlaceholder;

-(void)gl_setFadeImageWithURL:(NSURL*)url placeholderImage:(UIImage*)placeholderImage;
-(void)gl_setFadeImageWithURL:(NSURL*)url placeholderImage:(UIImage*)placeholderImage completed:(void(^)(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL))completedBlock;

@end
