//
//  UINavigationBar+GLNavigation.h
//  GLCoreKit-Example
//
//  Created by geely on 2018/7/23.
//  Copyright © 2018年 geely. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GLNavigationHelper : NSObject

@property(nonatomic,strong)UIColor* defaultNavBarBarTintColor;
@property(nonatomic,strong)UIColor* defaultNavBarTintColor;
@property(nonatomic,strong)UIColor* defaultNavBarTitleColor;
@property(nonatomic,strong)UIColor* defaultNavBarButtonTitleColor;
@property(nonatomic,strong)UIColor* defaultViewControllerBackColor;

@property(nonatomic,strong)UIFont* defaultNavBarTitleFont;
@property(nonatomic,assign)UIStatusBarStyle defaultStatusBarStyle;
@property(nonatomic,strong)UIImage* defaultNavBarBackImage;

@property(nonatomic,strong)UIImage* defaultNavBarBackgroundImage;
@property(nonatomic,assign)BOOL defaultNavBarHidden;
@property(nonatomic,assign)BOOL defaultNavBarShadowImageHidden;
@property(nonatomic,assign)BOOL defaultUseCustomNavBar;
@property(nonatomic,assign)CGFloat defaultNavBarContentYOffseet;

@property(nonatomic,assign)CGFloat defaultSideBackLeftEdge;

@property(nonatomic,assign)CGFloat defaultCustomNavBarHeight;


+ (instancetype)sharedInstance;


+ (CGFloat)defaultNavBarBackgroundAlpha;
+ (UIColor *)middleColor:(UIColor *)fromColor toColor:(UIColor *)toColor percent:(CGFloat)percent;
+ (CGFloat)middleAlpha:(CGFloat)fromAlpha toAlpha:(CGFloat)toAlpha percent:(CGFloat)percent;


@end



#pragma mark - UINavigationBar
@interface UINavigationBar (GLNavigation) <UINavigationBarDelegate>

/** 设置导航栏所有BarButtonItem的透明度 */
- (void)gl_setBarButtonItemsAlpha:(CGFloat)alpha hasSystemBackIndicator:(BOOL)hasSystemBackIndicator;

/** 设置导航栏在垂直方向上平移多少距离 */
- (void)gl_setTranslationY:(CGFloat)translationY;

/** 获取当前导航栏在垂直方向上偏移了多少 */
- (CGFloat)gl_getTranslationY;

- (void)gl_setBackgroundImage:(UIImage *)image;
- (void)gl_setBackgroundColor:(UIColor *)color;
- (void)gl_setBackgroundAlpha:(CGFloat)alpha;
@end









