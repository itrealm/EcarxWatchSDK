//
//  UINavigationController+Autorotate.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2018/9/11.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface UINavigationController (Autorotate)

@end


@interface UITabBarController (Autorotate)

@end
