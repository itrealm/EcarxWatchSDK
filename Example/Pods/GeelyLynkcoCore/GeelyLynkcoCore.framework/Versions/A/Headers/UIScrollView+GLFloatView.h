//
//  UIScrollView+GLFloatView.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2018/9/11.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (GLFloatView)


-(void)gl_floatSubView:(UIView*)subView;
-(void)gl_floatSubView:(UIView*)subView whenOffsetYGreaterThanOrEqual:(CGFloat)contentOffsetY;
@end




#if __has_include(<SGPagingView/SGPagingView.h>)
#import <SGPagingView/SGPagingView.h>

@interface UITableView (GLFloatView)

-(void)gl_floatPageTitleView:(SGPageTitleView*)subView;
-(void)gl_floatPageTitleView:(SGPageTitleView*)subView whenOffsetYGreaterThanOrEqual:(CGFloat)contentOffsetY;
@end


@interface UICollectionView (GLFloatView)

-(void)gl_floatPageTitleView:(SGPageTitleView*)subView;
-(void)gl_floatPageTitleView:(SGPageTitleView*)subView whenOffsetYGreaterThanOrEqual:(CGFloat)contentOffsetY;
@end

#endif



