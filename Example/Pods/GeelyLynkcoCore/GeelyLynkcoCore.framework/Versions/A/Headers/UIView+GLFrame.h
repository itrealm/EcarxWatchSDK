//
//  UIView+Frame.h
//  iOS-Categories (https://github.com/shaojiankui/iOS-Categories)
//
//  Created by Jakey on 14/12/15.
//  Copyright (c) 2014年 www.skyfox.org. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (GLFrame)

// shortcuts for positions
@property (nonatomic) CGFloat gl_centerX;
@property (nonatomic) CGFloat  gl_centerY;

@property (nonatomic) CGFloat  gl_x;
@property (nonatomic) CGFloat gl_y;

@property (nonatomic) CGFloat  gl_top;
@property (nonatomic) CGFloat gl_bottom;
@property (nonatomic) CGFloat gl_right;
@property (nonatomic) CGFloat gl_left;

@property (nonatomic) CGFloat gl_width;
@property (nonatomic) CGFloat gl_height;

@property (nonatomic,readonly) CGFloat gl_maxY;

@property (nonatomic,readonly) CGFloat gl_maxX;
@end
