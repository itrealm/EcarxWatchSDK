//
//  UIView+GLLine.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2018/9/11.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, GLViewLinePosition) {
    GLViewLinePositionNone                  = 0,
    GLViewLinePositionLeft                  = 1 << 0,
    GLViewLinePositionRight                 = 1 << 1,
    GLViewLinePositionBottom                = 1 << 2,
    GLViewLinePositionTop                   = 1 << 3,

};

@interface UIView (GLLine)

@property (nonatomic, strong)UIView* gl_leftLine;
@property (nonatomic, strong)UIView* gl_rightLine;
@property (nonatomic, strong)UIView* gl_topLine;
@property (nonatomic, strong)UIView* gl_bottomLine;
@property (nonatomic, assign)GLViewLinePosition gl_position;

@property (nonatomic, assign)UIEdgeInsets gl_lineEdgeInsets;
@property (nonatomic, assign)CGFloat gl_lineWidth;
@property (nonatomic, strong)UIColor* gl_lineColor;

@property (nonatomic, assign)UIEdgeInsets gl_lineTopEdgeInsets;
@property (nonatomic, assign)UIEdgeInsets gl_lineLeftEdgeInsets;
@property (nonatomic, assign)UIEdgeInsets gl_lineBottomEdgeInsets;
@property (nonatomic, assign)UIEdgeInsets gl_lineRightEdgeInsets;


-(void)gl_showLintWithPosition:(GLViewLinePosition)positionl;

@end


