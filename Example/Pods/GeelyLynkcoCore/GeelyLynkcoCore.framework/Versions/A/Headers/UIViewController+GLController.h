//
//  GLBaseViewController.h
//  GLCoreKit-Example
//
//  Created by kzy on 2018/7/22.
//  Copyright © 2018年 kzy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLBadgeLabel.h"
#import "GLNavigationBar.h"
#import "GLHButton.h"

typedef NS_ENUM(NSInteger, GLBaseBarButtonStyle) {
    GLBaseBarButtonLeftStyle = 0,
    GLBaseBarButtonRightStyle
};


@interface GLNavigationButton :GLHButton
@property(nonatomic,assign,readonly)GLBaseBarButtonStyle barStyle;
@end



@interface UIViewController (GLController)

@property(nonatomic,assign)BOOL gl_baseControllerEnable;

@property(nonatomic,strong)UIColor* gl_titleColor;

@property(nonatomic,readonly)GLNavigationBar* gl_navigationBar;

@property(nonatomic,assign)NSInteger gl_maxCountWhenShowMoreButton;
@property(nonatomic,strong)NSString* gl_moreButtonTitle;
@property(nonatomic,strong)UIImage* gl_moreButtonImage;

/**
 是否允滑动返回
 */
@property(nonatomic,assign)BOOL gl_canSideBack;
/**
 滑动返回有效范围
 */
@property (nonatomic, assign) CGFloat gl_sideBackLeftEdge;

/**
 使用自定义导航栏
 */
@property(nonatomic,assign)BOOL gl_useCustomNavigationBar;
/**
 自定义导航栏高度
 仅当useCustomNavigationBar为YES时，只对自定义导航栏生效
 */
@property(nonatomic,assign)CGFloat gl_customNavigationBarHeight;


@property(nonatomic,readonly)CGFloat gl_startYOffset;





#pragma mark- 添加导航栏按钮


/**
 添加导航按钮

 @param style 左侧或右侧按钮
 @param title 按钮标题
 @param image 按钮显示源，可以是以下类型UIImage，NSString(网络地址、本地地址、图片名称)，NSURL
 @param target 按钮点击事件target
 @param selector 按钮点击事件selector
 @param badgeItem 按钮角标badge配置
 */

- (GLNavigationButton*)gl_addBarButotn:(GLBaseBarButtonStyle)style title:(NSString*)title image:(id)image target:(id)target selector:(SEL)selector badgeItem:(GLBadgeItem*)badgeItem forKey:(NSString*)key;
- (GLNavigationButton*)gl_addBarButotn:(GLBaseBarButtonStyle)style image:(id)image target:(id)target selector:(SEL)selector;

/**
 添加导航按钮
 
 @param style 左侧或右侧按钮
 @param title 按钮标题
 @param image 按钮显示源，可以是以下类型UIImage，NSString(网络地址、本地地址、图片名称)，NSURL
 @param badgeItem 按钮角标badge配置
 @param block 按钮点击事件block
 */
- (GLNavigationButton*)gl_addBarButotn:(GLBaseBarButtonStyle)style title:(NSString*)title image:(id)image badgeItem:(GLBadgeItem*)badgeItem forKey:(NSString*)key action:(void(^)(id sender,id vc))block;
- (GLNavigationButton*)gl_addBarButotn:(GLBaseBarButtonStyle)style title:(NSString*)title image:(id)image forKey:(NSString*)key action:(void(^)(id sender,id vc))block;



- (void)gl_addCustomBarItem:(UIView*)view style:(GLBaseBarButtonStyle)style forKey:(NSString*)key;
- (void)gl_addCustomBarItem:(UIView*)view style:(GLBaseBarButtonStyle)style badgeItem:(GLBadgeItem*)badgeItem forKey:(NSString*)key;


/**
 设置按钮黑色半透明圆形背景

 @param alpha 不透明值
 */
- (void)gl_setBarButtonBackgroundMaskAlpha:(CGFloat)alpha;

/**
 返回指定的导航栏按钮

 @param style 左侧或右侧按钮
 @param key 按钮标识符
 @return 按钮UIView
 */
- (UIView*)gl_customBarItemForStyle:(GLBaseBarButtonStyle)style forKey:(NSString*)key;

- (void)gl_removeBarItemForStyle:(GLBaseBarButtonStyle)style forKey:(NSString*)key;

- (void)gl_setNavigationTitleView:(UIView*)titleView;



- (void)gl_setNavBarBackgroundImage:(UIImage *)image;
- (UIImage *)gl_navBarBackgroundImage;

- (void)gl_setNavBarBarTintColor:(UIColor *)color;
- (UIColor *)gl_navBarBarTintColor;

- (void)gl_setNavBarBackgroundAlpha:(CGFloat)alpha;
- (CGFloat)gl_navBarBackgroundAlpha;

- (void)gl_setNavBarTintColor:(UIColor *)color;
- (UIColor *)gl_navBarTintColor;

- (void)gl_setNavBarTitleColor:(UIColor *)color;
- (UIColor *)gl_navBarTitleColor;

- (void)gl_setNavBarTitleFont:(UIFont *)font;
- (UIFont *)gl_navBarTitleFont;

- (void)gl_setStatusBarStyle:(UIStatusBarStyle)style;
- (UIStatusBarStyle)gl_statusBarStyle;


- (void)gl_setNavBarShadowImageHidden:(BOOL)hidden;
- (BOOL)gl_navBarShadowImageHidden;

- (void)gl_setBarButtonItemsAlpha:(CGFloat)alpha;


- (void)gl_setNavBarHidden:(BOOL)hidden;
- (void)gl_hiddenSystemNavBar;
- (BOOL)gl_navBarHidden;

- (void)gl_setPushToCurrentVCFinished:(BOOL)isFinished;

- (void)gl_setTranslationY:(CGFloat)translationY;

- (BOOL)gl_canUpdateNavigationBar;

@end
