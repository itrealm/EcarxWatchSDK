//
//  GLFormViewController.h
//  KTools
//
//  Created by zhiyong.kuang on 2017/9/28.
//  Copyright © 2017年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLFormKit.h"
#if __has_include("GLBaseViewController.h")
#import "GLBaseViewController.h"
#endif


@class GLFormError;
@class GLError;


@interface GLFormDelegate : NSObject<UITableViewDataSource,UITableViewDelegate>
@end



@interface UIViewController (GLForm)

/**
 表单对应的UITableView
    gl_tableView.delegate 和 gl_tableView.dataSource 指向gl_form_delegate （不要修改）
 
 */
@property(nonatomic,readonly)UITableView* gl_tableView;
@property(nonatomic,readonly)GLFormDelegate* gl_form_delegate;
@property(nonatomic,strong)GLForm* gl_form;

-(void)gl_registerForm:(GLForm*)form;

-(void)gl_registerForm:(GLForm*)form configTableView:(void(^)(UITableView* tableView))configBlock;


- (void)gl_reloadFormRow:(GLFormRow*)row;
- (void)gl_reloadFormSection:(GLFormSection*)section;
- (void)gl_reloadSection:(NSUInteger)section;
- (void)gl_reloadSections:(NSIndexSet *)sections;
- (void)gl_reloadForm;

@end


@interface UIView (GLForm)

/**
 表单对应的UITableView
 gl_tableView.delegate 和 gl_tableView.dataSource 指向gl_form_delegate （不要修改）
 
 */
@property(nonatomic,readonly)UITableView* gl_tableView;
@property(nonatomic,readonly)GLFormDelegate* gl_form_delegate;
@property(nonatomic,strong)GLForm* gl_form;

-(void)gl_registerForm:(GLForm*)form;

-(void)gl_registerForm:(GLForm*)form configTableView:(void(^)(UITableView* tableView))configBlock;


- (void)gl_reloadFormRow:(GLFormRow*)row;
- (void)gl_reloadFormSection:(GLFormSection*)section;
- (void)gl_reloadSection:(NSUInteger)section;
- (void)gl_reloadSections:(NSIndexSet *)sections;
- (void)gl_reloadForm;

@end


#pragma mark- class GLFormOptionViewController
@interface GLFormOptionViewController : UIViewController
@property(nonatomic,weak)GLFormRow* formRow;
@property(nonatomic,assign)GLFormRowAction actionType;
@property(nonatomic,strong)NSString* labelTitle;
@property(nonatomic,strong)NSString* navTitle;

@property(nonatomic,strong)NSArray* options;
@end
