/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: LoginActionModel.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#import <Foundation/Foundation.h>
#import "GLBaseNetWork.h"

@interface GLLoginManager : NSObject
+ (GLLoginManager*)sharedManager;

@property(nonatomic,readonly)BOOL isLogin;
@property(nonatomic,readonly)NSString* token;
@property(nonatomic,readonly)NSString* svcToken;
@property(nonatomic,readonly)NSString* refreshToken;
@property(nonatomic,readonly)NSString* userId;


@property(nonatomic,readonly)NSDictionary* userInfo;


- (void)startLogin;
- (void)startLoginWithInfo:(NSDictionary *)params;
- (void)exitLogIn;


- (void)updateTokenFromWeex;
- (void)updateUserInfo:(NSDictionary*)userInfo;
- (void)clearUserInfo;

- (void)startRefreshTokenWithResult:(void(^)(BOOL success,id data))successBlock;

@end



@interface GLBaseNetWork (GLLoginManager)
@end


