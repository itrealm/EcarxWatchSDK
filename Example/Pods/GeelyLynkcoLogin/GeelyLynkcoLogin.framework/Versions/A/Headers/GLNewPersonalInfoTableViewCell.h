/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLNewPersonalInfoTableViewCell.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */


#import <UIKit/UIKit.h>

typedef void(^GetInputStr)(NSString *info);

/**
 新的个人信息cell
 */
@interface GLNewPersonalInfoTableViewCell : UITableViewCell
typedef void(^ClickGetVerifyButton)(UIButton *sender);
typedef void(^ChangeValidateImage)();


/** 设置文本框是否可以编辑 */
@property (nonatomic, assign) BOOL canEdit;
/** 设置文本框文字颜色 */
@property (nonatomic, strong) UIColor *textColor;
/** 标题文本颜色 */
@property (nonatomic, strong) UIColor *titleColor;
/** 通过block获取输入的文字内容 */
@property (nonatomic, copy) GetInputStr getInputStr;
/** 是否为密码输入 */
@property (nonatomic, assign) BOOL isPassWord;
/** 图片名 */
@property (nonatomic, strong) NSString *imageName;
/** 信息内容*/
@property (nonatomic, strong) UITextField *contentTextField;
/** 标题文本 */
@property (nonatomic, strong) NSString *titleText;
/** 是否显示右箭头 */
@property (nonatomic, assign) BOOL showRightBoult;
/** 是否显示顶部分割线 */
@property (nonatomic, assign) BOOL showTopLine;
/** 字符数量限制 */
@property (nonatomic, assign) NSInteger wordLimitNumber;
/** 收起键盘 */
@property (nonatomic, assign) BOOL hiddenKeyBoard;
/** 是否是快捷登录 */
@property (nonatomic,getter=isFastLogin,assign) BOOL fastLogin;
/** 按钮是否可以点 */
@property (nonatomic, getter=isButtonEnable) BOOL buttonEnable;
/** 发送验证码按钮 */
@property (nonatomic, strong) UIButton *getCaptchaButton;
/** 点击获取验证码按钮的回调*/
@property (nonatomic, copy) ClickGetVerifyButton clickGetVerifyButton;
/** 点击改变图形验证码的回调*/
@property (nonatomic, copy) ChangeValidateImage changeValidateImage;

@property (nonatomic, strong) NSData *imageData;

@property (nonatomic, strong) UIImageView *codeImageView;

- (void)configCellWithTitle:(NSString *)title andContext:(NSString *)context;
@end
