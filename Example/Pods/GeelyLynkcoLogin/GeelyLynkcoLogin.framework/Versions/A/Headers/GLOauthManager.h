//
//  GLMenuManager.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2018/9/5.
//  Copyright © 2018年 zhiyong.kuang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, GLOauthErrorCode) {
    GLOauthErrorCodeSuccess = 0,    //成功
    GLOauthErrorCodeParamInvalid,   //参数非法
    GLOauthErrorCodeUserReject,     //用户拒绝授权
    GLOauthErrorCodeDataError,      //数据异常

};

UIKIT_EXTERN NSString* GLOauthEnvTagTest;     // dev环境    test环境                      http://uaa.test.show.cmait.top:8090
UIKIT_EXTERN NSString* GLOauthEnvTagUAT;      // UAT环境                                https://cepssouat.lynkco.com
UIKIT_EXTERN NSString* GLOauthEnvTagTrain;    // 培训环境 train环境                       https://cepssotraining.lynkco.com
UIKIT_EXTERN NSString* GLOauthEnvTagDebug;    // 测试环境 联调环境                        http://uaa.show.cmait.top:8090
UIKIT_EXTERN NSString* GLOauthEnvTagDev;      // dev环境                                http://uaa.dev.cmait.top
UIKIT_EXTERN NSString* GLOauthEnvTagAdhoc;    // 预发布环境                              https://cephomepre.lynkco.com
UIKIT_EXTERN NSString* GLOauthEnvTagRelease;  // 发布环境 正式环境 生产环境                 https://sso.lynkco.com


typedef void(^GLOauthCallBackBlock) (NSDictionary* result,NSError *error);

/**
 第三方应用授权管理
 */
@interface GLOauthManager : NSObject
+ (GLOauthManager*)sharedManager;

/*返回结果
@"test"     // dev环境    test环境                      http://uaa.test.show.cmait.top:8090
@"uat"      // UAT环境                                https://cepssouat.lynkco.com
@"train"    // 培训环境 train环境                       https://cepssotraining.lynkco.com
@"debug"    // 测试环境 联调环境                        http://uaa.show.cmait.top:8090
@"dev"      // dev环境                                http://uaa.dev.cmait.top
@"adhoc"    // 预发布环境                              https://cephomepre.lynkco.com
@"release"  // 发布环境 正式环境 生产环境                 https://sso.lynkco.com
*/
@property(nonatomic,readonly)NSString* currentEnvTagString;
@property(nonatomic,readonly)NSString* currentEnvSSOHost;


///预留接口
/// 第三方获取授权code
/// @param app_id 应用唯一标识
/// @param redirect_uri 复位向地址，需要进行UrlEncode，为了安全复位向地址请使用https协议，主要适用于H5
/// @param state 用于保持请求和回调的状态，授权请求后原样带回给第三方。该参数可用于防止csrf攻击（跨站请求伪造攻击），建议第三方带上该参数，可设置为简单的随机数加session进行校验。请注意如果该参数包含特殊的url字符，请自行转码
/// @param scope 作用域
/// @param callback 结果回调(NSDictionary* result,NSError *error) 其中，当error不为空时，授权失败，error.code可参考GLOauthErrorCode
//result示例：
//{
//"code": "3232fdjdjkk36278",
//"state": "teststate"
//}
-(void)authWithAppId:(NSString*)app_id
         redirectUri:(NSString*)redirect_uri
               state:(NSString*)state
               scope:(NSString*)scope
            callBack:(GLOauthCallBackBlock)callback;

/**
 第三方获取授权code

 @param client_id 应用唯一标识
 @param redirect_uri 复位向地址，需要进行UrlEncode，为了安全复位向地址请使用https协议
 @param state 用于保持请求和回调的状态，授权请求后原样带回给第三方。该参数可用于防止csrf攻击（跨站请求伪造攻击），建议第三方带上该参数，可设置为简单的随机数加session进行校验。请注意如果该参数包含特殊的url字符，请自行转码
 @param callback 结果回调(NSDictionary* result,NSError *error) 其中，当error不为空时，授权失败，error.code可参考GLOauthErrorCode
 result示例：
 {
 "code": "3232fdjdjkk36278",
 "state": "teststate"
 }
 */
-(void)getAppCodeByClientId:(NSString*)client_id
                redirectUri:(NSString*)redirect_uri
                      state:(NSString*)state
                   callBack:(GLOauthCallBackBlock)callback;



@end
