/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLPersonalApiAddress.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#ifndef GLPersonalApiAddress_h
#define GLPersonalApiAddress_h

// 用户登录
#define G_USER_KEY_LOGIN @"uaa/obtainToken"
//用户快捷登录
#define G_USER_FAST_LOGIN @"/uaa/mobileCodeLogin"
// 用户退出登录
#define G_USER_KEY_LOGOUT @"tokenLogout"



/* 新发送注册验证码(手机) */
#define G_USER_ACCOUNT_SEND_REGSECURITYCODE       @"api/user/sendSms"

/* 获取图形验证码(手机) */
#define G_USER__GETGENERATORCAPTCHA       @"api/user/generatorCaptcha"

/** 发送验证码(邮箱) */
#define G_SEND_EMAILCODE       @"api/user/emailCode"



/** 绑定deviceId */
#define G_BOUND_DEVICEID @"api/member/app/updateDeviceIds"


/** 刷新 token */
#define G_USER_REFRESH_TOKEN_ACTION                     @"oauth2/refresh_token"



#endif /* GLPersonalApiAddress_h */
