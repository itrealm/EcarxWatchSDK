/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLThirdLoginView.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#import <UIKit/UIKit.h>
typedef void(^ClickButton)(NSInteger buttonTag);
/**
 三方登录的 view
 */
@interface GLThirdLoginView : UIView
/** 按钮点击回调 */
@property (nonatomic, copy) ClickButton clickButton;
@end
