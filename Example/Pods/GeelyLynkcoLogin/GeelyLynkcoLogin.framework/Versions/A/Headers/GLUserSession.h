//
//  GLUserSession.h
//  GeelyLynkcoLogin
//
//  Created by shuaishuai on 2019/8/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


extern NSString* const sw_user_event_register;
extern NSString* const sw_user_event_login;
extern NSString* const sw_user_event_third_login;
extern NSString* const sw_user_event_login_cancel;
extern NSString* const sw_user_event_update;
extern NSString* const sw_user_event_logout;
extern NSString* const sw_user_event_token_refresh;
extern NSString* const sw_user_event_token_expired;
extern NSString* const sw_user_event_force_quit;

typedef void(^GLEVENTCALLBACK)(NSString *eventName,id eventData);

@interface GLUserToken : NSObject
@property(nonatomic,copy)NSString *accessToken;
@property(nonatomic,copy)NSString *refreshToken;
+ (instancetype)createNew:(NSString *)accessToken
             refreshToken:(NSString *)refreshToken;
@end


@interface GLUserSession : NSObject
@property(nonatomic,readonly)BOOL isLogin;
@property(nonatomic,readonly)GLUserToken *userToken;
@property(nonatomic,readonly)NSDictionary *userData;
@property(nonatomic,readonly)NSDictionary *reservedDic;
@property (nonatomic, readonly) NSDictionary * pushDic;

///单例
+ (GLUserSession *)share;

///前端在用户注册成功之后调用
- (void)gl_register;
///前端在用户登录成功之后调用,如验证码/密码登录
- (void)gl_login:(NSDictionary *)userData userToken:(GLUserToken *)userToken;
///前端在三方快捷登录成功之后调用,如微信/微博/qq等三方平台
- (void)gl_thirdPartLogin:(NSDictionary *)userData userToken:(GLUserToken *)userToken;
///前端在用户取消登录之后调用,如关闭登录页面
- (void)gl_cancelLogin;
///用户手动退出登录之后调用
- (void)gl_logout;
//用户收到强制下线通知调用
- (void)gl_forceQuit;

///前端在修改用户数据成功之后调用（如修改手机号/密码/昵称/头像/三方账号绑定-解绑/手机号绑定-解绑等）
- (void)gl_updateUserInfo:(NSDictionary *)userData;
///刷新token成功之后调用,替换本地token信息
- (void)gl_refreshToken:(GLUserToken *)userToken;
/// token刷新失败后调用,清除本地用户数据
- (void)gl_refreshTokenFail;

- (BOOL)gl_updateReserved:(NSString *)reservedName
             reservedData:(NSDictionary *)reservedData;
- (NSDictionary *)gl_readReserved:(NSString *)reservedName;


///用于响应全局UserEvent的事件回调
- (BOOL)gl_listenUserEvents:(GLEVENTCALLBACK)eventCallBack;
///注销用户事件监听
- (BOOL)gl_cancelListenUserEvent;

@end

NS_ASSUME_NONNULL_END
