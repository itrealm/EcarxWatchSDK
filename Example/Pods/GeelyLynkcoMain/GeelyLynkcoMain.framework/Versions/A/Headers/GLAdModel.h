//
//  GLAdModel.h
//  GeelyLynkcoMain
//
//  Created by yang.duan on 2019/10/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLAdModel : NSObject

/// 页面关闭倒计时
@property (nonatomic, assign)NSInteger countdown;

/// 启动页点击路由地址类型（ 0:native ,1. weex  2:h5 3:外部app链接
@property (nonatomic, copy)NSString *routerType;

/// 跳转资源类型11891 活动 11892 内容 11893 精品 11894 整车 11895 其他
@property (nonatomic, assign)NSInteger resourceType;

/// 版本, 根据版本判断是否更新
@property (nonatomic, assign)NSInteger version;
/*
 广告内容链接地址
 */
@property (nonatomic, copy) NSString *pageSource;
/*
 是否支持跳转(0:否 1:是)
 */
@property (nonatomic, assign) NSInteger canJump;
/*
 1 IOS 2 Android
 */
@property (nonatomic, assign) NSInteger platform;
/*
 启动页形式(1:静态图 2:动态图GIF 3:微视频MP4/AVI/MOV/WMV
 */
@property (nonatomic, assign) NSInteger pageType;

/*
 跳转URL  resourceType 为11895 时候生效 h5格式
 */
@property (nonatomic, copy) NSString *jumpUrl;

/// 启动跳转配置
@property (nonatomic, copy) NSString *JumpConfig;

/*
 启动页点击路由地址 （例如：
 weex类型地址格式：https://xxx.js
 h5地址地址格式：https://xxx.html
 app内路由地址格式：/host:/path
 APP外链接地址格式：scheme://host:/path
 ）
 */
@property (nonatomic, copy) NSString *routerUrl;

/// 资源请求参数对象
@property (nonatomic, strong) NSDictionary *resource;
/*
 启动页状态: 1:启用 2:停用
 */
@property (nonatomic, assign) NSInteger status;

@end

NS_ASSUME_NONNULL_END
