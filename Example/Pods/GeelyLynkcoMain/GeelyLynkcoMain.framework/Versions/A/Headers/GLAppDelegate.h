//
//  GLAppDelegate.h
//  GeelyLynkcoMain
//
//  Created by _coCo_ on 2019/6/21.
//  Copyright © 2019 GEELY. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLAppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UIWindow *window;
//@property (strong, nonatomic) UIView *launchView;
@property (nonatomic, assign) BOOL allowRotation;

@property (nonatomic, strong) NSString* buildType;

- (void)initWeexKit;
///初始化weexKit
- (void)initAPIGateway;
@end

NS_ASSUME_NONNULL_END
