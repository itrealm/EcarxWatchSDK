//
//  GLCheckPrivacy.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/11/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLCheckPrivacy : NSObject

//@property (nonatomic, strong, class) UIViewController * rootViewController;

+ (void)checkAccountPrivacyWithController:(UIViewController *)rootViewController;

@end

NS_ASSUME_NONNULL_END
