/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLCustomView.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#import <Foundation/Foundation.h>
//#import "YYText.h"


typedef void(^handle)(void);

/**
 快速创建自定义控件
 */
@interface GLCustomView : NSObject

/**
 创建自定义label

 @param text 文字
 @param fontSize 字体大小
 @param textColor 字体颜色
 @param supView 父视图
 @return 创建的label
 */
+ (UILabel *)createLabelWithText:(NSString *)text andFontSize:(NSInteger)fontSize andTextColor:(UIColor *)textColor inSupView:(UIView *)supView;

/**
 自定义 textField

 @param placeHolder 默认文字
 @param fontSize 字体大小
 @param textColor 文字颜色
 @param keyboardType 键盘类型
 @param supView 父视图
 @return 创建的 textField
 */
+ (UITextField *)creatTextFieldWithPlaceHolder:(NSString *)placeHolder andFontSize:(CGFloat)fontSize andTextColor:(UIColor *)textColor andKeyboardType:(UIKeyboardType)keyboardType inSupView:(UIView *)supView;

/**
 自定义按钮

 @param title 标题
 @param textColor 文本颜色
 @param normalBackColor 正常背景色
 @param highLightBackColor 按下背景色
 @param fontSize 文字大小
 @param supView 父视图
 @return 创建的 button
 */
+ (UIButton *)creatButtonWithTitle:(NSString *)title andTextColor:(UIColor *)textColor andNormalBackColor:(UIColor *)normalBackColor andHighLightBackColor:(UIColor *)highLightBackColor andFontSize:(CGFloat)fontSize inSupView:(UIView *)supView;


+ (UIButton *)createButtonWithTitle:(NSString *)title andFont:(UIFont *)font andTextColor:(UIColor *)color inSupView:(UIView *)supView;
/**
 根据文字计算文字所占的宽度

 @param text 文字
 @param fontSize 字体大小
 @return 文字宽度
 */
+ (CGFloat)getTextWidthWithText:(NSString *)text andFontSize:(CGFloat)fontSize;

/**
 根据颜色返回图片

 @param color 颜色
 @return 图片
 */
+ (UIImage *)createImageWithColor:(UIColor*) color;
+ (UIImage *)createrandomColorImage;

/**
 创建一个富文本 label

 @param text  整体问题
 @param attributedStringArr 设置的分段文字
 @param fontArr 设置的分段字体
 @param textColorArr 设置的分段颜色
 @return 创建的 label
 */



+ (void)getAlertControllerWithTitle:(NSString *)title andMessage:(NSString *)message andActionTitle:(NSString *)actionTitle andActionTyep:(UIAlertActionStyle)actionType handle:(handle)actionHandle cancle:(handle)cancleHandle inRootVC:(UIViewController *)rootVC;

@end
