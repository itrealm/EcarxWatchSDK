//
//  GLFeedbackManager.h
//  
//
//  Created by zhiyong.kuang on 2019/3/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLFeedbackManager : NSObject
+ (instancetype)sharedManager;
-(void)light;
-(void)medium;
-(void)heavy;

-(void)vibrate;
@end

NS_ASSUME_NONNULL_END
