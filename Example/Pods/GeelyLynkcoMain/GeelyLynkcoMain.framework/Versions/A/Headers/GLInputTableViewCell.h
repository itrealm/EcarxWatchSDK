/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLInputTableViewCell.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#import <UIKit/UIKit.h>

typedef void(^GetInputText)(NSString *inputText);

/**
 通用的输入 cell
 */
@interface GLInputTableViewCell : UITableViewCell
/** 输入文本框 */
@property (nonatomic, strong) UITextField *inputTextField;
/** 输入信息的回调 */
@property (nonatomic, copy) GetInputText getInputText;
/** 提醒文字 */
@property (nonatomic, strong) NSString *placeHolder;
/** 有内容时显示的文字 */
@property (nonatomic, strong) NSString *topTitle;
/** 错误提醒文字 */
@property (nonatomic, strong) NSString *errorText;
/** 显示错误信息 */
@property (nonatomic, assign) BOOL showError;
@property (nonatomic, strong) UILabel *showErrorLabel;
@end


/**
 获取验证码的 cell
 */
@interface GLGetCaptchaTableViewCell : GLInputTableViewCell
typedef void(^ClickGetCaptchaButton)(UIButton *sender);

/** 发送验证码按钮 */
@property (nonatomic, strong) UIButton *getCaptchaButton;

@property (nonatomic, getter=isButtonEnable) BOOL buttonEnable;
/** 点击获取验证码按钮的回调*/
@property (nonatomic, copy) ClickGetCaptchaButton clickGetCaptchaButton;
@end
