//
//  GLJPushItem.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2019/12/17.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLPushCenter.h"

UIKIT_EXTERN NSString* GLJPushTypeIdentifier;



@interface GLJPushItem : NSObject<GLPushCenterProtocol>

@property (nonatomic,copy)NSString *appKey;
@property (nonatomic,copy)NSString *appChannel;
@property(nonatomic,readonly) NSString *deviceId;
@property(nonatomic,readonly) NSString *typeIdentifier;
@property (nonatomic,copy)NSString *registrationId;
@property (nonatomic,copy)GLPushReceiveMessageBlock receiveRemoteNotification;


- (void)setupWithOptions:(NSDictionary *)launchingOptions result:(void(^)(BOOL success,id data))resultBlock;
- (void)registerDeviceToken:(NSData *)deviceToken;
- (void)setAlias:(NSString *)alias tags:(NSSet *)tags;
- (void)unsetAliasAndTags;
- (void)handleRemoteNotification:(NSDictionary *)remoteInfo;

@end
