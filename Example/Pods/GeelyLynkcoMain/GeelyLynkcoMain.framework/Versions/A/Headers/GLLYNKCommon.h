//
//  GLLYNKCommon.h
//  Pods
//
//  Created by 薛立恒 on 2019/6/22.
//

#ifndef GLLYNKCommon_h
#define GLLYNKCommon_h

#define GET_TOKEN Router.openUrlWithParams(@"lynkco://login/gettoken").run()

/** 插入 access_token */
#define kInsetAccessToken(baseUrl) [NSString stringWithFormat:@"%@?access_token=%@", baseUrl, GET_TOKEN]

#import "UILabel+config.h"


#endif /* GLLYNKCommon_h */
