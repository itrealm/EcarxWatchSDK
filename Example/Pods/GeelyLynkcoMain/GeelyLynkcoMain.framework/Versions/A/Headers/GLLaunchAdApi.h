//
//  GLLaunchAdApi.h
//  Geelyconsumer
//  广告页面网络请求相关
//  Created by yang.duan on 2019/9/26.
//  Copyright © 2019年 geely. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^SuccessBlock)(id result);
typedef void(^FailBlock)(NSError *error);

/**
 个人信息接口
 */
@interface GLLaunchAdApi : NSObject

/**
 获取启动页数据
 
 @param dic 参数
 */
+ (void)getLaunchAdData:(NSDictionary *)dic success:(SuccessBlock)success fail:(FailBlock)fail;


@end

