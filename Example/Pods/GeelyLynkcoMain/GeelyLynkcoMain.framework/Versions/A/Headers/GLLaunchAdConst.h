//
//  GLLaunchAdConst.h
//  StartUpPageDemo
//
//  Created by yang.duan on 2019/9/27.
//  Copyright © 2019年 geely. All rights reserved.
//


#import <UIKit/UIKit.h>

#define GLLaunchAdDeprecated(instead) __attribute__((deprecated(instead)))

#define XHWeakSelf __weak typeof(self) weakSelf = self;

#ifdef DEBUG
#define GLLaunchAdLog(FORMAT, ...) fprintf(stderr,"%s:%d\t%s\n",[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define GLLaunchAdLog(...)
#endif

#define GLISGIFTypeWithData(data)\
({\
BOOL result = NO;\
if(!data) result = NO;\
uint8_t c;\
[data getBytes:&c length:1];\
if(c == 0x47) result = YES;\
(result);\
})

#define GLISVideoTypeWithPath(path)\
({\
BOOL result = NO;\
if([path hasSuffix:@".mp4"])  result =  YES;\
(result);\
})

#define XHDataWithFileName(name)\
({\
NSData *data = nil;\
NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:nil];\
if([[NSFileManager defaultManager] fileExistsAtPath:path]){\
data = [NSData dataWithContentsOfFile:path];\
}\
(data);\
})

#define DISPATCH_SOURCE_CANCEL_SAFE(time) if(time)\
{\
dispatch_source_cancel(time);\
time = nil;\
}

#define REMOVE_FROM_SUPERVIEW_SAFE(view) if(view)\
{\
[view removeFromSuperview];\
view = nil;\
}

UIKIT_EXTERN NSString *const GLCacheImageUrlStringKey;
UIKIT_EXTERN NSString *const GLCacheVideoUrlStringKey;

UIKIT_EXTERN NSString *const GLLaunchAdWaitDataDurationArriveNotification;
UIKIT_EXTERN NSString *const GLLaunchAdDetailPageWillShowNotification;
UIKIT_EXTERN NSString *const GLLaunchAdDetailPageShowFinishNotification;
/** GIFImageCycleOnce = YES(GIF不循环)时, GIF动图播放完成通知 */
UIKIT_EXTERN NSString *const GLLaunchAdGIFImageCycleOnceFinishNotification;
/** videoCycleOnce = YES(视频不循环时) ,video播放完成通知 */
UIKIT_EXTERN NSString *const GLLaunchAdVideoCycleOnceFinishNotification;
/** 视频播放失败通知 */
UIKIT_EXTERN NSString *const GLLaunchAdVideoPlayFailedNotification;
UIKIT_EXTERN BOOL GLLaunchAdPrefersHomeIndicatorAutoHidden;



