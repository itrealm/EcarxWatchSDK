//
//  GLLaunchAdDownload.h
//  StartUpPageDemo
//
//  Created by yang.duan on 2019/9/27.
//  Copyright © 2019年 geely. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#pragma mark - GLLaunchAdDownload

typedef void(^GLLaunchAdDownloadProgressBlock)(unsigned long long total, unsigned long long current);//下载进度回调

typedef void(^GLLaunchAdDownloadImageCompletedBlock)(UIImage *_Nullable image, NSData * _Nullable data, NSError * _Nullable error);

typedef void(^GLLaunchAdDownloadVideoCompletedBlock)(NSURL * _Nullable location, NSError * _Nullable error);

typedef void(^GLLaunchAdBatchDownLoadAndCacheCompletedBlock) (NSArray * _Nonnull completedArray);

@protocol GLLaunchAdDownloadDelegate <NSObject>

- (void)downloadFinishWithURL:(nonnull NSURL *)url;

@end

@interface GLLaunchAdDownload : NSObject
@property (assign, nonatomic ,nonnull)id<GLLaunchAdDownloadDelegate> delegate;
@end

@interface GLLaunchAdImageDownload : GLLaunchAdDownload

@end

@interface GLLaunchAdVideoDownload : GLLaunchAdDownload

@end

#pragma mark - GLLaunchAdDownloader
@interface GLLaunchAdDownloader : NSObject

+(nonnull instancetype )sharedDownloader;

- (void)downloadImageWithURL:(nonnull NSURL *)url progress:(nullable GLLaunchAdDownloadProgressBlock)progressBlock completed:(nullable GLLaunchAdDownloadImageCompletedBlock)completedBlock;

- (void)downLoadImageAndCacheWithURLArray:(nonnull NSArray <NSURL *> * )urlArray;
- (void)downLoadImageAndCacheWithURLArray:(nonnull NSArray <NSURL *> * )urlArray completed:(nullable GLLaunchAdBatchDownLoadAndCacheCompletedBlock)completedBlock;

- (void)downloadVideoWithURL:(nonnull NSURL *)url progress:(nullable GLLaunchAdDownloadProgressBlock)progressBlock completed:(nullable GLLaunchAdDownloadVideoCompletedBlock)completedBlock;

- (void)downLoadVideoAndCacheWithURLArray:(nonnull NSArray <NSURL *> * )urlArray;
- (void)downLoadVideoAndCacheWithURLArray:(nonnull NSArray <NSURL *> * )urlArray completed:(nullable GLLaunchAdBatchDownLoadAndCacheCompletedBlock)completedBlock;

@end
