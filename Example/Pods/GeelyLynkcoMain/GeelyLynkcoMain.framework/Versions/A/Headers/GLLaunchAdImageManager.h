//
//  GLLaunchAdImageManager.h
//  StartUpPageDemo
//
//  Created by yang.duan on 2019/9/27.
//  Copyright © 2019年 geely. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import "GLLaunchAdDownloader.h"

typedef NS_OPTIONS(NSUInteger, GLLaunchAdImageOptions) {
    /** 有缓存,读取缓存,不重新下载,没缓存先下载,并缓存 */
    GLLaunchAdImageDefault = 1 << 0,
    /** 只下载,不缓存 */
    GLLaunchAdImageOnlyLoad = 1 << 1,
    /** 先读缓存,再下载刷新图片和缓存 */
    GLLaunchAdImageRefreshCached = 1 << 2 ,
    /** 后台缓存本次不显示,缓存OK后下次再显示(建议使用这种方式)*/
    GLLaunchAdImageCacheInBackground = 1 << 3
};

typedef void(^XHExternalCompletionBlock)(UIImage * _Nullable image,NSData * _Nullable imageData, NSError * _Nullable error, NSURL * _Nullable imageURL);

@interface GLLaunchAdImageManager : NSObject

+(nonnull instancetype )sharedManager;

- (void)loadImageWithURL:(nullable NSURL *)url options:(GLLaunchAdImageOptions)options progress:(nullable GLLaunchAdDownloadProgressBlock)progressBlock completed:(nullable XHExternalCompletionBlock)completedBlock;

@end

