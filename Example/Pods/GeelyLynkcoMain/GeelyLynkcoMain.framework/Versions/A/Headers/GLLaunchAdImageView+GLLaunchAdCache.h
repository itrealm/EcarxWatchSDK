//
//  GLLaunchAdImageView+GLLaunchAdCache.h
//  StartUpPageDemo
//
//  Created by yang.duan on 2019/9/27.
//  Copyright © 2019年 geely. All rights reserved.
//

#import "GLLaunchAdView.h"
#import "GLLaunchAdImageManager.h"

@interface GLLaunchAdImageView (GLLaunchAdCache)

/**
 设置url图片
 
 @param url 图片url
 */
- (void)gl_setImageWithURL:(nonnull NSURL *)url;

/**
 设置url图片
 
 @param url 图片url
 @param placeholder 占位图
 */
- (void)gl_setImageWithURL:(nonnull NSURL *)url placeholderImage:(nullable UIImage *)placeholder;

/**
 设置url图片
 
 @param url 图片url
 @param placeholder 占位图
 @param options GLLaunchAdImageOptions
 */
- (void)gl_setImageWithURL:(nonnull NSURL *)url placeholderImage:(nullable UIImage *)placeholder options:(GLLaunchAdImageOptions)options;

/**
 设置url图片
 
 @param url 图片url
 @param placeholder 占位图
 @param completedBlock XHExternalCompletionBlock
 */
- (void)gl_setImageWithURL:(nonnull NSURL *)url placeholderImage:(nullable UIImage *)placeholder completed:(nullable XHExternalCompletionBlock)completedBlock;

/**
 设置url图片
 
 @param url 图片url
 @param completedBlock XHExternalCompletionBlock
 */
- (void)gl_setImageWithURL:(nonnull NSURL *)url completed:(nullable XHExternalCompletionBlock)completedBlock;


/**
 设置url图片
 
 @param url 图片url
 @param placeholder 占位图
 @param options GLLaunchAdImageOptions
 @param completedBlock XHExternalCompletionBlock
 */
- (void)gl_setImageWithURL:(nonnull NSURL *)url placeholderImage:(nullable UIImage *)placeholder options:(GLLaunchAdImageOptions)options completed:(nullable XHExternalCompletionBlock)completedBlock;

/**
 设置url图片
 
 @param url 图片url
 @param placeholder 占位图
 @param GIFImageCycleOnce gif是否只循环播放一次
 @param options GLLaunchAdImageOptions
 @param cycleOnceFinishBlock gif播放完回调(GIFImageCycleOnce = YES 有效)
 @param completedBlock XHExternalCompletionBlock
 */
- (void)gl_setImageWithURL:(nonnull NSURL *)url placeholderImage:(nullable UIImage *)placeholder GIFImageCycleOnce:(BOOL)GIFImageCycleOnce options:(GLLaunchAdImageOptions)options GIFImageCycleOnceFinish:(void(^_Nullable)(void))cycleOnceFinishBlock completed:(nullable XHExternalCompletionBlock)completedBlock ;


@end

