//
//  GLLaunchAdModel.h
//  StartUpPageDemo
//
//  Created by yang.duan on 2019/9/24.
//  Copyright © 2019年 geely. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GLLaunchAdModel : NSObject

/**
 *  广告URL
 */
@property (nonatomic, copy) NSString *value;

/**
 *  点击打开连接
 */
//@property (nonatomic, copy) NSString *openUrl;

@property (nonatomic, strong) NSDictionary *mobileImageSizeDic;

/**
 *  分辨率宽
 */
@property(nonatomic,assign,readonly)CGFloat width;

/**
 *  分辨率高
 */
@property(nonatomic,assign,readonly)CGFloat height;
//图片类型或者是视频类型
@property (nonatomic, copy) NSString *type;


- (instancetype)initWithDict:(NSDictionary *)dict;

@end

