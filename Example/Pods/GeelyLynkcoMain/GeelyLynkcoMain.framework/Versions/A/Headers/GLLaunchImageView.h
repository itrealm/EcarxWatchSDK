//
//  GLLaunchImageView.h
//  StartUpPageDemo
//
//  Created by yang.duan on 2019/9/24.
//  Copyright © 2019年 geely. All rights reserved.
//

#import <UIKit/UIKit.h>
/** 启动图来源 */
typedef NS_ENUM(NSInteger,SourceType) {
    /** LaunchImage(default) */
    SourceTypeLaunchImage = 1,
    /** LaunchScreen.storyboard */
    SourceTypeLaunchScreen = 2,
};


@interface GLLaunchImageView : UIImageView

- (instancetype)initWithSourceType:(SourceType)sourceType;
//返回一个UIimage类型的值
+(UIImage *)imageFromLaunchScreen;
+(UIImage *)imageFromLaunchImage;
@end
