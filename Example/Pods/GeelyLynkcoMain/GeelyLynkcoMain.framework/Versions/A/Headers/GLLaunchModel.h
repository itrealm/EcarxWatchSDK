//
//  GLLaunchModel.h
//  GeelyLynkcoMain
//
//  Created by yang.duan on 2019/9/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLLaunchModel : NSObject

@property (nonatomic, copy) NSString *pageTypeName;
@property (nonatomic, assign) NSInteger status; //0为启用，1为停用
@property (nonatomic, assign) NSInteger pageType; //1:静态图 2:动态图 3:微视频
@property (nonatomic, assign) NSInteger vaild;//("1 有效 ,0 无效")
@property (nonatomic, assign) NSInteger canJump;//("是否支持跳转(0:否 1:是)")
@property (nonatomic, assign) NSInteger isdefault;
@property (nonatomic, copy) NSString *updatedTime;
@property (nonatomic, copy) NSString *enableTime;
@property (nonatomic, copy) NSString *channel;
@property (nonatomic, copy) NSString *endTime;
@property (nonatomic, copy) NSString *resourceId;////////////////////////////////////////////////////////////////////////////////
@property (nonatomic, copy) NSString *brandCode;
@property (nonatomic, copy) NSString *resourceTypeName;
@property (nonatomic, assign) NSInteger version;
@property (nonatomic, assign) NSInteger resourceType;
@property (nonatomic, copy) NSString *brandName;
@property (nonatomic, copy) NSString *pageName;
@property (nonatomic, copy) NSString *iosImage;
@property (nonatomic, assign) NSInteger countdown;//页面关闭倒计时


@end

NS_ASSUME_NONNULL_END
