/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLCustomFont.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#ifndef GLCustomFont_h
#define GLCustomFont_h


/**  常规字体 */
#define kNORMALFONT(fontSize)     [UIFont systemFontOfSize:(fontSize * kScale)]
/** 粗体 */
#define kBOLDFONT(fontSize)     [UIFont boldSystemFontOfSize:(fontSize * kScale)]

#pragma mark - 字体号
#define FONT14 [UIFont systemFontOfSize:(14 * SCALE_X)]

/**  常规字体 */
#define YSNORMALFONT(fontSize)     [UIFont systemFontOfSize:(fontSize * SCALE_X)]


#define FONT_1 22
#define FONT_2 16
#define FONT_3 16
#define FONT_4 14
#define FONT_5 14
#define FONT_6 16
#define FONT_7 14
#define FONT_8 14
#define FONT_9 14
#define FONT_10 14
#define FONT_11 14
#define FONT_12 22
#define FONT_13 60
#define FONT_14 18
#define FONT_15 14
#define FONT_16 50
#define FONT_17 18
#define FONT_18 14
#define FONT_19 14
#define FONT_20 30
#define FONT_21 13
#define FONT12 12

#define font8_9 9
#define font10_12 12
#define font12_14 14
#define font13_15 15
#define font14_16 16
#define font15_17 17
#define font16_18 18
#define font17_19 19
#define font18_20 20
#define font20_22 22
#define font24_26 26
#define font26_28 28
#define font23_25 25

#endif /* GLCustomFont_h */
