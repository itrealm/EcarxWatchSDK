/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLCustomSize.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#ifndef GLynkCoSize_h
#define GLynkCoSize_h

#import "GLSupportKit.h"
/*
 4    inch 320*568
 4.7  inch 375*667
 5.5  inch 414*736
 */

#define SCALE_X      SCREENWIDTH/375.0
#define SCALE_Y      SCREENHEIGHT/667.0

//总结：由网易新闻的app得出
//头条一栏： 所有设备上控件的高度一样 ， 里面的字体大小也是一样， 这里用了文字弹性控件
//图片： 这列根据屏幕的宽度进行了等比缩放适配。字体也是进行了等比适配。 还有文字流式适配。
// 5  6  比例一致，6p 放大
#define PLUS_SCALE_X      (IS_IPHONE6P?SCALE_X:1)
#define PLUS_SCALE_Y      (IS_IPHONE6P?SCALE_Y:1)


/** 系统尺寸相关信息都在这里 */

/* 设备屏幕宏 */
#define KScreenFrame [[UIScreen mainScreen] bounds]  // 当前屏幕frmae
#define KScreenWidth KScreenFrame.size.width          // 当前屏幕宽度
#define KScreenHeight KScreenFrame.size.height        // 当前屏幕高度

#define SCREENWIDTH  [UIScreen mainScreen].bounds.size.width
#define SCREENHEIGHT [UIScreen mainScreen].bounds.size.height
#define VIEWHEIGHT   self.view.frame.size.height
#define APPHEIGHT [UIScreen mainScreen].bounds.size.height-64

/** 屏幕宽 */
#define G_SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
/** 屏幕高 */
#define G_SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
/** 状态栏高    */
#define G_STATUSBAR_HEIGHT [UIApplication sharedApplication].statusBarFrame.size.height

/** 视图距离屏幕左边边距 */
#define kSPACELEFT 16
#define kLeftPadding (kSPACELEFT + 9)
#define kTableViewContentInsetTopSize 14
/** 中间内容的宽度 */
#define kContentViewWidth 267
/** 右箭头按钮的宽度 */
#define kRightBoultWidth 25
/** 分割线的高度 */
#define kSepLineHeight .5

/** 导航栏高度 */
#define G_STATUS_HEIGHT ((IS_IPHONE_X==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs== YES || IS_IPHONE_Xs_Max== YES) ? 44.0 : 20.0)
#define G_NAV_HEIGHT ((IS_IPHONE_X==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs== YES || IS_IPHONE_Xs_Max== YES) ? 88.0 : 64.0)
#define G_TABBAR_HEIGHT ((IS_IPHONE_X==YES || IS_IPHONE_Xr ==YES || IS_IPHONE_Xs== YES || IS_IPHONE_Xs_Max== YES) ? 83.0 : 49.0)
/** 编辑 cell高度 */
#define G_EDIT_CELL_HEIGHT 80

/** 屏幕高度比(适配等比例时使用,标准为iphone6) */
#define G_SCREEN_HEIGHT_PERCENT G_SCREEN_HEIGHT/667
/** 屏幕宽度比(适配等比例时使用,标准为iphone6) */
#define G_SCREEN_WIDTH_PERCENT G_SCREEN_WIDTH/375

#define kScale (G_SCREEN_WIDTH > 375 ? 1 : G_SCREEN_WIDTH / 375)


/** 视图距离屏幕左边边距 */
#define YsAwayLeft  (16*SCALE_X)
#define YsAwayRight  (-16*SCALE_X)

#endif /* GLCustomSize_h */
