


#ifndef GLCommonStr_h
#define GLCommonStr_h

#pragma mark - app scheme配置

static NSString *const appScheme = @"lynkco";

#define APP_VERSION_UPDATE_URL @"APP_VERSION_UPDATE_URL"    //APP更新



#define BASE_UC_URL @"base_uc_url"    // 用户中心模块URL
#define BASE_UAA_URL @"base_uaa_url"       // 用户认证测试地址
#define BASE_MESSAGE_URL @"base_message_url"  // 消息
#define BASE_WEEX_ROOT_URL @"BASE_WEEX_ROOT_URL"//weex url
#define BASE_WEEX_ROOT_SEVICE_URL @"BASE_WEEX_ROOT_SEVICE_URL"//weex service url

#define BASE_MAIN_URL @"base_main_url"    //用户后台
#define BASE_WEB_URL @"base_web_url"    //web

#define BASE_MAINTENANCE_URL @"base_maintenance_url"  //维保
#define BASE_MAINTENANCE_URL_NEW @"base_maintenance_url_new"  //维保新域名

#define BASE_CHARGING_URL @"base_charging_url"//充电桩URL
#define BASE_DEALER_URL @"base_dealer_url"//经销商URL
#define BASE_CARDETAIL_URL @"base_cardetail_url" //车辆详情

#define BASE_USERGUIDE_URL @"base_userGuide_url" //用户手册的URL


#define BASE_WEEX_EXPLORE_INDEX_URL @"BASE_WEEX_EXPLORE_INDEX_URL"      //weex 探索
#define BASE_WEEX_MALL_INDEX_URL @"BASE_WEEX_MALL_INDEX_URL"            //weex 商城
#define BASE_WEEX_ACTIVITY_INDEX_URL @"BASE_WEEX_ACTIVITY_INDEX_URL"    //weex 活动
#define BASE_WEEX_MINE_INDEX_URL @"BASE_WEEX_MINE_INDEX_URL"            //weex 我的


/*
 
 Page Path
 
 */
#import "GLNetworkEnvConfig.h"
#import "GLWeexHostManger.h"

//#define WEEXURL(PATH) [[GLNetworkEnvConfig sharedInstance] getCurUrlByHostKey:BASE_WEEX_ROOT_URL path: PATH ]
#define WEEXURL(PATH) WEEX_URL(PATH)

#pragma mark - weex 链接配置
//weex 探索
#define WEEX_PATH_EXPLORE_INDEX_URL @"pages/exploration/index.js"
//weex 商城
#define WEEX_PATH_MALL_INDEX_URL @"pages/shop-mall/index/index.js"
//weex 活动
#define WEEX_PATH_ACTIVITY_INDEX_URL @"pages/activity/index.js"
//weex 我的
#define WEEX_PATH_MINE_INDEX_URL @"pages/user/index.js"

//伙伴计划
#define WEEX_PATH_PAGE_PARTNER @"pages/user/share/page-partner.js"
//经销商查询
#define WEEX_PATH_CHANGE_DEALER @"pages/mycar/dealer-search/page-dealerSearchMap.js"
//我的服务经销商
#define WEEX_PATH_MY_DEALER @"pages/mycar/my-dealer/page-my-dealer.js"
//登录页
#define WEEX_PATH_LOGIN_INDEX_URL @"pages/login/index.js"
//投稿须知
#define WEEX_PATH_POST_ARTICLE @"pages/user/homePage/post-article/notice/index.js"
//试乘试驾
#define WEEX_PATH_TEST_DRIVE @"pages/test-drive/index.js"
//车商城
#define WEEX_PATH_CAR_MALL @"pages/shop-mall/index/index.js"
//隐私政策
#define WEEX_PATH_PRIVACY @"pages/protocol/privacy/index.js"
//维保_经销商选择
#define WEEX_PATH_SERVICE_DEALER_LIST @"pages/mycar/change-dealer/page-change-dealer.js"
//维保_预约详情
#define WEEX_PATH_SERVICE_APPOINTMENT_DETAIL @"pages/user/settings/reservation/detail/index.js"
//维保_经销商详情
#define WEEX_PATH_SERVICE_DEALER_DETAIL @"pages/mycar/dealer-search/page-dealerDetail.js"
//维保_品牌视频
#define WEEX_PATH_SERVICE_BRAND_VIDEO @"pages/mycar/brand-video/index.js"
//扫一扫_经销商名片
#define WEEX_PATH_DEALER_CARD @"pages/user/homePage/dealer/index.js"
//扫一扫_消费者名片
#define WEEX_PATH_CUSTOMER_CARD @"pages/user/homePage/index.js"
//扫一扫_活动详情
#define WEEX_PATH_ACTIVITY_DETAIL @"pages/activity/activityDetail/page-activityDetail.js"
//扫一扫_用户签到
#define WEEX_PATH_CHECK_IN @"pages/user/checkIn/index.js"
//扫一扫_不需要单独处理的扫描结果
#define WEEX_PATH_SCAN_RESULT @"pages/exploration/scan-result/page-scan-result.js"

//IM跳转路由路径
//LYNKCO_APP_1001  LYNKCO_APP_1004
#define SW_TOROOT @"pages/sw/toRoot.js?"
//LYNKCO_APP_1005
#define ARTICLE_INDEX @"pages/exploration/article/index.js?"
//LYNKCO_APP_1006
#define ACTIVITYDETAIL_PAGE_ACTIVETYDETAIL @"pages/activity/activityDetail/page-activityDetail.js?"
//LYNKCO_APP_1007
#define CAR_INTRUDUCE_PAGE_INTRODUCE @"pages/lynkco/car-introduce/page-introduce.js?"
//LYNKCO_APP_1008
#define GOODS_PAGE_INFO @"pages/shop-mall/goods/page-info.js?"
//LYNKCO_APP_1009
#define ORDERDETAIL_INDEX @"pages/lynkco/lynk-fullcar/orderDetail/index.js?"
//LYNKCO_APP_1010
#define ORDER_PAGE_DETAIL @"pages/shop-mall/order/page-detail.js?"
//LYNKCO_APP_1011 LYNKCO_APP_1012
#define EVALUATE_PAGE_ADDEVALDETAIL @"pages/evaluate/index.js?"
//LYNKCO_APP_10013  LYNKCO_APP_10014
#define RESERVATION_PAGE_RESERVATION @"pages/user/settings/reservation/page-reservation.js?"
//LYNKCO_APP_1015
#define ORDER_PAGE_SHIP @"pages/shop-mall/order/page-ship.js?"
//LYNKCO_APP_1016
#define COUPON_INDEX @"pages/user/cardCoupon/coupon/index.js?"
//LYNKCO_APP_1017 LYNKCO_APP_1018
#define REFUND_DETAIL_INDEX @"pages/shop-mall/refund/detail/index.js?"



#pragma mark - 支持环境切换第三方环境参数配置
///阿里的API网关appkey
#define API_GATEWAY_APP_KEY @"API_GATEWAY_APP_KEY"
//API网关appSecret
#define API_GATEWAY_APP_SECRET @"API_GATEWAY_APP_SECRET"


#pragma mark - Countly配置
#define GL_COUNTLY_APP_KEY @"GL_COUNTLY_APP_KEY"
#define GL_COUNTLY_APP_SERVERURL @"GL_COUNTLY_APP_SERVERURL"


#pragma mark - OSS配置
#define GL_OSS_APP_BUCKET @"GL_OSS_APP_BUCKET"
#define GL_OSS_APP_KEY @"GL_OSS_APP_KEY"
#define GL_OSS_APP_SECRET @"GL_OSS_APP_SECRET"

#define kPARAM @"parameter"

#pragma mark- Notification
/** 用户登录通知 */
#define kUserLoginNotification @"kUserLoginNotification"
/** 用户退出登录通知 */
#define kUserLogoutNotification @"kUserLogoutNotification"





#pragma mark - 用户信息相关

#define G_USER_TOKEN @"access_token"
/** refreshToken    */
#define G_USER_REFRESH_TOKEN @"refreshToken"

/** 用户ID */
#define G_USER_OPENID @"id"
/**用户姓名*/
#define G_USER_NAME @"displayName"
/**用户手机号*/
#define G_USER_PHONE @"mobile"
/** 账户创建时间 */
#define G_ACCOUNT_CREATTIME @"createdAt"
/** 昵称 */
#define G_USER_NICKNAME @"nickName"
//头像名称
#define G_USER_HEAD_IMAGE @"gUserHeadImage"

#define kBUNDLEIDENTIFIER ([[NSBundle mainBundle] bundleIdentifier])

#pragma mark -
#pragma mark - =================DEBUG================
// 高德地图key
#define kMapKeyDict @{@"com.lynkco.customer":@"748240e77d98bbaf1d32ae0ce351ebeb",\
                      @"com.lynkco.customer.beta":@"310124db799f4babea9e1936d8089198"}

#define kMapKey kMapKeyDict[kBUNDLEIDENTIFIER]?:@"748240e77d98bbaf1d32ae0ce351ebeb"
// 云推
#define kCloudKeyDict @{@"com.lynkco.customer":@"24698004",\
                    @"com.lynkco.customer.beta":@"27845840"}
#define kCloudKey kCloudKeyDict[kBUNDLEIDENTIFIER]?:@"24698004"

#define kCloudSecretDict @{@"com.lynkco.customer":@"6401f64bc45f91972c475cde24a05897",\
                    @"com.lynkco.customer.beta":@"aaa2029d9f1f867303f6ba5693aec03b"}
#define kCloudSecret kCloudSecretDict[kBUNDLEIDENTIFIER]?:@"6401f64bc45f91972c475cde24a05897"

// 极光推送
#define kJPushAppChannel @"developer-default"
#define kJPushAppKey @"c0c567157cd75562e7ba47be"

// Bugly
#define kBuglyReleaseKeyDict @{@"com.lynkco.customer":@"29e1dc32da",\
                            @"com.lynkco.customer.beta":@"83feb70889"}

#define kBuglyReleaseKey kBuglyReleaseKeyDict[kBUNDLEIDENTIFIER]?:@"29e1dc32da"

#define kBuglyDebugKeyDict @{@"com.lynkco.customer":@"0e03c5af97",\
                            @"com.lynkco.customer.beta":@"83feb70889"}

#define kBuglyDebugKey kBuglyDebugKeyDict[kBUNDLEIDENTIFIER]?:@"0e03c5af97"


//友盟
#define kUMKeyDict @{@"com.lynkco.customer":@"596ef86f07fe656d4c000525",\
                    @"com.lynkco.customer.beta":@"5d6a19864ca35739c4000889"}
#define kUMKey kUMKeyDict[kBUNDLEIDENTIFIER]?:@"596ef86f07fe656d4c000525"


#define kUMChannelId @"App Store"
//微博
#define kWEIBO_KEYDict @{@"com.lynkco.customer":@"4216424891",\
                    @"com.lynkco.customer.beta":@"1636581281"}
#define kWEIBO_KEY kWEIBO_KEYDict[kBUNDLEIDENTIFIER]?:@"4216424891"

#define kWEIBO_SECRETDict @{@"com.lynkco.customer":@"37e225af71be4bb04420e3402089621a",\
                          @"com.lynkco.customer.beta":@"ec1f2a7189ac5bbfbc5c1c883073958c"}
#define kWEIBO_SECRET kWEIBO_SECRETDict[kBUNDLEIDENTIFIER]?:@"37e225af71be4bb04420e3402089621a"
//微信

#define kWEIXIN_KEYDict @{@"com.lynkco.customer":@"wxba0c1a3ba7c182a2",\
                        @"com.lynkco.customer.beta":@"wxba0c1a3ba7c182a2"}
#define kWEIXIN_KEY kWEIXIN_KEYDict[kBUNDLEIDENTIFIER]?:@"wxba0c1a3ba7c182a2"

#define kWEIXIN_SECRETDict @{@"com.lynkco.customer":@"279837c522e5970faf1f0b753565b255",\
                           @"com.lynkco.customer.beta":@"279837c522e5970faf1f0b753565b255"}
#define kWEIXIN_SECRET kWEIXIN_SECRETDict[kBUNDLEIDENTIFIER]?:@"279837c522e5970faf1f0b753565b255"
//QQ
#define QQ_KEYDict @{@"com.lynkco.customer":@"1106540778",\
                    @"com.lynkco.customer.beta":@"1109750893"}
#define QQ_KEY QQ_KEYDict[kBUNDLEIDENTIFIER]?:@"1106540778"

#define QQ_SECRETDict @{@"com.lynkco.customer":@"64xpPYWT43dnYgny",\
                      @"com.lynkco.customer.beta":@"lDXneCqYmACIq8S0"}
#define QQ_SECRET QQ_SECRETDict[kBUNDLEIDENTIFIER]?:@"64xpPYWT43dnYgny"


#define OSS_BUCKET_PRIVATE              @"lynk-app-sit"                           // bucket名称 bucketName
#define OSS_ACCESSKEY_ID                @"LTAI4FrAFvQtRD43cTYRN394"                              // 子账号id
#define OSS_SECRETKEY_ID                @"O0ega762qZysTZ2OroFs3yNNdCfVjG"                          // 子账号secret
#define OSS_ENDPOINT                    @"http://oss-cn-hangzhou.aliyuncs.com"      // 访问的阿里云endpoint
#define OSS_RESULT_URL                  @"https://lynk-app-sit.oss-cn-hangzhou.aliyuncs.com"
#define OSS_ROOTPATH_NAME [NSString stringWithFormat:@"%@-%@",@"lynkcoapp",[[GLNetworkEnvConfig sharedInstance] getCurEnvKey]]

#import "GLLYNKCommon.h"
#import "GLCustomColor.h"
#import "GLLynkCoColor.h"
#import "GLLynkCoFont.h"
#import "GLLynkCoSize.h"


#endif /* GLCommonStr_h */
