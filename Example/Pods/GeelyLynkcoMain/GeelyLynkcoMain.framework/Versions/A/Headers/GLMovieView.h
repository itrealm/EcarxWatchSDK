//
//  GLMovieView.h
//  StartAppWithLaunchMovie
//
//  Created by shuaishuai on 2019/9/6.
//  Copyright © 2019 shuaishuai. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class GLMovieView;
@protocol GLMovieViewProtocol <NSObject>
- (void)didPlayToEndTimeNotification:(GLMovieView *)movieView;
@end
@interface GLMovieView : UIView
//@property (nonatomic,strong) NSURL *movieURL;
@property(nonatomic,weak) id<GLMovieViewProtocol> delegate;
+ (instancetype)MovieView;
- (void)startMovieURL:(NSURL *)movieURL;
@end

NS_ASSUME_NONNULL_END
