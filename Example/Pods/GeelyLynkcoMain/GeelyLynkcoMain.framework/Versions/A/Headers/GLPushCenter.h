//
//  GLPushCenter.h
//  GLCoreKit-Example
//
//  Created by zhiyong.kuang on 2019/12/17.
//  Copyright © 2019年 zhiyong.kuang. All rights reserved.
//

#import <Foundation/Foundation.h>

UIKIT_EXTERN NSString* GLAliPushTypeIdentifier;
UIKIT_EXTERN NSString* GLJPushTypeIdentifier;


typedef void(^GLPushReceiveMessageBlock) (NSDictionary *userInfo);


@protocol GLPushCenterProtocol <NSObject>

@property(nonatomic,copy) NSString *deviceAlias;
@property(nonatomic,readonly) NSString *deviceId;
@property(nonatomic,readonly) NSString *typeIdentifier;
@property (nonatomic,copy)GLPushReceiveMessageBlock receiveRemoteNotification;


- (void)setupWithOptions:(NSDictionary *)launchingOptions result:(void(^)(BOOL success,id data))resultBlock;
- (void)registerDeviceToken:(NSData *)deviceToken;
- (void)setAlias:(NSString *)alias tags:(NSSet *)tags;
- (void)unsetAliasAndTags;
- (void)handleRemoteNotification:(NSDictionary *)remoteInfo;

@end




@interface GLPushCenter : NSObject
+ (instancetype)defaultCenter;

/**
 注册通知
 */
- (void)registerRemoteNotification;

- (void)registerObserver:(void(^)(NSDictionary* message,NSString* typeIdentifier))action;

/**
 启动SDK

 如同时支持极光,阿里,FCM三种: GLPushTypeJPush | GLPushTypeAliPush | GLPushTypeFCM

 @param launchingOptions 启动参数
 @param type 推送平台:GLPushType
 */
- (void)setupWithOptions:(NSDictionary *)launchingOptions config:(id<GLPushCenterProtocol>)config result:(void(^)(BOOL success,id data))resultBlock;

/**
 注册APNS设备token到推送平台

 @param deviceToken APNS设备token
 */
- (void)registerDeviceToken:(NSData *)deviceToken;

/**
 处理收到的 APNS 消息
 
 @param remoteInfo APNS 消息
 */
- (void)handleRemoteNotification:(NSDictionary *)remoteInfo;

/**
 获取设备ID

 @param type
 */
- (NSString *)deviceId:(NSString*)typeIdentifier;

/**
 设置别名和标签

 @param alias 别名
 @param tags  标签
 @param type 推送平台:GLPushType
 */
- (void)setAlias:(NSString *)alias tags:(NSSet *)tags forTypeIdentifier:(NSString*)typeIdentifier;

/**
 删除别名和标签
 @param typeIdentifier 推送平台
 */
- (void)unsetAliasAndTagsForTypeIdentifier:(NSString*)typeIdentifier;

/**
 删除所有别名和标签
 */
- (void)unsetAllAliasAndTags;

/**
 获取别名

 */
- (NSString *)deviceAliasForTypeIdentifier:(NSString*)typeIdentifier;

@end
