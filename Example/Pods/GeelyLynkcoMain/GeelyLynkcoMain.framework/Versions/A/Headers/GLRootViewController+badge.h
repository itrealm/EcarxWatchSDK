//
//  GLRootViewController+badge.h
//  GeelyLynkcoMain
//
//  Created by shuaishuai on 2019/9/23.
//

//#import "GLRootViewController.h"
#import <GeelyLynkcoMain/GLRootViewController.h>


NS_ASSUME_NONNULL_BEGIN

@interface GLRootViewController (badge)

- (void)showBadgeIndex:(NSUInteger)index withString:(NSString *)string;

- (void)hideBadgeIndex:(NSUInteger)index;

@end

NS_ASSUME_NONNULL_END
