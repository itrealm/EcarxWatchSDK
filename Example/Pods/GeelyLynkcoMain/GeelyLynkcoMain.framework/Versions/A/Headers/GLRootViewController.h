//
//  GLRootViewController.h
//  GeelyLynkcoMain
//
//  Created by shuaishuai on 2019/6/21.
//  Copyright © 2019 GEELY. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MAIN_HOST @"glMain"
#define MAIN_PATH @"get_main"


///同一个item下的双击tabbaritem事件
#define GLDoubleClickSameTabItemNotification @"GLDoubleClickSameTabItemNotification"
///同一个item下的单击tabbaritem事件（同一个item下的双击认为是无效的点击，这里除外）
#define GLClickSameTabItemNotification @"GLClickSameTabItemNotification"
///将要切换到其他item的单击事件
#define GLSwitchOtherTabItemNotification @"GLSwitchOtherTabItemNotification"

///单击tabbaritem事件,（同一个item下的双击认为是无效的点击，这里除外）
#define GLClickTabItemNotification @"GLClickTabItemNotification"

@interface GLRootViewController : UITabBarController

- (void)changeTab:(NSNumber *)index;


@end


