/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLSelectButton.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#import <UIKit/UIKit.h>
/** 选择按钮 */
@interface GLSelectButton : UIButton

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImageView *selectImageView;
@property (nonatomic, strong) UILabel *textLabel;

@property (nonatomic, getter=isSelectState) BOOL selectState;
@end
