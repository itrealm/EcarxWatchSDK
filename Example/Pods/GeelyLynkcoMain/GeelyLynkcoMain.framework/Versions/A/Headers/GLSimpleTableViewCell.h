/*
 * ------------------------------------------------------------------
 * Copyright  2017 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product: GeelyConsumer
 *   Module Name: GLSimpleTableViewCell.h
 *  Date Created: ___DATE___
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * ___DATE___
 * ------------------------------------------------------------------
 */

#import <UIKit/UIKit.h>

/** 显示简单信息的 cell */
@interface GLSimpleTableViewCell : UITableViewCell
/** 标题 */
@property (nonatomic, strong) NSString *title;
/** 副标题 */
@property (nonatomic, strong) NSString *subTitle;
/** 标题字体颜色 */
@property (nonatomic, strong) UIColor *titleColor;
/** 副标题字体颜色 */
@property (nonatomic, strong) UIColor *subTitleColor;
/** 标题字体 */
@property (nonatomic, strong) UIFont *titleFont;
/** 标题 */
@property (nonatomic, strong) UILabel *simpleTitleLabel;
/** Co币展示Label */
@property (nonatomic, strong) UILabel *coCoinLabel;
/** Co币数量 */
@property (nonatomic, strong) NSString *coCount;
/** 副标题 */
@property (nonatomic, strong) UILabel *subTitleLabel;
/** 右箭头 */
@property (nonatomic, strong) UIImageView *rightRoult;
/** 01图片的显示 */
@property (nonatomic, assign, getter=isShowImage) BOOL showImage;

@property (nonatomic, getter=isHiddenRightBoult) BOOL hiddenRightBoult;

@property (nonatomic, getter=isShowCoCoinLabel) BOOL showCoCoinLabel;
@end
