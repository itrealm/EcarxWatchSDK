//
//  GLWeexDebugViewController.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/22.
//  weex调试页面

#import <UIKit/UIKit.h>


static NSString *SWX_URL_KEY_1 = @"SWX_URL_KEY_1";
static NSString *SWX_URL_KEY_2 = @"SWX_URL_KEY_2";
static NSString *SWX_URL_KEY_3 = @"SWX_URL_KEY_3";
static NSString *SWX_URL_KEY_4 = @"SWX_URL_KEY_4";

@interface GLWeexDebugViewController : UIViewController

+ (instancetype)createrVC;

@end

@interface NSString (empty)
- (BOOL)isEmp;
@end
