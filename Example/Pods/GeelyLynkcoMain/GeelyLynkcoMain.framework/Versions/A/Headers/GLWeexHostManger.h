//
//  GLWeexHostManger.h
//  GeelyLynkcoMain
//
//  Created by shuaishuai on 2019/11/12.
//

#import <Foundation/Foundation.h>

@interface GLWeexHostManger : NSObject
@end

///获取当前环境的BASE_WEEX_ROOT_URL
FOUNDATION_EXTERN NSString *GL_BASE_WEEX_ROOT_URL(void);

///拼接当前环境下的j链接（先BASE_WEEX_ROOT_URL，再BASE_WEEX_ROOT_SEVICE_URL）
NSString *WEEX_URL(NSString *path);


