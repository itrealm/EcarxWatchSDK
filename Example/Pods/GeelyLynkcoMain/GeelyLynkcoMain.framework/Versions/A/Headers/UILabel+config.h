//
//  UILabel+config.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/8/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (config)

+ (UILabel *)gl_createUILabelWithTextColor:(UIColor *)textColor
                                      font:(UIFont *)font
                                 superView:(UIView *)superView;

+ (UILabel *)gl_createUILabelWithText:(NSString *)text
                            TextColor:(UIColor *)textColor
                                 font:(UIFont *)font
                            superView:(UIView *)superView;

+ (UILabel *)gl_createUILabelWithTextColor:(UIColor *)textColor
                                      font:(UIFont *)font
                                 alignment:(NSTextAlignment *)alignment
                                 superView:(UIView *)superView;

+ (UILabel *)gl_createUILabelWithText:(NSString *)text
                            TextColor:(UIColor *)textColor
                                 font:(UIFont *)font
                            alignment:(NSTextAlignment *)alignment
                            superView:(UIView *)superView;

@end

NS_ASSUME_NONNULL_END
