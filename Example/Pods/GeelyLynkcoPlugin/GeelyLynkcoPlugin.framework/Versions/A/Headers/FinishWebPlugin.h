//
//  SWCScanPlugin.h
//  GeelyLynkcoPlugin
//
//  Created by yang.duan on 2019/8/27.
//

#import <UIKit/UIKit.h>
#import <Cordova/CDVPlugin.h>


NS_ASSUME_NONNULL_BEGIN

@interface FinishWebPlugin : CDVPlugin
//使用时需要再config.xml中配置
/*
 <feature name="scan">
    <param name="ios-package" value="SWCScanPlugin" />
 </feature>
 */
@end

NS_ASSUME_NONNULL_END
