//
//  GLAVPlayerManager.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/9/21.
//

#import <Foundation/Foundation.h>
#import "ZFPlayerMediaPlayback.h"
#import <AVFoundation/AVFoundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLAVPlayerManager : NSObject <ZFPlayerMediaPlayback>

@property (nonatomic, strong, readonly) AVURLAsset *asset;
@property (nonatomic, strong, readonly) AVPlayerItem *playerItem;
@property (nonatomic, strong, readonly) AVPlayer *player;
@property (nonatomic, assign) NSTimeInterval timeRefreshInterval;
/// 视频请求头
@property (nonatomic, strong) NSDictionary *requestHeader;

@end

NS_ASSUME_NONNULL_END
