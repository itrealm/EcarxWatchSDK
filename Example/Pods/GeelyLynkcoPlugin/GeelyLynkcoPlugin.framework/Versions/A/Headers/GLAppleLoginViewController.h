//
//  GLAppleLoginView.h
//  AFNetworking
//
//  Created by 薛立恒 on 2020/4/3.
//

#import <UIKit/UIKit.h>
#import <AuthenticationServices/AuthenticationServices.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, GLAppleLoginResultType) {
    GLAppleLoginResultTypeSuccess,
    GLAppleLoginResultTypeCancel,
    GLAppleLoginResultTypeFailed,
    GLAppleLoginResultTypeInvalid,
    GLAppleLoginResultTypeNotHandle,
    GLAppleLoginResultTypeUnknownError,
};

typedef void(^GLAppleLoginCompletion)(GLAppleLoginResultType eventName, NSDictionary *params);

@interface GLAppleLoginViewController : UIViewController <ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding>

///苹果登录按钮
@property (nonatomic, strong) ASAuthorizationAppleIDButton *appleLoginBtn;
///苹果登录按钮Type
@property (nonatomic,assign) ASAuthorizationAppleIDButtonType appleButtonType;
///苹果登录按钮Style
@property (nonatomic,assign) ASAuthorizationAppleIDButtonStyle appleButtonStyle;
///苹果登录按钮圆角
@property (nonatomic,assign) CGFloat cornerRadius;
///苹果登录按钮的宽
@property (nonatomic,assign) NSInteger loginWidth;
///苹果登录按钮的高
@property (nonatomic,assign) NSInteger loginHeight;

@property (nonatomic,copy) GLAppleLoginCompletion completion;

- (void)resetConfig;

@end

NS_ASSUME_NONNULL_END
