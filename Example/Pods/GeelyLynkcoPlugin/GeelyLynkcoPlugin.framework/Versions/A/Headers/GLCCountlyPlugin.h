//
//  GLCCountlyPlugin.h
//  GeelyLynkcoPlugin
//
//  Created by yang.duan on 2019/8/22.
//  被废弃

#import <UIKit/UIKit.h>
#import <Cordova/CDVPlugin.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLCCountlyPlugin : CDVPlugin
//使用时需要再config.xml中配置
/*   <feature name="GLCCountlyPlugin">
 <param name="ios-package" value="GLCCountlyPlugin" />
 </feature>  */
@end

NS_ASSUME_NONNULL_END
