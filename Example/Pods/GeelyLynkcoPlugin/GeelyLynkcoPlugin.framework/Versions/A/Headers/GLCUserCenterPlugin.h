//
//  GLCUserCenterPlugin.h
//  GeelyLynkcoPlugin
//
//  Created by shuaishuai on 2019/8/16.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
//这个废弃，使用SWXUserSessionPlugin

NS_ASSUME_NONNULL_BEGIN

@interface GLCUserCenterPlugin : CDVPlugin

@end

NS_ASSUME_NONNULL_END
