//
//  GLCUserCenterPlugin.h
//  GeelyLynkcoPlugin
//
//  Created by shuaishuai on 2019/8/16.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLCarNetWorkPlugin : CDVPlugin

@end


@interface GLCordovaTestResultController : GLBaseViewController
@property(nonatomic,strong)NSString* result;

@end

NS_ASSUME_NONNULL_END
