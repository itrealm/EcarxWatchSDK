//
//  GLEnvironmentModule.h
//  GeelyLynkcoPlugin
//
//  Created by shuaishuai on 2019/8/22.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLEnvironmentModule : NSObject

@end

NS_ASSUME_NONNULL_END
