//
//  GLInstantMessageModule.h
//  GeelyLynkcoPlugin
//
//  Created by 侯炎辉 on 2019/11/14.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLInstantMessageModule : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
