//
//  GLLoadingView.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/9/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, GLLoadingType) {
    GLLoadingTypeKeep,
    GLLoadingTypeFadeOut,
};

@interface GLLoadingView : UIView

/// 默认是GLLoadingTypeKeep
@property (nonatomic, assign) GLLoadingType animType;

/// 默认是白色
@property (nonatomic, strong, null_resettable) UIColor *lineColor;

/// 设置环绕在周围的这个进度条的线宽
@property (nonatomic) CGFloat lineWidth;

/// 当停止之后是否隐藏
@property (nonatomic) BOOL hidesWhenStopped;

/// 动画的执行时长
@property (nonatomic, readwrite) NSTimeInterval duration;

/// 是否在动画
@property (nonatomic, assign, readonly, getter=isAnimating) BOOL animating;

/**
 *  开始动画
 */
- (void)startAnimating;

/**
 *  停止动画
 */
- (void)stopAnimating;

@end

NS_ASSUME_NONNULL_END
