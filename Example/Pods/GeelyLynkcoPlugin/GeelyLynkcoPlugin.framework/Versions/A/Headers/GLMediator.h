//
//  GLMediator.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/8/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLMediator : NSObject

+ (UIViewController *)currentViewController;
+ (void)jumpToPlayerVC:(id)data;

@end

NS_ASSUME_NONNULL_END
