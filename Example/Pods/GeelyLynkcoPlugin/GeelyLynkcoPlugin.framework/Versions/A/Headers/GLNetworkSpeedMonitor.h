//
//  GLNetworkSpeedMonitor.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/9/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString *const GLDownloadNetworkSpeedNotificationKey;
extern NSString *const GLUploadNetworkSpeedNotificationKey;
extern NSString *const GLNetworkSpeedNotificationKey;

@interface GLNetworkSpeedMonitor : NSObject

@property (nonatomic,copy,readonly) NSString *downloadNetworkSpeed;
@property (nonatomic,copy,readonly) NSString *uploadNetworkSpeed;

//开始监听网速
- (void)startNetworkSpeedMonitor;
//停止监听网速
- (void)stopNetworkSpeedMonitor;

@end

NS_ASSUME_NONNULL_END
