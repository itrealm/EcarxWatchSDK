//
//  GLPlayerViewController.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/9/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/** 视频状态 */
typedef NS_ENUM(NSInteger, GLPlayerPlayState) {
    GLPlayerPlayStateUnknowed,                         //默认的状态
    GLPlayerPlayStatePlaying,                          //正在播放
    GLPlayerPlayStatePaused,                           //暂停
    //    WXPlaybackStateStopped,
    GLPlayerPlayStateFinish,                           //结束播放
    GLPlayerPlayStateFailed,                           //播放失败
};

@interface GLPlayerViewController : UIViewController

/// 播放状态的回调
@property (nonatomic, copy) void (^playbackStateChanged)(GLPlayerPlayState state);
/// 需要播放的视频的URL
@property (nonatomic,strong) NSURL *src;
/// 控制播放状态
@property (nonatomic,assign) BOOL playStatus;
/// 视频的名称
@property (nonatomic,copy) NSString *videoTitle;
/// 是否自动播放
@property (nonatomic,assign) BOOL autoPlay;
/// 在还未播放视频的时候,默认显示的图片的URL   注意:若不传的话,会默认的取视频的首帧
@property (nonatomic,copy) NSString *poster;
/// 是否显示控制栏
@property (nonatomic,assign) BOOL controls;
/// 视频的总时长
@property (nonatomic,assign) CGFloat length;
/// 是否全屏
@property (nonatomic,assign) BOOL fullscreen;
/// 屏幕比例压缩方式
@property (nonatomic,copy) NSString *aspect;
/// 是否允许播放下一个视频
@property (nonatomic,assign) BOOL isAllowedPlayNext;
/// 竖屏的时候是否禁用滑动手势,默认为NO
@property (nonatomic,assign) BOOL disablePanGesture;

/// 重新设置配置
- (void)resetConfig;
/// 设置播放器暂停或者播放的状态
- (void)playerSelectedState:(BOOL)selected;

@end

NS_ASSUME_NONNULL_END
