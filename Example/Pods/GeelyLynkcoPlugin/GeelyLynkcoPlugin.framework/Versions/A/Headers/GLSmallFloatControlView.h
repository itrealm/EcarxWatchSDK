//
//  GLSmallFloatControlView.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/9/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLSmallFloatControlView : UIView

@property (nonatomic, copy, nullable) void(^closeClickCallback)(void);

@end

NS_ASSUME_NONNULL_END
