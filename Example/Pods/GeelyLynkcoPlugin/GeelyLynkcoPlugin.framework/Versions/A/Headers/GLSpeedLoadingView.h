//
//  GLSpeedLoadingView.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/9/19.
//

#import <UIKit/UIKit.h>
#import "GLLoadingView.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLSpeedLoadingView : UIView

@property (nonatomic, strong) GLLoadingView *loadingView;

@property (nonatomic, strong) UILabel *speedTextLabel;

/**
 *  开始动画
 */
- (void)startAnimating;

/**
 *  结束动画
 */
- (void)stopAnimating;


@end

NS_ASSUME_NONNULL_END
