//
//  GLVideoLoading.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/7/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLVideoLoading : UIView

@property (nonatomic,assign) CGFloat lineWidth;
@property (nonatomic,strong) UIColor *lineColor;

+ (instancetype)defaultLoading;
- (void)startLoading;
- (void)stopLoading;

@end

NS_ASSUME_NONNULL_END
