//
//  GLVideoPlayerView.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/7/8.
//

#import <UIKit/UIKit.h>
#import "GLVideoLoading.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, GLVideoPlayerViewEvent) {
    GLVideoPlayerViewEventBack,                 //返回
    GLVideoPlayerViewEventFullScreen,           //全屏
    GLVideoPlayerViewEventSmallScreen,          //竖屏
    GLVideoPlayerViewEventPlay,                 //播放
    GLVideoPlayerViewEventPause,                //暂停
    GLVideoPlayerViewEventNext,                 //下一集
    GLVideoPlayerViewEventSeek,                 //拖动进度
    GLVideoPlayerViewEventDefinition,           //切换清晰度
    GLVideoPlayerViewEventShare,                //分享
    GLVideoPlayerViewEventCast,                 //投屏
    GLVideoPlayerViewEventSpeedUp,              //加速播放
    GLVideoPlayerViewEventSpeedDown,            //停止加速播放
    GLVideoPlayerViewEventChangeCastDevice,     //切换投屏设备
    GLVideoPlayerViewEventStopCast,             //停止投屏
    GLVideoPlayerViewEventShadowStartPlay,      //开始播放按钮
};

@class GLVideoPlayerControlView;
@protocol GLVideoPlayerControlViewDelegate <NSObject>

- (void)playerView:(GLVideoPlayerControlView *)playerView event:(GLVideoPlayerViewEvent)event;

@end

@interface GLVideoPlayerControlView : UIView

@property (nonatomic,weak) id<GLVideoPlayerControlViewDelegate> delegate;
@property (nonatomic,assign) BOOL fullScreen;
@property (nonatomic,assign) BOOL playing;
@property (nonatomic,assign) BOOL hiddenView;
@property (nonatomic,assign) BOOL showShadowView;

@property (nonatomic,strong) UIView *navView;//顶部导航层
@property (nonatomic,strong) UIView *loadingView;//中间载入层
@property (nonatomic,strong) UIView *controlView;//底部控制层

//顶部导航层
@property (nonatomic,strong) UIButton *backButton;//返回箭头按钮
@property (nonatomic,strong) UILabel *titleLabel;//标题文案
@property (nonatomic,strong) UIButton *definitionButton;//清晰度按钮
@property (nonatomic,strong) UIButton *castButton;//投屏按钮

//中间载入层
@property (nonatomic,strong) UILabel *videoNameLabel;//加载中的label
@property (nonatomic,strong) GLVideoLoading *loadingIndicator;//加载中的动画控件

//底部控制层
@property (nonatomic,strong) UIButton *playButton;//控制暂停/播放的开关
@property (nonatomic,strong) UIButton *nextButton;//下一集按钮
@property (nonatomic,strong) UILabel *currentTimeLabel;//播放时间label
@property (nonatomic,strong) UISlider *progressSlider;//进度条
@property (nonatomic,strong) UILabel *totalTimeLabel;//总播放时间label
@property (nonatomic,strong) UIButton *fullScreenButton;//全屏按钮

//投屏
@property (nonatomic,strong) UIView *castControlView;//投屏时显示的view
@property (nonatomic,strong) UIButton *changeCastDeviceButton;//切换设备按钮
@property (nonatomic,strong) UIButton *stopCastButton;//结束播放按钮
@property (nonatomic,strong) UILabel *castStateLabel;//投屏状态label

//遮罩
@property (nonatomic,strong) UIView *shadowView;
@property (nonatomic,strong) UIButton *startPlayButton;
@property (nonatomic,strong) UIImageView *shadowImageView;
@property (nonatomic,strong) UILabel *shadowTimeLabel;

//这个主要是提供给内嵌的视频组件
- (void)customForSmallScreen;

- (void)showLoadingView:(NSString *)tips animation:(BOOL)animate;
- (void)hideLoadingView:(NSString *)tips animation:(BOOL)animate;
//- (void)updateCastState:(


@end

NS_ASSUME_NONNULL_END
