//
//  GLVideoPlayerUtils.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/7/18.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLVideoPlayerUtils : NSObject

@property (nonatomic,assign) float volume;

+ (instancetype)sharedUtils;
+ (float)getSsystemVolume;
+ (void)setSystemVolume:(float)volume;

+ (void)setSystemBrightness:(CGFloat)brightness;

@end

NS_ASSUME_NONNULL_END
