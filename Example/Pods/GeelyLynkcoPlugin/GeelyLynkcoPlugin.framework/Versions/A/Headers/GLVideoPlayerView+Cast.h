//
//  GLVideoPlayerView+Cast.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/7/18.
//

#import "GLVideoPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLVideoPlayerView (Cast)

- (void)setUpCast;

//显示投屏设备选取界面
- (void)showCastDevicesView;

//切换设备
- (void)changeCastDevice;

//停止投屏
- (void)stopCast;

//根据投屏状态更新UI
//- (void)updateCastState:(

@end

NS_ASSUME_NONNULL_END
