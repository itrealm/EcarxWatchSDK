//
//  GLVideoPlayerView+Gesture.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/7/18.
//

#import "GLVideoPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLVideoPlayerView (Gesture) <UIGestureRecognizerDelegate>

- (void)setUpGesture;
- (void)disableGesture;

@end

NS_ASSUME_NONNULL_END
