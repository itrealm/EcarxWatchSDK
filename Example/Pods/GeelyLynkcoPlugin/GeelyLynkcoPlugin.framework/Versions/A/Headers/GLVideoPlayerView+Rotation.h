//
//  GLVideoPlayerView+Rotation.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/7/17.
//

#import "GLVideoPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLVideoPlayerView (Rotation)

- (void)setUpRotation;
- (void)disableRotation;
- (void)setOrientation:(UIInterfaceOrientation)orientation animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
