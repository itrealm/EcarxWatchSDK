//
//  GLVideoPlayerView+getFistFrame.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/9/3.
//

#import "GLVideoPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface GLVideoPlayerView (getFistFrame)

- (UIImage*)getVideoPreViewImage:(NSURL *)path;

@end

NS_ASSUME_NONNULL_END
