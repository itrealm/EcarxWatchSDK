//
//  GLVideoPlayerView.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/7/16.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GLVideoPlayerControlView.h"

NS_ASSUME_NONNULL_BEGIN

/** 视频手势类型 */
typedef NS_ENUM(NSInteger, GLVideoPlayerControlViewGestureType) {
    GLVideoPlayerControlViewGestureTypeNone = 0,       //默认
    GLVideoPlayerControlViewGestureTyperBrightness,    //亮度
    GLVideoPlayerControlViewGestureTypeVolume,         //音量
    GLVideoPlayerControlViewGestureTypeProgress,       //进度
    GLVideoPlayerControlViewGestureTypeError,          //错误
};

/** 视频状态 */
typedef NS_ENUM(NSInteger, GLVideoPlayerState) {
    GLVideoPlayerStatePlaying,                          //正在播放
    GLVideoPlayerStatePaused,                           //暂停
    //    WXPlaybackStateStopped,
    GLVideoPlayerStateFinish,                           //结束播放
    GLVideoPlayerStateFailed,                           //播放失败
};

@interface GLVideoPlayerView : UIView
/** 视频状态回调 */
@property (nonatomic, copy) void (^playbackStateChanged)(GLVideoPlayerState state);
/** 播放按钮点击之后的回调 */
@property (nonatomic, copy) void (^posterClickHandle)(void);

#pragma mark - weex需要传入层级关系
@property (nonatomic,weak) UIView *superView;                               //视频组件的父视图
@property (nonatomic,weak) UIView *rootView;                                //视频组件的根视图
@property (nonatomic,strong) UIView *containerView;                         //视频组件在点击全屏之后的容器view

#pragma mark - 视频的宽高(主要是用作小视频模式中适配或者说缩放使用)
@property (nonatomic,assign) CGFloat smallVideoWidth;                       //视频宽
@property (nonatomic,assign) CGFloat smallVideoHeight;                      //视频高

#pragma mark - weex视频中要传入的参数
/**
 是否是自动播放
 注意:如果是自动播放的话,只针对非小视频的情况,如果是小视频的情况下,没有自动播放的概念
 */
@property (nonatomic,assign) BOOL isAutoPlay;
/**
 是否显示视频底部的操作栏,包括开始暂停按钮,全屏按钮,播放进度
 */
@property (nonatomic,assign) BOOL showControlView;
/**
 是否是小视频
 注意:如果是小视频的话,是不能自动播放的,需要用户点击播放按钮,才会播放;并且全屏播放之后,不可以调整进度,可以参考微信朋友圈
 */
@property (nonatomic,assign) BOOL showSmallVideo;
/**
 视频的URL
 */
@property (nonatomic,strong) NSString *videoUrl;
/**
 视频的首帧图
 如果该值不传的话,会默认取视频的首帧图,如果传入的话,会优先使用传入的图
 */
@property (nonatomic,strong) NSString *posterURL;
/**
 视频的清晰图,暂时还不支持切换清晰度
 */
@property (nonatomic,strong) NSString *definition;
/**
 视频播放的总时长
 在视频不是自动播放的时候,显示在右下角的视频总长度
 */
@property (nonatomic,strong) NSString *shadowTime;

#pragma mark - 视频相关
@property (nonatomic,strong) AVPlayer *player;
@property (nonatomic,strong) AVPlayerLayer *playerLayer;
@property (nonatomic,assign,readonly) BOOL isPlaying;                           //视频是否正在播放

@property (nonatomic,assign) CGSize portraintSize;                              //视频在组件中的大小,根据weex容器的大小决定
@property (nonatomic,assign) CGSize landscapeSize;                              //视频在全屏之后的大小,这里是指非小视频的情况下

@property (nonatomic,strong) GLVideoPlayerControlView *controlView;             //视频底部的操作view
@property (nonatomic,assign) UIInterfaceOrientation lastOrientation;            //上一个屏幕横竖屏的模式
@property (nonatomic,assign) CGPoint originalLocation;
@property (nonatomic,assign) GLVideoPlayerControlViewGestureType gestureType;

/**
 开始加载视频
 */
- (void)loadVideo:(NSString *)videoId;


/**
 小视频模式下,点击全屏开始播放的时候,点击屏幕会调用这个方法
 */
- (void)dismissSmallVideoFullScreen;

@end

NS_ASSUME_NONNULL_END
