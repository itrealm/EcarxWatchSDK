//
//  GLVolumeBrightnessView.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/9/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, GLVolumeBrightnessType) {
    GLVolumeBrightnessTypeVolume,       // volume
    GLVolumeBrightnessTypeumeBrightness // brightness
};

@interface GLVolumeBrightnessView : UIView

@property (nonatomic, assign, readonly) GLVolumeBrightnessType volumeBrightnessType;
@property (nonatomic, strong, readonly) UIProgressView *progressView;
@property (nonatomic, strong, readonly) UIImageView *iconImageView;

- (void)updateProgress:(CGFloat)progress withVolumeBrightnessType:(GLVolumeBrightnessType)volumeBrightnessType;

/// 添加系统音量view
- (void)addSystemVolumeView;

/// 移除系统音量view
- (void)removeSystemVolumeView;

@end

NS_ASSUME_NONNULL_END
