//
//  GLWCountlyModule.h
//  GeelyLynkcoPlugin
//
//  Created by shuaishuai on 2019/10/18.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLWCountlyModule : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
