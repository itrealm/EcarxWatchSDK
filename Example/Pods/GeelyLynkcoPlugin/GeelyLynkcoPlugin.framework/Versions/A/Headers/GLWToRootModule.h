//
//  GLWToRootModule.h
//  GeelyLynkcoPlugin
//
//  Created by shuaishuai on 2019/8/20.
// 回到APP首页插件

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLWToRootModule : NSObject <WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
