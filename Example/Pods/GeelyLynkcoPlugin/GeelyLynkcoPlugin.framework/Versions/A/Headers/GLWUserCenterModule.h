//
//  GLWUserCenterModule.h
//  GeelyLynkcoPlugin
//
//  Created by shuaishuai on 2019/8/16.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLWUserCenterModule : NSObject <WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
