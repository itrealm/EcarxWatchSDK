//
//  NSString+Custom.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/8/16.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Custom)

+ (instancetype)timeFormatFromSeconds:(NSTimeInterval)seconds;

@end

NS_ASSUME_NONNULL_END
