//
//  NavigatePlugin.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/9/18.
//

#import <Cordova/CDVPlugin.h>

NS_ASSUME_NONNULL_BEGIN

@interface NavigatePlugin : CDVPlugin

@end

NS_ASSUME_NONNULL_END
