//
//  SWCCountlyPlugin.h
//  GeelyLynkcoPlugin
//
//  Created by yang.duan on 2019/8/27.
//

#import <UIKit/UIKit.h>
#import <Cordova/CDVPlugin.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWCCountlyPlugin : CDVPlugin
//使用时需要再config.xml中配置
/*
 <feature name="countly">
    <param name="ios-package" value="SWCCountlyPlugin" />
 </feature>
 */
@end

NS_ASSUME_NONNULL_END
