//
//  SWCSharePayLoginPlugin.h
//  GeelyLynkcoPlugin
//
//  Created by yang.duan on 2019/8/27.
//

#import <UIKit/UIKit.h>
#import <Cordova/CDVPlugin.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWCSharePayLoginPlugin : CDVPlugin
//使用时需要再config.xml中配置
/*
 <feature name="thirdExtention">
     <param name="ios-package" value="SWCSharePayLoginPlugin" />
 </feature>
 */
@end

NS_ASSUME_NONNULL_END
