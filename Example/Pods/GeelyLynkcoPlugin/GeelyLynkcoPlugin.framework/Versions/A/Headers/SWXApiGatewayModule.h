//
//  SWXApiGatewayModule.h
//  AliAPIDemo
//
//  Created by shuaishuai on 2019/7/11.
//  Copyright © 2019 shuaishuai. All rights reserved.
// 阿里云网关

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXApiGatewayModule : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
