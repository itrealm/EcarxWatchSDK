//
//  SWXApiOSSModule.h
//  GeelyLynkcoPlugin
//
//  Created by yang.duan on 2019/8/20.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXApiOSSModule : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
