//
//  SWXUserSessionPlugin.h
//  CordovaPlugin
//
//  Created by shuaishuai on 2019/8/23.
//

#import "CDVPlugin.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWXUserSessionPlugin : CDVPlugin

@end

NS_ASSUME_NONNULL_END
