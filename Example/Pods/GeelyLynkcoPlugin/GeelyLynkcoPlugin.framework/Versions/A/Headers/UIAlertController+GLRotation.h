//
//  UIAlertController+GLRotation.h
//  AFNetworking
//
//  Created by 薛立恒 on 2020/2/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIAlertController (GLRotation)

- (BOOL)shouldAutorotate;

@end

NS_ASSUME_NONNULL_END
