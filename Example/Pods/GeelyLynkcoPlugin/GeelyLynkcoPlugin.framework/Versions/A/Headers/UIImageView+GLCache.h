//
//  UIImageView+GLCache.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/9/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^GLDownLoadDataCallBack)(NSData *data, NSError *error);
typedef void (^GLDownloadProgressBlock)(unsigned long long total, unsigned long long current);

@interface GLImageDownloader : NSObject<NSURLSessionDownloadDelegate>

@property (nonatomic,strong) NSURLSession *session;
@property (nonatomic,strong) NSURLSessionDownloadTask *task;

@property (nonatomic,assign) unsigned long long totalLength;
@property (nonatomic,assign) unsigned long long currentLength;

@property (nonatomic,copy) GLDownLoadDataCallBack callBackOnFinished;
@property (nonatomic,copy) GLDownloadProgressBlock progressBlock;

- (void)startDownloadImageWithUrl:(NSString *)url progress:(GLDownloadProgressBlock)progress finished:(GLDownLoadDataCallBack)finished;

@end

typedef void (^GLImageBlock)(UIImage *image);

@interface UIImageView (GLCache)

/**
 *当图片下载完成时候的block
 */
@property (nonatomic,copy) GLImageBlock completion;
/**
 *图片下载器
 */
@property (nonatomic,strong) GLImageDownloader *imageDownloader;
/**
 *如果图片下载失败之后,重新下载的次数,默认为2次
 */
@property (nonatomic,assign) NSUInteger attemptToReloadTimesForFailedURL;
/**
 *是否会自动的去适配View的尺寸,默认是NO
 *如果YES的话,只有在图片下载成功之后才会剪裁图片
 */
@property (nonatomic,assign) BOOL shouldAutoClipImageToViewSize;

/**
 设置image的URL和占位图的名字

 @param url 网络图片地址
 @param placeholderImageName 占位图的名字
 */
- (void)setImageWithURLString:(NSString *)url placeholderImageName:(NSString *)placeholderImageName;

/**
 设置image的URL和占位图image

 @param url 网络图片地址
 @param placeholderImage 占位图的image
 */
- (void)setImageWithURLString:(NSString *)url placeholder:(UIImage *)placeholderImage;

/**
 设置image的URL和占位图image,并且下载是异步的而且做了缓存

 @param url 网络图片地址
 @param placeholerImage 占位图的image
 @param completion 操作成功之后的回调
 */
- (void)setImageWithURLString:(NSString *)url placeholder:(UIImage *)placeholerImage completion:(void (^)(UIImage *image))completion;

/**
 设置image的URL和占位图名,并且下载是异步的而且做了缓存

 @param url 网络图片地址
 @param placeholderImageName 占位图名
 @param completion 操作成功之后的回调
 */
- (void)setImageWithURLString:(NSString *)url placeholderImageName:(NSString *)placeholderImageName completion:(void (^)(UIImage *image))completion;

@end

NS_ASSUME_NONNULL_END
