//
//  GLCountlyTool.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/8/7.
//

#import <Foundation/Foundation.h>
#import <Countly/Countly.h>
//#import "Countly.h"

//NS_ASSUME_NONNULL_BEGIN
static inline BOOL GLCountlyToolIsObjectEmpty(id obj) { // 空对象
    return obj == nil || [obj isEqual:[NSNull null]] || ([obj respondsToSelector:@selector(length)] && [(NSData *) obj length] == 0) ||
    ([obj respondsToSelector:@selector(count)] && [(NSArray *) obj count] == 0);
}

static inline BOOL GLCountlyToolIsStringEmpty(NSString *string) { // 空字符串
    return string == nil || [string isEqual:[NSNull null]] || string.length == 0 || [string isEqualToString:@"<null>"] ||
    [string isEqualToString:@"(null)"];
}

@interface GLCountlyTool : NSObject

@property (nonatomic, readonly)CountlyConfig *glConfig;

@property (nonatomic, assign)BOOL isInitialized;

+ (instancetype)shareInstance;//单例构造方法

//- (NSString *)featchAppkey;
//
//- (NSString *)featchServelUrl;
//
//- (BOOL)featchIsinitalized;

- (NSString *)featchDeviceID;

///默认会从GLNetworkEnvConfigc类中配置GL_COUNTLY_APP_SERVERURL和GL_COUNTLY_APP_KEY
- (void)defaultConfigCountly;
//- (void)configCountly;
- (void)configCountlyServerUrl:(NSString *)serverUrl appKey:(NSString *)appKey;
- (void)configCountlyServerUrl:(NSString *)serverUrl appKey:(NSString *)appKey deviceId:(NSString *)deviceId isReportCrash:(BOOL)isReportCrash;


/**上报无参数的简单事件*/
- (void)recordEvent:(NSString *)key;
/**上报携带次数count的无参数事件*/
- (void)recordEvent:(NSString *)key count:(NSUInteger)count;
/**上报携带次数count+总数sum的无参数事件*/
- (void)recordEvent:(NSString *)key count:(NSUInteger)count sum:(CGFloat)sum;
/**带参数的事件上报*/
- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation;
/**带参数+count的事件上报*/
- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(NSUInteger)count;
/**带参数+count+sum的事件上报*/
- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(NSUInteger)count sum:(CGFloat)sum;
/**带参数+count+sum+duration的事件上报*/
- (void)recordEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(NSUInteger)count sum:(CGFloat)sum duration:(NSTimeInterval)duration;


/**计时事件开始*/
- (void)startEvent:(NSString *)key;
/**计时事件结束*/
- (void)endEvent:(NSString *)key;
/**携带参数的计时事件结束*/
- (void)endEvent:(NSString *)key segmentation:(NSDictionary *)segmentation count:(NSUInteger)count sum:(CGFloat)sum;

/**同时设置用户&自定义属性*/
- (void)initUserData:(NSDictionary *)data customData:(NSDictionary *)customData;

/**设置定位数据*/
- (void)initCountryCode:(NSString *)countryCode city:(NSString *)city location:(NSString *)location ipAddress:(NSString *)ipAddress;

/**控制台日志打印开关*/
- (void)loggingEnabled:(BOOL)enabled;
/**打印异常*/
- (void)logException:(id)ex;
///**开启异常上报*/
//- (void)enableCrashReporting;

- (void)beginSession;


@end

//NS_ASSUME_NONNULL_END
