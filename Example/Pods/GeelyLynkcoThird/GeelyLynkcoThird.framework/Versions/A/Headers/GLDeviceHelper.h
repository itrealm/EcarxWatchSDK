//
//  GLDeviceHelper.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/8/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLDeviceHelper : NSObject

+ (NSString *)featchDeviceID;

+ (void)changeDeviceId:(NSString *)deviceId;

+ (NSString *)featchDeviceType;
/**
 设置服务器的最新时间
 */
+ (void)pushServerTime:(long)serverTime;

/**
 拉取设备当前时间戳
 */
+ (long)pullRealDeviceTime;
/** 返回设备UUID */
+ (NSString *)deviceUUID;
/**
 返回当前的网络状态
 */
+ (NSString *)netWorkStateStr;

@end

NS_ASSUME_NONNULL_END
