//
//  GLLynkcoThirdRouter.h
//  GeelyLynkcoThird
//
//  Created by shuaishuai on 2019/8/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLLynkcoThirdRouter : NSObject

/*
 1. 初始化api网关配置：
        路由地址 ://lynkcoThird/apiGatewayConfig
        参数：{
                appKey:@"", //api网关APPkey
                appSecret:@"", //api网关appSecret
                isDuplexClient:@"", //配置的是单通道请求还是双通道，如果yes：配置双通道；NO：配置单通道
             }
 
 
 */

@end

NS_ASSUME_NONNULL_END
