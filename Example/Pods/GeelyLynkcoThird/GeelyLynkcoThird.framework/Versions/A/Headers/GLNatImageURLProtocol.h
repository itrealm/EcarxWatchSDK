//
//  SWXHooliURLProtocol.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/7/22.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

//保存到本地的图片、视频等路径使用以SWX_STATIC_PREFIXPATH开头， 这里使用了nat开头为了与nat库保持一致


@interface GLNatImageURLProtocol : NSURLProtocol

@end


