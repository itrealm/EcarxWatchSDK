//
//  GLOSSWrapper.h
//  GeelyLynkcoThird
//
//  Created by yang.duan on 2019/8/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^GLOSSUploadResultBlock)(BOOL success, id result);
typedef void(^GLOSSUploadProgressBlock)(CGFloat progress);
typedef void(^GLOSSUploadSuccessBlock)(id successResult);
typedef void(^GLOSSUploadFailureBlock)(NSError *failureResult);

static inline BOOL GLOSSWrapperIsObjectEmpty(id obj) { // 空对象
    return obj == nil || [obj isEqual:[NSNull null]] || ([obj respondsToSelector:@selector(length)] && [(NSData *) obj length] == 0) ||
    ([obj respondsToSelector:@selector(count)] && [(NSArray *) obj count] == 0);
}
static inline BOOL GLOSSWrapperIsStringEmpty(NSString *string) { // 空字符串
    return string == nil || [string isEqual:[NSNull null]] || string.length == 0 || [string isEqualToString:@"<null>"] ||
    [string isEqualToString:@"(null)"];
}
@interface GLOSSWrapper : NSObject

@property (nonatomic, readonly) NSString *bucketName;
@property (nonatomic, readonly) NSString *endpoint;
@property (nonatomic, readonly) NSString *accessKeyId;
@property (nonatomic, readonly) NSString *secretKeyId;
@property (nonatomic, readonly) NSString *rootPath;
@property (nonatomic, readonly) NSString *cdnHost;



+ (instancetype)sharedManager;

//self.cdnHost = params[@"cdnHost"]?:@"";
//self.bucketName = params[@"bucketName"]?:@"";
//self.endpoint = params[@"endpoint"]?:@"";
//self.accessKeyId = params[@"accessKeyId"]?:@"";
//self.secretKeyId = params[@"secretKeyId"]?:@"";
//self.rootPath = params[@"rootPath"]?:@"";

- (void)configOSSAPI:(NSDictionary *)params;
/**
 为weex提供的接口

 @param uploadFileUrl 上传图片的地址
 @param resultBlock 上传结果
 */
- (void)ossUploadImage:(NSString *)uploadFileUrl path:(NSString *)path result:(GLOSSUploadResultBlock)resultBlock;

/**
 为weex提供的接口
 
 @param resultBlock 上传结果
 */
- (void)ossUploadImageDatastr:(NSString *)imageDataStr path:(NSString *)path result:(GLOSSUploadResultBlock)resultBlock;


///**
// upload image asynchronously 普通的上传
// 
// @param filePath 需要带后缀比如.MP4、.png、.jpg
// @param success success block
// @param failure failure block
// */


- (void)asyncPutFile:(NSString *)filePath
                path:(NSString *)path
             success:(GLOSSUploadSuccessBlock)success
             failure:(GLOSSUploadFailureBlock)failure
            progress:(GLOSSUploadProgressBlock)progress;


- (void)asyncPutImage:(UIImage *)image
                 path:(NSString *)path
              success:(GLOSSUploadSuccessBlock)success
              failure:(GLOSSUploadFailureBlock)failure
             progress:(GLOSSUploadProgressBlock)progress;



//fileType为视频后缀，比如.mp4、.jpg、.png等
- (void)asyncPutData:(NSData *)data
            fileType:(NSString *)fileType
                path:(NSString *)path
             success:(GLOSSUploadSuccessBlock)success
             failure:(GLOSSUploadFailureBlock)failure
            progress:(GLOSSUploadProgressBlock)progress;






- (void)asyncUploadMapInfo:(NSDictionary *)infoDic
                   success:(GLOSSUploadSuccessBlock)success
                   failure:(GLOSSUploadFailureBlock)failure
                  progress:(GLOSSUploadProgressBlock)progress __attribute__((deprecated("已弃用.")));


@end

NS_ASSUME_NONNULL_END
