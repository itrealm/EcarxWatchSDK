//
//  GLPayModule.h
//  GeelyConsumerThird
//
//  Created by yang.duan on 2019/6/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSUInteger, PayType)
{
    GLPayModuleWXPay       = 1,//微信支付
    GLPayModuleBankUnion   = 2, //银联支付
    GLPayModuleAliPay      = 3//支付宝支付
    
};
/**
 返回支付结果

 @param success 支付是否成功
 @param message 返回支付结果相关信息
 */
typedef void(^PayResult)(BOOL success, id message);

@interface GLPayModule : NSObject
+ (id)shareInstance;//单例构造方法
/**
 d连连支付接口

 @param params 支付传入参数实例
 
 @{@"vc":vc, 调用该接口的控制器页面
   @"param":@{"valid_order" : 38,  订单有效时间 （不是必填）
 "info_order" : "测试精品伞",   订单描述 （不是必填）
 "wechat_app_id" : "wxba0c1a3ba7c182a2",微信 app_id（不是必填）
 "name_goods" : "测试精品伞", 商品名称 s（必填）
 "notify_url" : "https:\/\/cephomeuat.lynkco.com\/api\/front\/mpay\/lianlianPay\/resultNotify", 服务器异步通 知地址 （是必填）
 "sign" : "ZNVSkSBRC4FcUMpRDQFLhM1ti0QPCIas4e4c5P3+GVqt6WZTQFDaJqoaf0e5S89P5cy6FrcPt0tR5PwT9ESRMFU582j6l0nCrj\/VtASQdfOWM\/hOvM4dLDDB12rEGiXeAuaxQvWB39wRhObJgqId\/Eb13Bpb1\/PtGEcWAPQ9xmoIwUpaF3EPlAjIpWG\/gFNqnPcNbhflDQZQyyXxuo6YYzdZllEn3wPoC4x+PRzDGiMUEzscnRPbDLsqTqhQWpQPVLiO1p3Yc5fJvM6IavK4GGmnzRMDEAhZvo\/1svzZtYYMG7KEzMxsLfGOt9Lmhw\/oG+ar73l+j2Ckw+pS\/DBdHQ==",  签名（是必填）
 "oid_partner" : "201708230000825004",  用 户 所 属 商 户 号（是必填）
 "user_id" : "104363", 用户唯一编号（是必填）
 "dt_order" : "20190622152804", 商户订单时间（是必填）
 "risk_item" : "{\"frms_ware_category\":\"4003\",\"user_info_mercht_userno\":\"104363\",\"user_info_dt_register\":\"20170601120000\",\"user_info_bind_phone\":\"13754319137\",\"goods_count\":\"1\",\"delivery_addr_province\":\"330000\",\"delivery_addr_city\":\"330100\",\"delivery_phone\":\"13754319137\",\"logistics_mode\":\"2\",\"delivery_cycle\":\"12h\"}", 风险控制参数（是必填）
 "pay_type" : "Y",  支付方式 （是必填）（Y为微信支付，2为银联支付）
 "shareing_data" : "", 分账信息数据（不是必填）
 "money_order" : "500.00", 交易金额（是必填）
 "sign_type" : "RSA", 签名方式（是必填，必须用RSA）
 "no_order" : "1931742000000368194", 商户唯一订单 号（是必填）
 "busi_partner" : "109001" 商户业务类型（是必填） （虚拟商品销售:101001 实物商品销售:109001）},
 @"payType":@"" 支付方式PayType 类型

 }

 */
- (void)LLPayWithParams:(NSDictionary *)params result:(PayResult)result;

@end

NS_ASSUME_NONNULL_END
