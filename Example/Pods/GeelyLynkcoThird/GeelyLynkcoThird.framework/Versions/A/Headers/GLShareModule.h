//
//  GLShareModule.h
//  GeelyConsumerMain
//
//  Created by 朱伟特 on 2018/7/11.
//  Copyright © 2018年 朱伟特. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GLSharePayLoginConfigration.h"

@class GLShareModel;

typedef NS_ENUM(NSUInteger, GLShareMessageType){
    GLMessageAuto         = 0,//自动适配类型，视传入的参数来决定
    GLMessageText         = 1,//纯文本分享
    GLMessageImage        = 2,//纯图片分享
    GLMessagePage         = 3,//网页分享
    GLMessageTextAndImage = 4,//图文混排（微博）
    GLMessageMusic        = 5,//音乐分享
    GLMessageVideo        = 6 //视频分享
};
typedef NS_ENUM(NSUInteger, GLSharePlatform){
    GLPlatformUndefine          = 0,
    GLPlatformWX_Session    = 1,//分享到微信会话
    GLPlatformWX_TimeLine   = 2,//分享到微信朋友圈
    GLPlatformSina          = 3,//分享到新浪微博
    GLPlatformQQ            = 4, //分享到QQ
    GLPlatformQQZone          = 5, //分享到qq空间
};

typedef void(^ShareResult)(BOOL success, id shareResult);//分享回调

#pragma mark -------------------------------------------------------------------

@interface GLShareModel : NSObject<NSCopying>

#warning content必传项
@property (nonatomic, copy) NSString * content;//内容（description）,必传项

@property (nonatomic, strong) id image;//图片（本地图片，可以理解为缩略图）

@property (nonatomic, strong) id thumImage;//缩略图（UIImage或者NSData类型，或者image_url）

@property (nonatomic, copy) NSString * url;//网页链接

@property (nonatomic, copy) NSString * title;//标题

@property (nonatomic, copy) NSString * imageURL;//分享图片链接

@property (nonatomic, copy) NSString * musicURL;//音乐链接

@property (nonatomic, copy) NSString * videoURL;//视频链接

@property (nonatomic, strong) NSArray *shareImageArr;
@end

#pragma mark -------------------------------------------------------------------

@interface GLShareModule : NSObject

@property (nonatomic, assign) BOOL wxInstalled;

@property (nonatomic, assign) BOOL weiboInstalled;

@property (nonatomic, assign) BOOL QQInstalled;

+ (id)shareInstance;

/**
 分享到系统自带分享ui

 @param model model是分享内容
 @param messageType 分享的消息类型
 @param controller 分享页面的controller
 @param result 分享结果
 */
- (void)shareWithModel:(GLShareModel *)model messageType:(GLShareMessageType)messageType viewController:(UIViewController *)controller result:(ShareResult)result;//分享到系统自带分享控件平台，参数model是分享内容，messageType是分享的消息类型，controller是分享页面的controller，result分享之后的回调,使用Mob分享的时候model的image参数必须要传

/**
 分享到各个平台

 @param model model是分享内容
 @param messageType 分享的消息类型
 @param platform 分享到哪个平台
 @param controller 分享页面的controller
 @param result 请求结果
 */
- (void)shareWithModel:(GLShareModel *)model messageType:(GLShareMessageType)messageType platform:(GLSharePlatform)platform viewController:(UIViewController *)controller result:(ShareResult)result;

+ (BOOL)handleOpenURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;//分享scheme跳转。在GLSchemeBackHandler中实现

+ (BOOL)handleOpenURL:(NSURL *)url options:(NSDictionary *)options;//分享scheme跳转。在GLSchemeBackHandler中实现

@end
