//
//  GLSharePayLoginConfigration.h
//  GeelyConsumerThird
//
//  Created by yang.duan on 2019/6/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLSharePayLoginConfigration : NSObject

@property (nonatomic, strong) NSArray * platformArray;//自定义平台数组，在使用系统自带的分享控件时设置

+ (id)shareInstance;//单例构造方法

//- (void)startInit;//初始化方法

/**
 初始化第三方

 @param params {
 wexinappId: wexinappId,
 weixinappKey: weixinappKey,
 weiboId: weiboId,
 weiboKey: weiboKey,
 weiboUrl: weiboUrl,
 qqzoneId: qqzoneId,
 qqzoneKey: qqzoneKey
 }
 */
- (void)initThirdExtentionSdkWithParams:(NSDictionary *)params;
@end

NS_ASSUME_NONNULL_END
