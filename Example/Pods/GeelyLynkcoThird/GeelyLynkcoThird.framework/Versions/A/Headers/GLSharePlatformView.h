//
//  GLSharePlatformView.h
//  GeelyConsumerThird
//
//  Created by yang.duan on 2019/6/4.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN


typedef void(^GLShareViewCnacelBlock)();

typedef void (^GLSharePlatformViewBlock) (NSInteger platformBlock);

@interface GLSharePlatformView : UIView

@property (nonatomic, strong) NSString* title;
@property (nonatomic, readonly) UIView* headerView;

@property (nonatomic, copy) GLShareViewCnacelBlock cancelBlock;

@property (nonatomic, copy) GLSharePlatformViewBlock clickPlatform;

-(void)updateMenuDatas:(NSArray*)datas;

@end

NS_ASSUME_NONNULL_END
