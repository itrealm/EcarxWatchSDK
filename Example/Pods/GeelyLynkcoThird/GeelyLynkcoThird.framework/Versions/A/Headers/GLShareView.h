//
//  GLShareView.h
//  GeelyConsumerThird
//
//  Created by yang.duan on 2019/6/4.
//

#import <UIKit/UIKit.h>
@class GLShareModel;
#import "GLShareModule.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^clickPlatform) (void);

typedef void(^GLShareViewCallBackBlock)(BOOL success,NSInteger platForm);

@interface GLShareView : UIView

//@property (nonatomic, strong) NSDictionary *parames;

@property (nonatomic, copy) clickPlatform clickplatBlock;//无参无返回值

@property (nonatomic, copy) GLShareViewCallBackBlock result;//无参无返回值

@property (nonatomic, strong) GLShareModel *shareModel;

@property (nonatomic, strong) UIViewController *viewController;

@property (nonatomic, assign) GLShareMessageType shareType;

- (void)showView;

- (void)hideView;
+ (void)showViewWithParames:(NSDictionary *)parames;

@end

NS_ASSUME_NONNULL_END
