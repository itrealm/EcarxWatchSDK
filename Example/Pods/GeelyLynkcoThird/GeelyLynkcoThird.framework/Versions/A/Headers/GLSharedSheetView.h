//
//  GLSharedSheetView.h
//  GeelyConsumerThird
//
//  Created by zhiyong.kuang on 2019/08/14.
//

#import <UIKit/UIKit.h>
@class GLShareModel;
#import "GLShareModule.h"



//定义枚举类型
typedef enum GLSharedSheetViewType {
    GLSharedSheetViewTypeDefault  = 0,
    GLSharedSheetViewTypeRound,
} shareViewType;

//指明枚举类型
//-------in parameters---------------

NS_ASSUME_NONNULL_BEGIN
static inline BOOL GLSharedSheetViewIsObjectEmpty(id obj) { // 空对象
    return obj == nil || [obj isEqual:[NSNull null]] || ([obj respondsToSelector:@selector(length)] && [(NSData *) obj length] == 0) ||
    ([obj respondsToSelector:@selector(count)] && [(NSArray *) obj count] == 0);
}

static inline BOOL GLSharedSheetViewIsStringEmpty(NSString *string) { // 空字符串
    return string == nil || [string isEqual:[NSNull null]] || string.length == 0 || [string isEqualToString:@"<null>"] ||
    [string isEqualToString:@"(null)"];
}

typedef void(^clickPlatform) (void);

typedef void(^GLShareViewCallBackBlock)(BOOL success,NSInteger platForm);

typedef void(^GLShareSheetViewCallBackBlock)(BOOL success,id result);


@interface GLSharedSheetView : UIView
@property (nonatomic,assign) shareViewType state; //操作类型

//@property (nonatomic, strong) NSDictionary *parames;

@property (nonatomic, copy) clickPlatform clickplatBlock;//无参无返回值

@property (nonatomic, copy) GLShareViewCallBackBlock result;//无参无返回值

@property (nonatomic, copy) GLShareSheetViewCallBackBlock shareViewBlock;//无参无返回值

@property (nonatomic, strong) GLShareModel *shareModel;

@property (nonatomic, strong) UIViewController *viewController;

@property (nonatomic, assign) GLShareMessageType shareType;

@property (nonatomic, copy,class) NSString* hiddenNotificationName;

- (void)showSheetView;

- (void)hidden;
+ (void)showSheetViewWithParames:(NSDictionary *)parames;

@end

NS_ASSUME_NONNULL_END
