//
//  GLThirdPartLoginModule.h
//  GeelyConsumerMain
//
//  Created by 朱伟特 on 2018/7/11.
//  Copyright © 2018年 朱伟特. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GLSharePayLoginConfigration.h"

@class GLThirdPartLoginModel;

typedef NS_ENUM(NSUInteger, GLThirdPartLoginType){
    GLLoginWX       = 1,    //微信登录
    GLLoginSina     = 2,    //微博登录
    GLLoginQQ       = 3     //QQ登录
};


/**
 登录结果回调

 @param success 是否登录成功
 @param loginResult @{@"thirdPartName":model.thirdPartName,@"thirdPartIcon":model.thirdPartIcon,@"thirdPartGender":model.thirdPartGender,@"thirdPartId":model.thirdPartId,@"thirdPartLoginType":@(model.type)}
 */
typedef void(^LoginResult)(BOOL success, id loginResult);//登录成功后的回调

@interface GLThirdPartLoginModel : NSObject<NSCopying>

@property (nonatomic, copy) NSString * thirdPartName;//第三方昵称

@property (nonatomic, copy) NSString * thirdPartIcon;//第三方头像链接

@property (nonatomic, copy) NSString * thirdPartGender;//第三方性别,有可能会是男女，有可能会是mw

@property (nonatomic, copy) NSString * thirdPartId;//第三方唯一id

@property (nonatomic, assign) GLThirdPartLoginType type;//登陆方式

@end

@interface GLThirdPartLoginModule : NSObject

+ (id)shareInstance;

- (void)getThirdPartLoginInfo:(GLThirdPartLoginType)type viewController:(UIViewController *)controller withBlock:(LoginResult)block;//根据登陆方式枚举值，进行第三方登录

@end
