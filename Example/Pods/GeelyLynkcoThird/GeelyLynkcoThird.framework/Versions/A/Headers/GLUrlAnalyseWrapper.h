//
//  GLUrlAnalyseWrapper.h
//  GeelyLynkcoThird
//
//  Created by duanyang on 2020/3/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^GLUrlAnalyseWrapperSuccessBlock)(NSInteger code, id result);
typedef void(^GLUrlAnalyseWrapperFailureBlock)(NSString *errorDes);

static inline BOOL GLUrlAnalyseWrapperIsObjectEmpty(id obj) { // 空对象
    return obj == nil || [obj isEqual:[NSNull null]] || ([obj respondsToSelector:@selector(length)] && [(NSData *) obj length] == 0) ||
    ([obj respondsToSelector:@selector(count)] && [(NSArray *) obj count] == 0);
}
static inline BOOL GLUrlAnalyseWrapperIsStringEmpty(NSString *string) { // 空字符串
    return string == nil || [string isEqual:[NSNull null]] || string.length == 0 || [string isEqualToString:@"<null>"] ||
    [string isEqualToString:@"(null)"];
}
@interface GLUrlAnalyseWrapper : NSObject

+ (GLUrlAnalyseWrapper *)shareInstances;

- (void)openUrl:(NSString *)url success:(GLUrlAnalyseWrapperSuccessBlock)successBlock failure:(GLUrlAnalyseWrapperFailureBlock)failBlock;

@end

NS_ASSUME_NONNULL_END
