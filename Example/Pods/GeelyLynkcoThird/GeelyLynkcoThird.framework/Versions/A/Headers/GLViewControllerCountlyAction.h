//
//  GLViewControllerCountlyAction.h
//  GeelyLynkcoThird
//
//  Created by shuaishuai on 2019/10/29.
//

#import <DZViewControllerLifeCircleAction/DZViewControllerLifeCircleAction.h>

NS_ASSUME_NONNULL_BEGIN

///ViewController 显示countly上报
@interface GLViewControllerCountlyAction : DZViewControllerLifeCircleBaseAction
@property(nonatomic,strong,readonly)NSDictionary *countlyViewRecordingMap;
@end

///注册需要countly自动上报的ViewController标识
FOUNDATION_EXTERN void GLCountlyPageRecordRegistVC(NSArray *vc_id_list);

///移除已注册countly自动上报的ViewController标识
FOUNDATION_EXTERN void GLCountlyPageRecordRemoveVC(NSArray *vc_id_list);


NS_ASSUME_NONNULL_END
