//
//  GLWechatPayManager.h
//  GeelyLynkcoThird
//
//  Created by yang.duan on 2019/9/12.
//

#import <Foundation/Foundation.h>
#import "WXApi.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^PayResult)(BOOL success, id result);
@interface GLWechatPayManager : NSObject <WXApiDelegate>

+(instancetype)sharedInstance;
/**
 * 微信支付执行
 */
- (void)weixinPayWithParams:(NSDictionary *)params response:(PayResult)response;


@property (nonatomic, copy)PayResult payResultBlock;

@end

NS_ASSUME_NONNULL_END
