//
//  GLWordFilter.h
//  Pods
//
//  Created by yang.duan on 2020/1/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLWordFilter : NSObject
+ (instancetype)sharedInstance;

//添加过滤敏感词
-(void)insertWords:(NSString *)words;
//过滤敏感词
- (NSString *)filter:(NSString *)str;
@end

NS_ASSUME_NONNULL_END
