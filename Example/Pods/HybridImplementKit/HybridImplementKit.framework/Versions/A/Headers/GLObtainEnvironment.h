//
//  GLObtainEnvironment.h
//  Pods
//
//  Created by shuaishuai on 2019/9/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GLObtainEnvironment : NSObject
/// GeelyLynkcoCore调用GLNetworkEnvConfig中的getCurEnvKey方法，获取对应返回值

+ (NSString *)get_curEnvKey;
+ (NSString *)get_curHost:(NSString *)hostkey;
@end

NS_ASSUME_NONNULL_END
