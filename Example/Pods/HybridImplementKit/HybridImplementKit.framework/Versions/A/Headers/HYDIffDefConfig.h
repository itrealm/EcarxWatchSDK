//
//  HYDIffDefConfig.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/10/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYDIffDefConfig : NSObject

#pragma mark - 分类重写
///APPkey
+(NSString *) appKey;
/////加载资源类型
//+(NSString *) loadType;
///查询请求URL
+(NSString *) requestUrl;
///运行的weex/cordova的SDK的版本
+(NSString *) runVersion;
///工程项目本地存放默认项目根路径
+ (NSBundle *)rootBundle;

///工程内集成的业务包版本配置
+(NSDictionary *) originBundles;
///工程内集成的业务包hash值配置
+(NSDictionary *) originBundlesHash;
+(NSDictionary *) originBundlesTypeName;
@end

NS_ASSUME_NONNULL_END
