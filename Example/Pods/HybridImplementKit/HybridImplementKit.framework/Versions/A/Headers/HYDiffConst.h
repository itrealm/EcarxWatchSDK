//
//  HYDiffConst.h
//  Pods
//
//  Created by shuaishuai on 2019/9/18.
//

#ifndef HYDiffConst_h
#define HYDiffConst_h



static NSString *const swdiff_appVersion = @"appVersion";


#pragma mark - 请求
/*
 "appKey" : "应用唯一标识"
 "appV" : app版本
 "platform" : app的平台
 "runV": weex/cordova的SDK的版本
 "moduleList":[
     {
     "moduleName":"业务模块名称"
     "moduleBundleV":"app中的业务bundle的版本"
     },
     {
     "moduleName":"业务模块名称"
     "moduleBundleV":"app中的业务bundle的版本"
     },
 ]
 */
static NSString *const swdiff_appKey = @"appKey";
static NSString *const swdiff_appV = @"appV";
static NSString *const swdiff_platform = @"platform";
static NSString *const swdiff_runV = @"runV";
static NSString *const swdiff_moduleList = @"moduleList";
static NSString *const swdiff_moduleName = @"moduleName";
static NSString *const swdiff_moduleBundleV = @"moduleBundleV";

#pragma mark - 响应

///这里moduleList需要返回所有的列表
/*
 {
  "code":"200",
  "msg":"",
  "platform":"iOS",
  "moduleList"
  [
      {
      "loadType":"1", 业务类型，weex/cordova"
      "moduleName":"业务模块名称"
      "latestBundleV":"1.0.1", 更新后版本号
      "fullupdate":"0",0：不是整包更新；1:整包更新，如果为1，则 patch 则不起作用
      "patch":{
          "downloadUrl":"https:xxxxxx.zip", patch包下载地址
          "patchHash":"xxx", patch包的md5值
          "bundleHash":"xxxx", 差异合并后js文件md5值
         },
      "bundle":{
          "downloadUrl":"https:xxxxxx.zip", bundle完整包下载地址
          "bundleHash":"xxxx", bundleHash的md5值
         },
      "downloadPolicy":"0",0：总是下载, 1:wifi下载，2: 4g和wifi下载
      "forceUpdate":"0",0：不是强制更新；1:强制更新
      "needGoBack":"0",0：不需要回退版本；1:需要回退版本
      "des":"",更新描述
      },
  ],
 }
 */
static NSString *const swdiff_code = @"code";
static NSString *const swdiff_msg = @"msg";
//static NSString *const swdiff_platform = @"platform";
//static NSString *const swdiff_moduleList = @"moduleList";
static NSString *const swdiff_loadType = @"loadType";
static NSString *const swdiff_latestBundleV = @"latestBundleV";
static NSString *const swdiff_fullupdate = @"fullupdate";
static NSString *const swdiff_patch = @"patch";
static NSString *const swdiff_downloadUrl = @"downloadUrl";
static NSString *const swdiff_patchHash = @"patchHash";
static NSString *const swdiff_bundleHash = @"bundleHash";
static NSString *const swdiff_bundle = @"bundle";
static NSString *const swdiff_downloadPolicy = @"downloadPolicy";
static NSString *const swdiff_needGoBack = @"needGoBack";
static NSString *const swdiff_forceUpdate = @"forceUpdate";

static NSString *const swdiff_des = @"des";

#pragma mark - 文件夹名
/*
根目录：+rootpath
每种缓存类型独立目录：type，这里有两种corodva和weex
每个业务一个独立目录，这里有两个示例业务：LDRNDefault和LDRNTest
每个业务有四个子目录：origin、patch、merge和index
每个业务有入口文件index.js(index.html)和资源文件夹assets

### 四个子目录:
origin:客户端内置bundle的解压目录，第一次会加载内置的复制一份到沙盒
index:最新bundle目录，客户端首先加载该目录下的main.jsbundle，如果不存在则加载origin目录下的
patch:差异包的下载目录
merge:基础bundle与差异包合并文件存放目录，合并完成后客户端下次启动将该目录文件移动到index目录下

─ root_res
    ├── DefaultBusiness1
    │   ├── index
    │   ├── merge
    │   ├── origin
    │   └── patch
    └── DefaultBusiness2
    ├── index
    ├── merge
    ├── origin
    └── patch

在本地工程中存放打包时候默认版本，放在+bundleRootPath路径中，在路径下以DefaultBusiness1.zip格式存放

参考：https://www.jianshu.com/p/5c60b73d0c0b
*/

static NSString *const swdiff_filename_sboxroot = @"sboxroot";

static NSString *const swdiff_filename_assets = @"assets";
static NSString *const swdiff_filename_index = @"index";
static NSString *const swdiff_filename_merge = @"merge";
static NSString *const swdiff_filename_patch = @"patch";
static NSString *const swdiff_filename_origin = @"origin";
//static NSString *const swdiff_assets = @"assets";
//static NSString *const swdiff_assets = @"assets";


#pragma mark - other

static NSString *const swdiff_ture = @"1";
static NSString *const swdiff_false = @"0";


#endif /* HYDiffConst_h */
