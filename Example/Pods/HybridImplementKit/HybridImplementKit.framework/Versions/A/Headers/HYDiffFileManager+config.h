//
//  HYDiffFileManager+config.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/10/31.
//


#import "HYDiffFileManager.h"

#define ABS_SBox(relPath)  [HYDiffFileManager hy_abs_sboxpath:relPath]

NS_ASSUME_NONNULL_BEGIN

@interface HYDiffFileManager (config)

#pragma mark - 内部配置
+ (NSString *)hy_appKey;
//+ (NSString *)hy_loadType;
+ (NSString *)hy_requestUrl;
+ (NSString *)hy_runVersion;
+ (NSString *)hy_bundleRootPath;
+ (NSString *)hy_sandboxRootpathName;

///这里配置的bundle为所有的多业务包，后面需要新增业务包，则需要新增这里；
///所以目前暂不支持动态新增一类业务
+ (NSDictionary *)hy_originBundles;
+ (NSDictionary *)hy_originBundlesHash;
+ (NSDictionary *)hy_originBundlesTypeName;

#pragma mark - 路径
#pragma mark 工程路径
///本地工程zip包路径
+ (NSString *)hy_localBundlZipPath:(NSString *)fileName;
///本地工程包路径
+ (NSString *)hy_localBundleFilePath:(NSString *)fileName;

#pragma mark 相对路径转绝对路径
///相对路径转换为沙盒绝对路径
+ (NSString *)hy_abs_sboxpath:(NSString *)relPath;

#pragma mark 相对路径

///获取到存储文件沙盒根路径  : /root
+ (NSString *)hy_rel_sboxRootPath;

///获取到存储文件沙盒类型路径: /root/type
+ (NSString *)hy_rel_sboxTypeFolderPathByBundleName:(NSString *)bundleName;

///获取到存储文件沙盒业务文件路径:  /root/type/budleName
+ (NSString *)hy_rel_sboxBundleFolderPathByBundleName:(NSString *)bundleName;

///获取到存储文件沙盒业务文件路径:  /root/type/budleName/ testBundleName_0_0_1
+ (NSString *)hy_rel_sboxBundleNamePathByBundleName:(NSString *)bundleName
                                        versionName:(NSString *)versionName;
///获取到存储文件沙盒业务文件路径:  /root/type/budleName/ testBundleName_0_0_1.zip
+ (NSString *)hy_rel_sboxBundleNameZipPathByBundleName:(NSString *)bundleName
                                           versionName:(NSString *)versionName;
///获取到存储文件沙盒业务文件路径:  /root/type/budleName/ testBundleName_0_0_1_merge.zip
+ (NSString *)hy_rel_sboxBundleNameMergeZipPathByBundleName:(NSString *)bundleName
                                                versionName:(NSString *)versionName;

///获取到存储文件沙盒业务patch文件夹路径: /root/type/budleName/patch
+ (NSString *)hy_rel_sboxBundlePatchFolderPathByBundleName:(NSString *)bundleName;
///获取到存储文件沙盒业务patch文件夹下patch文件路径  /root/type/budleName/patch/patchName.patch
+ (NSString *)hy_rel_sboxBundlePatchFilePathByBundleName:(NSString *)bundleName;

#pragma mark - version
///获取当前正在使用的版本号
+ (NSString *)hy_curVersion:(NSString *)bundleName;
///获取最新的版本号
+ (NSString *)hy_latestVersion:(NSString *)bundleName;

#pragma mark - name
///根据业务模块名和版本号根据生成规则生成存储的 业务文件名
///生成规则：testBundleName + 0.0.1 ==> testBundleName_0_0_1
+ (NSString *)hy_fileName:(NSString *)bundleName
              versionName:(NSString *)versionName;
///根据业务模块名和版本号根据生成规则生成存储的 业务zip文件名
///生成规则：testBundleName + 0.0.1 ==> testBundleName_0_0_1.zip
+ (NSString *)hy_zipFileName:(NSString *)bundleName
                 versionName:(NSString *)versionName;

///生成patch合并后临时merge的zip文件名（含后缀）
///生成规则：testBundleName + 0.0.1 ==> testBundleName_0_0_1_merge.zip
+ (NSString *)hy_zipMergeFileName:(NSString *)bundleName
                   versionName:(NSString *)versionName;

///根据业务模块名和版本号根据生成规则生成存储的 业务patch文件名
///生成规则：testBundleName ==> testBundleName.patch
+ (NSString *)hy_patchFileName:(NSString *)bundleName;



#pragma mark - 储存记忆key
///记录各业务加载地址指向的key
//+hy_save_indicatorKey:
//{
//    "+getSaveKeyIndicator:":HYDiffMemory
//    "weex_testModule":HYDiffMemory
//}
+(NSString *)hy_save_indicatorKey;
///获取存储hy_save_indicatorKey下的dic业务的的key值
+ (NSString *)getSaveKeyIndicator:(NSString *)bundleName;

///记录最新的业务列表
//"moduleName":{
//    "version":"0.0.1",
//    "hash":"",
//    "type":"weex/h5"
//}
//+ (NSString *)hy_save_bundleListKey;
///记录已经下载完成列表的key
+(NSString *)hy_save_hadDownloadKey;
///记录服务端获取到最新的列表key （存储的是所有的列表）
//+ (NSString *)hy_save_needUpdateListKey;
+ (NSString *)hy_save_latestBundleListKey;

///存储APP版本信息，标记是否记录是第一次进行差异包更新
+ (NSString *)hy_save_appInfoKey;



@end

NS_ASSUME_NONNULL_END
