//
//  HYDiffFileManager.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/10/31.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
#define HYLog(fmt, ...) NSLog((@"HY-> %s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define HYLog(...)
#endif



NS_ASSUME_NONNULL_BEGIN

@interface HYDiffFileManager : NSObject

#pragma mark - 沙盒操作
///清空hy_rel_sboxRootpath文件
+ (void) hy_clearSandboxCache;

#pragma mark  更新指针地址
///清空hy_save_indicatorKey下存储的指针dic
+ (void)hy_clearIndicator;
///更新地址指针
+ (void)hy_updateIndicator;

#pragma mark updateSandboxPath
///更新hy_rel_sboxRootpath下的各个目录文件夹，如果没有则创建
+ (void) hy_updateSandboxPath;

///copy项目中的文件到沙盒中并解压，并将curIndicator指针指向copy后解压的文件路径 (耗时的)
+ (void) hy_copyDefBundleToSandbox;

///  初次加载，将工程内项目文件夹压缩到沙盒下，当前运行指针指向工程文件目录
+ (void) hy_setDefBundleToSandbox;

/// 初次加载，快速初始化默认运行指针，指向本地工程文件夹
+ (void)hy_setDefIndicator;


#pragma mark - path api
///获取当前业务需要运行的地址
+ (NSString *)getCurShouldLoadBundlePath:(NSString *)bundleName;
///获取当前业务需要运行的相对地址
+ (NSString *)getCurShouldLoadBundleRelPath:(NSString *)bundleName;

#pragma mark - download
+ (void) hy_downloadPatchFile:(NSString *)url
                   bundleName:(NSString *)bundleName;

#pragma mark - version
///对比版本，根据规则返回是否需要更新
+(BOOL)isNeedUpdateVersionByCurBundleV:(NSString *)curBundleV
                         latestBundleV:(NSString *)latestBundleV;
+(BOOL)isNeedUpdateVersionByBundleName:(NSString *)bundleName
                         latestBundleV:(NSString *)latestBundleV;
+ (BOOL)isNeedUpdateVersionByBundleName:(NSString *)bundleName;

#pragma mark - path name
///根据zip包获取解压包相对路径
+ (NSString *)unZipSboxRelPath:(NSString *)zipPath;

///向hy_save_hadDownloadKey添加bundleName，认为已经下载
+ (void)hy_addSaveHadDownloadList:(NSString *)bundleName;

#pragma mark - fundition
+ (id)getUserDefValue:(NSString *)key;
+ (BOOL)synUserDefValue:(NSString *)key
                  value:(id)value;
@end

NS_ASSUME_NONNULL_END
