//
//  HYDiffMemory.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/10/31.
//

#import <Foundation/Foundation.h>



@interface HYDiffMemory : NSObject <NSCopying>

///在沙河下，前一次zip包指向的相对路径，get获取到的为绝对路径
@property (nonatomic,copy)NSString *rel_preIndicatorZipPath;
@property (nonatomic,copy)NSString *preBundleVersion;

///在沙河下，当前运行的包指向的相对路径，get获取到的为绝对路径
@property (nonatomic,copy)NSString *rel_curIndicatorPath;
@property (nonatomic,copy)NSString *rel_curIndicatorZipPath;
@property (nonatomic,copy)NSString *curBundleVersion;

///在沙河下，下次将要运行的包指向的相对路径，get获取到的为绝对路径
@property (nonatomic,copy)NSString *rel_nextIndicatorPath;
@property (nonatomic,copy)NSString *rel_nextIndicatorZipPath;
@property (nonatomic,copy)NSString *nextBundleVersion;

@property (nonatomic,copy)NSString *curBundleName;



//将字典转成当前对象
- (void)convert:(NSDictionary*)dataSource;
//将对象转成字典
- (NSDictionary*)changeToDic;

@end

@interface NSObject (changeToDiffMemory)
- (HYDiffMemory *)changeToDiffMemory;
@end
