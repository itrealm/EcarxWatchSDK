//
//  HYDiffObjectToDictionary.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/11/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYDiffObjectToDictionary : NSObject
+ (NSDictionary*)getObjectData:(id)obj;
@end

NS_ASSUME_NONNULL_END
