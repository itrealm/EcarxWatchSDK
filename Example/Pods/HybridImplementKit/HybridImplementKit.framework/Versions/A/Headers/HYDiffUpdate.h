//
//  HYDiffUpdate.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/10/21.
//

#import <Foundation/Foundation.h>

typedef NSString *(^HY_FORCE_VERSION_BLOCK)(BOOL isKillApp);
#define HY_ABS_SBox(relPath)  [HYDiffUpdate hy_abs_sboxpath:relPath]
#define HY_LOCAL_PATH(HOST,PATH)  [HYDiffUpdate hy_regularLocalPath:HOST path:PATH]

@protocol HYForceUpdateVersionProtocol <NSObject>
- (void)forceUpdateVersion:(NSString *)bundleName description:(NSString *)description handleBlock:(HY_FORCE_VERSION_BLOCK)handleBlock;
@end

@interface HYDiffUpdate : NSObject

@property(nonatomic,assign) id<HYForceUpdateVersionProtocol> delegate;

/// 返回一个单例，new/init也会是单例
+ (instancetype)share;

/// 获取资源包相对路径
- (NSString *)resourceRelPath:(NSString *)bundleName;

/// 获取资源包相对路径
/// @param bundleName 业务名称
/// @param complete 响应
- (void)resourceRelPath:(NSString *)bundleName
               complete:(void(^)(NSString *resourceRelPath))complete;

/// 获取资源包绝对路径
/// @param bundleName 业务名称
/// @param complete 响应
- (void)resourcePath:(NSString *)bundleName
            complete:(void(^)(NSString *resourcePath))complete;

/// 快速获取取资源地址 （需要调用+ resourcePath:complete:之后才能使用，仅用于频繁读取的调用）
/// @param bundleName 要获取的bundle名字
- (NSString *)quickResourcePath:(NSString *)bundleName;

#pragma mark - 路径
///相对路径转换为沙盒绝对路径
+ (NSString *)hy_abs_sboxpath:(NSString *)relPath;
///拼接合格的本地路径
+ (NSString *)hy_regularLocalPath:(NSString *)host path:(NSString *)path;

@end


