//
//  HYDiffVersionManager.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/9/16.
//

#import <Foundation/Foundation.h>
#import "HYDiffUpdate.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^SW_SUCCESS_CALLBACK)(NSArray *data, NSURLResponse *response);
typedef void (^SW_FAILED_CALLBACK)(NSError *error, NSURLResponse *response);

@interface HYDiffVersionManager : NSObject

@property(nonatomic,assign) id<HYForceUpdateVersionProtocol> delegate;

///异步更新资源：如有之前未下载完的继续下载并处理，不再检测更新；如没有未下载完的，查询最新资源列表
- (void)hy_asyUpdateResBundles;


///发起请求检测资源更新
//+ (void)checkResBundlesVersion:(SW_SUCCESS_CALLBACK)success
//                   withFailure:(SW_FAILED_CALLBACK)failure;



@end

NS_ASSUME_NONNULL_END
