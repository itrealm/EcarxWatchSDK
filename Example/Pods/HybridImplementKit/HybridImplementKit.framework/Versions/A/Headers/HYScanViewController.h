//
//  SWScanViewController.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/9/23.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class HYScanViewController;
@protocol HYScanProtocol <NSObject>

/// 扫描二维码结果回调
/// @param scanViewController 当前的扫描VC
/// @param captureOutput 扫描AVCaptureOutput
/// @param metadataObjects 扫描结果输出
/// @param connection 该代理能否处理该扫描结果
- (BOOL)scanViewController:(HYScanViewController *)scanViewController captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection;
@end

@interface HYScanViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

- (void)addDelegate:(id<HYScanProtocol>) delegate;
- (void)removeDelegate:(id<HYScanProtocol>) delegate;

//@property (nonatomic,assign) id<SWScanProtocol> delegate;

@end


