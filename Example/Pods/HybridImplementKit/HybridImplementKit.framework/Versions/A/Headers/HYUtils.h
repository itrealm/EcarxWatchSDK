//
//  HYUtils.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/10/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HYUtils : NSObject

#ifdef __cplusplus
extern "C" {
#endif
    
/**
 * @abstract execute asynchronous action block on the main thread.
 *
 */
void HYPerformBlockOnMainThread( void (^ _Nonnull block)(void));

#ifdef __cplusplus
}
#endif

@end

NS_ASSUME_NONNULL_END
