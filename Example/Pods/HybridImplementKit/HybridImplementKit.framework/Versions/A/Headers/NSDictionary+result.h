//
//  NSDictionary+result.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/8/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (result)

+ (NSDictionary *)result_ok:(id)data;

+ (NSDictionary *)result_n:(NSString *)errorCode
                       msg:(NSString *)msg
                      data:(id)data;

@end

NS_ASSUME_NONNULL_END
