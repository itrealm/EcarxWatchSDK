//
//  NSObject+hookMethod.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2018/10/29.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWMethod : NSObject
+ (id)methodWithObjCMethod: (Method)method;
+ (id)methodWithSelector: (SEL)sel implementation: (IMP)imp signature: (NSString *)signature;
- (id)initWithObjCMethod: (Method)method;
- (id)initWithSelector: (SEL)sel implementation: (IMP)imp signature: (NSString *)signature;

- (SEL)selector;
- (NSString *)selectorName;
- (IMP)implementation;
- (NSString *)signature;

// for ObjC method instances, sets the underlying implementation
// for selector/implementation/signature instances, just changes the pointer
- (void)setImplementation: (IMP)newImp;
@end

@interface NSObject (hookMethod)

+ (void)swizzling_instanceMethod_fromClass:(Class)fromClass
                              fromSelector:(SEL)fromSelector
                                   toClass:(Class)toClass
                                toSelector:(SEL)toSelector;

/**
 替换当前对象中的某个实例方法
 对象中的method list获取method结构体中替换某个方法
 
 @param class 对象class  如果:[self class]
 @param origSelector 要替换掉的对实例方法
 @param newSelector 要替换成为的实例方法  （如果不想覆盖origSelector中实现的方法，可在newSelector中调用一次newSelector自己）
 */
+ (void)swizzling_instanceMethod_class:(Class)class origSelector:(SEL)origSelector newSelector:(SEL)newSelector;

/**
 替换当前类中某个类方法
 
 @param class 对象class 如果:[self class]
 @param origSelector 要替换掉的对类方法
 @param newSelector 要替换成为的类方法  （如果不想覆盖origSelector中实现的方法，可在newSelector中调用一次newSelector自己）
 */
+ (void)swizzling_classMethod_class:(Class)class origSelector:(SEL)origSelector newSelector:(SEL)newSelector;


/*
 [NSObject swizzling_addMethod: [SWMethod methodWithSelector: @selector(sw_addMethodTester) implementation: (IMP)AddMethodTester signature: @"v@:"]];

 */
+ (void)swizzling_addMethod: (SWMethod *)method;
+ (NSArray *)swizzling_methods;

+ (Class)swizzling_setSuperclass: (Class)newSuperclass;

@end

NS_ASSUME_NONNULL_END
