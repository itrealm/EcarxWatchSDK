//
//  SWAutoEvenService.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/8/8.
//

#import <Foundation/Foundation.h>
#import <SweetCoreKit/SWEventService.h>

NS_ASSUME_NONNULL_BEGIN


@interface SWAutoSubscriber : NSObject
@property(nonatomic,copy)NSString *subscriberID;
@property(nonatomic)SEL sel;

@end

@interface SWAutoEvenService : NSObject
@property(nonatomic,strong,readonly)NSDictionary *subscriberMap;

+ (instancetype)service;

/**
 发布事件
 
 @param eventName 事件名
 */
- (void)publishEventName:(NSString *)eventName;

/**
 发布事件
 
 @param eventName 事件名
 @param eventData 事件数据
 */
- (void)publishEventName:(NSString *)eventName
                  object:(id _Nullable)object
               eventData:(NSDictionary * _Nullable)eventData;

/**
 添加事件订阅者
 
 @param subscriber 订阅者
 @param eventName 事件名
 @param selector 执行方法
 */
- (BOOL)addSubscriber:(NSString *)subscriberID
             selector:(SEL)selector
            eventName:(NSString *)eventName
               object:(id _Nullable)object;

/**
 添加事件订阅者
 
 @param subscriber 订阅者
 @param eventName 事件名
 @param object 对象
 @param queue 队列,决定 block 在哪个线程队列中执行, nil  在发布通知的线程中执行
 @param block 执行回调
 */
- (BOOL)addSubscriber:(NSString *)subscriberID
            eventName:(NSString *)eventName
               object:(id _Nullable)object
                queue:(NSOperationQueue * _Nullable)queue
                block:(EventBlock _Nullable)block;

/**
 移除事件订阅者
 
 @param subscriber 订阅者
 @param eventName 事件名
 */
- (BOOL)removeSubscriber:(NSString *)subscriberID
               eventName:(NSString *)eventName
                  object:(id _Nullable)object;

/**
 移除事件订阅者
 
 @param subscriber 订阅者
 */
- (BOOL)removeSubscriber:(NSString *)subscriberID;



@end

NS_ASSUME_NONNULL_END
