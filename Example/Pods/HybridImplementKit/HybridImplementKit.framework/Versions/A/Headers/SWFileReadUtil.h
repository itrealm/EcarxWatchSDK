//
//  SWFileReadUtil.h
//  Pods
//
//  Created by shuaishuai on 2018/11/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWFileReadUtil : NSObject
/**读取.json配置文件*/
+ (NSDictionary *)deconJsonFile:(NSString *)jsonPath;
@end

NS_ASSUME_NONNULL_END
