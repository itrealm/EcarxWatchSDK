//
//  SWMarkingProtocol.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/8/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SWMarkingProtocol <NSObject>

//容器ID
@property (nonatomic, copy) NSString *containerID;

@end

NS_ASSUME_NONNULL_END
