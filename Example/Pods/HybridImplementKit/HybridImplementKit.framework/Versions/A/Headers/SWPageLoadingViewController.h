//
//  SWPageLoadingViewController.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2018/10/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

//原始端需要实现的协议
@protocol SWPageLoadingProtocol <NSObject>
//页面reload重载
- (void)reloadPage;
//webview页面停止重载
- (void)stopReload;
//关闭当前loading页面
- (void)close;
@end


@interface SWPageLoadingViewController : UIViewController

/*
 面向前端接口：（提供给前端调用 如：prompt('onPageReLoad')）
 重载当前页面（eloads the current page.）：
 onPageReLoad
 暂停刷新
 stopPageReLoad
 
 前端预定义接口：（提供给原始段调用）
 function switchPageLoadStatus(pageStatus, message) {
 switch (pageStatus) {
 case 0:
 startLoading(message)
 break
 case 1:
 stopLoading()
 break
 case -1:
 showPageError(message)
 break
 default:
 return
 }
 }
 */

//白屏页面和错误页面的本地路径
@property( copy,nonatomic) NSURL *loadSourceUrl;
@property (nonatomic,copy) NSString *loadViewBackColor;

@property(weak,nonatomic)id<SWPageLoadingProtocol> delegate;

/**
 提供给原始端调用
 切换等待状态
 
 @param loadStatus 等待状态，默认为0（0:正在加载；1:加载失败；2:加载成功完成）
 @param message 显示的文字信息 （如错误信息）
 */
- (void)switchPageLoadStatus:(NSInteger)loadStatus message:(NSString *)message;


//--------------------------------------

- (void)reloadSourceUrl:(NSURL *)loadSourceUrl;







@end

NS_ASSUME_NONNULL_END
