//
//  SWRemoteDownLoad.h
//  Pods
//
//  Created by shuaishuai on 2018/11/29.
//

#import <Foundation/Foundation.h>

extern  NSString* const k_sw_resourceID_key;

typedef NS_ENUM(NSUInteger, SWRemoteDownLoadProgressType) {
    SWRemoteDownLoadProgressType_WillLoad,
    SWRemoteDownLoadProgressType_Loading,
    SWRemoteDownLoadProgressType_LoadSuccess,
    SWRemoteDownLoadProgressType_LoadFail,
    SWRemoteDownLoadProgressType_WillUnZip,
    SWRemoteDownLoadProgressType_UnZiping,
    SWRemoteDownLoadProgressType_UnZipSuccess,
    SWRemoteDownLoadProgressType_UnZipFail
};

typedef void(^SWRemoteDownLoad_Progress_Block)(SWRemoteDownLoadProgressType progressType,float progressScale);
typedef void(^SWRemoteDownLoad_Completion_Block)(NSString *unzipPath,NSError *error);
typedef void(^SWRemoteUnzip_Completion_Block)(NSError *error);


@interface SWRemoteDownLoad : NSObject

#pragma mark - 更新本地map
//更新缓存plist中一条资源信息
+ (BOOL)updateResource:(NSDictionary *)newResourceConfig;
///获取整个plist缓存数据
+ (NSArray *)getResourceMap;
///根据resourceID获取某条数据
+ (NSDictionary *)getResource:(NSString *)resourceID;
//根据给定的key-value从map中获一条数据
+ (NSDictionary *)getResourceByKey:(NSString *)key value:(NSString *)value;
//根据resourceID 删除一条plist缓存数据
+ (BOOL)deleteResource:(NSString *)resourceID;

#pragma mark - 下载并解压
- (void)downloadZip:(NSString *)zipDownloadUrl
        toUnzipName:(NSString *)unzipName
      progressBlock:(SWRemoteDownLoad_Progress_Block)progressBlock
    completionBlock:(SWRemoteDownLoad_Completion_Block)completionBlock;

//- (void)downloadZip:(NSString *)zipDownloadUrl
//    toUnzipFilePath:(NSString *)unzipFilePath
//      progressBlock:(SWRemoteDownLoad_Progress_Block)progressBlock
//    completionBlock:(SWRemoteDownLoad_Completion_Block)completionBlock;

#pragma mark - 路径创建

///获取压缩包根目录
+ (NSString *)get_zipFileAbsoluteDirectory;
///获取解压包根目录
+ (NSString *)get_unzipFileAbsoluteDirectory;
///根据解压包绝对路径获取解压包的相对路径
+ (NSString *)get_unzipFileRelativePath:(NSString *)unzipFileAbsolutePath;
///根据解压包相对路径获取j沙盒绝对路径
+ (NSString *)get_unzipFileAbsolutePath:(NSString *)unzipFileRelativePath;





@end


