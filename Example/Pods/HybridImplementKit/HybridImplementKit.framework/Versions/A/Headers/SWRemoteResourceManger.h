//
//  SWRemoteResourceManger.h
//  Pods
//
//  Created by shuaishuai on 2018/11/29.
//

#import <Foundation/Foundation.h>
#import "SWRemoteDownLoad.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWResourceObj : NSObject
@property(nonatomic,copy)NSString *groupID;
@property(nonatomic,copy)NSString *resourceID;
@property(nonatomic,copy)NSString *resoureceRelativePath;
@property(nonatomic,assign)BOOL isExpired;
@property(nonatomic,copy)NSString *registerTime;
@property(nonatomic,copy)NSString *version;

@end

@interface SWRemoteResourceManger : NSObject

//+ (SWRemoteResourceManger *)share;

///注册某个资源包（表新增一条数据）
/*
 resourcePath指向资源包根目录，会在根目录下查找.json文件并解析，解析结果追加上一些字段后（resourceID、离线包链接、离线包地址)返回一个dic，进行数据库存储+离线包下载
 */
//- (void)registerResource:(NSString *)resourceID resourcePath:(NSString *)resourcePath completion:(void(^)(NSDictionary *resourceConfig))completion;
///卸载某个资源包（会清除该资源包的相关数据）
//- (void)unloadResource:(NSString *)resourceID;
///卸载所有资源
//- (void)unloadAll;


//- (void)registerResourceGroup:(NSString *)groupID;

/**********************************************************************************/

+ (instancetype)shareInstance;

/**
 注册一个资源包，resourceID已经注册的直接从本地拿

 @param resourceID 资源包ID
 @param resourceZIPUrl 资源zip包远程地址
 @param groupID 将资源包挂在groupID下，如果为空则在默认的group下
 @param progress 进程回调
 @param completion 资源包注册完成回调
 */
- (void)registerResource:(NSString *)resourceID
          resourceZIPUrl:(NSString *)resourceZIPUrl
                 inGroup:(NSString *)groupID
                progress:(void(^)(SWRemoteDownLoadProgressType progressType))progress
              completion:(void(^)(SWResourceObj *resourceObj))completion;

- (void)registerResource:(NSString *)resourceZIPUrl
                 version:(NSString *)version
              resourceID:(NSString *)resourceID
                 inGroup:(NSString *)groupID
                progress:(void(^)(SWRemoteDownLoadProgressType progressType))progress
              completion:(void(^)(SWResourceObj *resourceObj))completion;

/**
 快速加载已经注册过的资源包

 @param resourceID 资源包ID
 @param groupID 加载某个group下的,如果为空则认为是默认的group下的
 @param completion 结果回调，如果未注册过或未获取到则返回nil
 */
- (void)loadRegisteredResource:(NSString *)resourceID
                         group:(NSString *)groupID
                    completion:(void(^)(SWResourceObj *resourceObj))completion;
- (SWResourceObj *)loadRegisteredResource:(NSString *)resourceID
                                    group:(NSString *)groupID;

/**
 卸载某个资源包
 
 @param resourceID 资源包ID
 @param groupID 资源包所在groupID
 @return YES：成功；NO：失败
 */
- (BOOL)unloadResource:(NSString *)resourceID
                 group:(NSString *)groupID;

/**
 卸载某个资源包
 
 @param groupID groupID
 @return YES：成功；NO：失败
 */
- (BOOL)unloadResource:(NSString *)groupID;

/**
 卸载所有资源

 @return YES：成功；NO：失败
 */
- (BOOL)unloadAll;


@end

NS_ASSUME_NONNULL_END
