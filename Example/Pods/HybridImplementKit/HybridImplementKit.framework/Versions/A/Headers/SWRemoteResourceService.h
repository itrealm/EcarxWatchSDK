//
//  SWRemoteResourceService.h
//  Pods
//
//  Created by shuaishuai on 2018/12/17.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWRemoteResourceService : NSObject
+ (NSString *)checkRemoteUrl:(NSString *)url;
@end

NS_ASSUME_NONNULL_END
