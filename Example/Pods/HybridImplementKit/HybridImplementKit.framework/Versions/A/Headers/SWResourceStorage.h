//
//  SWResourceStorage.h
//  Pods
//
//  Created by shuaishuai on 2018/12/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWResourceStorage : NSObject

/*
 key1-|
      |_dic|
           |_dic

 */

#pragma mark - plistc持久存储操作
/**
 根据键更新值
 
 @param jsonObjc 值，为字典、数组或字符串
 @param key 键，重复了会覆盖
 @return 操作是否成功
 */
+ (BOOL)setPlist_jsonObjc:(id)jsonObjc forKey:(NSString *)key;

/**
 获取值

 @param key 键
 @return 返回的值，如果无返回nil
 */
+ (id)getPlist_key:(NSString *)key;

/**
 删除一个键值

 @param key 键
 @return 操作是否成功
 */
+ (BOOL)deletePlist_key:(NSString *)key;

/**
 删除所有
 
 @return 操作是否成功
 */
+ (BOOL)deletePlist_all;

#pragma mark - 路径创建
///对字符串进行一次编码
+ (NSString *)createEncodeStr:(NSString *)str;

//创建解压文件的相对路径
//+ (NSString *)createRelativeUnzipPath:(NSString *)packageName;
////根据相对路径拼装沙盒临时绝对路径（在NSCachesDirectory下）
//+ (NSString *)createAbsoluteDirectoryPath:(NSString *)relativePath;


@end

NS_ASSUME_NONNULL_END
