//
//  SWURIInterceptor.h
//  HybridImplementKit
//
//  Created by shuaishuai on 2019/5/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWURIInterceptor : NSObject

+ (instancetype)createInterceptor;

/**
 更新白名单表
 */
- (void)updateWhitList;

/**
 读取本地白名单，判断该URL是否被允许
 
 @param routerURLStr 需要拦截的URL
 @return 是否允许
 */
/*
 匹配规则：
 如：
 平台配置有：“https://www.baidu.com”
 则：“https://www.baidu.com/....” 都被允许
 
 */
- (BOOL)check_url:(NSString *)routerURLStr;


@end

NS_ASSUME_NONNULL_END
