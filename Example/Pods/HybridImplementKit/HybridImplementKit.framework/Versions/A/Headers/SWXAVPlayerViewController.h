//
//  SWXAVPlayerViewController.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/10/6.
//

#import <AVKit/AVKit.h>

NS_ASSUME_NONNULL_BEGIN

static NSString *kAVPlayerViewControllerDismissingNotification = @"kAVPlayerViewControllerDismissingNotification";

@interface SWXAVPlayerViewController : AVPlayerViewController

@end

NS_ASSUME_NONNULL_END
