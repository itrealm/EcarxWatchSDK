//
//  SWXHooliURLProtocol.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/7/22.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

//保存到本地的图片、视频等路径使用以SWX_STATIC_PREFIXPATH开头， 这里使用了nat开头为了与nat库保持一致
#define SWX_STATIC_PREFIXPATH @"nat://static"
#define SWX_STATIC_PREFIXPATH_IMAGE [NSString stringWithFormat:@"%@/%@",SWX_STATIC_PREFIXPATH,@"image"]
#define SWX_STATIC_PREFIXPATH_AUDIO [NSString stringWithFormat:@"%@/%@",SWX_STATIC_PREFIXPATH,@"audio"]
#define SWX_STATIC_PREFIXPATH_VIDEO [NSString stringWithFormat:@"%@/%@",SWX_STATIC_PREFIXPATH,@"video"]

@interface SWXHooliURLProtocol : NSURLProtocol

@end


