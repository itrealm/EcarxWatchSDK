//
//  SWXImageHandle.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/7/22.
//

#import <Foundation/Foundation.h>
#import "SWXMWPhotoBrowser.h"



@interface SWXImageHandle : NSObject
typedef void (^SWXImgCallback)(NSString *code,NSString *msg, id result);
+ (SWXImageHandle *)singletonManger;
//图片选择器 参数 :
//      limit     图片张数  默认为9
//      quality   图片质量
//      width     宽
//      height    高
// 返回值 @{@"path":@"图片路径"}
- (void)pick:(NSDictionary *)params :(SWXImgCallback)callback;

//图片浏览器  参数 :
//      files     图片路径数组
//      params   : {
//      current   默认第几张
//      style   String (dots | label | none, def: dots)
//      }
- (void)preview:(NSArray *)files :(NSDictionary *)params :(SWXImgCallback)callback;

//获取图片宽高  图片格式
- (void)info:(NSString *)path :(SWXImgCallback)callback;
//获取图片的exif信息
- (void)exif:(NSString *)path :(SWXImgCallback)callback;


- (void)getPhotoWithLocalIdentifier:(NSString *)localIdentifier completion:(void(^)(UIImage *photo,PHAsset *asset,NSDictionary *info,BOOL isDegraded))completion;
- (void)getVideoWithLocalIdentifier:(NSString *)localIdentifier completion:(void (^)(AVPlayerItem * playerItem,PHAsset *asset, NSDictionary * info))completion;
+ (NSString *)typeForImageData:(NSData *)data;
@end


