//
//  SWXMWGridCell.h
//  SWXMWPhotoBrowser
//
//  Created by Michael Waterfall on 08/10/2013.
//
//

#import <UIKit/UIKit.h>
#import "SWXMWPhoto.h"
#import "SWXMWGridViewController.h"

@interface SWXMWGridCell : UICollectionViewCell {}

@property (nonatomic, weak) SWXMWGridViewController *gridController;
@property (nonatomic) NSUInteger index;
@property (nonatomic) id <SWXMWPhoto> photo;
@property (nonatomic) BOOL selectionMode;
@property (nonatomic) BOOL isSelected;

- (void)displayImage;

@end
