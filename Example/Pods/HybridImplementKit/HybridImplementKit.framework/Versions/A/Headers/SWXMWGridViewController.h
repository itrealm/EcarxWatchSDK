//
//  SWXMWGridViewController.h
//  SWXMWPhotoBrowser
//
//  Created by Michael Waterfall on 08/10/2013.
//
//

#import <UIKit/UIKit.h>
#import "SWXMWPhotoBrowser.h"

@interface SWXMWGridViewController : UICollectionViewController {}

@property (nonatomic, assign) SWXMWPhotoBrowser *browser;
@property (nonatomic) BOOL selectionMode;
@property (nonatomic) CGPoint initialContentOffset;

- (void)adjustOffsetsAsRequired;

@end
