//
//  SWXMWPhotoBrowser.h
//  SWXMWPhotoBrowser
//
//  Created by Michael Waterfall on 14/10/2010.
//  Copyright 2010 d3i. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWXMWPhoto.h"
#import "SWXMWPhotoProtocol.h"
#import "SWXMWCaptionView.h"
#import <AVKit/AVPlayerViewController.h>

// Debug Logging
#if 0 // Set to 1 to enable debug logging
#define MWLog(x, ...) NSLog(x, ## __VA_ARGS__);
#else
#define MWLog(x, ...)
#endif

@class SWXMWPhotoBrowser;

@protocol SWXMWPhotoBrowserDelegate <NSObject>

- (NSUInteger)numberOfPhotosInPhotoBrowser:(SWXMWPhotoBrowser *)photoBrowser;
- (id <SWXMWPhoto>)photoBrowser:(SWXMWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index;

@optional

- (id <SWXMWPhoto>)photoBrowser:(SWXMWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index;
- (SWXMWCaptionView *)photoBrowser:(SWXMWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index;
- (NSString *)photoBrowser:(SWXMWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index;
- (void)photoBrowser:(SWXMWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index;
- (void)photoBrowser:(SWXMWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index;
- (BOOL)photoBrowser:(SWXMWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index;
- (void)photoBrowser:(SWXMWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected;
- (void)photoBrowserDidFinishModalPresentation:(SWXMWPhotoBrowser *)photoBrowser;
- (void)longTapGestureAction:(UIImage *)image;

@end

@interface SWXMWPhotoBrowser : UIViewController <UIScrollViewDelegate, UIActionSheetDelegate, AVPlayerViewControllerDelegate>

@property (nonatomic, weak) IBOutlet id<SWXMWPhotoBrowserDelegate> delegate;
@property (nonatomic) BOOL zoomPhotosToFill;
@property (nonatomic) BOOL displayNavArrows;
@property (nonatomic) BOOL displayActionButton;
@property (nonatomic) BOOL displaySelectionButtons;
@property (nonatomic) BOOL alwaysShowControls;
@property (nonatomic) BOOL enableGrid;
@property (nonatomic) BOOL enableSwipeToDismiss;
@property (nonatomic) BOOL enableLongTap;
@property (nonatomic) BOOL startOnGrid;
@property (nonatomic) BOOL autoPlayOnAppear;
@property (nonatomic) NSUInteger delayToHideElements;
@property (nonatomic, readonly) NSUInteger currentIndex;

// Customise image selection icons as they are the only icons with a colour tint
// Icon should be located in the app's main bundle
@property (nonatomic, strong) NSString *customImageSelectedIconName;
@property (nonatomic, strong) NSString *customImageSelectedSmallIconName;

// Init
- (id)initWithPhotos:(NSArray *)photosArray;
- (id)initWithDelegate:(id <SWXMWPhotoBrowserDelegate>)delegate;

// Reloads the photo browser and refetches data
- (void)reloadData;

// Set page that photo browser starts on
- (void)setCurrentPhotoIndex:(NSUInteger)index;

// Navigation
- (void)showNextPhotoAnimated:(BOOL)animated; 
- (void)showPreviousPhotoAnimated:(BOOL)animated;

@end
