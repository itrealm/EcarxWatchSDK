//
//  ZoomingScrollView.h
//  SWXMWPhotoBrowser
//
//  Created by Michael Waterfall on 14/10/2010.
//  Copyright 2010 d3i. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SWXMWPhotoProtocol.h"
#import "SWXMWTapDetectingImageView.h"
#import "SWXMWTapDetectingView.h"

@class SWXMWPhotoBrowser, SWXMWPhoto, SWXMWCaptionView;

@interface SWXMWZoomingScrollView : UIScrollView <UIScrollViewDelegate, SWXMWTapDetectingImageViewDelegate, MWTapDetectingViewDelegate> {

}

@property () NSUInteger index;
@property (nonatomic) id <SWXMWPhoto> photo;
@property (nonatomic, weak) SWXMWCaptionView *captionView;
@property (nonatomic, weak) UIButton *selectedButton;
@property (nonatomic, weak) UIButton *playButton;

- (id)initWithPhotoBrowser:(SWXMWPhotoBrowser *)browser;
- (void)displayImage;
- (void)displayImageFailure;
- (void)setMaxMinZoomScalesForCurrentBounds;
- (void)prepareForReuse;
- (BOOL)displayingVideo;
- (void)setImageHidden:(BOOL)hidden;

@end
