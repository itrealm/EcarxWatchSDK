//
//  LYShareModel.h
//  LYVehicleShareModule
//
//  Created by fish on 2020/6/5.
//

#import <GLServiceSDK/GLServiceSDK.h>

@protocol LYShareCarInfo;

@interface LYShareCarInfo : BaseModel

@property(nonatomic,copy)NSString *vin;
@property(nonatomic,copy)NSString *temId;
@property(nonatomic,copy)NSString *plateNo;
@property(nonatomic,copy)NSString *modelName;
@property(nonatomic,copy)NSString *modelCode;
@property(nonatomic,copy)NSString *imageUrl;

@end

@interface LYShareListData : BaseModel

@property(nonatomic,copy)NSArray<LYShareCarInfo> *acceptedList;
@property(nonatomic,copy)NSArray<LYShareCarInfo> *sharedList;

@end

@interface LYShareInfo : BaseModel
//相对 vin 码车辆关系：1、表示车主 2、表示分享对象
@property(nonatomic)int userType;
//车分享状态：1、待接受，2、车主已取消，3、用户已取消、4、已接受未开始，5、已接受已开始，6、已接受已超时，7、已拒绝，8、已失效，9、已完成
@property(nonatomic)int userStatus;
//车分享状态：1、未分享，2、已分享
@property(nonatomic)int ownerStatus;
//车分享开始时间、时间戳单位秒
@property(nonatomic)long long startTime;
//车分享结束时间、时间戳单位秒
@property(nonatomic)long long endTime;
//车分享类型：1、固定时间分享 2、永久权限分享
@property(nonatomic)int shareType;

@end
