//
//  LYVehicleShareModule.h
//  GLVehicleShareModule
//
//  Created by fish on 2019/2/21.
//

#import <Foundation/Foundation.h>
#import <LYVehicleShareModule/LYShareModel.h>
@class RACSignal;

//车分享提供功能,返回UIViewController
#define LYVehicleShareModuleHome @"ykt://apicloud/web"

//车分享需要的外部功能
//登录,LID & Auth2
#define LYVehicleShareModuleCallLogin @"ykt://lynkco/user/authlogin"
//调用微信分享,参数:{"title":"title","url":"",@"desc":""}
#define LYVehicleShareModuleCallShareWeChat @"ykt://apicloud/lynkCoShareWeChat"
//跳转到Lynk & Co 整车商城UI,参数:{"controller":id(UIViewController)}
#define LYVehicleShareModuleCallCarMall @"ykt://lynkco/carmall"

@interface LYVehicleShareModule : NSObject

//车分享1.0
+ (UIViewController *)carShare10ViewController;

//车分享1.5
//首页入口
+ (UIViewController *)carShare15ViewController;

// 车分享详情页面,非nil时需跳转页面,nil是不跳转
// @param content GLPushNotification中jsonContent
+ (UIViewController *)carShare15DetailViewController:(NSString *)content;

//获取分享列表
+ (RACSignal<LYShareListData*> *)fetchShareList;
//获取分享信息
+ (RACSignal<LYShareInfo*> *)fetchShareInfo:(NSString *)vin;

@end
