//
//  SWApiGatemayManger.h
//  GeelyLynkcoThird
//
//  Created by shuaishuai on 2019/7/12.
//

#import <Foundation/Foundation.h>
#import "CAClient.h"
#import "CADuplexClient.h"

/*
 使用单例包装下阿里api网关相关对象，保证APP全局一份
 
 ApiGatewaySdk 为阿里api网关官方sdk，这里以源码方式引入
 
 */

NS_ASSUME_NONNULL_BEGIN

@interface SWApiGatemayManger : NSObject

///单通道请求对象，默认- (CAClient*) init;实例化
@property(nonatomic,strong)CAClient *client;
///多通道请求对象，默认- (CAClient*) init;实例化，需要设置host；或自己实例化使用-(instancetype) initWithHost: (NSString*)host;
@property(nonatomic,strong)CADuplexClient *dupleXClient;

///单例，APP全局持有一个
+ (instancetype)share;

@end

NS_ASSUME_NONNULL_END
