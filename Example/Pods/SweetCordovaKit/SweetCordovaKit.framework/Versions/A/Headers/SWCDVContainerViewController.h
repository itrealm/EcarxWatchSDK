//
//  SWCDVContainerViewController.h
//  Pods
//
//  Created by shuaishuai on 2019/8/13.
//
#import <UIKit/UIKit.h>
#import <Cordova/CDVViewController.h>

#define SWC_ColorFromRGBHEX(rgbValue)     [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

NS_ASSUME_NONNULL_BEGIN

extern NSString* const SWCDV_WebDidStartLoad_Notification;
extern NSString* const SWCDV_webViewDidFinishLoad_Notification;
extern NSString* const SWCDV_webDidFailLoad_Notification;
extern NSString* const SWCDV_webShouldStartLoad_Notification;

@interface SWCDVContainerViewController : CDVViewController
@property(nonatomic,strong)UIColor *progressColor;
@property(nonatomic,assign)BOOL isShowProgress;
@property (nonatomic, strong) NSURL *pageLoadSourceURL;
@property(nonatomic,assign)BOOL isBounces;
@property (nonatomic,strong) UIColor *backgroundColor;
@property (nonatomic,assign) BOOL isFastenWebView;
/// 是否从状态栏最上面开始渲染H5页面,默认为NO,从状态栏下面开始渲染H5页面
@property (nonatomic,assign) BOOL isHideStatusView;

- (instancetype)initWithUrl:(NSString *)url pageLoadSourceURL:(NSString *)pageLoadSourceUrl;

///Reload webViewEngine With urlString
- (BOOL)reloadWithUrlStr:(NSString *)urlStr;


+ (void)viewAutoSizeToSuperView:(UIView *)subView;
@end

NS_ASSUME_NONNULL_END
