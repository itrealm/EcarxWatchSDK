//
//  SWCScanOutHandler.h
//  SweetCoreKit
//
//  Created by shuaishuai on 2019/9/23.
//

#import <Foundation/Foundation.h>
#import <HybridImplementKit/HYScanViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWCScanOutHandler : NSObject<HYScanProtocol>

- (void)openURL:(NSString*)URL
        context:(UIViewController *)context;
- (BOOL)isAvailable_cordova:(NSString*)urlStr;

@end

NS_ASSUME_NONNULL_END
