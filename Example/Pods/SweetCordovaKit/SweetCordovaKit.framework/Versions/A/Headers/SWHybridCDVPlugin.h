//
//  SWHybridCDVPlugin.h
//  Pods
//
//  Created by shuaishuai on 2019/8/12.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface SWHybridCDVPlugin : CDVPlugin
- (NSString *)actionKeyPath;
@end


