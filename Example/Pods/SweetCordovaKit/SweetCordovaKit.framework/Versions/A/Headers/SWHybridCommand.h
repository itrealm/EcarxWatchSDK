//
//  SWHybridCommand.h
//  Pods
//
//  Created by shuaishuai on 2019/8/12.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface SWHybridCommand : NSObject

@property (nonatomic, strong) NSString *handleName;
@property (nonatomic, strong) NSString *moduleName;
@property (nonatomic, strong) NSString *className;
@property (nonatomic, strong) NSString *methodName;
@property (nonatomic, strong) NSDictionary *params;
@property (nonatomic, weak) UIView *webView;
@property (nonatomic, weak) UIViewController *controller;

-(void)excuteSuccessCallBack:(id)result;
-(void)excuteFailedCallBack:(id)result;

@end


@interface SWHybridWkCommand : SWHybridCommand
@property (nonatomic, strong) void(^responseCallback)(id);
@end

@interface SWHybridCDVCommand : SWHybridCommand
@property (nonatomic, weak) id <CDVCommandDelegate> commandDelegate;
@property (nonatomic, copy) NSString* callbackId;
@end
