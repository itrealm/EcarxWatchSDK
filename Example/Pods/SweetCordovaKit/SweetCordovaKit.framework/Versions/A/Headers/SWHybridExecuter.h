//
//  SWHybridExecuter.h
//  Pods
//
//  Created by shuaishuai on 2019/8/12.
//

#import <Foundation/Foundation.h>
#import "SWHybridCommand.h"

@interface SWHybridExecuter : NSObject

+(id)executerObjForKeyPath:(NSString*)keyPath;

+(void)registerExecuter:(Class)aclass forKeyPath:(NSString*)keyPath;
+(void)removeExecuter:(Class)aclass forKeyPath:(NSString*)keypath;

-(void)exectue:(SWHybridCommand*)command;


@end


