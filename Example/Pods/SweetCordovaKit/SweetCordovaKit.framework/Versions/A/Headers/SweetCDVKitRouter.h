//
//  SweetCDVKitRouter.h
//  Pods
//
//  Created by shuaishuai on 2019/8/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SweetCDVKitRouter : NSObject

/*
 路由地址：/h5/host
    参数：
        h5_index_url：H5链接地址
        page_load_url：错误页地址
 
 */

@end

NS_ASSUME_NONNULL_END
