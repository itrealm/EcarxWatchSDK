//
//  SweetCordovaKit.h
//  Pods
//
//  Created by shuaishuai on 2019/8/13.
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <Cordova/CDVViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface SweetCordovaKit : NSObject

@property(nonatomic,strong)CDVViewController *ViewController;

- (void)registerPlugin:(CDVPlugin*)plugin className:(NSString*)className;
- (void)registerPlugin:(CDVPlugin*)plugin pluginName:(NSString*)pluginName;

- (void)registerPlugin:(Class)pluginClass;

@end

NS_ASSUME_NONNULL_END
