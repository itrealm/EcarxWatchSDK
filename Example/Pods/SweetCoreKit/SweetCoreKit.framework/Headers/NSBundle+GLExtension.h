//
//  NSBundle+GLExtension.h
//  Pods-sweet-CoreKit_Example
//
//  Created by yinhaichao on 2018/5/2.
//

#import <Foundation/Foundation.h>

@interface NSBundle (GLExtension)

/**
 获取资源bundle

 @param bundleClass [self class]
 @param bundleName 当前模块/组件名
 */
+ (NSBundle *)gl_resourceBundle:(Class)bundleClass bundleName:(NSString *)bundleName;

@end
