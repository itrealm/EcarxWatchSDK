//
//  NSObject+HCPerform.h
//  HCRouterKit
//
//  Created by Goot on 2018/10/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (HCPerform)

- (id)performSelector:(SEL)aSelector withParameter:(id)parameter;

@end

NS_ASSUME_NONNULL_END
