//
//  NSObject+MethodSwizzling.h
//  Pods
//
//  Created by yinhaichao on 2018/5/9.
//

#import <Foundation/Foundation.h>

@interface NSObject (MethodSwizzling)


/**
 交换实例方法

 */
+ (void)hook_swizzleInstanceSelector:(SEL)origSelector withSelector:(SEL)newSelector;

/**
 交换类方法

 */
+ (void)hook_swizzleClassSelector:(SEL)origSelector withSelector:(SEL)newSelector;

@end
