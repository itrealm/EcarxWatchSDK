//
//  SWABTService.h
//  Pods
//
//  Created by shuaishuai on 2018/12/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^BACKBLOCK)(int);
typedef void(^INT_BACKBLOCK)(int);

@interface SWABTService : NSObject


/**
 实例化单例

 @return 单例对象
 */
+ (instancetype)sharedInstance;

/**
 设置平台请求的IP地址，如果该方法不被调用或者传入nil则默认从mainBundle下plist读取API_BASE_URL
 */
- (void)setUpServiceIP:(NSString *)serviceIP;

/**
 检测APP更新
 
 @param completion 查询到的APP更新信息数据
 versionInfo：查询到最新APP版本信息
 onceUpdated：该版本的曾经更新情况：0:未提示过更新，-1:用户取消过更新，-2:用户忽略该版本更新，1:用户确定该过版本的更新
 updateBlock: 回传的用户更新情况，0:未提示过更新，-1:用户取消过更新，-2:用户忽略该版本更新，1:用户确定该过版本的更新
 */
- (void)checkAppVersion:(void(^)(NSDictionary *versionInfo, int onceUpdated, INT_BACKBLOCK updateBlock))completion;

/**
 直接从平台获取最新的APP版本信息

 @param completion 信息回调
 */
- (void)appVersionInfo:(void(^)(NSDictionary *versionInfo))completion;


/**
 向平台上报一次采集到的手机端信息，默认或自动调起一次
 */
- (void)pushMobileData;

/**
 开启路由心跳，默认会自动开启
 */
- (void)startBeatHeart_router;

/**
 关闭路由心跳
 */
- (void)stopBeatHeart_router;


#pragma mark - 持久存储NSUserDefaults

+ (BOOL)saveUserDefaults:(NSString *)key obj:(id)obj;
+ (id)getObjFromUserDefaults:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
