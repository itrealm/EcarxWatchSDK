//
//  SWCoreConfiguration.h
//  Pods
//
//  Created by 殷海超 on 2018/5/30.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const SWEnvironmentDevolop;
FOUNDATION_EXPORT NSString *const SWEnvironmentTest;
FOUNDATION_EXPORT NSString *const SWEnvironmentPre_release;
FOUNDATION_EXPORT NSString *const SWEnvironmentProduct;

@interface SWCoreConfiguration : NSObject

/**
 设置当前环境（default:SWEnvironmentDevolop）
 */
@property (assign, nonatomic) NSInteger curEnviroment;
/**
 环境变量
 */
@property (copy, nonatomic) NSMutableArray *enviroments;

/**
 自定义配置
 */
@property (copy, nonatomic) NSMutableDictionary *config;

/**
 路由scheme
 */
@property (copy, nonatomic) NSString *scheme;

+ (instancetype)defaultConfiguration;

@end
