//
//  SWCoreMacro.h
//  Pods
//
//  Created by 殷海超 on 2018/6/4.
//

#ifndef SWCoreMacro_h
#define SWCoreMacro_h

/** RGB颜色 */
#define UIColorFromRGB(r,g,b,a)          [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

//RGB颜色十六进制
#define UIColorFromRGB16(rgbValue)       [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


/** 是否为iOS8及以上系统 */
#define iOS8 ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0)
/** 是否为iOS9及以上系统 */
#define iOS9 ([[UIDevice currentDevice].systemVersion doubleValue] >= 9.0)
/** 是否为iOS10及以上系统 */
#define iOS10 ([[UIDevice currentDevice].systemVersion doubleValue] >= 10.0)
/** 是否为iOS11及以上系统 */
#define iOS11 ([[UIDevice currentDevice].systemVersion doubleValue] >= 11.0)


/** 屏幕宽度 */
#define ScreenWidth           [UIScreen mainScreen].bounds.size.width
/** 屏幕高度 */
#define ScreenHeight          [UIScreen mainScreen].bounds.size.height
/** 导航栏高度 */
#define NavigationHeight      44.0f
/** Tab栏高度 */
#define TabbarHeight          49.0f

/** weak对象 */
#define WeakSelf(weakSelf) __weak __typeof(&*self)weakSelf = self

#endif /* SWCoreMacro_h */
