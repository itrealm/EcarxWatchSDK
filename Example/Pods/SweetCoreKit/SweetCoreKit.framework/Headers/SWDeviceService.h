//
//  SWDeviceService.h
//  Pods
//
//  Created by 殷海超 on 2018/5/30.
//

#import <Foundation/Foundation.h>

@interface SWDeviceService : NSObject

/**
 获取设备唯一标识符(设备UUID，理论上UUID在app每次启动时都会改变，所以我们吧UUID保存在钥匙串中，以此作为设备唯一标识符，同一开发者下的应用之间UUID相同且唯一;
 
 @return 设备唯一标识符
 */
+ (NSString *)getDeviceId;

@end
