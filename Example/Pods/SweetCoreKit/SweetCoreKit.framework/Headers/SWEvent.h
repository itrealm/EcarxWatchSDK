//
//  SWEventData.h
//  Pods
//
//  Created by 殷海超 on 2018/5/24.
//

#import <Foundation/Foundation.h>

@interface SWEvent : NSObject <NSObject>

/**
 事件名
 */
@property (copy, nonatomic) NSString *eventName;

/**
 事件对象
 */
@property (strong, nonatomic) id object;

/**
 事件数据
 */
@property (copy, nonatomic) NSDictionary *eventData;

- (instancetype)initWithEventName:(NSString *)eventName
                           object:(id _Nullable)object
                        eventData:(NSDictionary * _Nullable)eventData;

@end
