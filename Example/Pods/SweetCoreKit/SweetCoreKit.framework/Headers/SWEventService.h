//
//  SWEventManagementService.h
//  Pods
//
//  Created by 殷海超 on 2018/5/24.
//

#import <Foundation/Foundation.h>
#import "SWEvent.h"

#pragma ==========  用户会话事件  ==========

//登录事件
FOUNDATION_EXPORT NSString *const SWEventLoginKey;
//登出事件
FOUNDATION_EXPORT NSString *const SWEventLogoutKey;
//用户信息更新事件
FOUNDATION_EXPORT NSString *const SWEventUserInfoUpdateKey;


NS_ASSUME_NONNULL_BEGIN

/**
 订阅事件执行回调

 @param event 回调信息
 */
typedef void(^EventBlock)(SWEvent *event);

/**
 事件管理服务
 */
@interface SWEventService : NSObject

/**
 service单例

 @return 事件管理服务
 */
+ (instancetype)defaultService;

/**
 发布事件
 
 @param eventName 事件名
 */
- (void)publishEventName:(NSString *)eventName;

/**
 发布事件
 
 @param eventName 事件名
 @param eventData 事件数据
 */
- (void)publishEventName:(NSString *)eventName
                  object:(id _Nullable)object
               eventData:(NSDictionary * _Nullable)eventData;

/**
 添加事件订阅者

 @param subscriber 订阅者
 @param eventName 事件名
 @param selector 执行方法
 */
- (void)addSubscriber:(id)subscriber
             selector:(SEL)selector
            eventName:(NSString *)eventName
               object:(id _Nullable)object;

/**
 添加事件订阅者
 
 @param subscriber 订阅者
 @param eventName 事件名
 @param object 对象
 @param queue 队列,决定 block 在哪个线程队列中执行, nil  在发布通知的线程中执行
 @param block 执行回调
 */
- (void)addSubscriber:(id)subscriber
            eventName:(NSString *)eventName
               object:(id _Nullable)object
                queue:(NSOperationQueue * _Nullable)queue
                block:(EventBlock _Nullable)block;

/**
 移除事件订阅者

 @param subscriber 订阅者
 @param eventName 事件名
 */
- (void)removeSubscriber:(id)subscriber
               eventName:(NSString *)eventName
                  object:(id _Nullable)object;

/**
 移除事件订阅者

 @param subscriber 订阅者
 */
- (void)removeSubscriber:(id)subscriber;

@end

NS_ASSUME_NONNULL_END
