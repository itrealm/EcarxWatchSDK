//
//  SWKeyChainService.h
//  Pods
//
//  Created by 殷海超 on 2018/6/6.
//

#import <Foundation/Foundation.h>

@interface SWKeyChainService : NSObject

/**
 存储数据/对象到KeyChain

 @param object 数据/对象
 @param key key
 @return 存储是否成功
 */
+ (BOOL)saveObject:(id<NSCoding>)object forKey:(NSString * _Nonnull)key;

/**
 存储数据/对象到KeyChain

 @param object 数据/对象
 @param key key
 @param group 用与应用间共享KeyChain数据（此处应填入Capabilities -> KeyChain Sharing -> KeyChain Groups）注：应用间KeyChain共享数据的前提是：应用属于同一开发者账号下，否则无法共享
 @return 存储是否成功
 */
+ (BOOL)saveObject:(id<NSCoding>)object forKey:(NSString * _Nonnull)key forAccessGroup:(NSString *)group;

/**
 加载数据/对象

 @param key key
 @return 加载成功后返回数据/对象
 */
+ (id)loadObjectForKey:(NSString * _Nonnull)key;

/**
 加载数据/对象

 @param key key
 @param group 用与应用间共享KeyChain数据（此处应填入Capabilities -> KeyChain Sharing -> KeyChain Groups）注：应用间KeyChain共享数据的前提是：应用属于同一开发者账号下，否则无法共享
 @return 加载成功后返回数据/对象
 */
+ (id)loadObjectForKey:(NSString * _Nonnull)key forAccessGroup:(NSString *)group;

/**
 删除数据/对象

 @param key key
 @return 删除是否成功
 */
+ (BOOL)deleteObjectForkey:(NSString * _Nonnull)key;

/**
 删除数据/对象

 @param key key
 @param group 用与应用间共享KeyChain数据（此处应填入Capabilities -> KeyChain Sharing -> KeyChain Groups）注：应用间KeyChain共享数据的前提是：应用属于同一开发者账号下，否则无法共享
 @return 删除是否成功
 */
+ (BOOL)deleteObjectForkey:(NSString * _Nonnull)key forAccessGroup:(NSString *)group;

@end
