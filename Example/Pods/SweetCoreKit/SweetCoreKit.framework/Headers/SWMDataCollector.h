//
//  SWMDataCollector.h
//  Pods
//
//  Created by shuaishuai on 2018/12/24.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^LOCATIONBLOCK)(CLLocation *currLocation);
typedef void(^DICTIONARYBLOCK)(NSDictionary *dic);
typedef void(^STRINGBLOCK)(NSString *string);

NS_ASSUME_NONNULL_BEGIN

@interface SWMDataCollector : NSObject


#pragma mark - device
///deviceID
+ (NSString *)getDeviceId;
///机型号
+ (NSString *)device;
///系统名称
+ (NSString *)osName;
///手机系统版本
+ (NSString *)osVersion;
///手机分辨率
+ (NSString *)resolution;
///手机当前方向
+ (NSString *)orientation;
///获取手机l电量
+ (NSInteger)batteryLevel;
///网络类型（WiFi还是4G）
+ (NSUInteger)connectionType;
///剩余RAM空间
+ (unsigned long long)freeRAM;
///全部RAM空间
+ (unsigned long long)totalRAM;
///剩余内存
+ (unsigned long long)freeDisk;
///全部内存
+ (unsigned long long)totalDisk;

#pragma mark - IP
///获取设备公网ip
+ (void)deviceNetIp:(STRINGBLOCK)netIpBlock;

#pragma mark - 定位
/**
 获取地理位置坐标
 @param locationBlock 坐标回调
 */
+ (void)location:(LOCATIONBLOCK)locationBlock;

/**
 获取反编码的地理位置 （以"en-US"语言方式返回）
 @param reverseLocationBlock 反编码后回调
 */
+ (void)reverseLocation:(DICTIONARYBLOCK)reverseLocationBlock;

#pragma mark - APP
///AppBundleID
+ (NSString *)appBundleID;
///APP版本信息
+ (NSString *)appVersion;
///APPd build
+ (NSString *)appBuild;
//+ (NSString *)locale;
///AppName
+ (NSString *)appName;
///获取app所有信息字典
+ (NSDictionary *)appInfo;

@end

NS_ASSUME_NONNULL_END
