//
//  SWRouterService.h
//  Pods
//
//  Created by 殷海超 on 2018/5/31.
//

#import <Foundation/Foundation.h>
#import "HCRouterKit.h"

#define ROUTER_PATH [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/router_list/router_list.plist"]

NS_ASSUME_NONNULL_BEGIN

@interface SWRouterService : NSObject

/**
 注册路由Scheme 一个项目只拥有一个scheme
 
 @param scheme 路由以scheme://host/path/为地址,parameters为参数跳转,此方法注册App路由的唯一scheme(可取公司英文名/汉语拼音首字母);
 */
+ (BOOL)registerRouterWithScheme:(NSString *)scheme;

/**
 向路由注册host
 
 @param host 路由以scheme://host/path/为地址,parameters为参数跳转,此方法添加一个classQuickName（类的快捷问名）;
 @param className 类名
 */
+ (void)registerHost:(NSString *)host
        forClassName:(NSString *)className;

/**
 向路由注册path

 @param path 路由以scheme://host/path/为地址,parameters为参数跳转,此方法添加一个path（类的快捷问名);
 @param selectorName 方法名
 @param host host名
 */
+ (void)registerPath:(NSString *)path
     forSelectorName:(NSString *)selectorName
            baseHost:(NSString *)host;

/**
 执行路由跳转
 
 @param url 路由地址 scheme://host/path/
 */
+ (void)openURL:(NSString *)url DEPRECATED_MSG_ATTRIBUTE("use +openUrl: instead");

/**
 执行路由跳转
 
 @param url 路由地址 scheme://host/path/
 @param parameters 参数
 */
+ (void)openURL:(NSString *)url parameters:(NSDictionary *_Nullable)parameters DEPRECATED_MSG_ATTRIBUTE("use +openUrl:parameters: instead");

/**
 执行路由跳转
 
 @param url 路由地址 scheme://host/path/
 @param parameters 参数
 @param completion 回调数据
 */
+ (void)openURL:(NSString *)url
     parameters:(NSDictionary *_Nullable)parameters
     completion:(HCRouterCompletionBlock _Nullable)completion DEPRECATED_MSG_ATTRIBUTE("use +openUrl:parameters: instead");

/**
 执行路由跳转
 
 @param url 路由地址 scheme://host/path/
 @param parameters 参数
 @param completion 回调数据
 @param finished 路由执行者回调（A通过路由跳转到B,B返回A时执行的回调。可用于B->A传递数据）回调需遵循HCRouterFinishedProtocol协议，通过
 - (void)router_performFinishedObject:(id _Nullable)finishedObject;
 进行回调
 */
+ (void)openURL:(NSString *)url
     parameters:(NSDictionary *_Nullable)parameters
     completion:(HCRouterCompletionBlock _Nullable)completion
       finished:(HCRouterFinishedBlock _Nullable)finished DEPRECATED_MSG_ATTRIBUTE("use +openUrl:parameters:callback: instead");

/**
 执行路由跳转
 
 @param url 路由地址 scheme://host/path/
 @param parameters 参数
 @param completion 回调数据
 @param finished 路由执行者回调（A通过路由跳转到B,B返回A时执行的回调。可用于B->A传递数据）回调需遵循HCRouterFinishedProtocol协议，通过
 - (void)router_performFinishedObject:(id _Nullable)finishedObject;
 进行回调
 */
+ (void)openMainURL:(NSString *)url
         parameters:(NSDictionary *_Nullable)parameters
         completion:(HCRouterCompletionBlock _Nullable)completion
           finished:(HCRouterFinishedBlock _Nullable)finished DEPRECATED_MSG_ATTRIBUTE("use +openMainUrl instead");
/**
 设置主路由
 
 @return 返回app入口《UIViewController类型》
 */

+ (id)openMainUrl;

/**
 执行路由跳转
 
 @return url 路由地址 scheme://classQuickName/selectorQuickName/
 */
+ (_Nullable id)openUrl:(NSString *)url;

/**
 执行路由跳转
 
 @param url 路由地址 scheme://classQuickName/selectorQuickName/
 @param parameters 参数
 */
+ (_Nullable id)openUrl:(NSString *)url
             parameters:(NSDictionary *_Nullable)parameters;

/**
 执行路由跳转
 
 @param url 路由地址 scheme://classQuickName/selectorQuickName/
 @param parameters 参数
 @param callback 路由执行者回调（A通过路由跳转到B,B返回A时执行的回调。可用于B->A传递数据）
 */
+ (_Nullable id)openUrl:(NSString *)url
             parameters:(NSDictionary *_Nullable)parameters
               callback:(HCRouterFinishedBlock _Nullable)callback;

@end

NS_ASSUME_NONNULL_END
