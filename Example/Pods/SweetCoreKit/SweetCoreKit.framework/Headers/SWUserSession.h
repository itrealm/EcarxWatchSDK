//
//  SWUserSession.h
//  Pods
//
//  Created by 殷海超 on 2018/5/24.
//

#import <Foundation/Foundation.h>

@interface SWUserSession : NSObject <NSCoding, NSCopying>

/**
 用户id
 */
@property (copy, nonatomic) NSString *userId;

/**
 用户名
 */
@property (copy, nonatomic) NSString *name;

/**
 用户头像
 */
@property (copy, nonatomic) NSString *avatar;

/**
 用户token
 */
@property (copy, nonatomic) NSString *token;

/**
 用户标签
 */
@property (copy, nonatomic) NSArray *tags;

/**
 用户其他信息
 */
@property (copy, nonatomic) NSDictionary *userDic;

@end
