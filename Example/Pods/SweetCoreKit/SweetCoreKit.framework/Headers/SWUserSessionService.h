//
//  SWUserSessionService.h
//  Pods
//
//  Created by 殷海超 on 2018/5/24.
//

#import <Foundation/Foundation.h>
#import "SWUserSession.h"

/** 会话信息key */
FOUNDATION_EXPORT NSString *const SWUserSessionKey;

/** 会话信息字典key */
FOUNDATION_EXPORT NSString *const SWUserDicKey;

NS_ASSUME_NONNULL_BEGIN

/**
 用户信息Block

 @param userSession 用户会话
 */
typedef void(^UserSessionBlock)(NSDictionary * _Nullable userSession);

/**
 用户会话服务
 */
@interface SWUserSessionService : NSObject

/**
 service单例
 
 @return 用户会话服务
 */
+ (instancetype)defaultService;

/**
 登录
 
 @param block 登录回调
 */
- (void)login:(UserSessionBlock)block;

/**
 登录

 @param parameters 参数
 @param block 登录回调
 */
- (void)login:(NSDictionary * _Nullable)parameters userBlock:(UserSessionBlock)block;

/**
 登出
 */
- (void)logout;

/**
 获取用户会话信息

 @param userSessionBlock 用户会话信息回调
 */
- (void)getUser:(UserSessionBlock)userSessionBlock;

/**
 获取用户会话信息

 @return 用户会话信息
 */
- (NSDictionary *)getUser;

/**
 获取当前登录状态

 @return YES：已登录  NO：未登录
 */
- (BOOL)isLogin;

/**
 通过KVC修改用户会话信息的某一条
 
 @param key session属性key
 @param value 值
 */
- (void)updateUserSessionforKey:(NSString *)key value:(id _Nonnull)value;

/**
 更新用户会话信息

 @param session 用户会话信息
 */
- (void)updateUserSession:(NSDictionary *)session;

@end

NS_ASSUME_NONNULL_END
