//// NSDictionary+buildResult.h
        // SweetWeexKit
        //
        // Created by shuaishuai on 2018/9/18.
        // 
        //
    

#import <Foundation/Foundation.h>

@interface NSDictionary (buildResult)

+ (NSDictionary *)result_ok:(id)data;

+ (NSDictionary *)result_n:(NSString *)errorCode
                       msg:(NSString *)msg
                      data:(id)data;

@end
