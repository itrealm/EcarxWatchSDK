//
//  NSString+swxMD5.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (swxMD5)
- (NSString *)md5_32bit;
- (NSString *)MD5_32BIT;
@end

NS_ASSUME_NONNULL_END
