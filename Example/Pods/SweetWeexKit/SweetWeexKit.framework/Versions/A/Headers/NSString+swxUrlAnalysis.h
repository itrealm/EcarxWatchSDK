//
//  NSString+swxUrlAnalysis.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/3.
//  分析weex链接规则

#import <Foundation/Foundation.h>

static NSString *const SWX_k_CONFIG_FIleNAME = @"weexconfig.json";

@interface NSString (swxUrlAnalysis)

#pragma mark - URL处理工具
- (NSString *)swx_safe;

///url拼接参数，成为一个带参数的URL
- (NSString *)swx_connectParams:(NSDictionary *)params;

///除去URL中的参数
- (NSString *)swx_cleanUrl;

///链接字符串统一转化为本地地址URL,防止？被转码%3F
- (NSURL *)swx_fileUrl;

#pragma mark - weex前端链接解析规则

///解析weex链接地址，获取唯一识别标志的链接（根目录链接地址）
- (NSString *)swx_baseUrl;


///解析weex链接地址，获取config文件地址
- (NSString *)swx_configUrl;

///检测是否与当前链接地址属于同一个模块（是否当前weex检测规则）
- (BOOL)swx_isOneMudleUrl:(NSString *)otherUrl;

#pragma Mark - 解析config.json文件

//使用baseurl：拼接相对路径，得出完整的URL
- (NSString *)swx_connection:(NSString *)relativePath;

@end


