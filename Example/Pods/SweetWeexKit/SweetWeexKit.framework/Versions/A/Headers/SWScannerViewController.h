//// SWScannerViewController.h
        // Pods
        //
        // Created by shuaishuai on 2018/9/14.
        // 
        //
    

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

//扫描会开启一个小程序
@interface SWScannerViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@property(nonatomic,assign)BOOL isMiniApp;

@end
