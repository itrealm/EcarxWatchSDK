//// SWWXConfigurationPlugin.h
        // SweetWeexKit
        //
        // Created by shuaishuai on 2018/9/18.
        // 
        //
    

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

@interface SWWXConfigurationPlugin : NSObject <WXModuleProtocol>

@end
