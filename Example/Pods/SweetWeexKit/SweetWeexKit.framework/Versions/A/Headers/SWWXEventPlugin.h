//// SWWXEventPlugin.h
        // SweetWeexKit
        //
        // Created by shuaishuai on 2018/9/17.
        // 
        //
    

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

/*
 sw-0   200 0
 问题：
 1.code:插件自身的通用code码 和 业务code码 防止重复； ”成功“时候同为一个code码-->使用插件自身的通用code码 ;
 2.携带js方法的规则
 
 callback
 {
 "code":"native返回的错误码，0:调用成功;其他表示调用失败:具体失败code由各自业务定义",
 "msg":"错误描述",
 "data":{
 "原生/js按业务定义的key1":"value1",
 "原生/js按业务定义的key2":"value2",
 ...
 }
 }
 */

@interface SWWXEventPlugin : NSObject <WXModuleProtocol>

@end
