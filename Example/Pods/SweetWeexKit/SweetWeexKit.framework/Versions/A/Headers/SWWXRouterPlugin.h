//// SWWXJumpRouter.h
        // Pods
        //
        // Created by shuaishuai on 2018/9/14.
        // 
        //
    

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

@interface SWWXRouterPlugin : NSObject <WXModuleProtocol>

#pragma mark - weex路由处理
/**
 特殊的处理weex路由
 
 @param routerUrl 路由链接 已经转换为iOS通用路由格式："schem://host/path"
 @param parameters 前端携带的参数
 @param completion 执行结束
 {
     @"code":@"", //成功时候“0”
     @"message":@"",
     @"data":@""
 }
 */
- (void)routerSWXModel_handle:(NSString *)routerUrl
                   parameters:(NSDictionary *_Nullable)parameters
                 curContainer:(UIViewController *)curContainer
                   completion:(void(^)(NSDictionary * result))completion;

@end
