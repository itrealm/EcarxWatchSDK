//
//  SWWXStatusBarPlugin.h
//  SweetWeexKit
//
//  Created by geely on 2018/9/27.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWWXStatusBarPlugin : NSObject <WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
