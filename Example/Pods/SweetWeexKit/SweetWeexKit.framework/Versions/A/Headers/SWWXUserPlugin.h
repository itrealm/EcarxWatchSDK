//// SWWXUserPlugin.h
        // SweetWeexKit
        //
        // Created by shuaishuai on 2018/9/17.
        // 
        //
    

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

@interface SWWXUserPlugin : NSObject <WXModuleProtocol>

@end
