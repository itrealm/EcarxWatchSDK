//
//  SWWebImageDownloader.h
//  SweetWeexPlugin
//
//  Created by shuaishuai on 2019/8/22.
//

#import <SDWebImage/SDWebImageDownloader.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWWebImageDownloader : SDWebImageDownloader
+ (SWWebImageDownloader *)singletonDownloader;

///使用该方法获取存储在本地的图片
- (nullable SDWebImageDownloadToken *)downloadImageWithURL:(nullable NSURL *)url
                                                   options:(SDWebImageDownloaderOptions)options
                                                  progress:(nullable SDWebImageDownloaderProgressBlock)progressBlock
                                                 completed:(nullable SDWebImageDownloaderCompletedBlock)completedBlock;
@end

NS_ASSUME_NONNULL_END
