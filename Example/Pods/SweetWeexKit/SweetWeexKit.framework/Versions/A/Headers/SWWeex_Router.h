//// SWWeex_Router.h
        // SweetWeexKit
        //
        // Created by shuaishuai on 2018/9/14.
        // 
        //
    

#import <Foundation/Foundation.h>

@interface SWWeex_Router : NSObject


#pragma mark: 小程序/应用模块

    /*
    1.
     新创建一个小程序页面（SWXMiniProgramViewController）
        使用说明：如果是小程序模块，外部请加UINavigationController；如果作为小程序次级页面，则外部请直接push。
     路由地址：
        [scheme]://weex/swxMiniProgramVC
     参数：
         @{
             weex_index_url:@""  //远程资源链接
             page_load_url：@“”  //白屏页面或者错误页面链接，（APP层写死？）
         }
     
     2.
      新创建一个应用页面（SWXApplicationViewController）
         使用说明：
            如果是应用模块，外部请加UINavigationController；如果作为应用次级页面，则外部请直接push；
            此外，应用页面可以作为一个APP的根视图
      路由地址：
        [scheme]://weex/swxApplicantionVC
      参数：
         @{
             weex_index_url:@""  //远程资源链接
             page_load_url：@“”  //白屏页面或者错误页面链接，（APP层写死？）
         }
     
    3.
     新创建APP主页面（SWXApplicationViewController）(应用页面)
        使用说明：
            该页面实际是一个应用页面，可以作为一个APP的根视图
     路由地址：
         [scheme]://weex/swxMainVC
     参数：
         @{
             weex_index_url:@""  //远程资源链接
             page_load_url：@“”  //白屏页面或者错误页面链接，（APP层写死？）
         }
     
    4.
     开启一个扫描页面
       使用说明：扫描weex生成的二维码，会唤起一个小程序模块加载
     路由地址：
         [scheme]://weex/swxScanner
     参数：nil
     @{
        weex_mode:@"" //app:应用；applet：小程序：nil：默认小程序
     }
     
    5.
     新建一个小程序或应用视图
        使用说明：会根据传入的参数新建对应视图（小程序、应用、原始视图）
     路由地址：
        [scheme]://weex/host
     参数：
         @{
            weex_index_url:@"",  //远程资源链接
            weex_mode:@"", //app:应用；applet：小程序：nil：默认小程序
            container_id:@"",
            page_load_url：@“”,  //白屏页面或者错误页面链接，
            hide_loading：yes/NO,  //是否隐藏loading
            host_backgroundColor：RGB16 NSNumber,  //host背景颜色
            load_backgroundColor：RGB16 NSNumber,  //loadVC背景颜色


         }
     
    6.
     新创建weex自带的容器页面（WXBaseViewController）
        使用说明：
            该页面是weex原SDK提供的加载容器页面
     路由地址：
        [scheme]://weex/wxViewController
     参数：
         @{
            weex_index_url:@""  //远程资源链接
         }
     
    7.
     新创建weex自带的带导航容器页面（WXRootViewController）
        使用说明：
            该页面是weex原SDK提供的加载容器页面，已经被放入UINavigationController
     路由地址：
        [scheme]://weex/wxNAViewController
     参数：
         @{
            weex_index_url:@""  //远程资源链接
         }
     
     
     */




@end
