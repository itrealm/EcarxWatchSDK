//// SWWeextKit.h
        // Pods
        //
        // Created by shuaishuai on 2018/9/13.
        // 
        //
    

#import <Foundation/Foundation.h>
#import <WeexSDK/WXBaseViewController.h>
#import <WeexSDK/WXRootViewController.h>
#import "SWScannerViewController.h"
//#import <SweetWeexKit/SWXHostViewController.h>

#import "SWXMiniProgramViewController.h"
#import "SWXApplicationViewController.h"
#import "SWXPluginLoader.h"


@interface SWWeextKit : NSObject

@property (nonatomic, strong) NSURL *swxPageLoadSourceURL;

@property (nonatomic, strong) NSDictionary *environment;


/**
 Initializes the SWWeextKit environment

 @param appGroup  Group or organization of your app, default value is @“GEELY”.
 @param appName  Name of your app, default is value for CFBundleDisplayName in main bundle.
 @param externalUserAgent  External user agent of your app, all requests sent by weex will set the user agent on header,  default value is nil.
 */
+ (void)initWeexKit:(NSString *)appGroup
            appName:(NSString *)appName
  externalUserAgent:(NSString *)externalUserAgent;

+ (instancetype)shareSWWeextKit;

+ (void)bugLog:(BOOL)isLog;

+ (void)netConfig:(void(^)(id obj,NSError *error))completed;

#pragma mark - swx
+ (SWXHostBaseViewController *)createSWXHostBaseViewController:(NSURL *)sourceURL pageLoadURL:(NSURL *)pageLoadURL;
+ (SWXMiniProgramViewController *)createSWXMiniProgramViewController:(NSURL *)sourceURL pageLoadURL:(NSURL *)pageLoadURL;
+ (SWXApplicationViewController *)createSWXApplicationViewController:(NSURL *)sourceURL pageLoadURL:(NSURL *)pageLoadURL;

#pragma mark - wx
+ (WXBaseViewController *)createWXBaseViewController:(NSURL *)sourceURL;
+ (WXRootViewController *)createWXNavigationRootViewController:(NSURL *)sourceURL;

#pragma mark - scanner
+ (SWScannerViewController *)createScannerViewController;

@end
