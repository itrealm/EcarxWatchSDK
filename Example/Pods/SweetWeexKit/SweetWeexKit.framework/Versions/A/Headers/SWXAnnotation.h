//
//  SWXAnnotation.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/7/17.
//

#import <Foundation/Foundation.h>
#import "SWWeextKit.h"
#import "SWXPluginMacro.h"

//#ifndef SWXModuleSectName
//#define SWXModuleSectName "SWXModules"
//#endif
//
//#ifndef SWXComponentSectName
//#define SWXComponentSectName "SWXComponents"
//#endif
//
//#ifndef SWXHandlerSectName
//#define SWXHandlerSectName "SWXHandlers"
//#endif


//#define SWX_DATA(sectname) __attribute((used, section("__DATA,"#sectname" ")))

//#define SWX_Export_Module(name) \
//class SWWeextKit; char * k##name##_mod SWX_DATA(SWXModules) = ""#name"";

/**
 * this macro is used to auto regester moudule.
 *  example: WX_PlUGIN_EXPORT_MODULE(test,WXTestModule)
 **/
//#define SWX_PlUGIN_EXPORT_MODULE(jsname,classname) \
//class SWWeextKit; char * k##jsname##_service SWX_DATA(SWXModules) = "{ \""#jsname"\" : \""#classname"\"}";
//#define SWX_PlUGIN_EXPORT_MODULE(jsname,classname) \
//char * k##jsname##_service SWX_DATA(SWXModules) = "{ \""#jsname"\" : \""#classname"\"}";
//
///**
// *  this macro is used to auto regester component.
// *  example:WX_PlUGIN_EXPORT_COMPONENT(test,WXTestCompnonent)
// **/
//#define SWX_PlUGIN_EXPORT_COMPONENT(jsname,classname) \
//class SWWeextKit; char * k##jsname##_service SWX_DATA(SWXComponents) = "{ \""#jsname"\" : \""#classname"\"}";
//
///**
// *  this macro is used to auto regester handler.
// *  example:WX_PlUGIN_EXPORT_HANDLER(WXImgLoaderDefaultImpl,WXImgLoaderProtocol)
// **/
////#define SWX_PlUGIN_EXPORT_HANDLER(jsimpl,jsprotocolname) \
////class SWWeextKit; char * k##jsname##_service SWX_DATA(SWXHandlers) = "{ \""#jsimpl"\" : \""#jsprotocolname"\"}";
//#define SWX_PlUGIN_EXPORT_HANDLER(jsimpl,jsprotocolname) \
//char * k##jsname##_service SWX_DATA(SWXHandlers) = "{ \""#jsimpl"\" : \""#jsprotocolname"\"}";

//----------------------------------


@interface SWXAnnotation : NSObject

@end


