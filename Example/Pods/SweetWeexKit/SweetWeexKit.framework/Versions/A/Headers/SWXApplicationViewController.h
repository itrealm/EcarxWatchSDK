//
//  SWXApplicationViewController.h
//  SweetWeexKit
//
//  Created by geely on 2018/10/17.
// 应用模式

#import "SWXHostBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWXApplicationViewController : SWXHostBaseViewController

+ (SWXApplicationViewController *)createSWXApplicationViewController:(NSURL *)weexSourceURL;

@end

NS_ASSUME_NONNULL_END
