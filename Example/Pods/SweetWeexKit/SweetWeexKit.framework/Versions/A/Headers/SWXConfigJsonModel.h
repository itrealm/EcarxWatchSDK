//
//  SWXConfigJsonModel.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/5/28.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXPreloadPageModel : NSObject
@property(nonatomic,copy)NSString *log;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *describe;
@end

@interface SWXPreloadCellModel : NSObject
@property(nonatomic,copy)NSString *url;
@property(nonatomic,copy)NSString *isForce;
@end


@interface SWXConfigJsonModel : NSObject

@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *version;
@property(nonatomic,copy)NSString *describe;
@property(nonatomic,copy)NSString *indexUrl;

@property(nonatomic,copy)NSString *loadPageUrl;
@property(nonatomic,strong)SWXPreloadPageModel *preloadPage;
@property(nonatomic,strong)NSArray *preload;
@property(nonatomic,strong)NSArray *forcePreload;

+ (instancetype)createInstance:(NSDictionary *)configJson;

@end

NS_ASSUME_NONNULL_END
