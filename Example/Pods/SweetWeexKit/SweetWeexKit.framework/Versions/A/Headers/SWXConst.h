//
//  SWXConst.h
//  Pods
//
//  Created by shuaishuai on 2019/5/28.
//

#ifndef SWXConst_h
#define SWXConst_h

#define SWX_RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#define SWX_RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#define SWX_ColorFromRGBHEX(rgbValue)     [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define SWX_ColorFromRGBHEXA(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:(a)]



/*---------------------------------------------------------------------*/

///SweetWeex基座路由地址
static NSString * const SWX_k_WeexHostUrl = @"/weex/host";
///SweetWeex的loading地址
static NSString * const SWX_k_PageLoadUrl = @"page_load_url";

///SweetWeex下级页面weex地址
static NSString * const SWX_k_PushUrl = @"url";
///进入到下级是否有动画
static NSString * const SWX_k_PushAnimated = @"animated";
///SweetWeex容器ID
static NSString * const SWX_k_PushContainerId = @"container_id";
///是否隐藏loading
static NSString * const SWX_k_hideLoading = @"hide_loading";
///host背景颜色
static NSString * const SWX_k_hostBackgroundColor = @"host_backgroundColor";
///load背景颜色
static NSString * const SWX_k_loadBackgroundColor = @"load_backgroundColor";

///SweetWeex基座原生导航设置对象
static NSString * const SWX_k_PushWeexNaviInfo= @"weex_navi_info";

///SweetWeex每个页面的loading页参数设置对象
static NSString * const SWX_k_PushLoadingPageOption = @"loading_page_option";



///SweetWeex进行路由是填写的weex地址
static NSString * const SWX_k_RouterWeexIndexUrl = @"weex_index_url";
///SweetWeex基座路由参数：获取的基座是否带导航
static NSString * const SWX_k_RouterWeexIncludeNac = @"weex_include_nav";
///SweetWeex进入的的weex模式参数
static NSString * const SWX_k_RouterWeexMode = @"weex_mode";
///SweetWeex小程序模式
static NSString * const SWX_k_RouterWeexModeAPPLet = @"applet";
///SweetWeex应用模式
static NSString * const SWX_k_RouterWeexModeAPP = @"app";



/*---------------------------------------------------------------------*/




#endif /* SWXConst_h */
