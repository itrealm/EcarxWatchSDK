//
//  SWXContainerViewController.h
//  SweetWeexKit
//
//  Created by geely on 2018/10/15.
//  自定义的weex容器，主要用于渲染weex前端页面

#import <UIKit/UIKit.h>
#import <WeexSDK/WXSDKInstance.h>
#import "SWXWeexLoadViewInfo.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * The SWXContainerViewController class provides the infrastructure for managing the weex view in your app. It is
 * responsible for creating a weex instance or rendering the weex view, for observing the lifecycle of the
 * view such as "appear" or "disappear"、"foreground" or "background" etc. You can initialize this controller by
 * special bundle URL.
 */

@interface SWXContainerViewController : UIViewController<UIGestureRecognizerDelegate, SWXWeexLoadViewInfo>

@property (nonatomic, strong, readonly) NSURL *sourceURL;
@property (nonatomic, strong, readonly) NSURL *pageLoadSourceURL;

/**
 hide loading view

 @param isHide YES/NO
 */
- (void)isHideLoading:(BOOL)isHide;

/**
 * @abstract initializes the viewcontroller with bundle url, no pageLoad.
 
 @param sourceURL The url of bundle rendered to a weex vi
 @return a object the class of SWXContainerViewController.
 */
- (instancetype)initWithSourceURL:(NSURL *)sourceURL;


/**
 * @abstract initializes the viewcontroller with bundle url
 
 @param sourceURL sourceURL The url of bundle rendered to a weex vi
 @param pageLoadSourceURL The url of a loading view
 @return a object the class of SWXContainerViewController.
 */
- (instancetype)initWithSourceURL:(NSURL *)sourceURL pageLoadSourceURL:(NSURL *)pageLoadSourceURL;

/**
 * @abstract refreshes the weex view in controller.
 */
- (void)refreshWeex;



@end

NS_ASSUME_NONNULL_END
