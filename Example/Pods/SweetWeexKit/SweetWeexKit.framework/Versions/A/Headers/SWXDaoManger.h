//
//  SWXDaoManger.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/5/29.
//
/*
 1. 总表：存放总的配置信息，每一个元素代表一个模块为SWXWeexMoudleModel
 2. 强制预加表：每个模块新建一个表对应recover，每一个元素为SWXRecoverMapModel，存放强制预加载阶段的资源
 3. 缓存表：每个模块新建一个表对应preLoadPage，每一个元素为SWXPreloadMapModel，存放缓存加载阶段的资源
 */


#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>
#import "SWXWeexMoudleModel.h"
#import "SWXPreloadMapModel.h"
#import "SWXRecoverMapModel.h"

///总表
@interface SWXDaoMap : NSObject

@end

@interface SWXDaoManger : NSObject

+ (instancetype)shareDao;

#pragma mark - 查找or创建
/**
 在数据库中进行查找
 
 @param name 模块名
 @param version 模块版本号
 @return 返回的模块对象
 */
//- (SWXWeexMoudleModel *)findDao_daoMapTable:(NSString *)name
//                                    version:(NSString *)version;

/**
 在数据库中，查询或创建一个一条SWXWeexMoudleModel数据；如果name和version不能查到则创建一个新的；如果configJson为空，只会进行查找
 
 @param name 模块名称
 @param version 模块版本号
 @param configJson 模块配置文件，如果x查找不到会根据该文件创建新的
 @return 返回的模块对象
 */
- (SWXWeexMoudleModel *)findOrNewDao_daoMapTable:(NSString *)name
                                         version:(NSString *)version
                                      configJson:(NSDictionary *)configJson;

#pragma mark - 创建
//- (SWXWeexMoudleModel *)newDao_moudelTable:(NSDictionary *)configJson;


#pragma mark - 覆盖更新整个表
/**
 更新整个表名为modelList中model的表，要求modelList中model的类型相同，
 
 @param modelList model对象数组，要求modelList中model的类型相同
 @return 是否成功
 */
- (BOOL)updateDao_preloadMapModel:(NSArray* _Nonnull)modelList;


@end


