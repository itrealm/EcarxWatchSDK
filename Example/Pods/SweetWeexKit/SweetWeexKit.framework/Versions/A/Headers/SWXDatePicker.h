//
//  SWXDatePicker.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/19.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIDatePicker.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXDatePicker : UIDatePicker

- (void)runSelfPrivatePropert;
- (void)swx_runSelfPrivateMethod;
//UIDatePicker

//设置选中的颜色
//设置背景
//设置选中的背景
//设置

///设置文字颜色
- (void)swx_textColor:(UIColor *)color;

///设置选中的文字颜色
- (void)swx_selectionTextColor:(UIColor *)color;

///设置文字背景颜色
- (void)swx_backgroundColor:(UIColor *)color;

///设置选中的背景颜色
- (void)swx_selectionBackgroundColor:(UIColor *)color;

////设置文字颜色
//- (void)swx_textColor:(UIColor *)color;

///设置区域语言 中文："zh_CN" ,英文："en_US"（默认）
- (void)swx_setLocaleWithIdentifier:(NSString *)identifier;

- (void)swx_setDateComponents:(int)num;

/**
 设置模式
 
 @param mode
 0://UIDatePickerModeTime
 1:UIDatePickerModeDate
 2:UIDatePickerModeDateAndTime
 3:UIDatePickerModeCountDownTimer
 */
- (void)swx_datePickerMode:(NSInteger)mode;
@end

NS_ASSUME_NONNULL_END
