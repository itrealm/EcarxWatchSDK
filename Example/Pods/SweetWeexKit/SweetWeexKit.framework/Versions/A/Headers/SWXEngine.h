//
//  SWXEngine.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/5/29.
//

#import <Foundation/Foundation.h>
#import <SweetWeexKit/SWXHostBaseViewController.h>
#import <SweetWeexKit/SWXLaunchViewController.h>

//FOUNDATION_EXPORT NSString * const SWX_k_EventName_weexRenderFinish;

typedef void(^SWXCALLBACK)(id comption, NSError *error);

@interface SWXWeexPortal : NSObject
@property (nonatomic,strong)SWXLaunchViewController *launchVC;
@property (nonatomic,strong)SWXHostBaseViewController *portalHostVC;
@end

@interface SWXEngine : NSObject
- (void)enterWeexPortal:(NSString *)weexUrl
                 hostVC:(SWXHostBaseViewController *)hostVC
               callBack:(SWXCALLBACK)callBack;
@end


