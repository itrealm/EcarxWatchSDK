//
//  SWXEventModule.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/8/8.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

typedef void(^SWEVENTCALLBACK)(NSString *eventName,id eventData);

NS_ASSUME_NONNULL_BEGIN

@interface SWXEventModule : NSObject

@end

NS_ASSUME_NONNULL_END
