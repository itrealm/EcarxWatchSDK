//
//  SWXEventNameConst.h
//  Pods
//
//  Created by shuaishuai on 2019/6/13.
//

#ifndef SWXEventNameConst_h
#define SWXEventNameConst_h

#import "DWEventBus.h"

///开始获取config配置信息
static NSString *const SWX_k_EventName_StartConfig = @"SWX_k_EventName_StartConfig";
///获取config配置信息
static NSString *const SWX_k_EventName_LoadConfig_Success = @"SWX_k_EventName_LoadConfig_Success";
///获取配置信息失败
static NSString *const SWX_k_EventName_LoadConfig_Failure = @"SWX_k_EventName_LoadConfig_Failure";

///根据配置文件进行预热任务
///成功完成loadPageUrl预热任务
static NSString * const SWX_k_EventName_LoadPage_Finish = @"SWX_k_EventName_LoadPage_Finish";
///成功完成preloadPage预热任务
static NSString * const SWX_k_EventName_PreLoadPage_Finish = @"SWX_k_EventName_PreLoadPage_Finish";
///成功完成preload预热任务
static NSString * const SWX_k_EventName_PreloadMap_Finish = @"SWX_k_EventName_PreloadMap_Finish";
///成功完成recover预热任务
static NSString * const SWX_k_EventName_RecoverMap_Finish = @"SWX_k_EventName_RecoverMap_Finish";

///成功完成所有的预热下载任务
static NSString * const SWX_k_EventName_PreloadTask_Finish = @"SWX_k_EventName_PreloadTask_Finish";

///weex页面渲染完成（每个weex页面渲染完成都会发送）
static NSString *const SWX_k_EventName_weexRender_Finish = @"SWX_k_EventName_weexRender_Finish";

///完成所有的预热任务，包括静默preload部分
static NSString *const SWX_k_EventName_ALLPreload_Finish = @"SWX_k_EventName_ALLPreload_Finish";


#endif /* SWXEventNameConst_h */
