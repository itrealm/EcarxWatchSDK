//
//  SWXEventServiceModule.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/8/16.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>
#import <SweetCoreKit/SWEventService.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^SWXEVENTBLOCK)(id obj);

@interface SWXSubscriber : NSObject
@property(nonatomic,copy)SWXEVENTBLOCK block;
@end

@interface SWXEventServiceModule : NSObject
- (BOOL)_publish:(NSString *)eventName eventData:(NSDictionary *)eventData;
- (BOOL)_listen:(NSString *)eventName
  eventCallback:(SWXEVENTBLOCK)eventCallback;
- (BOOL)_cancelListen:(NSString *)eventName;
@end

NS_ASSUME_NONNULL_END
