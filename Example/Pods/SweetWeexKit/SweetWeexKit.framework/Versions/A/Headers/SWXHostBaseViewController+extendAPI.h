//
//  SWXHostBaseViewController+extendAPI.h
//  SweetWeexKit
//
//  Created by geely on 2018/10/17.
//

#import "SWXHostBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWXHostBaseViewController ()

//是否设置为全屏沉浸式，如果沉浸式时导航栏会隐藏
@property (assign, nonatomic) BOOL isShowImmerse;

/**
 设置为全屏沉浸式
 如果设置沉浸式，
 @param isShowImmerse 是否是沉浸式
 */
- (void)setUpShowImmerse:(BOOL)isShowImmerse;

/**
 设置状态栏隐藏(YES)或显示(NO)
 
 @param isHide YES：隐藏；NO:显示(默认)
 */
- (void)setUpStatusBarHidden:(BOOL)isHide;

/**
 设置状态栏字体颜色
 
 @param style 0:UIStatusBarStyleDefault,黑色(默认); >=1:UIStatusBarStyleLightContent,白色
 */
- (void)setUpStatusBarStyle:(NSInteger)style;

/**
 更改样式
 
 @param opaque 样式，YES:状态字体为白色 NO:状态字体为黑色(默认)
 */
- (void)setUpNavBarIsOpaque:(BOOL)opaque;

//设置导航标题文字
- (void)setUpNaTitle:(NSString *)title;

/**
 设置导航标题文字颜色，font属性
 
 @param color 颜色
 @param fontName 字体名称
 @param fontSize 字体大小
 */
- (void)setUpNaTitleColor:(UIColor *)color fontName:(NSString *)fontName fontSize:(CGFloat)fontSize;

/**
 更改导航栏颜色和图片
 
 @param color 颜色
 @param barImage 图片
 */
- (void)setUpNavBarBackGroundColor:(UIColor *_Nullable)color image:(UIImage *_Nullable)barImage;

/**
 设置导航左侧按钮
 
 @param title 名称
 @param iconImg 图片
 */
- (void)setUpNaLeftItem:(NSString *)title iconImg:(UIImage *)iconImg;

/**
 设置导航右侧按钮
 
 @param title 名称
 @param iconImg 图片
 */
- (void)setUpNaRightItem:(NSString *)title iconImg:(UIImage *)iconImg;

/**
 设置最右侧导航按钮隐藏
 
 @param isHide 是否隐藏
 */
- (void)setUpNaRightHide:(BOOL)isHide;

@end


NS_ASSUME_NONNULL_END
