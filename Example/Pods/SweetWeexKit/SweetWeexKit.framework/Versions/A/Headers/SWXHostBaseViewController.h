//
//  SWXHostBaseViewController.h
//  SweetWeexKit
//
//  Created by geely on 2018/10/17.
//  作为SWXContainerViewController的外层嵌套，主要用于设置各种样式

#import <UIKit/UIKit.h>
#import <HybridImplementKit/SWMarkingProtocol.h>
#import "SWXWeexLoadViewInfo.h"

/*
 "navigationBarInfo":{
 "isHide": true,         //是否隐藏
 "title":"标题",
 "navigationBarTintColor":"white",
 "background": {
 "bgColor":"#00ff00",
 "bgImage":""
 },
 "title": {
 "titleText":"我是标题",
 "titleColor":"#00ff00",
 "font": {
 "fontName":"#00ff00",
 "fontSize":"16"
 }
 }
 }
 */

@protocol SWXNavigationInfoProtocol <NSObject>
@property (nonatomic,assign) BOOL isHide;
@property (nonatomic,copy) NSString *titleText;
@property (nonatomic,strong) UIColor *titleColor;
@property (nonatomic,copy) NSString *titleFontName;
@property (nonatomic,assign) CGFloat titleFontSize;
@property (nonatomic,strong) UIColor *backgroundColor;
@property (nonatomic,strong) UIImage *backgroundImage;
@end
@interface SWXNavigationInfo : NSObject <SWXNavigationInfoProtocol>
+ (instancetype)createSWXNavigationInfo:(NSDictionary *)navigationInfoDic;
@end


@interface SWXHostBaseViewController : UIViewController <SWMarkingProtocol, SWXWeexLoadViewInfo>

//远程源链接
@property (strong, nonatomic) NSURL *weexSourceURL;

//本地白屏链接
@property (strong, nonatomic) NSURL *pageLoadSourceURL;

//标记下是否是小程序模式 YES:是小程序，NO:是应用
@property (assign, nonatomic) BOOL isMiniprogram;

//导航配置信息
@property (strong, nonatomic) id<SWXNavigationInfoProtocol> navigationInfo;

//容器ID
@property (nonatomic, copy) NSString *containerID;

///是否隐藏loading页面
@property (nonatomic, assign) BOOL isHideLoading;


///背景颜色
@property (nonatomic, assign) long hostBackgroundColor;
    
@end


