//
//  SWXHostViewController.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/5/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXHostViewController : UIViewController

//容器ID
@property (nonatomic, copy) NSString *containerID;
//标记下是否是小程序模式 YES:是小程序，NO:是应用
@property (assign, nonatomic) BOOL isApplet;

///载入渲染
- (void)loadWeexURL:(NSURL *)weexURL pageLoadURL:(NSURL *)pageLoadURL;

@end

NS_ASSUME_NONNULL_END
