//
//  SWXLaunchViewController.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/4/29.
//

#import <UIKit/UIKit.h>
#import "SWXWeexMoudleModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SWXLaunchViewController : UIViewController

+ (instancetype)createWaitLaunchViewController:(UIImage *)logImg
                                          name:(NSString *)name;
- (void)addToParentVC:(UIViewController *)parentVC;
- (void)remove;

- (void)show_default;
///显示到业务配置的log
- (void)showLog:(SWXPreLoadPageModel *)preloadPageModel;
///显示获取配置信息失败
- (void)showGetConfigError;


@end

NS_ASSUME_NONNULL_END
