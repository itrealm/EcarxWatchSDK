//
//  SWXLoadingManger.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/5/27.
//  

#import <Foundation/Foundation.h>
#import <HybridImplementKit/SWPageLoadingViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXLoadingManger : NSObject
@property (nonatomic, strong) SWPageLoadingViewController *pageLoadingVC;

+ (instancetype)share;

- (void)destroy;

@end

NS_ASSUME_NONNULL_END
