//
//  SWXMiniProgramNaRightView.h
//  SweetWeexKit
//
//  Created by geely on 2018/10/11.
// 设置小程序样式导航右边按钮

#import <UIKit/UIKit.h>

//NS_ASSUME_NONNULL_BEGIN

@protocol SWXHostNaRightProtocol <NSObject>
- (void)naRightMoreBtnOnClick;
- (void)naRightCloseBtnOnClick;
@end

@interface SWXMiniProgramNaRightView : UIView

@property (assign, nonatomic) id<SWXHostNaRightProtocol> delegate;

- (void)setUpBgColor:(UIColor *)bgColor bgImage:(UIImage *)bgImage;
- (void)setUpMoreBtnIcon:(UIImage *)icon highlightedIcon:(UIImage *)highlightedIcon;
@end

//NS_ASSUME_NONNULL_END
