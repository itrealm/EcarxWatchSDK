//
//  SWXMiniProgramViewController.h
//  SweetWeexKit
//
//  Created by geely on 2018/10/16.
// 小程序模式

#import "SWXHostBaseViewController.h"

typedef void(^MenuAlertItemBlock)(NSString *itemID);


NS_ASSUME_NONNULL_BEGIN

//小程序模式视图
@interface SWXMiniProgramViewController : SWXHostBaseViewController

+ (SWXMiniProgramViewController *)createSWXMiniProgramViewController:(NSURL *)weexSourceURL;

/**
 设置小程序右侧按钮背景
 
 @param bgColor 背景颜色
 @param bgImage 背景图片
 */
- (void)setUpRightBgColor:(UIColor *)bgColor bgImage:(UIImage *)bgImage;

/**
 设置小程序右侧按钮中more按钮图标
 
 @param icon 按钮Normal下的颜色
 @param highlightedIcon 按钮Highlighted下的颜色
 */
- (void)setUpRightMoreBtnIcon:(UIImage *)icon highlightedIcon:(UIImage *)highlightedIcon;

/**
 添加底部弹窗
 
 @param menuList items(List) => 底部弹窗item数组,用于展示弹窗内容
                    Item(Object) => item对象
                        text(string) => item内容
                        id(string) => item的id,用于点击事件itemClick的回调
 @param itemClick 点击事件
 */
- (void)popMenuAlert:(NSArray *)menuList itemClick:(MenuAlertItemBlock)itemClick;

@end

NS_ASSUME_NONNULL_END
