//
//  SWXMutablePickerModule.h
//  GeelyLynkcoPlugin
//
//  Created by shuaishuai on 2019/7/31.
//
/*
 [
     [
         1-1,
         1-2,
         1-3,
         1-4,
         1-5,
         1-6
     ],
     [
         [2-1-1,2-1-2],
         [2-2-1,2-2-2],
         [2-3-1,2-3-2]
     ],
     [
         [3-1-1,3-1-2,3-1-3],
         [3-2-1,3-2-2],
         [3-3-1,3-3-2,3-3-3,3-3-4]
    ],
     [
         1-1,
         [1-2-1,1-2-2],
         1-3
     ],
 ]

 或
 [
     1,
     2,
     3,
     4,
     5,
     6
 ]
 */


#import <Foundation/Foundation.h>
#import "WXModuleProtocol.h"


@interface SWXMutablePickerModule : NSObject<WXModuleProtocol,UIPickerViewDelegate,UIPickerViewDataSource>
- (void)pick:(NSDictionary *)options callback:(WXModuleKeepAliveCallback)callback;
@end


