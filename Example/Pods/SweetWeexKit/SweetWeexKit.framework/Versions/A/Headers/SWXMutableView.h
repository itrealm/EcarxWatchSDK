//
//  SWXMutableView.h
//  SweetWeexPlugin
//
//  Created by shuaishuai on 2019/8/1.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXMutableView : UIView
- (void)addHorizontalView:(UIView *)subView;
@end

NS_ASSUME_NONNULL_END
