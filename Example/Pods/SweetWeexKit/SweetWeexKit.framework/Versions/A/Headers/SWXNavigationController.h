//
//  SWXNavigationController.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/5/23.
//

#import <UIKit/UIKit.h>
#import <SweetWeexKit/SWXHostBaseViewController.h>
NS_ASSUME_NONNULL_BEGIN

@interface SWXNavigationController : UINavigationController

+ (instancetype)createSWXNavigationController:(SWXHostBaseViewController *)host;

@end

NS_ASSUME_NONNULL_END
