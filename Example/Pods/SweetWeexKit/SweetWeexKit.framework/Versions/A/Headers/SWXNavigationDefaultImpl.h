//
//  SWXNavigationDefaultImpl.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/4/15.
//  该方法重新实现WXNavigationProtocol协议，覆盖WeexSDK自带的WXNavigationDefaultImpl

#import <Foundation/Foundation.h>
#import <WeexSDK/WXNavigationProtocol.h>

/*
 navBarInfoList:[
 {
    isHide: false,
    // navigationBarTintColor: 'white',
    background: {
        bgColor: '#FF0000',
        bgImage: ''
    },
    title: {
        titleText: '我是标题',
        titleColor: '#00ff00',
        font: {
            fontName: 'Marion-Regular',
            fontSize: 46
        }
    }
 },
 
 */




@interface SWXNavigationDefaultImpl : NSObject <WXNavigationProtocol>

@end


