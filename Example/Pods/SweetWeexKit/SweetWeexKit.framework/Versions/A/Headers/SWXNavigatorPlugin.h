//
//  SWXNavigatorPlugin.h
//  SweetWeexKit
//
//  Created by geely on 2018/10/15.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXNavigatorPlugin : NSObject <WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
