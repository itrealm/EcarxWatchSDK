//
//  SWXPickerModule.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/19.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXPickerModule : NSObject <WXModuleProtocol,UIPickerViewDelegate>

@end

NS_ASSUME_NONNULL_END
