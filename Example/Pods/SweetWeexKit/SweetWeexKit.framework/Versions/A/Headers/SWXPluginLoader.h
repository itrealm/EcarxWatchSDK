//
//  SWXPluginLoader.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/7/17.
//

#import <Foundation/Foundation.h>



@interface SWXPluginLoader : NSObject

+ (void)registerModule:(NSString *)name withClass:(Class)clazz;

+ (void)registerComponent:(NSString *)name withClass:(Class)clazz;
+ (void)registerComponent:(NSString *)name withClass:(Class)clazz withProperties:(NSDictionary *)properties;

+ (void)registerHandler:(id)handler withProtocol:(Protocol *)protocol;


@end


