//
//  SWXPluginMacro.h
//  
//
//  Created by shuaishuai on 2019/7/17.
//

#import <WeexSDK/WXDefine.h>

#ifndef SWXPluginMacro_h
#define SWXPluginMacro_h


#ifndef SweetWeexPluginSectName
#define SweetWeexPluginSectName "SweetWeexPlugins"
#endif

#ifndef SWXModulePluginName
#define SWXModulePluginName @"SWXModule"
#endif

#ifndef SWXComponentPluginName
#define SWXComponentPluginName @"SWXComponent"
#endif

#ifndef SWXHandlerPluginName
#define SWXHandlerPluginName @"SWXHandler"
#endif

#define SWX_DATA(sectname) __attribute((used, section("__DATA,"#sectname" ")))
//#define SWeexPluginDATA SWX_DATA(SweetWeexPlugins);
#define SWeexPluginDATA __attribute((used, section("__DATA,SweetWeexPlugins")))

#define SWX_PLUGIN_NAME_SEPARATOR(module,jsname,classname,separator) module#separator#jsname#separator#classname

#define SWX_PLUGIN_NAME(module,jsname,classname) SWX_PLUGIN_NAME_SEPARATOR(module,jsname,classname,&)

#define SWX_PlUGIN_EXPORT_MODULE_DATA(jsname,classname) \
char const * k##classname##Configuration SWeexPluginDATA = SWX_PLUGIN_NAME("SWXModule",jsname,classname);

#define  SWX_PlUGIN_EXPORT_COMPONENT_DATA(jsname,classname)\
char const * k##classname##Configuration SWeexPluginDATA = SWX_PLUGIN_NAME("SWXComponent",jsname,classname);

#define SWX_PlUGIN_EXPORT_HANDLER_DATA(jsimpl,jsprotocolname)\
char const * k##jsimpl##jsprotocolname##Configuration SWeexPluginDATA = SWX_PLUGIN_NAME("SWXHandler",jsimpl,jsprotocolname);

/***********************************************************************/


/**
 * this macro is used to auto regester moudule.
 *  example: WX_PlUGIN_EXPORT_MODULE(test,WXTestModule)
 **/
#define SWX_PlUGIN_EXPORT_MODULE(jsname,classname) SWX_PlUGIN_EXPORT_MODULE_DATA(jsname,classname)

/**
 *  this macro is used to auto regester component.
 *  example:WX_PlUGIN_EXPORT_COMPONENT(test,WXTestCompnonent)
 **/
#define SWX_PlUGIN_EXPORT_COMPONENT(jsname,classname) SWX_PlUGIN_EXPORT_COMPONENT_DATA(jsname,classname)

/**
 *  this macro is used to auto regester handler.
 *  example:WX_PlUGIN_EXPORT_HANDLER(WXImgLoaderDefaultImpl,WXImgLoaderProtocol)
 **/
#define SWX_PlUGIN_EXPORT_HANDLER(jsimpl,jsprotocolname) SWX_PlUGIN_EXPORT_HANDLER_DATA(jsimpl,jsprotocolname)


/*********************************************************/

/**
 *  @abstract export public method
 */
#define SWX_EXPORT_METHOD(method) WX_EXPORT_METHOD(method)

/**
 *  @abstract export public method, support sync return value
 *  @warning the method can only be called on js thread
 */
#define SWX_EXPORT_METHOD_SYNC(method) WX_EXPORT_METHOD_SYNC(method)

#endif /* SWXPluginMacro_h */
