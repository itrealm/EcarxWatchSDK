//
//  SWXPortal.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/24.
//

#import <Foundation/Foundation.h>
#import <SweetWeexKit/SWXHostBaseViewController.h>
#import <SweetWeexKit/SWXLaunchViewController.h>
#import <SweetWeexKit/SWXWeexMoudleModel.h>


@interface SWXPortalVC : NSObject
@property (nonatomic,strong)SWXLaunchViewController *launchVC;
@property (nonatomic,strong)SWXHostBaseViewController *portalHostVC;
@end

@interface SWXPortal : NSObject

///当前显示的模块的
@property (nonatomic,strong)SWXWeexMoudleModel *curWeexMoudel;


///创建一个新的weex容器
- (SWXHostBaseViewController *)createWeexHost:(NSString *)url;

///从其他模块进入到另一个weex模块某页面（由该方法执行push操作，会显示进入模块的loading页面）
 - (void)gotoOtherWeexMoudel:(NSString *)url;




@end


