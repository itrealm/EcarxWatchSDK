//
//  SWXPreUtils.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/7/2.
//

#import <Foundation/Foundation.h>



@interface SWXPreUtils : NSObject

///根据一个weex连接获取preLoad的表名称（生成规则是一个包名对于一个表名）
- (NSString *)preloadMapTbName:(NSString *)remoteUrl;

///根据一个weex连接获取recover的表名称（生成规则是一个包名对于一个表名）
- (NSString *)recoverMapTbName:(NSString *)remoteUrl;


@end


