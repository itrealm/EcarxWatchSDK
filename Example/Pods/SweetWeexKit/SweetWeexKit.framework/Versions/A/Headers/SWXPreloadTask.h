//
//  SWXPreloadTask.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/11.
//

#import <Foundation/Foundation.h>

typedef void(^SWXPRELOADBACKBLOCK)(NSString *remoteUrl, NSString *localRelativePath, NSError *error);

@interface SWXPreloadTask : NSObject

///必填配置
@property (nonatomic,copy)NSString *baseUrlStr;
@property (nonatomic,copy)NSString *moudleName;
@property (nonatomic,copy)NSString *version;

///相对路径砖化为沙盒临时绝对路径
+ (NSString *)get_absolutePath:(NSString *)relativePath;

///初始化
- (void)initialization:(NSString *)baseUrlStr
            moudleName:(NSString *)moudleName
               version:(NSString *)version;


- (void)loadTask_loadPageZIP:(NSString *)loadUrl
                    completion:(SWXPRELOADBACKBLOCK)completion;

- (void)loadTask_preloadPage:(NSString *)loadUrl
                    completion:(SWXPRELOADBACKBLOCK)completion;

- (void)loadTask_loadFileCell:(NSString *)loadUrl
                     completion:(SWXPRELOADBACKBLOCK)completion;



@end


