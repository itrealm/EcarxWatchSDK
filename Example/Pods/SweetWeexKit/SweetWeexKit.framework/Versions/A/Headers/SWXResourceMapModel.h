//
//  SWXResourceMapModel.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/5/29.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXResourceMapModel : NSObject

///远程链接
@property(nonatomic,copy)NSString *remoteUrl;
//本地的相对于沙盒路径
@property(nonatomic,copy)NSString *localRelativelyPath;
///所有的每组存储都携带config的build号，以标记该条数据是否之前更新过，不再进行重复更新
@property(nonatomic,copy)NSString *build;

@end

NS_ASSUME_NONNULL_END
