//
//  SWXSDKInstance.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/18.
//

#import <WeexSDK/WXSDKInstance.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXSDKInstance : WXSDKInstance

@end

NS_ASSUME_NONNULL_END
