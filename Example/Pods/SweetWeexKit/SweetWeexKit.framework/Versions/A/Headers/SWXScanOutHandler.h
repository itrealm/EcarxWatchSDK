//
//  SWXScanOutHandler.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/9/23.
//

#import <Foundation/Foundation.h>
#import <HybridImplementKit/HYScanViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXScanOutHandler : NSObject<HYScanProtocol>

@property(nonatomic,assign)BOOL isMiniApp;

///开启扫一扫结果页
- (void)openURL:(NSString*)URL
        context:(UIViewController *)context;
- (BOOL)isAvailable_weex:(NSString*)urlStr;


@end

NS_ASSUME_NONNULL_END
