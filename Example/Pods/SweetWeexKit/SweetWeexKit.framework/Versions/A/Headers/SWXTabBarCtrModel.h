//
//  SWXTabBarCtrModel.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/22.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXTabBarCtrModel : NSObject <WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
