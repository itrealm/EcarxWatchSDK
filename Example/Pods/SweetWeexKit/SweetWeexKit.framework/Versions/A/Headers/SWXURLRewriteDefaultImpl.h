//
//  SWXURLRewriteDefaultImpl.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/11/1.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXURLRewriteProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXURLRewriteDefaultImpl : NSObject<WXURLRewriteProtocol>

@end

NS_ASSUME_NONNULL_END
