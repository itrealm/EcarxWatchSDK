//
//  SWXUtil.h
//  SweetWeexKit
//
//  Created by geely on 2018/10/11.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define SW_Screen_Width [UIScreen mainScreen].bounds.size.width
#define SW_Screen_Height [UIScreen mainScreen].bounds.size.height
//#define SW_IS_IPHONE_X (SW_Screen_Height == 812.0f) ? YES : NO
#define SW_IS_IPHONE_X \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})

#define SW_Height_NavContentBar 44.0f
#define SW_Height_StatusBar (SW_IS_IPHONE_X==YES)?44.0f: 20.0f
#define SW_Height_NavBar   (SW_IS_IPHONE_X==YES)?88.0f: 64.0f
#define SW_Height_TabBar   (SW_IS_IPHONE_X==YES)?83.0f: 49.0f
#define SWX_STATUS_BAR_BIGGER_THAN_20 [UIApplication sharedApplication].statusBarFrame.size.height > 20

///状态栏需要额外增加的高度(适配开启热点时)
#define SW_STATUS_BAR_HOTSPOL_EXTRA_ADD \
({CGFloat extraAdd = 0;\
if (!SW_IS_IPHONE_X && SWX_STATUS_BAR_BIGGER_THAN_20) {\
extraAdd = 20;\
}\
(extraAdd);})



NS_ASSUME_NONNULL_BEGIN

@interface SWXUtil : NSObject
+ (void)viewAutoSizeToSuperView:(UIView *)subView;

+ (UIViewController *)swx_getCurrentViewController;
+ (UIViewController *)swx_getRootViewController;

@end

NS_ASSUME_NONNULL_END
