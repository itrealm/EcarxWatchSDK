//
//  SWXUtils.h
//  Pods
//
//  Created by shuaishuai on 2019/10/8.
//

#ifndef SWXUtils_h
#define SWXUtils_h

//--------
//如果release状态就不执行NSLog函数
#ifndef __OPTIMIZE__
#define NSLog(...) NSLog(__VA_ARGS__)
#else
# define NSLog(...) {}
#endif

#ifdef DEBUG
#define SWXLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#define SWXLog(...)
#endif

#ifdef DEBUG
#define SWXString [NSString stringWithFormat:@"%s", __FILE__].lastPathComponent
#define SWXDLog(...) printf("%s: %s [%d]: %s\n\n",[[NSString dh_stringDate] UTF8String], [DDString UTF8String] ,__LINE__, [[NSString stringWithFormat:__VA_ARGS__] UTF8String]);
#else
#define SWXDLog(...)
#endif

#endif /* SWXUtils_h */
