//
//  SWXWeexLoadViewInfo.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/11/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SWXWeexLoadViewInfo <NSObject>

@property (nonatomic,strong) NSString *loadViewBackGroundColor;

@end

NS_ASSUME_NONNULL_END
