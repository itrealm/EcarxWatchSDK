//
//  SWXWeexModuleConfig.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/6/24.
//  某个模块下weex的host相关页面的统一配置

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXWeexModuleConfig : NSObject

///模块ID
@property(nonatomic,copy)NSString *moduleID;

///背景颜色
@property(nonatomic,copy)NSString *bgColor;

///loading页面路径
@property (nonatomic, strong) NSURL *swxPageLoadSourceURL;

@end

NS_ASSUME_NONNULL_END
