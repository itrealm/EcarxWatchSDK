//
//  SWXWeexConfigModel.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/5/29.
//

#import <Foundation/Foundation.h>
#import <BGFMDB/BGFMDB.h>
//#import "SWXLoadingModel.h"

//预加载页面
@interface SWXPreLoadPageModel : NSObject
@property(nonatomic,copy)NSString *logRemoteUrl;
@property(nonatomic,copy)NSString *logLocalRelativelyPath;
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *describe;
@end

//每页的loading文件包
@interface SWXLoadPageModel : NSObject
@property(nonatomic,copy)NSString *zipRemoteUrl;
@property(nonatomic,copy)NSString *unzipLocalRelativelyPath;
- (NSString *)indexUrl;
@end


@interface SWXWeexMoudleModel : NSObject

@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *version;
@property(nonatomic,copy)NSString *build;
@property(nonatomic,copy)NSString *describe;
@property(nonatomic,copy)NSString *baseUrl;
@property(nonatomic,copy)NSString *indexUrl;

@property(nonatomic,strong)SWXPreLoadPageModel *preLoadPageModel;
@property(nonatomic,strong)SWXLoadPageModel *loadPageModel;

#pragma mark - 表关联
///关联的表名
@property(nonatomic,copy)NSString *recoverTableName;
///需预热文件的数据库表名
@property(nonatomic,copy)NSString *preloadTableName;

@end


