//
//  SweetWeexKit.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/8/23.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXBaseViewController.h>
#import <WeexSDK/WXRootViewController.h>
#import <SweetWeexKit/SWXMiniProgramViewController.h>
#import <SweetWeexKit/SWXApplicationViewController.h>
#import <SweetWeexKit/SWScannerViewController.h>
#import <SweetWeexKit/SWXPluginLoader.h>

NS_ASSUME_NONNULL_BEGIN

@interface SweetWeexKitConfig : NSObject
@property (nonatomic, copy) NSString *appGroup;
@property (nonatomic, copy) NSString *appName;
@property (nonatomic, copy) NSString *externalUserAgent;
@property (nonatomic, strong) NSURL *pageLoadSourceURL;

@end


@interface SweetWeexKit : NSObject


/**
 Returns SweetWeexKit instance

 @return SweetWeexKit instance
 */
+ (instancetype)shareKit;

+ (void)config:(SweetWeexKitConfig *)config;

+ (void)isDebug:(BOOL)isDebug;

+ (void)netConfig:(void(^)(id obj,NSError *error))completed;


#pragma mark -  SweetWeexKit HOST
+ (SWXHostBaseViewController *)createSWXHostBaseViewController:(NSURL *)sourceURL pageLoadURL:(NSURL *)pageLoadURL;
+ (SWXMiniProgramViewController *)createSWXMiniProgramViewController:(NSURL *)sourceURL pageLoadURL:(NSURL *)pageLoadURL;
+ (SWXApplicationViewController *)createSWXApplicationViewController:(NSURL *)sourceURL pageLoadURL:(NSURL *)pageLoadURL;
+ (SWScannerViewController *)createScannerViewController;

#pragma mark - Weex Host
+ (WXBaseViewController *)createWXBaseViewController:(NSURL *)sourceURL;
+ (WXRootViewController *)createWXNavigationRootViewController:(NSURL *)sourceURL;

@end

NS_ASSUME_NONNULL_END
