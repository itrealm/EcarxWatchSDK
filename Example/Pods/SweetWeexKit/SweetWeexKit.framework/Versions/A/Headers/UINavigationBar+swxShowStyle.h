//
//  UINavigationBar+showStyle.h
//  SweetWeexKit
//
//  Created by geely on 2018/10/11.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UINavigationBar (swxShowStyle)


/**
 通过kvc找到系统导航栏背景层，可以把自定义层添加到背景层

 @return 系统导航栏背景层，如果为空返回nil
 */
- (UIView *)getNaBarBackgroundView;

/**
 更改导航栏颜色和图片
 
 @param color 颜色
 @param barImage 图片
 */
- (void)navBarBackGroundColor:(UIColor *)color image:(UIImage *)barImage;

/**
 更改导航栏颜色和图片
 
 @param color 颜色
 @param barImage 图片
 @param opaque 样式，YES:状态字体为白色 NO:状态字体为黑色(默认)
 */
- (void)navBarBackGroundColor:(UIColor *_Nullable)color image:(UIImage *_Nullable)barImage isOpaque:(BOOL)opaque;

/**
 更改样式
 
 @param opaque 样式，YES:状态字体为白色 NO:状态字体为黑色(默认)
 */
- (void)navBarIsOpaque:(BOOL)opaque;

/**
 更改透明度
 
 @param alpha 导航栏透明度
 @param opaque 样式，YES:状态字体为白色 NO:状态字体为黑色(默认)
 */
- (void)navBarAlpha:(CGFloat)alpha isOpaque:(BOOL)opaque;



/**
 导航栏背景高度
 注意*这里并没有改导航栏高度，只是改了自定义背景高度
 
 @param height 高度
 @param opaque 样式，YES:状态字体为白色 NO:状态字体为黑色(默认)
 */
- (void)navBarMyLayerHeight:(CGFloat)height isOpaque:(BOOL)opaque;

/**
 隐藏底线
 */
- (void)navBarBottomLineHidden:(BOOL)hidden;

//还原回系统导航栏
- (void)navBarToBeSystem;

@end

#pragma mark -- 自定义导航栏层
@interface MyNavView :UIView
@property (nonatomic, assign) CGFloat   alpha;
@property (nonatomic, assign) BOOL      hiddenBottomLine;
@property (nonatomic, strong) UIColor   * _Nullable backColor;
@property (nonatomic, strong) UIImage   * _Nullable backImage;
@end

NS_ASSUME_NONNULL_END
