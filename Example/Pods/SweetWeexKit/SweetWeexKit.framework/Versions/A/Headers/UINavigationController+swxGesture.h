//
//  UINavigationController+gesture.h
//  Pods
//
//  Created by geely on 2018/10/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UINavigationController (swxGesture)
- (void)setUpGestureRecognizerDelegate;
@end

NS_ASSUME_NONNULL_END
