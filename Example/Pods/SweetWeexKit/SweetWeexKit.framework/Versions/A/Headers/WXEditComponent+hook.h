//
//  WXEditComponent+hook.h
//  SweetWeexPlugin
//
//  Created by shuaishuai on 2019/8/8.
//


#import "WXEditComponent.h"
#import "WXImageComponent.h"
NS_ASSUME_NONNULL_BEGIN

@interface WXEditComponent (hook)
@property(nonatomic,assign)CGRect gl_scrolledViewOrignFrame;
@property(nonatomic,weak)UIView* gl_scrolledView;

@end


@interface WXImageComponent (hook)

@end

@interface UIImageView (hook)
@property(nonatomic,strong)NSString* gl_filterName;

@end
NS_ASSUME_NONNULL_END
