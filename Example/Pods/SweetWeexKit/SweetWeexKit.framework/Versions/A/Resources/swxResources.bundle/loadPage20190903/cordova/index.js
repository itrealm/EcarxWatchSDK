(function() {
    /**
     * 动态加载CSS
     * @param {string} url 样式地址
     */
    function dynamicLoadCss(url) {
        console.log('cordovaEnv', cordovaEnv)
        var head = document.getElementsByTagName('head')[0]
        var script = document.createElement('script')
        script.type = 'text/javascript'
        script.src = url
        head.appendChild(script)
    }

    var u = navigator.userAgent, cordovaEnv = null
        href = location.href,
        loadUrl = 'http://admp-doc.test.geely.com/lynkco-weex/cordova/';
    if (u.indexOf('x-cordova-platform/android') > -1) cordovaEnv = 'android'
    else if (u.indexOf('x-cordova-platform/ios') > -1) cordovaEnv = 'ios'
    else cordovaEnv = 'web'
    // alert(u);
    // alert(cordovaEnv);
    // if (u.indexOf('Android') > -1 || u.indexOf('Adr') > -1) cordovaEnv = 'android'
    // if (!!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) cordovaEnv = 'ios'
    if (cordovaEnv !== 'web') {
        // 这里区分本地开发环境和外网环境
        if (href.indexOf('https://') >= 0 || href.indexOf('http://118.31.180.217/app-h5') >= 0 || href.indexOf('http://118.31.181.26/app-h5') >= 0) {
            // 外网要走相对路径
            let envHost = 'https://geely-admp-test-oss.oss-cn-hangzhou.aliyuncs.com',
                envPath = location.pathname.split('/pages/')[0];
            // dynamicLoadCss(envHost + envPath + '/cordova/' + cordovaEnv + '/cordova.js')
            // 暂时处理全部去一个地址
            dynamicLoadCss('https://geely-admp-test-oss.oss-cn-hangzhou.aliyuncs.com/lynkco2-sit/cordova/' + cordovaEnv + '/cordova.js')
        } else {
            // 内网统一去这里
            dynamicLoadCss('http://admp-doc.test.geely.com/lynkco-weex/cordova/' + cordovaEnv + '/cordova.js')
        }
        document.addEventListener("deviceready", function() {
          if (typeof window.LYNKInitForWeb === 'function') window.LYNKInitForWeb();
        }, false);
    } else {
        if (typeof window.LYNKInitForWeb === 'function') window.LYNKInitForWeb();
    }
})()
