//
//  GLPictureCropTarget.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/8/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ChooseImageBlock)(UIImage *image);

@interface GLPictureCropTarget : NSObject



@end

NS_ASSUME_NONNULL_END
