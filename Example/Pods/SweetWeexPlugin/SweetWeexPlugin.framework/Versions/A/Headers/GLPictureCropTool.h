//
//  GLPictureCropTool.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/8/5.
//

#import <Foundation/Foundation.h>

#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;


NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM (NSUInteger, GLPictureCropToolType) {
    GLPictureCropToolSelectImage = 0,
    GLPictureCropToolTakePhoto      ,
};

typedef void(^ChoosImageBlock)(UIImage *image);

typedef void (^CropImageBlock) (NSString *imagePath);
@interface GLPictureCropTool : NSObject

+ (instancetype)sharedTool;

//裁剪图片并通过oss上传
- (void)cropImage:(GLPictureCropToolType)type finished:(CropImageBlock)finished;

- (void)choosePhoto:(GLPictureCropToolType)type finished:(ChoosImageBlock)finished;
/**
 *  选择相册／拍摄
 */
- (void)photoCrop:(ChoosImageBlock)finished;

@end

NS_ASSUME_NONNULL_END
