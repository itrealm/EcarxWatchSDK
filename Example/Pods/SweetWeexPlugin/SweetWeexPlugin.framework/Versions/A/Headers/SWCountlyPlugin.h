//
//  SWCountlyPlugin.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/8/6.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWCountlyPlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
