//
//  SWXBannerJumpPlugin.h
//  SweetWeexPlugin
//
//  Created by duanyang on 2020/3/12.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXBannerJumpPlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
