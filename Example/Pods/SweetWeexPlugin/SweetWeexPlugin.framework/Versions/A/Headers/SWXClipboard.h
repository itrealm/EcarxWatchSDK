//
//  SWXClipboard.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/11/26.
//  粘贴板

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>
NS_ASSUME_NONNULL_BEGIN

@interface SWXClipboard : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
