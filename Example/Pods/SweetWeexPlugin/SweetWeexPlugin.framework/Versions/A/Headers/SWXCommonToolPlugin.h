//
//  SWXCommonToolPlugin.h
//  GeelyConsumerThird
//
//  Created by yang.duan on 2019/9/19.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WeexSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXCommonToolPlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
