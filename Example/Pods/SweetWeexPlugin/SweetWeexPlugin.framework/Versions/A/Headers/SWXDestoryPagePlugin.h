//
//  SWXDestoryPagePlugin.h
//  Pods
//
//  Created by yang.duan on 2019/12/23.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXDestoryPagePlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
