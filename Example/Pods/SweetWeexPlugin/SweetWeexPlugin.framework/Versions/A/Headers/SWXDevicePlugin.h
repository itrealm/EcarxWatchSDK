//
//  SWXDevicePlugin.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/10/24.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>
NS_ASSUME_NONNULL_BEGIN

@interface SWXDevicePlugin : NSObject <WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
