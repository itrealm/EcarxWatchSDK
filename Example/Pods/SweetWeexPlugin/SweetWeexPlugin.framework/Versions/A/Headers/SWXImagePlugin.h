//
//  SWXImagePlugin.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/8/5.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXImagePlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
