//
//  SWXKeyboardEventModule.h
//  SweetWeexPlugin
//
//  Created by shuaishuai on 2019/8/7.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXKeyboardEventModule : NSObject

@end

NS_ASSUME_NONNULL_END
