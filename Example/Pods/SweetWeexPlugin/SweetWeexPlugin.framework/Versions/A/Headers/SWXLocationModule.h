//
//  SWXLocationModule.h
//  AFNetworking
//
//  Created by 薛立恒 on 2020/5/5.
//

#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXLocationModule : NSObject <WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
