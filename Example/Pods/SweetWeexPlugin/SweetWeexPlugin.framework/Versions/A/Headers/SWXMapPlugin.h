//
//  SWXMapPlugin.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/8/2.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WeexSDK.h>
NS_ASSUME_NONNULL_BEGIN

@interface SWXMapPlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
