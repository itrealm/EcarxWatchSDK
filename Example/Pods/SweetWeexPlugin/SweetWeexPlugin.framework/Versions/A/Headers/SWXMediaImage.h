//
//  SWXMediaImage.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/7/23.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WeexSDK.h>

NS_ASSUME_NONNULL_BEGIN

@protocol SWXMediaImagePro <WXModuleProtocol>

- (void)pick:(NSDictionary *)params :(WXModuleCallback)callback;
- (void)preview:(NSArray *)files :(NSDictionary *)params :(WXModuleCallback)callback;
- (void)info:(NSString *)path :(WXModuleCallback)callback;
- (void)exif:(NSString *)path :(WXModuleCallback)callback;

@end



@interface SWXMediaImage : NSObject <SWXMediaImagePro>

@end

NS_ASSUME_NONNULL_END
