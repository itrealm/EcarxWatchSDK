//
//  SWXOpenAppPlugin.h
//  GeelyLynkcoPlugin
//
//  Created by yang.duan on 2019/11/7.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>
NS_ASSUME_NONNULL_BEGIN

@interface SWXOpenAppPlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
