//
//  SWXQRSharePlugin.h
//  AFNetworking
//
//  Created by 薛立恒 on 2019/11/10.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WeexSDK.h>
NS_ASSUME_NONNULL_BEGIN

@interface SWXQRSharePlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
