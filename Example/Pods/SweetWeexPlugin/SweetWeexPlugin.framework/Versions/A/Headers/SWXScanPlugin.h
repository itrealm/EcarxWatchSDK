//
//  SWXScanPlugin.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/6/25.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXScanPlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
