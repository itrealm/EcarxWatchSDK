//
//  SWXSettingPlugin.h
//  AFNetworking
//
//  Created by 朱伟特 on 2019/8/29.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WeexSDK.h>
NS_ASSUME_NONNULL_BEGIN

@interface SWXSettingPlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
