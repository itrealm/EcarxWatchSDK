//
//  SWXSharePayLoginPlugin.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/6/24.
// 该类实现需要在app的podfile中pod 'GeelyConsumerThird'


#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXSharePayLoginPlugin : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
