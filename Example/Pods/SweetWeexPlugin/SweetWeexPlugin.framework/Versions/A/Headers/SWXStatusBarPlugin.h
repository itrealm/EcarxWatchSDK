//
//  SWXStatusBarPlugin.h
//  SweetWeexPlugin
//
//  Created by shuaishuai on 2019/4/25.
// 状态栏修改

#import <Foundation/Foundation.h>
#import <WeexSDK/WXModuleProtocol.h>


NS_ASSUME_NONNULL_BEGIN

@interface SWXStatusBarPlugin : NSObject <WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
