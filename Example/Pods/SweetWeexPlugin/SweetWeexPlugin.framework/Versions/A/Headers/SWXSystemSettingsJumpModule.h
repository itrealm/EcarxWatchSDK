//
//  SWXSystemSettingsJumpModule.h
//  GeelyLynkcoThird
//
//  Created by shuaishuai on 2019/10/29.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WeexSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXSystemSettingsJumpModule : NSObject<WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
