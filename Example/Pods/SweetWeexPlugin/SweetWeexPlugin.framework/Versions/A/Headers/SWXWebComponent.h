//
//  SWXWebComponent.h
//  SweetWeexKit
//
//  Created by shuaishuai on 2019/8/18.
//

#import <WeexSDK/WeexSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXWebComponent : WXComponent

- (void)notifyWebview:(NSDictionary *) data;

- (void)postMessage:(NSDictionary *) data;

- (void)reload;

- (void)goBack;

- (void)goForward;


@end

NS_ASSUME_NONNULL_END
