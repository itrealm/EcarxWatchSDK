//
//  SWXWebModule.h
//  SweetWeexPlugin
//
//  Created by shuaishuai on 2019/8/19.
//

//#import <WeexSDK/WXDomModule.h>
#import <WeexSDK/WXModuleProtocol.h>

NS_ASSUME_NONNULL_BEGIN

@interface SWXWebModule : NSObject <WXModuleProtocol>

@end

NS_ASSUME_NONNULL_END
