//
//  UIImage+Crop.h
//  SweetWeexPlugin
//
//  Created by yang.duan on 2019/8/5.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Crop)
/*
 * 改变图片size
 */
+ (UIImage*)imageWithImageSimple:(UIImage*)image
                    scaledToSize:(CGSize)newSize;
/**
 * 将image适配屏幕
 */
+ (UIImage *)fitScreenWithImage:(UIImage *)image;
/*
 * 裁剪图片
 */
- (UIImage *)cropImageWithX:(CGFloat)x
                          y:(CGFloat)y
                      width:(CGFloat)width
                     height:(CGFloat)height;

@end

NS_ASSUME_NONNULL_END
