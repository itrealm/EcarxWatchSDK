#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "NatModal.h"
#import "UIAlertView+Expand.h"
#import "WXToast.h"

FOUNDATION_EXPORT double NatModalVersionNumber;
FOUNDATION_EXPORT const unsigned char NatModalVersionString[];

