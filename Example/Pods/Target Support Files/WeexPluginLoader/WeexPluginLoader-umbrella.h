#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "WeexPluginLoader.h"
#import "WPLMacro.h"
#import "WPLRegister.h"
#import "WXAnnotation.h"

FOUNDATION_EXPORT double WeexPluginLoaderVersionNumber;
FOUNDATION_EXPORT const unsigned char WeexPluginLoaderVersionString[];

