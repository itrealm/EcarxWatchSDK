# WeexPluginLoader

替换GitHub的WeexPluginLoader


[![CI Status](https://img.shields.io/travis/shuaishuai/WeexPluginLoader.svg?style=flat)](https://travis-ci.org/shuaishuai/WeexPluginLoader)
[![Version](https://img.shields.io/cocoapods/v/WeexPluginLoader.svg?style=flat)](https://cocoapods.org/pods/WeexPluginLoader)
[![License](https://img.shields.io/cocoapods/l/WeexPluginLoader.svg?style=flat)](https://cocoapods.org/pods/WeexPluginLoader)
[![Platform](https://img.shields.io/cocoapods/p/WeexPluginLoader.svg?style=flat)](https://cocoapods.org/pods/WeexPluginLoader)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WeexPluginLoader is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WeexPluginLoader'
```

## Author

shuaishuai, 957715883@qq.com

## License

WeexPluginLoader is available under the MIT license. See the LICENSE file for more info.
