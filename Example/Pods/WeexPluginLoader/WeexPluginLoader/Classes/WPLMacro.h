//
//  WPLMacro.h
//  Pods
//
//  Created by shuaishuai on 2019/7/30.
//

#ifndef WPLMacro_h
#define WPLMacro_h


#ifndef WeexPluginSectName
#define WeexPluginSectName "WeexPlugins"
#endif

#ifndef WXModulePluginName
#define WXModulePluginName @"WXModules"
#endif

#ifndef WXComponentPluginName
#define WXComponentPluginName @"WXComponents"
#endif

#ifndef WXHandlerPluginName
#define WXHandlerPluginName @"WXHandlers"
#endif

#define WX_DATA(sectname) __attribute((used, section("__DATA,"#sectname" ")))
//#define SWeexPluginDATA SWX_DATA(SweetWeexPlugins);
#define WeexPluginDATA __attribute((used, section("__DATA,WeexPlugins")))

#define WX_PLUGIN_NAME_SEPARATOR(module,jsname,classname,separator) module#separator#jsname#separator#classname

#define WX_PLUGIN_NAME(module,jsname,classname) WX_PLUGIN_NAME_SEPARATOR(module,jsname,classname,&)

#define WX_PlUGIN_EXPORT_MODULE_DATA(jsname,classname) \
char const * k##classname##Configuration WeexPluginDATA = WX_PLUGIN_NAME("WXModules",jsname,classname);

#define  WX_PlUGIN_EXPORT_COMPONENT_DATA(jsname,classname)\
char const * k##classname##Configuration WeexPluginDATA = WX_PLUGIN_NAME("WXComponents",jsname,classname);

#define WX_PlUGIN_EXPORT_HANDLER_DATA(jsimpl,jsprotocolname)\
char const * k##jsimpl##jsprotocolname##Configuration WeexPluginDATA = WX_PLUGIN_NAME("WXHandlers",jsimpl,jsprotocolname);


#endif /* WPLMacro_h */
