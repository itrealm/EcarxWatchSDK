# EcarxWatchSDK

[![CI Status](https://img.shields.io/travis/itrealm/EcarxWatchSDK.svg?style=flat)](https://travis-ci.org/itrealm/EcarxWatchSDK)
[![Version](https://img.shields.io/cocoapods/v/EcarxWatchSDK.svg?style=flat)](https://cocoapods.org/pods/EcarxWatchSDK)
[![License](https://img.shields.io/cocoapods/l/EcarxWatchSDK.svg?style=flat)](https://cocoapods.org/pods/EcarxWatchSDK)
[![Platform](https://img.shields.io/cocoapods/p/EcarxWatchSDK.svg?style=flat)](https://cocoapods.org/pods/EcarxWatchSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EcarxWatchSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'EcarxWatchSDK'
```

## Author

itrealm, 3273948281@qq.com

## License

EcarxWatchSDK is available under the MIT license. See the LICENSE file for more info.
